import { CssBaseline, IconButton, Snackbar } from '@material-ui/core/es';
import { Close } from '@material-ui/icons';
import * as React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import browserHistory from './browserHistory';
import Loadable from './components/Loadable';
import Website from './Website';
import useObservable from './components/hooks/useObservable';
import {
  getNotification,
  shiftNotification,
} from './stores/modules/uiNotifications';

export interface RoutingProps {}

export default Routing;

function Routing() {
  const [open, updateOpen] = React.useState(false);
  const handleCloseSnack = React.useCallback((evt, reason?: any) => {
    if (reason !== 'clickaway') {
      updateOpen(false);
    }
  }, []);
  const handleExited = React.useCallback(() => {
    shiftNotification();
  }, []);
  const notification = useObservable(() => getNotification());

  React.useEffect(() => {
    if (notification) {
      updateOpen(true);
    }
  }, [notification]);

  return (
    <>
      <CssBaseline />
      <Snackbar
        key={notification && notification.message}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleCloseSnack}
        onExited={handleExited}
        autoHideDuration={notification && notification.autoHideDuration}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={
          <span id="message-id">{notification && notification.message}</span>
        }
        action={[
          ...(notification ? notification.actions : []),
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={handleCloseSnack}
          >
            <Close />
          </IconButton>,
        ]}
      />
      <Router history={browserHistory}>
        <Switch>
          <Route
            path="/app"
            render={() => (
              <Loadable fullscreen factory={() => import('./App')} />
            )}
          />
          <Route component={Website} />
        </Switch>
      </Router>
    </>
  );
}
