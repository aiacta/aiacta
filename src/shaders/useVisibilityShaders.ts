import * as Babylon from 'babylonjs';
import { autorun, computed } from 'mobx';
import { useEffect } from 'react';
// eslint-disable-next-line import/no-webpack-loader-syntax
import CalculateVisibilityWorker from 'worker-loader!../worker/calculateVisibility';
import { AccessType, BattlemapModel, WorldModel } from '../stores/models';
import { myself } from '../stores/modules/gameData';
import OffscreenTexture from '../utils/OffscreenTexture';

export default function useVisibilityShaders(
  scene: Babylon.Scene | null,
  map: BattlemapModel | WorldModel,
) {
  useEffect(() => {
    if (!scene) {
      return;
    }

    const [camera] = scene.activeCameras;

    const textureDimensions = {
      width:
        map instanceof BattlemapModel
          ? map.size.x * 64
          : Math.round(map.size.x),
      height:
        map instanceof BattlemapModel
          ? map.size.y * 64
          : Math.round(map.size.y),
    };

    const visibilityTexture = new OffscreenTexture(
      'lineOfSight',
      textureDimensions,
      scene,
    );
    const exploredTexture = new OffscreenTexture(
      'explored',
      textureDimensions,
      scene,
      map.explorationUrl,
    );

    const worker = new CalculateVisibilityWorker();
    worker.postMessage(
      {
        type: 'setup',
        canvas: visibilityTexture.getOffscreenCanvas(),
      },
      [visibilityTexture.getOffscreenCanvas()],
    );
    worker.addEventListener('message', () => {
      visibilityTexture.update(false);
      exploredTexture.update(false);
      setTimeout(() => {
        visibilityTexture.update(false);
        exploredTexture.update(false);
      }, 10);
    });

    const visibilityShader = createVisibilityShader(
      camera,
      map.size,
      visibilityTexture,
      exploredTexture,
    );

    const computedActors = computed(() =>
      (map instanceof BattlemapModel
        ? map.tokens
            .filter(t => myself().isAllowedTo(AccessType.SeeThrough, t))
            .map(({ position, sight }) => ({ position, sight }))
        : map.parties
            .filter(p => myself().isAllowedTo(AccessType.SeeThrough, p))
            .map(({ position, sight }) => ({ position, sight }))
      ).map(({ position, sight }) => ({
        position: {
          x: (position.x / map.size.x + 0.5) * textureDimensions.width,
          y: (position.y / map.size.y + 0.5) * textureDimensions.height,
        },
        sight: {
          normal: sight.normal,
          darkvision:
            (sight.darkvision / 5 / map.size.x) * textureDimensions.width,
          truesight:
            (sight.truesight / 5 / map.size.x) * textureDimensions.width,
          blindsight:
            (sight.blindsight / 5 / map.size.x) * textureDimensions.width,
        },
      })),
    );

    const computedSegments = computed(() => [
      {
        start: { x: 0, y: 0 },
        end: { x: textureDimensions.width, y: 0 },
      },
      {
        start: { x: textureDimensions.width, y: 0 },
        end: { x: textureDimensions.width, y: textureDimensions.height },
      },
      {
        start: { x: textureDimensions.width, y: textureDimensions.height },
        end: { x: 0, y: textureDimensions.height },
      },
      {
        start: { x: 0, y: textureDimensions.height },
        end: { x: 0, y: 0 },
      },
      ...map.walls.flatMap(({ vertices, closed }) => {
        const segments = [];
        for (let i = 1; i < vertices.length; i++) {
          segments.push({
            start: {
              x:
                (vertices[i - 1].x / map.size.x + 0.5) *
                textureDimensions.width,
              y:
                (vertices[i - 1].y / map.size.y + 0.5) *
                textureDimensions.height,
            },
            end: {
              x: (vertices[i].x / map.size.x + 0.5) * textureDimensions.width,
              y: (vertices[i].y / map.size.y + 0.5) * textureDimensions.height,
            },
          });
        }
        if (closed) {
          segments.push({
            start: {
              x:
                (vertices[vertices.length - 1].x / map.size.x + 0.5) *
                textureDimensions.width,
              y:
                (vertices[vertices.length - 1].y / map.size.y + 0.5) *
                textureDimensions.height,
            },
            end: {
              x: (vertices[0].x / map.size.x + 0.5) * textureDimensions.width,
              y: (vertices[0].y / map.size.y + 0.5) * textureDimensions.height,
            },
          });
        }
        return segments;
      }),
    ]);

    const computedLightSources = computed(() =>
      map.lightSources.map(({ position, lightSource }) => ({
        position: {
          x: (position.x / map.size.x + 0.5) * textureDimensions.width,
          y: (position.y / map.size.y + 0.5) * textureDimensions.height,
        },
        lightSource: {
          dim: (lightSource.dim / 5 / map.size.x) * textureDimensions.width,
          bright:
            (lightSource.bright / 5 / map.size.x) * textureDimensions.width,
        },
      })),
    );

    const disposeAutorun = autorun(
      () => {
        worker.postMessage({
          actors: computedActors.get(),
          segments: computedSegments.get(),
          lightSources: computedLightSources.get(),
          isDungeonMaster: myself().isDungeonMaster,
          globalIllumination:
            map instanceof BattlemapModel ? map.globalIllumination : 1,
        });
      },
      { delay: 50 },
    );
    return () => {
      visibilityTexture.dispose();
      exploredTexture.dispose();
      visibilityShader.dispose();
      disposeAutorun();
      worker.terminate();
    };
  }, [scene, map]);
}

function createVisibilityShader(
  camera: Babylon.Camera,
  mapSize: Babylon.Vector2,
  losTexture: Babylon.Texture,
  xplTexture: Babylon.Texture,
  tokens = false,
) {
  const visibilityShader = new Babylon.PostProcess(
    'visibility',
    'visibility',
    visibilityFragmentShaderParams,
    visibilityFragmentShaderSampler,
    1,
    camera,
  );

  visibilityShader.onApply = effect => {
    const width = camera.orthoRight! - camera.orthoLeft!;
    const height = camera.orthoTop! - camera.orthoBottom!;

    const invView = camera
      .getViewMatrix()
      .clone()
      .invert();

    effect
      .setVector2('viewportExtends', new Babylon.Vector2(width, height))
      .setVector2('worldExtends', mapSize)
      .setMatrix('viewMatrix', invView)
      .setBool('tokens', tokens);

    effect.setTexture('lineOfSight', losTexture);
    effect.setTexture('explored', xplTexture);
  };

  return visibilityShader;
}

// Line of Sight Fragment Shader
const visibilityFragmentShaderSampler = ['lineOfSight', 'explored'];
const visibilityFragmentShaderParams = [
  'viewportExtends',
  'worldExtends',
  'viewMatrix',
];
Babylon.Effect.ShadersStore.visibilityFragmentShader = `
#ifdef GL_ES
    precision highp float;
#endif

// Samplers
varying vec2 vUV;
uniform sampler2D textureSampler;

uniform sampler2D lineOfSight;
uniform sampler2D explored;

// Parameters
uniform vec2 viewportExtends;
uniform vec2 worldExtends;
uniform mat4 viewMatrix;
uniform bool tokens;

void main(void) {
    vec4 baseColor = texture2D(textureSampler, vUV);

    vec4 d = vec4((vUV.xy - 0.5) * viewportExtends, 0.0, 1.0);
    vec2 pWorld = (viewMatrix * d).xz / worldExtends + 0.5;

    vec4 losInfo = texture2D(lineOfSight, pWorld);
    vec4 exploredInfo = texture2D(explored, pWorld);

    if (pWorld.x < 0.0 || pWorld.x > 1.0 || pWorld.y < 0.0 || pWorld.y > 1.0) {
      discard;
    } else {
      // red channel = line of sight
      // green channel = light sources
      // blue channel = dark vision
      if(losInfo.r > 0.0 && losInfo.g > 0.0) {
        gl_FragColor = vec4(baseColor.rgb * (losInfo.g / 2.0 + 0.5), baseColor.a);
      } else {
        float avg = (baseColor.r + baseColor.g + baseColor.b) / 3.0;
        if(losInfo.b > 0.0) {
          gl_FragColor = vec4(avg, avg, avg, baseColor.a);
        } else if(exploredInfo.r > 0.0) {
          avg *= 0.6;
          gl_FragColor = vec4(avg, avg, avg, baseColor.a);
        } else {
          gl_FragColor = vec4(0.07, 0.07, 0.07, baseColor.a);
        }
      }
    }
}
`;
