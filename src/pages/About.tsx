import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  Typography,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({});

export interface AboutProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(About);

function About() {
  return (
    <>
      <section>
        <div>
          <Typography gutterBottom variant="h1">
            About
          </Typography>
        </div>
      </section>
    </>
  );
}
