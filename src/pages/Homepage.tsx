import {
  Button,
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const RoutedLink = (props: any) => <RouterLink to={props.href} {...props} />;

const styles = (theme: Theme) =>
  createStyles({
    feature: {
      display: 'grid',
      gridTemplateAreas: `
        'head'
        'pic'
        'text'`,
      '@media(min-width: 830px)': {
        gridTemplateAreas: `
          'pic head'
          'pic text'`,
        gridTemplateColumns: 'auto 1fr',
        gridTemplateRows: 'auto 1fr',
        '&:nth-child(even)': {
          gridTemplateAreas: `
            'head pic'
            'text pic'`,
          gridTemplateColumns: '1fr auto',
        },
      },
      '&:not(:last-child)': {
        marginBottom: 20,
      },
      '& h3': {
        gridArea: 'head',
      },
      '& p': {
        gridArea: 'text',
      },
      '& div': {
        gridArea: 'pic',
        paddingBottom: '56.25%',
        position: 'relative',
        marginBottom: 12,
        '@media(min-width: 830px)': {
          minWidth: 384,
          marginRight: 12,
        },
        '& img': {
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
          left: 0,
        },
      },
    },
  });

export interface HomepageProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Homepage);

function Homepage({ classes }: HomepageProps) {
  return (
    <>
      <section>
        <div>
          <Typography gutterBottom variant="h1">
            Aiacta – A Virtual Tabletop
          </Typography>
          <Typography gutterBottom>
            Aiacta is an opinionated virtual tabletop for Dungeons & Dragons 5th
            edition.
          </Typography>
          <Button
            color="primary"
            variant="contained"
            size="large"
            component={RoutedLink}
            href="/app"
          >
            Play now!
          </Button>
        </div>
      </section>
      <section>
        <div className={classes.feature}>
          <div>{/* <img /> */}</div>
          <Typography gutterBottom variant="h3">
            First class D&D support.
          </Typography>
          <Typography>
            Aiacta was built to support campaigns in a Dungeons & Dragons
            setting, so it is highly optimized to automate as much as possible
            for this ruleset.
          </Typography>
        </div>
        <div className={classes.feature}>
          <div>{/* <img /> */}</div>
          <Typography gutterBottom variant="h3">
            Serverless, peer to peer networking.
          </Typography>
          <Typography>
            Aiacta utilizes modern features and facilitates gameplay through
            peer to peer connections between each player.
          </Typography>
        </div>
      </section>
    </>
  );
}
