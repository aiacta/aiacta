import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  Typography,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({});

export interface FourOFourProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(FourOFour);

function FourOFour() {
  return (
    <>
      <section>
        <div>
          <Typography gutterBottom variant="h1">
            404 – This page does not exist...
          </Typography>
        </div>
      </section>
    </>
  );
}
