import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  Typography,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({});

export interface FAQProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(FAQ);

function FAQ() {
  return (
    <>
      <section>
        <div>
          <Typography gutterBottom variant="h1">
            FAQ's
          </Typography>
        </div>
      </section>
    </>
  );
}
