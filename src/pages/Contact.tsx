import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  Typography,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) => createStyles({});

export interface ContactProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Contact);

function Contact({ classes }: ContactProps) {
  return (
    <>
      <section>
        <div>
          <Typography gutterBottom variant="h1">
            Contact
          </Typography>
        </div>
      </section>
    </>
  );
}
