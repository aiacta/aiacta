declare module 'worker-loader!*' {
  class WebWorker extends Worker {
    constructor() {}
  }
  export default WebWorker;
}

declare module 'workerize-loader!*' {
  export default function(): any;
}
