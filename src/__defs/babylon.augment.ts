import { Selectable } from '../stores/modules/uiSelection';

declare module 'babylonjs/Meshes/abstractMesh' {
  interface AbstractMesh {
    selectable?: Selectable;
    isBackground?: boolean;
  }
}

declare module 'babylonjs/Materials/standardMaterial' {
  interface StandardMaterial {
    references?: number;
  }
}
