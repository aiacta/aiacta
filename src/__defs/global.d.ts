declare class OffscreenCanvas extends HTMLCanvasElement {
  constructor(width: number, height: number);
}
