namespace marksy {
  interface DefinedElements {
    h1?: (props: { id: string; children: React.ReactNode }) => React.ReactNode;
    h2?: (props: { id: string; children: React.ReactNode }) => React.ReactNode;
    h3?: (props: { id: string; children: React.ReactNode }) => React.ReactNode;
    h4?: (props: { id: string; children: React.ReactNode }) => React.ReactNode;
    blockquote?: (props: { children: React.ReactNode }) => React.ReactNode;
    hr?: () => React.ReactNode;
    ol?: (props: { children: React.ReactNode }) => React.ReactNode;
    ul?: (props: { children: React.ReactNode }) => React.ReactNode;
    li?: (props: { children: React.ReactNode }) => React.ReactNode;
    p?: (props: { children: React.ReactNode }) => React.ReactNode;
    table?: (props: { children: React.ReactNode }) => React.ReactNode;
    thead?: (props: { children: React.ReactNode }) => React.ReactNode;
    tbody?: (props: { children: React.ReactNode }) => React.ReactNode;
    tr?: (props: { children: React.ReactNode }) => React.ReactNode;
    th?: (props: { children: React.ReactNode }) => React.ReactNode;
    td?: (props: { children: React.ReactNode }) => React.ReactNode;
    a?: (props: {
      href: string;
      title: string;
      target: string;
      children: React.ReactNode;
    }) => React.ReactNode;
    strong?: (props: { children: React.ReactNode }) => React.ReactNode;
    em?: (props: { children: React.ReactNode }) => React.ReactNode;
    br?: () => React.ReactNode;
    del?: (props: { children: React.ReactNode }) => React.ReactNode;
    img?: (props: { src: string; alt: string }) => React.ReactNode;
    code?: (props: { language: string; code: string }) => React.ReactNode;
    codespan?: (props: { children: React.ReactNode }) => React.ReactNode;
  }

  interface CompileFn {
    (source: string, options?: any, context?: any): {
      tree: React.ReactNode;
      toc: any;
    };
  }
}

declare module 'marksy' {
  const marksy: (options: {
    createElement: typeof React.createElement;
    elements: marksy.DefinedElements;
  }) => marksy.CompileFn;
  export default marksy;
}

declare module 'marksy/jsx' {
  const marksy: (options: {
    createElement: typeof React.createElement;
    elements?: marksy.DefinedElements;
    components?: {
      [key: string]: React.ComponentType<any>;
    };
  }) => marksy.CompileFn;
  export default marksy;
}
