import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ActionableContent from '../components/ActionableContent';

storiesOf('ActionableContent', module).add('Simple', () => (
  <ActionableContent
    actionlike="Attack: Longsword. Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: (1d8 + 3) slashing damage."
    instigatorId="nobody"
  />
));
