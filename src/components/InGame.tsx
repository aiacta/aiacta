import {
  AppBar,
  Badge,
  createStyles,
  IconButton,
  Link,
  Theme,
  Toolbar,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Notifications } from '@material-ui/icons';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { AccessType } from '../stores/models';
import {
  battlemaps,
  characters,
  hasPendingChanges,
  isReady,
  monsters,
  worlds,
} from '../stores/modules/dataStores';
import { myself } from '../stores/modules/gameData';
import { isConnected } from '../stores/modules/network';
import { closeWindow, useOpenWindows } from '../stores/modules/uiView';
import { useMemoize } from '../utils/memoize';
import CharacterCreator from './CharacterCreator';
import CharacterSheet from './CharacterSheet';
import Chat from './Chat';
import ChoicesDialog from './Choices';
import CombatTracker from './CombatTracker';
import Compendium from './Compendium';
import Dashboard from './Dashboard';
import DatabaseExplorer from './DatabaseExplorer';
import useObservable from './hooks/useObservable';
import Map from './Map';
import MonsterSheet from './MonsterSheet';
import Omnibox from './Omnibox';
import PlayerToolbar from './PlayerToolbar';
import RouterLink from './RouterLink';
import Time from './Time';
import Window, { WindowStack } from './Window';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    toolbar: {
      paddingLeft: 24,
      paddingRight: 24,
      '& > p': {
        marginRight: theme.spacing.unit,
      },
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    mainArea: {
      display: 'flex',
      flexDirection: 'column',
      flex: '1 1 auto',
      position: 'relative',
      overflow: 'auto',
    },
    title: {
      flexGrow: 1,
      '& a': {
        textDecoration: 'none',
        color: 'inherit',
      },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
      display: 'flex',
      flexDirection: 'column',
      flexGrow: 1,
      height: '100vh',
    },
  });

export interface InGameProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(InGame);

function InGame({ classes }: InGameProps) {
  const isLoading = useObservable(() => !isReady());
  return (
    <Observer>
      {() => (
        <div className={classes.root}>
          <AppBar position="absolute" className={classes.appBar}>
            <Toolbar disableGutters className={classes.toolbar}>
              <Typography
                color="inherit"
                variant="h5"
                className={classes.title}
              >
                <Link component={RouterLink} href="/app">
                  Aiacta
                </Link>
              </Typography>
              {!isLoading && (
                <>
                  <Typography noWrap color="inherit">
                    {hasPendingChanges() ? 'Saving...' : 'All changes saved'}
                  </Typography>
                  <Typography color="inherit">
                    ({isConnected() ? 'connected' : 'not connected'})
                  </Typography>
                  <Typography noWrap color="inherit">
                    {myself().nickname}
                    {myself().isDungeonMaster ? ' (DM)' : null}
                  </Typography>
                  <Time />
                </>
              )}
              <IconButton color="inherit">
                <Badge badgeContent={4} color="secondary">
                  <Notifications />
                </Badge>
              </IconButton>
            </Toolbar>
          </AppBar>
          <main id="main" className={classes.content}>
            <div className={classes.appBarSpacer} />
            <div id="main-content" className={classes.mainArea}>
              {isLoading ? (
                <Loading />
              ) : (
                <>
                  <PlayerToolbar />
                  <Switch>
                    <Route
                      path="/app/map/:mapId"
                      render={({ match }) => {
                        const model =
                          worlds.get(match.params.mapId) ||
                          battlemaps.get(match.params.mapId);
                        if (
                          model &&
                          myself().isAllowedTo(
                            AccessType.Read | AccessType.Know,
                            model,
                          )
                        ) {
                          return <Map model={model} />;
                        }
                        return 'This is not the map you are looking for...';
                      }}
                    />
                    <Route component={Dashboard} />
                  </Switch>
                  <Omnibox />
                  <Windows />
                  <ChoicesDialog />
                </>
              )}
            </div>
          </main>
        </div>
      )}
    </Observer>
  );
}

function Loading() {
  return <>Loading...</>;
}

function Windows() {
  const openWindows = useOpenWindows();
  const toggleWindow = useMemoize((key: string) => () => closeWindow(key));

  return (
    <WindowStack>
      {([
        [
          'dbExplorer',
          'DB Explorer',
          'center',
          () => <DatabaseExplorer />,
          false,
        ],
        ['compendium', 'Compendium', 'center', () => <Compendium />, false],
        [
          'combatTracker',
          'Combat Tracker',
          'top right',
          () => <CombatTracker />,
          false,
        ],
        ['chat', 'Chat', 'bottom', () => <Chat />, true],
        [
          'characterCreator',
          'Character Creator',
          'center',
          () => <CharacterCreator />,
          false,
        ],
      ] as Array<[string, string, any, () => React.ReactNode, boolean]>).map(
        ([key, title, pos, render, alignBottom]) => (
          <Window
            key={key}
            open={openWindows.includes(key)}
            onClose={toggleWindow(key)}
            title={title}
            initialPosition={pos}
            keepPositionAndSize={key}
            draggable
            detachable
            resizable
            paper
            alignBottom={alignBottom}
          >
            {render()}
          </Window>
        ),
      )}
      {Array.from(openWindows)
        .filter(key => key.startsWith('monstersheet-'))
        .map(key => {
          const model = monsters.get(key.substr('monstersheet-'.length))!;
          return (
            model &&
            myself().isAllowedTo(AccessType.Read, model) && (
              <Window
                key={key}
                open
                onClose={toggleWindow(key)}
                title={`${model.name} — Sheet`}
                draggable
                detachable
                resizable
                paper
              >
                <MonsterSheet model={model} />
              </Window>
            )
          );
        })}
      {Array.from(openWindows)
        .filter(key => key.startsWith('charactersheet-'))
        .map(key => {
          const model = characters.get(key.substr('charactersheet-'.length));
          return (
            <Window
              key={key}
              open
              onClose={toggleWindow(key)}
              title={`${model ? model.name : '[deleted]'} — Sheet`}
              keepPositionAndSize={key}
              initialWidth={650}
              draggable
              detachable
              resizable
              paper
            >
              <CharacterSheet model={model} />
            </Window>
          );
        })}
    </WindowStack>
  );
}
