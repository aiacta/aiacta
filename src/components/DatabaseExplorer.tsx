import {
  Button,
  Dialog,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core/es';
import { DeleteForever } from '@material-ui/icons';
import { runInAction } from 'mobx';
import * as React from 'react';
import { allStores } from '../stores/modules/dataStores';
import ModelStore from '../stores/modules/dataStores/ModelStore';
import memoize, { memoizeMulti } from '../utils/memoize';
import useObservable from './hooks/useObservable';

// export interface DatabaseExplorerProps {}

export default DatabaseExplorer;

function DatabaseExplorer() {
  const [selected, updateSelected] = React.useState(null as any);
  const handleClick = React.useCallback(
    memoize((ent: any) => () => {
      updateSelected(ent);
    }),
    [],
  );
  const handleClose = React.useCallback(() => {
    updateSelected(null);
  }, []);
  const handleDelete = React.useCallback(
    memoizeMulti((store: ModelStore<any, any>, entity) => () => {
      runInAction(() => store.remove(entity));
    }),
    [],
  );
  const handleExport = React.useCallback(() => {
    // TODO handle export
    // dataStore
    //   .database!.allDocs({ include_docs: true, attachments: true })
    //   .then(docs => {
    //     const elem = document.createElement('a');
    //     elem.href = URL.createObjectURL(new Blob([JSON.stringify(docs.rows)]));
    //     elem.download = 'db_export.json';
    //     elem.style.display = 'none';
    //     document.body.appendChild(elem);
    //     elem.click();
    //     document.body.removeChild(elem);
    //   });
  }, []);
  const handleBlur = React.useCallback(
    (evt: React.FocusEvent<HTMLDivElement>) => {
      try {
        const { _id, _attachments, type, ...rest } = JSON.parse(
          evt.currentTarget.innerText,
        );
        const objKeys = Object.keys(rest);
        runInAction(() => {
          objKeys.forEach(key => (selected.document[key] = rest[key]));
          Object.keys(selected.document)
            .filter(
              key =>
                key !== '_id' &&
                key !== '_attachments' &&
                key !== 'type' &&
                key !== '_rev' &&
                !objKeys.includes(key),
            )
            .forEach(key => (selected.document[key] = undefined));
        });
      } catch (err) {
        console.error(err);
      }
    },
    [selected],
  );

  const stores = useObservable(() =>
    allStores.map(store => (
      <section key={store.type}>
        <Typography variant="h6">{store.type}</Typography>
        <List>
          {(store as any)
            .all()
            .sort((a: any, b: any) => a.lastChanged - b.lastChanged)
            .map((entity: any, idx: number) => (
              <ListItem key={entity._id} button onClick={handleClick(entity)}>
                <ListItemText
                  primary={`${idx + 1}. ${entity.name || 'N/A'}`}
                  secondary={entity._id}
                />
                <ListItemSecondaryAction>
                  <IconButton onClick={handleDelete(store, entity)}>
                    <DeleteForever />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
        </List>
      </section>
    )),
  );

  return (
    <>
      <Button onClick={handleExport}>Export</Button>
      <Dialog open={!!selected} onClose={handleClose}>
        <div
          contentEditable
          onBlur={handleBlur}
          dangerouslySetInnerHTML={{
            __html: selected
              ? `<pre><code>${JSON.stringify(
                  selected.asDocument,
                  null,
                  2,
                )}</code></pre>`
              : 'n/a',
          }}
        />
      </Dialog>
      {stores}
    </>
  );
}
