import { Close } from '@material-ui/icons';
import { SpeedDial, SpeedDialAction, SpeedDialIcon } from '@material-ui/lab/es';
import * as React from 'react';
import { useMemoize } from '../utils/memoize';

export interface SpeedSelectProps {
  ariaLabel: string;
  className: string;
  hidden?: boolean;
  icon?: React.ReactNode;
  options: Array<{ name: string; icon: React.ReactNode }>;
  onChange: (option: SpeedSelectProps['options'][0]) => void;
}

export default SpeedSelect;

function SpeedSelect({
  ariaLabel,
  className,
  hidden,
  icon,
  options,
  onChange,
}: SpeedSelectProps) {
  const [open, updateOpen] = React.useState(false);

  const handleOpen = React.useCallback(() => updateOpen(true), []);
  const handleClose = React.useCallback(() => updateOpen(false), []);
  const handleToggle = React.useCallback(() => updateOpen(s => !s), []);
  const handleClick = useMemoize(
    (opt: SpeedSelectProps['options'][0]) => () => {
      updateOpen(false);
      onChange(opt);
    },
    [onChange],
  );

  return (
    <SpeedDial
      ariaLabel={ariaLabel}
      className={className}
      hidden={hidden}
      icon={<SpeedDialIcon icon={icon} openIcon={<Close />} />}
      onBlur={handleClose}
      onClick={handleToggle}
      onClose={handleClose}
      onFocus={handleOpen}
      onMouseEnter={handleOpen}
      onMouseLeave={handleClose}
      open={open}
    >
      {options.map(opt => (
        <SpeedDialAction
          key={opt.name}
          icon={opt.icon}
          tooltipTitle={opt.name}
          tooltipOpen
          onClick={handleClick(opt)}
        />
      ))}
    </SpeedDial>
  );
}
