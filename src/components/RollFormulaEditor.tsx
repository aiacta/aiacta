import {
  createStyles,
  MenuItem,
  Select,
  TextField,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import classNames from '../utils/classNames';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      width: 300,
    },
  });

export interface RollFormulaEditorProps extends WithStyles<typeof styles> {
  value: string;
  onChange: (value: string) => void;
  presets?: Array<{
    label: string;
    value: string;
  }>;
  className?: string;
}

export default withStyles(styles)(RollFormulaEditor);

function RollFormulaEditor({
  value,
  onChange,
  presets,
  classes,
  className,
}: RollFormulaEditorProps) {
  const handleChange = React.useCallback(
    (
      evt: React.ChangeEvent<
        HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
      >,
    ) => {
      onChange(evt.target.value);
    },
    [onChange],
  );
  const handleSelectPreset = React.useCallback(
    (evt: React.ChangeEvent<HTMLSelectElement>) => {
      if (evt.target.value) {
        onChange(evt.target.value);
      }
    },
    [onChange],
  );
  return (
    <div className={classNames(classes.container, className)}>
      {presets && (
        <Select value={value} onChange={handleSelectPreset} fullWidth>
          <MenuItem value={value} disabled>
            <em>Custom</em>
          </MenuItem>
          {presets.map(p => (
            <MenuItem key={p.value} value={p.value}>
              {p.label}
            </MenuItem>
          ))}
        </Select>
      )}
      <TextField
        label="Roll formula"
        multiline
        rowsMax="4"
        value={value}
        onChange={handleChange}
        margin="normal"
        fullWidth
      />
    </div>
  );
}
