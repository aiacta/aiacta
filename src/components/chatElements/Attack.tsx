import * as React from 'react';

export interface AttackProps {
  instigatorName: string;
  targetName?: string;
  children: React.ReactNode;
}

export default function Attack({
  instigatorName,
  targetName,
  children,
}: AttackProps) {
  return (
    <span>
      {instigatorName} attacks
      {targetName && ` ${targetName}`}: {children}
    </span>
  );
}
