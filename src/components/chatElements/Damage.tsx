import * as React from 'react';

export interface DamageProps {
  instigatorName: string;
  type: string;
  targetName?: string;
  children: React.ReactNode;
}

export default function Damage({
  instigatorName,
  type,
  targetName,
  children,
}: DamageProps) {
  return (
    <span>
      {instigatorName} inflicts {children} {type} damage
      {targetName && ` on ${targetName}`}
    </span>
  );
}
