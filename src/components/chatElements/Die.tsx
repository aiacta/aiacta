import * as React from 'react';

export interface DieProps {
  value: number;
}

export default function Die({ value }: DieProps) {
  return <span>{value}</span>;
}
