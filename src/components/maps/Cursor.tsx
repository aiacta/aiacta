import * as Babylon from 'babylonjs';
import { reaction } from 'mobx';
import * as React from 'react';
import defaultIcon from '../../assets/baseline_person_white_48dp.png';
import { PlayerModel } from '../../stores/models';
import { memoizeMulti } from '../../utils/memoize';
import useMesh from './hooks/useMesh';

export interface CursorProps {
  scene: Babylon.Scene;
  model: PlayerModel;
  zIndex: number;
}

export default function Cursor({ scene, model, zIndex }: CursorProps) {
  const mesh = useMesh(scene, 'playerCursor', 1, 1);

  // TODO render waypoints

  React.useEffect(() => {
    const {
      color,
      activity: { position },
    } = model;

    const mat = new Babylon.StandardMaterial('material_cursor', scene);
    mat.diffuseTexture = getTexture(scene, defaultIcon);
    mat.diffuseTexture.hasAlpha = true;
    mat.disableLighting = true;
    mat.emissiveColor = Babylon.Color3.FromHexString(color);

    mesh.material = mat;
    mesh.isPickable = false;

    if (position) {
      mesh.position.set(position.x, zIndex, position.y);
    } else {
      mesh.setEnabled(false);
    }

    const observer = [
      reaction(
        () => model.activity.position,
        pos => {
          if (pos) {
            if (!mesh.isEnabled()) {
              mesh.setEnabled(true);
            }
            mesh.position.set(pos.x, mesh.position.y, pos.y);
          } else {
            mesh.setEnabled(false);
          }
        },
      ),
      reaction(
        () => model.color,
        color => {
          mat.emissiveColor = Babylon.Color3.FromHexString(color);
        },
      ),
    ];

    return () => {
      observer.forEach(o => o());
    };
  }, [mesh, model, scene, zIndex]);

  return null;
}

const getTexture = memoizeMulti((scene: Babylon.Scene, textureUrl: string) => {
  return new Babylon.Texture(textureUrl, scene);
});
