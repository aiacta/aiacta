import * as Babylon from 'babylonjs';
import 'babylonjs-materials';
import * as React from 'react';
import { WallModel } from '../../stores/models';
import { setActive } from '../../stores/modules/uiView';
import { memoizeMulti } from '../../utils/memoize';

const getMaterial = memoizeMulti((scene: Babylon.Scene) => {
  const mat = new Babylon.StandardMaterial('material_wall', scene);
  mat.disableLighting = true;
  mat.emissiveColor = new Babylon.Color3(0.5, 0.5, 1.0);
  return mat;
});

export interface WallProps {
  scene: Babylon.Scene;
  model: WallModel;
  visible: boolean;
  editing: boolean;
}

export default function Wall({ scene, model, visible }: WallProps) {
  const { vertices, thickness, closed, height } = model;
  const mat = getMaterial(scene);
  const [mesh, updateMesh] = React.useState(
    null as Babylon.AbstractMesh | null,
  );

  React.useEffect(() => {
    const pathArray = createPathArray(vertices, closed, thickness, height);

    const m = Babylon.MeshBuilder.CreateRibbon(
      'wall',
      {
        closePath: closed,
        pathArray,
      },
      scene,
    );
    m.material = mat;
    m.isPickable = true;
    m.selectable = model;

    updateMesh(m);

    setActive(model, m, true);
    return () => {
      setActive(model, m, false);
      m.dispose();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    if (!mesh) {
      return;
    }
    mesh.isVisible = visible;
  }, [mesh, visible]);

  return null;
}

function alignment(a: WallModel['vertices'], i: number, closed: boolean) {
  const isFirst = i === 0;
  const isLast = i === a.length - 1;

  if (!closed) {
    if (isFirst) {
      return perpendicular(a[i + 1].subtract(a[i]).normalize());
    }
    if (isLast) {
      return perpendicular(a[i].subtract(a[i - 1]).normalize());
    }
  }

  const prev = a[isFirst ? a.length - 1 : i - 1];
  const next = a[isLast ? 0 : i + 1];

  const v0 = perpendicular(a[i].subtract(prev).normalize());
  const v1 = perpendicular(next.subtract(a[i]).normalize());
  return Babylon.Vector2.Lerp(v0, v1, 0.5);

  function perpendicular(v: Babylon.Vector2) {
    return v.set(v.y, -v.x);
  }
}

function scaleToLength(v: Babylon.Vector2, l: number) {
  const m = l / Math.max(Math.abs(v.x), Math.abs(v.y));
  return v.set(v.x * m, v.y * m);
}

export function createPathArray(
  vertices: Babylon.Vector2[],
  closed: boolean,
  thickness: number,
  height: number,
) {
  const corners = [[-1, 0], [-1, 1], [1, 1], [1, 0]];
  const pathArray = vertices.reduce(
    (acc, { x, y }, i, a) => {
      const vd = scaleToLength(alignment(a, i, closed), thickness / 2);

      corners.forEach(([w, h], idx) => {
        acc[idx].push(
          new Babylon.Vector3(x + vd.x * w, height * h, y + vd.y * w),
        );
      });
      return acc;
    },
    [[], [], [], []] as Babylon.Vector3[][],
  );

  if (!closed) {
    const v0 = scaleToLength(alignment(vertices, 0, closed), thickness / 2);
    corners.forEach(([w, h], idx) => {
      pathArray[idx].unshift(
        new Babylon.Vector3(
          vertices[0].x + v0.x * w,
          0,
          vertices[0].y + v0.y * w,
        ),
      );
    });
    const v1 = scaleToLength(
      alignment(vertices, vertices.length - 1, closed),
      thickness / 2,
    );
    corners.forEach(([w, h], idx) => {
      pathArray[idx].push(
        new Babylon.Vector3(
          vertices[vertices.length - 1].x + v1.x * w,
          0,
          vertices[vertices.length - 1].y + v1.y * w,
        ),
      );
    });
  }

  return pathArray;
}
