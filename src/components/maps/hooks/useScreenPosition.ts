import * as Babylon from 'babylonjs';
import { ReferenceObject } from 'popper.js';
import { useEffect, useState } from 'react';

export default function useScreenPosition(
  scene: Babylon.Scene,
  mesh: Babylon.Mesh,
  enabled: boolean,
) {
  const [screenPosition, updateScreenPosition] = useState(
    null as ReferenceObject | null,
  );
  useEffect(() => {
    if (scene && enabled) {
      const { top } = scene
        .getEngine()
        .getRenderingCanvas()!
        .getBoundingClientRect();

      function afterRender() {
        const camera = scene.cameras[0];
        const transformationMatrix = camera
          .getViewMatrix()
          .multiply(camera.getProjectionMatrix());
        const viewport = camera.viewport.toGlobal(
          camera.getEngine().getRenderWidth(),
          camera.getEngine().getRenderHeight(),
        );
        const boundingInfo = mesh.getBoundingInfo();
        const projectedPositionMin = Babylon.Vector3.Project(
          boundingInfo.boundingBox.minimumWorld,
          Babylon.Matrix.Identity(),
          transformationMatrix,
          viewport,
        );
        const projectedPositionMax = Babylon.Vector3.Project(
          boundingInfo.boundingBox.maximumWorld,
          Babylon.Matrix.Identity(),
          transformationMatrix,
          viewport,
        );
        const projectedExtends = projectedPositionMax.subtract(
          projectedPositionMin,
        );
        updateScreenPosition({
          clientWidth: projectedExtends.x,
          clientHeight: projectedExtends.y,
          getBoundingClientRect() {
            return {
              left: projectedPositionMin.x,
              top: projectedPositionMax.y + top,
              width: projectedExtends.x,
              height: -projectedExtends.y,
              right: projectedPositionMax.x,
              bottom: projectedPositionMin.y + top,
            };
          },
        });
      }

      mesh.registerAfterRender(afterRender);
      return () => {
        mesh.unregisterAfterRender(afterRender);
      };
    }
    return;
  }, [scene, enabled, mesh]);

  return screenPosition;
}
