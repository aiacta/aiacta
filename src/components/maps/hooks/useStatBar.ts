import * as Babylon from 'babylonjs';
import { useEffect, useState } from 'react';
import { UILayer } from '../../hooks/useScene';

const textureWidth = 128;
const textureHeight = 32;

export default function useStatBar(
  scene: Babylon.Scene,
  id: string,
  parent: Babylon.Mesh,
  initialFill: number,
  color: string,
  background = '#fff',
  idx = 0,
) {
  const texture = useDynamicTexture(
    scene,
    'barTexture',
    textureWidth,
    textureHeight,
  );
  const [mesh] = useState(() => {
    const bb = parent.getBoundingInfo();
    const { x: width, z: height } = bb.maximum.subtract(bb.minimum);

    const m = Babylon.MeshBuilder.CreateGround(
      id,
      { width, height: 0.1 },
      scene,
    );
    m.isPickable = false;
    m.parent = parent;
    m.position.set(0, 0, height / 2 + 0.1 * (idx + 1));
    m.layerMask = UILayer;

    const mat = new Babylon.StandardMaterial('material_bar', scene);
    mat.diffuseTexture = texture;
    mat.disableLighting = true;
    mat.emissiveColor = Babylon.Color3.White();

    m.material = mat;

    return m;
  });
  const [fill, updateFill] = useState(initialFill);

  useEffect(() => {
    return () => {
      mesh.dispose();
    };
  }, [mesh]);

  useEffect(() => {
    const ctx = texture.getContext();
    ctx.fillStyle = background;
    ctx.fillRect(0, 0, textureWidth, textureHeight);
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, textureWidth * fill, textureHeight);

    texture.update(false);
  }, [background, color, fill, texture]);

  return [mesh, updateFill] as [typeof mesh, typeof updateFill];
}

function useDynamicTexture(
  scene: Babylon.Scene,
  name: string,
  width: number,
  height: number,
) {
  const [texture] = useState(
    () => new Babylon.DynamicTexture(name, { width, height }, scene, false),
  );

  useEffect(() => {
    return () => {
      texture.dispose();
    };
  }, [texture]);

  return texture;
}
