import * as Babylon from 'babylonjs';
import { useEffect, useState } from 'react';

export default function useDecal(
  scene: Babylon.Scene | null,
  id: string,
  parent: Babylon.Mesh | null,
  size?: Babylon.Vector3,
) {
  const [mesh, updateMesh] = useState(null as Babylon.Mesh | null);

  useEffect(() => {
    if (!scene || !parent) {
      return;
    }
    const m = Babylon.MeshBuilder.CreateDecal(id, parent, {
      size: size || parent.getBoundingInfo().boundingBox.extendSize,
    });

    updateMesh(m);

    return () => {
      m.dispose();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scene, parent, id]);

  return mesh;
}
