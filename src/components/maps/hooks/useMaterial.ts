import * as Babylon from 'babylonjs';
import { useEffect, useMemo } from 'react';
import { memoizeMulti } from '../../../utils/memoize';

const getMaterial = memoizeMulti((scene: Babylon.Scene, textureUrl: string) => {
  const mat = new Babylon.StandardMaterial('material_' + textureUrl, scene);
  mat.diffuseTexture = new Babylon.Texture(textureUrl, scene);
  mat.diffuseTexture.hasAlpha = true;
  mat.disableLighting = true;
  mat.emissiveColor = Babylon.Color3.White();
  mat.references = 0;
  return mat;
});

export default function useMaterial(scene: Babylon.Scene, imageUrl: string) {
  const material = useMemo(() => getMaterial(scene, imageUrl), [
    scene,
    imageUrl,
  ]);
  useEffect(() => {
    ++material.references!;

    return () => {
      if (--material.references! <= 0) {
        // TODO clear cache or dont dispose material!
        material.dispose();
      }
    };
  }, [material]);

  return material;
}
