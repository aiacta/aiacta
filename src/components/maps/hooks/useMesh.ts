import * as Babylon from 'babylonjs';
import { useEffect, useState } from 'react';

export default function useMesh(
  scene: Babylon.Scene,
  id: string,
  width: number,
  height: number,
) {
  const [mesh] = useState(() =>
    Babylon.MeshBuilder.CreateGround(id, { width, height }, scene),
  );

  useEffect(() => {
    return () => {
      mesh.dispose();
    };
  }, [mesh]);

  return mesh;
}
