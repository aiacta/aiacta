import * as Babylon from 'babylonjs';
import { observe } from 'mobx';
import * as React from 'react';
import defaultIcon from '../../assets/baseline_group_white_48dp.png';
import { PartyModel } from '../../stores/models';
import { setActive } from '../../stores/modules/uiView';
import useMaterial from './hooks/useMaterial';
import useMesh from './hooks/useMesh';

export interface PartyProps {
  scene: Babylon.Scene;
  model: PartyModel;
  zIndex: number;
  editing: boolean;
}

export default function Party({ scene, model, zIndex }: PartyProps) {
  const {
    position: { x, y },
  } = model;
  const mesh = useMesh(scene, 'party', 5, 5);
  const material = useMaterial(scene, defaultIcon);

  // TODO render waypoints

  React.useEffect(() => {
    mesh.material = material;
    mesh.selectable = model;
    mesh.position.set(x, zIndex, y);

    const observer = [
      observe(model, 'position', change => {
        mesh.position.set(
          change.newValue.x,
          mesh.position.y,
          change.newValue.y,
        );
      }),
    ];

    mesh.actionManager = new Babylon.ActionManager(scene);
    mesh.actionManager.registerAction(
      new Babylon.ExecuteCodeAction(
        Babylon.ActionManager.OnPointerOverTrigger,
        ev => {
          // (m.material as Babylon.StandardMaterial).emissiveColor = Babylon.Color3.White();
        },
      ),
    );
    mesh.actionManager.registerAction(
      new Babylon.ExecuteCodeAction(
        Babylon.ActionManager.OnPointerOutTrigger,
        ev => {
          // (m.material as Babylon.StandardMaterial).emissiveColor = Babylon.Color3.Black();
        },
      ),
    );

    setActive(model, mesh, true);
    return () => {
      setActive(model, mesh, false);
      observer.forEach(o => o());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [material, mesh, model]);

  return null;
}
