import * as Babylon from 'babylonjs';
import { observe, reaction } from 'mobx';
import * as React from 'react';
import defaultPog from '../../assets/baseline_account_circle_white_48dp.png';
import deadIcon from '../../assets/overlay_dying.png';
import { TokenModel } from '../../stores/models';
import { setActive, openSheet } from '../../stores/modules/uiView';
import useObservable from '../hooks/useObservable';
import useMaterial from './hooks/useMaterial';
import useMesh from './hooks/useMesh';
import useStatBar from './hooks/useStatBar';

export interface TokenProps {
  scene: Babylon.Scene;
  model: TokenModel;
  zIndex: number;
  editing: boolean;
}

export default function Token({ scene, model, zIndex }: TokenProps) {
  const {
    imageUrl,
    position: { x, y },
    size: { x: width, y: height },
  } = useObservable(() => ({
    imageUrl: model.imageUrl || defaultPog,
    position: model.position,
    size: model.size,
  }));

  const mesh = useMesh(scene, 'token', width, height);
  const material = useMaterial(scene, imageUrl || defaultPog);
  const skullDecal = useMesh(scene, model._id, width, height);
  const skullMaterial = useMaterial(scene, deadIcon);
  const [, updateHealthBar] = useStatBar(
    scene,
    model._id,
    mesh,
    model.health.current / model.healthMaximum,
    '#0f0',
    '#f00',
  );

  mesh.material = material;

  React.useEffect(() => {
    mesh.selectable = model;
    mesh.position.set(x, zIndex, y);

    skullDecal.parent = mesh;
    skullDecal.material = skullMaterial;
    skullDecal.setEnabled(model.health.current <= 0);

    const observer = [
      observe(model, 'position', change => {
        mesh.position.set(
          change.newValue.x,
          mesh.position.y,
          change.newValue.y,
        );
      }),
      reaction(
        () => model.health.current / model.healthMaximum,
        p => {
          updateHealthBar(p);
          skullDecal.setEnabled(p <= 0);
        },
      ),
      observe(model, 'isVisible', change => {
        mesh.setEnabled(change.newValue);
      }),
    ];

    mesh.actionManager = new Babylon.ActionManager(scene);
    mesh.actionManager.registerAction(
      new Babylon.ExecuteCodeAction(
        Babylon.ActionManager.OnDoublePickTrigger,
        ev => {
          openSheet(model.represents);
        },
      ),
    );

    setActive(model, mesh, true);
    return () => {
      setActive(model, mesh, false);
      observer.forEach(o => o());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mesh, model, scene, updateHealthBar]);

  return null;
}
