import * as Babylon from 'babylonjs';
import * as React from 'react';
import useMaterial from './hooks/useMaterial';
import useMesh from './hooks/useMesh';

export interface AssetProps {
  scene: Babylon.Scene;
  imageUrl: string;
  position: Babylon.Vector2;
  size: Babylon.Vector2;
}

export default function Asset({
  scene,
  imageUrl,
  size: { x: width, y: height },
}: AssetProps) {
  const mesh = useMesh(scene, 'asset', width, height);
  const material = useMaterial(scene, imageUrl);

  React.useEffect(() => {
    mesh.isPickable = false;
    mesh.isBackground = true;
    mesh.material = material;

    // setActive(model, mesh, true);
    return () => {
      // setActive(model, mesh, false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [material, mesh]);

  return null;
}
