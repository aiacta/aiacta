import { Paper, Popper, Typography, Zoom } from '@material-ui/core/es';
import * as Babylon from 'babylonjs';
import { observe } from 'mobx';
import * as React from 'react';
import defaultIcon from '../../assets/baseline_place_white_48dp.png';
import { PointOfInterestModel } from '../../stores/models';
import { navigateTo } from '../../stores/modules/uiRouting';
import { setActive } from '../../stores/modules/uiView';
import { memoizeMulti } from '../../utils/memoize';
import useMesh from './hooks/useMesh';
import useScreenPosition from './hooks/useScreenPosition';

const getTexture = memoizeMulti((scene: Babylon.Scene, textureUrl: string) => {
  return new Babylon.Texture(textureUrl, scene);
});

export interface PointOfInterestProps {
  scene: Babylon.Scene;
  model: PointOfInterestModel;
  zIndex: number;
  editing: boolean;
}

export default function PointOfInterest({
  scene,
  model,
  zIndex,
  editing,
}: PointOfInterestProps) {
  const {
    name,
    imageUrl,
    position: { x, y },
    size: { x: width, y: height },
    color,
  } = model;
  const mesh = useMesh(scene, 'pointOfInterest', width, height);

  React.useEffect(() => {
    const mat = new Babylon.StandardMaterial('material_' + name, scene);
    mat.diffuseTexture = getTexture(scene, imageUrl || defaultIcon);
    mat.diffuseTexture.hasAlpha = true;
    mat.disableLighting = true;
    mat.emissiveColor = Babylon.Color3.FromHexString(color);
    mesh.material = mat;
    mesh.selectable = model;
    mesh.position.set(x, zIndex, y);

    const observer = [
      observe(model, 'position', change => {
        mesh.position.set(
          change.newValue.x,
          mesh.position.y,
          change.newValue.y,
        );
      }),
      observe(model, 'color', change => {
        mat.emissiveColor = Babylon.Color3.FromHexString(change.newValue);
      }),
    ];

    mesh.actionManager = new Babylon.ActionManager(scene);
    mesh.actionManager.registerAction(
      new Babylon.ExecuteCodeAction(
        Babylon.ActionManager.OnDoublePickTrigger,
        ev => {
          if (model.linksTo) {
            navigateTo(model.linksTo);
          }
        },
      ),
    );

    setActive(model, mesh, true);
    return () => {
      setActive(model, mesh, false);
      mat.dispose();
      observer.forEach(o => o());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mesh, model, scene]);

  const screenPosition = useScreenPosition(scene, mesh, editing);

  return (
    <Popper
      transition
      open={editing}
      anchorEl={screenPosition}
      placement="right"
    >
      {({ TransitionProps }) => (
        <Zoom {...TransitionProps} timeout={100}>
          <Paper>
            <Typography>{name}</Typography>
            <input
              type="color"
              value={color}
              onChange={({ target: { value } }) => (model.color = value)}
            />
          </Paper>
        </Zoom>
      )}
    </Popper>
  );
}
