import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { VisibilityOff } from '@material-ui/icons';
import * as React from 'react';
import d20Icon from '../assets/d20-icon.png';
import { rollDice } from '../stores/modules/gameActions';
import classNames from '../utils/classNames';

const dragIcon = new Image();
dragIcon.src = d20Icon;

const styles = (theme: Theme) =>
  createStyles({
    text: {
      textDecoration: 'underline',
      cursor: 'pointer',
      position: 'relative',
    },
    withIcon: {
      // paddingRight: 18,
      '&:hover > svg': {
        display: 'block',
      },
    },
    icon: {
      display: 'none',
      position: 'absolute',
      right: -theme.spacing.unit,
      top: '50%',
      transform: 'translate(50%, -50%)',
      height: '100%',
    },
  });

export interface RollableContentProps extends WithStyles<typeof styles> {
  formula: string;
  context?: any;
  towerable?: boolean;
  draggable?: boolean;
  dragType?: string;
  dragData?: object | (() => any);
  children?: React.ReactNode;
}

export default withStyles(styles)(RollableContent);

function RollableContent({
  formula,
  context,
  towerable,
  draggable,
  dragType,
  dragData,
  classes,
  children,
}: RollableContentProps) {
  const handleDragStart = React.useCallback(
    (evt: React.DragEvent<HTMLElement>) => {
      evt.dataTransfer.setData(
        dragType || 'never',
        JSON.stringify(typeof dragData === 'function' ? dragData() : dragData),
      );
      evt.dataTransfer.setDragImage(dragIcon, 25, 25);
    },
    [dragData, dragType],
  );
  const handleRoll = React.useCallback(
    (towerRoll: boolean, evt: React.MouseEvent<any>) => {
      rollDice(formula, context, towerRoll).then(res =>
        console.log(res.toChatMessage(true)),
      );
      evt.stopPropagation();
    },
    [formula, context],
  );
  return (
    <span
      className={classNames(classes.text, towerable && classes.withIcon)}
      draggable={draggable}
      onDragStart={draggable ? handleDragStart : undefined}
      onClick={handleRoll.bind(undefined, false)}
    >
      {children}
      {towerable && (
        <VisibilityOff
          onClick={handleRoll.bind(undefined, true)}
          className={classes.icon}
        />
      )}
    </span>
  );
}
