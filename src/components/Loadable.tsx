import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  CircularProgress,
} from '@material-ui/core/es';
import * as React from 'react';
import classNames from '../utils/classNames';

const styles = (theme: Theme) =>
  createStyles({
    fullscreen: {
      width: '100vw',
      height: '100vh',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export interface LoadableProps extends WithStyles<typeof styles> {
  factory: () => Promise<{ default: React.ComponentType<any> }>;
  className?: string;
  fullscreen?: boolean;
  [key: string]: any;
}

export default withStyles(styles)(Loadable);

function Loadable({
  factory,
  className,
  fullscreen,
  classes,
  ...props
}: LoadableProps) {
  const [component, updateComponent] = React.useState<any>(null as null | {
    default: React.ComponentType;
  });
  React.useEffect(() => {
    factory().then(comp => updateComponent(comp));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return component === null ? (
    <div className={classNames(className, fullscreen && classes.fullscreen)}>
      <CircularProgress size={fullscreen ? 100 : undefined} />
    </div>
  ) : (
    <component.default className={className} {...props} />
  );
}
