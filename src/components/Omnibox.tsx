import DiceProgram from '@aiacta/dicelang';
import {
  createStyles,
  Divider,
  Input,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Modal,
  MuiThemeProvider,
  Theme,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import { Home } from '@material-ui/icons';
import * as React from 'react';
import TrieSearch from 'trie-search';
import { rollDice, sendChatMessage } from '../stores/modules/gameActions';
import { loadSRDContent, game } from '../stores/modules/gameData';
import { closeWindow, openWindow } from '../stores/modules/uiView';
import { darkTheme } from '../themes';
import useKeyCombos from './hooks/useKeyCombos';
import { getRawData } from '../stores/modules/dataStores';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -60px)',
      padding: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit,
      background: theme.palette.grey[800],
      borderRadius: theme.shape.borderRadius,
    },
    input: {
      fontSize: '1.5em',
      fontWeight: theme.typography.fontWeightRegular,
      background: theme.palette.grey[500],
      borderRadius: theme.shape.borderRadius,
      padding: theme.spacing.unit * 2,
      minWidth: 300,
    },
    divider: {
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit,
    },
    list: {
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit,
    },
    resultIcon: {
      marginLeft: theme.spacing.unit,
    },
    hints: {
      color: theme.palette.grey[500],
    },
  });

export interface OmniboxProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Omnibox);

const commandTrie = new TrieSearch('name', { min: 3, indexField: 'key' });
commandTrie.addAll([
  {
    key: 'open-chat',
    name: 'Chat: Open',
    exec: () => openWindow('chat'),
  },
  {
    key: 'close-chat',
    name: 'Chat: Close',
    exec: () => closeWindow('chat'),
  },
  {
    key: 'open-compendium',
    name: 'Compendium: Open',
    exec: () => openWindow('compendium'),
  },
  {
    key: 'close-compendium',
    name: 'Compendium: Close',
    exec: () => closeWindow('compendium'),
  },
  {
    key: 'open-combatTracker',
    name: 'Combat Tracker: Open',
    exec: () => openWindow('combatTracker'),
  },
  {
    key: 'close-combatTracker',
    name: 'Combat Tracker: Close',
    exec: () => closeWindow('combatTracker'),
  },
  {
    key: 'open-dbExplorer',
    name: 'Database Explorer: Open',
    exec: () => openWindow('dbExplorer'),
  },
  {
    key: 'close-dbExplorer',
    name: 'Database Explorer: Close',
    exec: () => closeWindow('dbExplorer'),
  },
  {
    key: 'open-characterCreator',
    name: 'Character: Create...',
    exec: () => openWindow('characterCreator'),
  },
  {
    key: 'load-srd-content',
    name: 'Apply SRD content',
    exec: () => loadSRDContent(),
  },
  {
    key: 'export-db',
    name: 'Export database',
    exec: () => {
      getRawData().then(({ rows }) => {
        const elem = document.createElement('a');
        elem.href = URL.createObjectURL(new Blob([JSON.stringify(rows)]));
        elem.download = `${game()!.name} Export.json`;
        elem.style.display = 'none';
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
      });
    },
  },
  {
    key: 'import-db',
    name: 'Import database...',
    exec: () => {},
  },
]);

function Omnibox({ classes }: OmniboxProps) {
  const [open, updateOpen] = React.useState(false);
  const [input, updateInput] = React.useState('');
  const [options, updateOptions] = React.useState([] as Array<{
    key: string;
    name: string;
    exec: () => void;
  }>);
  const [selection, updateSelection] = React.useState('');

  useKeyCombos({
    keys: [navigator.platform === 'MacIntel' ? 'meta' : 'control', 'k'],
    callback: evt => {
      evt.preventDefault();
      updateOpen(true);
      updateInput('');
    },
  });

  const handleClose = React.useCallback(() => updateOpen(!open), [open]);
  const handleInput = React.useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
      updateInput(value);

      const newOptions: typeof options = [];

      const rollMatch = /^roll (?<rollString>.+)$/.exec(value);
      if (rollMatch) {
        const program = new DiceProgram(rollMatch.groups!.rollString);
        if (program.errors.length === 0) {
          newOptions.push({
            key: 'dice',
            name: `Let fate decide: ${rollMatch.groups!.rollString}`,
            exec: () =>
              rollDice(program).then(res =>
                sendChatMessage(res.toChatMessage(true)),
              ),
          });
        } else {
          console.log(program.errors);
        }
      } else {
        newOptions.push(...commandTrie.get(value));
      }

      updateOptions(newOptions);
      if (newOptions.length > 0) {
        updateSelection(newOptions[0].key);
      } else {
        updateSelection('');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );
  const handleKeyDown = React.useCallback(
    (evt: React.KeyboardEvent<HTMLInputElement>) => {
      switch (evt.key) {
        case 'Enter':
          evt.preventDefault();
          updateOpen(false);
          if (selection) {
            options.find(opt => opt.key === selection)!.exec();
          }
          break;
        case 'ArrowUp':
        case 'ArrowDown':
        case 'Tab':
          evt.preventDefault();
          const idx = options.findIndex(opt => opt.key === selection);
          const chg = evt.key === 'ArrowUp' ? -1 : 1;
          updateSelection(options[Math.max(0, idx + chg) % options.length].key);
          break;
        default:
          break;
      }
    },
    [options, selection],
  );

  return (
    <MuiThemeProvider theme={darkTheme}>
      <Modal open={open} onClose={handleClose}>
        <div className={classes.container}>
          <Input
            placeholder="What would you like to do?"
            value={input}
            onChange={handleInput}
            onKeyDown={handleKeyDown}
            autoFocus
            autoCorrect="off"
            autoCapitalize="off"
            autoComplete="off"
            disableUnderline
            classes={{
              input: classes.input,
            }}
          />
          {options.length > 0 && (
            <List dense>
              {options.map(opt => (
                <ListItem
                  key={opt.key}
                  button
                  disableGutters
                  selected={opt.key === selection}
                >
                  <ListItemIcon classes={{ root: classes.resultIcon }}>
                    <Home />
                  </ListItemIcon>
                  <ListItemText primary={opt.name} />
                </ListItem>
              ))}
            </List>
          )}
          <Divider className={classes.divider} />
          <Typography className={classes.hints}>hint</Typography>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
