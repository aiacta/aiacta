import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import Program, { Context, Methods } from '@aiacta/dicelang';
import * as React from 'react';
import tymorasDice from '../utils/tymorasDice';

const styles = (theme: Theme) => createStyles({});
const waiting = Symbol('waiting');

export interface RollResultProps extends WithStyles<typeof styles> {
  formula: string;
  sync?: boolean;
  methods?: Partial<Methods>;
  context?: Context;
}

export default withStyles(styles)(RollResult);

function RollResult({
  formula,
  sync,
  methods = tymorasDice,
  context,
}: RollResultProps) {
  const [result, updateResult] = React.useState(waiting as any);
  React.useEffect(() => {
    if (!sync) {
      new Program(formula)
        .run(methods, context)
        .then(res => res.value)
        .catch(() => {
          return 'ERROR';
        })
        .then(val => updateResult(val || 'ERROR'));
    } else {
      try {
        updateResult(
          new Program(formula).runSync(methods, context).value || 'ERROR',
        );
      } catch (err) {
        updateResult('ERROR');
      }
    }
  }, [formula, context, sync, methods]);
  return result === waiting ? 'Rolling...' : result;
}
