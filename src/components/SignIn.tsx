import {
  Avatar,
  Button,
  createStyles,
  FormControl,
  Input,
  InputLabel,
  Paper,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Lock } from '@material-ui/icons';
import * as React from 'react';
import uuid from 'uuid/v4';
import { createUser } from '../stores/modules/gameData';
import { useMemoize } from '../utils/memoize';

const styles = (theme: Theme) =>
  createStyles({
    layout: {
      width: 'auto',
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
        .spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
  });

export interface SignInProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(SignIn);

function SignIn({ classes }: SignInProps) {
  const [inputs, updateInputs] = React.useState({
    username: '',
  });

  const handleInputChange = useMemoize(
    (prop: keyof typeof inputs) => (
      evt: React.ChangeEvent<HTMLInputElement>,
    ) => {
      const target = evt.target;
      updateInputs(s => ({ ...s, [prop]: target.value }));
    },
  );

  const handleSubmit = React.useCallback(
    (evt: React.FormEvent) => {
      createUser(uuid(), inputs.username);
      evt.preventDefault();
    },
    [inputs],
  );

  return (
    <>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Lock />
          </Avatar>
          <Typography variant="h5">Create a new user</Typography>
          <form className={classes.form} onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="username">Username</InputLabel>
              <Input
                id="username"
                name="username"
                autoComplete="username"
                autoFocus
                onChange={handleInputChange('username')}
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign in
            </Button>
          </form>
        </Paper>
      </main>
    </>
  );
}
