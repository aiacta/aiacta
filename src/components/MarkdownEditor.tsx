import {
  createStyles,
  IconButton,
  TextField,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Visibility } from '@material-ui/icons';
import * as React from 'react';
import Markdown from './Markdown';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
    },
    toggle: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
    editor: {
      maxWidth: '100%',
      width: '100vw',
    },
  });

export interface MarkdownEditorProps extends WithStyles<typeof styles> {
  value: string;
  onChange: (val: string) => void;
}

export default withStyles(styles)(MarkdownEditor);

function MarkdownEditor({ value, onChange, classes }: MarkdownEditorProps) {
  const [isEditing, updateIsEditing] = React.useState(false);
  const handleChange = React.useCallback(
    (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
      onChange(evt.currentTarget.value);
    },
    [onChange],
  );
  const handleToggleEdit = React.useCallback(() => {
    updateIsEditing(e => !e);
  }, []);

  return (
    <div className={classes.container}>
      {isEditing ? (
        <TextField
          className={classes.editor}
          label="Description"
          placeholder="Description"
          value={value}
          onChange={handleChange}
          multiline
          margin="normal"
          variant="outlined"
        />
      ) : (
        <Markdown>{value}</Markdown>
      )}
      <IconButton className={classes.toggle} onClick={handleToggleEdit}>
        <Visibility fontSize="small" />
      </IconButton>
    </div>
  );
}
