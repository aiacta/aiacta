import {
  Button,
  colors,
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Cancel, Crop, DeleteOutline, Done, Save } from '@material-ui/icons';
import * as React from 'react';
import defaultIcon from '../assets/ImageDrop.svg';
import classNames from '../utils/classNames';
import { useMemoize } from '../utils/memoize';
import DropZone, { DropInfo } from './DropZone';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateAreas: `
        'image'
        'pp'`,
      gridTemplateRows: '1fr auto',
      minHeight: 64,
    },
    dropzone: {
      gridArea: 'image',
      borderWidth: theme.spacing.unit / 2,
      borderColor: colors.grey[500],
      borderStyle: 'dashed',
      borderRadius: theme.spacing.unit / 2,
      padding: theme.spacing.unit / 2,
      textAlign: 'center',
    },
    image: {
      position: 'absolute',
      left: '50%',
      top: 0,
      // width: '100%',
      height: '100%',
      transform: 'translateX(-50%)',
    },
    cropOverlay: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: '100%',
      height: '100%',
      clipPath: 'inset(0 0 0 0)',
    },
    cropHandle: {
      position: 'absolute',
      pointerEvents: 'none',
      boxShadow: '0 0 0 9999em rgba(0, 0, 0, 0.5)',
      backgroundImage: `linear-gradient(90deg, ${
        theme.palette.action.active
      } 50%, transparent 50%), linear-gradient(90deg, ${
        theme.palette.action.active
      } 50%, transparent 50%), linear-gradient(0deg, ${
        theme.palette.action.active
      } 50%, transparent 50%), linear-gradient(0deg, ${
        theme.palette.action.active
      } 50%, transparent 50%)`,
      backgroundRepeat: 'repeat-x, repeat-x, repeat-y, repeat-y',
      backgroundSize: '15px 2px, 15px 2px, 2px 15px, 2px 15px',
      backgroundPosition: 'left top, right bottom, left bottom, right   top',
      animation: 'border-anim 1s infinite linear',
    },
    '@keyframes border-anim': {
      '0%': {
        backgroundPosition: 'left top, right bottom, left bottom, right   top',
      },
      '100%': {
        backgroundPosition:
          'left 15px top, right 15px bottom , left bottom 15px , right   top 15px',
      },
    },
    cropContainer: {
      position: 'relative',
    },
    postprocessActions: {
      marginTop: theme.spacing.unit,
      clipPath: 'inset(0 0 0 0)',
      height: 30,
      position: 'relative',
    },
    hidden: {
      transform: 'translateY(-110%)',
    },
    actions: {
      position: 'absolute',
      display: 'flex',
      width: '100%',
      transition: theme.transitions.create('transform'),
      '& button': {
        margin: `0 ${theme.spacing.unit / 2}px`,
      },
    },
    spacer: {
      flex: '1 1 0%',
    },
    leftIcon: {
      marginRight: theme.spacing.unit,
    },
    iconSmall: {
      fontSize: 20,
    },
  });

export interface ImageInputProps extends WithStyles<typeof styles> {
  className?: string;
  value?: File | Blob;
  preview?: string;
  imageRef?: React.Ref<HTMLImageElement>;
  allowCrop?: boolean;
  allowRemove?: boolean;
  cropRatio?: number;
  onChange: (dropInfo: DropInfo | undefined) => void;
}

export default withStyles(styles)(ImageInput);

function ImageInput({
  imageRef,
  value,
  preview,
  onChange,
  allowCrop,
  allowRemove,
  className,
  classes,
}: ImageInputProps) {
  const [dropInfo, updateDropInfo] = React.useState(undefined as
    | undefined
    | DropInfo);

  const handleDrop = React.useCallback(
    (di: DropInfo[] | DropInfo) => {
      if (!Array.isArray(di)) {
        if (allowCrop) {
          updateDropInfo(di);
        } else if (di.image) {
          onChange(di);
        }
      }
    },
    [allowCrop, onChange],
  );

  const cropRef = React.useRef(null as null | HTMLDivElement);
  const [[, ...cropRect], updateCropRect] = React.useState([0, 0, 0, 0, 0]);
  const [cropBounds, updateCropBounds] = React.useState([0, 0, 0, 0]);
  const handleBeginCrop = React.useCallback(
    (evt: React.MouseEvent<HTMLDivElement>) => {
      const { left, top } = evt.currentTarget.getBoundingClientRect();
      const cX = Math.max(
        cropBounds[0],
        Math.min(cropBounds[0] + cropBounds[2], evt.clientX - left),
      );
      const cY = Math.max(
        cropBounds[1],
        Math.min(cropBounds[1] + cropBounds[3], evt.clientY - top),
      );
      updateCropRect([1, cX, cY, 0, 0]);
    },
    [cropBounds],
  );
  const handleDragCrop = React.useCallback(
    (evt: React.MouseEvent<HTMLDivElement>) => {
      const { left, top } = evt.currentTarget.getBoundingClientRect();
      const cX = evt.clientX - left;
      const cY = evt.clientY - top;
      updateCropRect(([isCropping, ...v]) =>
        isCropping
          ? [
              isCropping,
              v[0],
              v[1],
              Math.min(
                cropBounds[2] - (v[0] - cropBounds[0]),
                Math.max(cropBounds[0] - v[0], cX - v[0]),
              ),
              Math.min(
                cropBounds[3] - (v[1] - cropBounds[1]),
                Math.max(cropBounds[1] - v[1], cY - v[1]),
              ),
            ]
          : [isCropping, ...v],
      );
    },
    [cropBounds],
  );
  const handleEndCrop = React.useCallback(() => {
    updateCropRect(([, ...v]) => [0, ...v]);
  }, []);
  const setupCrop = React.useCallback(() => {
    if (cropRef.current && dropInfo && dropInfo.image) {
      const { width, height } = cropRef.current.getBoundingClientRect();
      updateCropRect([0, 0, 0, 0, 0]);
      updateCropBounds([
        cropRef.current.offsetLeft - width / 2,
        cropRef.current.offsetTop,
        width,
        height,
      ]);
    }
  }, [dropInfo]);
  const saveCrop = React.useCallback(() => {
    if (cropRef.current && dropInfo && dropInfo.image) {
      const canvas = document.createElement('canvas');
      const { width: refWidth } = cropRef.current.getBoundingClientRect();
      const scaleFactor = dropInfo.image.width / refWidth;
      const [x, y, width, height] = [
        cropRect[0] - cropBounds[0],
        cropRect[1] - cropBounds[1],
        cropRect[2],
        cropRect[3],
      ].map(v => v * scaleFactor);
      // apply cropping
      if (width && height) {
        canvas.width = width;
        canvas.height = height;
        const ctx = canvas.getContext('2d')!;

        const img = new Image();
        img.src = dropInfo.preview;

        ctx.drawImage(img, x, y, width, height, 0, 0, width, height);
      }

      canvas.toBlob(blob => {
        if (blob) {
          updateDropInfo({
            file: new File([blob], dropInfo.file.name, {
              type: dropInfo.file.type,
            }),
            preview: URL.createObjectURL(blob),
            image: { width, height },
          });
        }
      });
    }
  }, [dropInfo, cropRect, cropBounds]);

  const [currentPostProcess, updateCurrentPostProcess] = React.useState('');
  const handlePostProcess = useMemoize(
    (type: string) => () => {
      updateCurrentPostProcess(type.startsWith('-') ? '' : type);
      switch (type) {
        case 'crop':
          setupCrop();
          break;
        case '-crop':
          saveCrop();
          break;
        default:
          break;
      }
    },
    [setupCrop, saveCrop],
  );
  const handleSaveImage = React.useCallback(
    (di: DropInfo | undefined) => () => {
      onChange(di);
      updateDropInfo(undefined);
    },
    [onChange],
  );

  const hasActions = allowCrop || allowRemove;

  return (
    <div className={classNames(classes.container, className)}>
      <DropZone
        allowPaste
        onDropImages={handleDrop}
        className={classNames(classes.dropzone)}
      >
        <img
          ref={elem => {
            if (typeof imageRef === 'function') {
              imageRef(elem);
            } else if (imageRef) {
              (imageRef as any).current = elem;
            }
            cropRef.current = elem;
          }}
          className={classes.image}
          src={dropInfo ? dropInfo.preview : preview || defaultIcon}
          alt=""
        />
        {currentPostProcess === 'crop' && (
          <div
            className={classes.cropOverlay}
            onMouseDown={handleBeginCrop}
            onMouseMove={handleDragCrop}
            onMouseUp={handleEndCrop}
          >
            <div
              className={classes.cropHandle}
              style={{
                left: cropRect[0] + Math.min(cropRect[2], 0),
                top: cropRect[1] + Math.min(cropRect[3], 0),
                width: Math.abs(cropRect[2]),
                height: Math.abs(cropRect[3]),
              }}
            />
          </div>
        )}
      </DropZone>
      {hasActions && (
        <div className={classes.postprocessActions}>
          <div
            className={classNames(
              classes.actions,
              currentPostProcess && classes.hidden,
            )}
          >
            {allowCrop && dropInfo && (
              <Button
                variant="contained"
                size="small"
                color="primary"
                onClick={handlePostProcess('crop')}
              >
                <Crop
                  className={classNames(classes.leftIcon, classes.iconSmall)}
                />
                Crop
              </Button>
            )}
            <div className={classes.spacer} />
            {allowRemove && (
              <Button
                variant="contained"
                size="small"
                color="secondary"
                onClick={handleSaveImage(undefined)}
                disabled={!value && !dropInfo}
              >
                <DeleteOutline
                  className={classNames(classes.leftIcon, classes.iconSmall)}
                />
                Remove
              </Button>
            )}
            {dropInfo && (
              <Button
                variant="contained"
                size="small"
                color="primary"
                onClick={handleSaveImage(dropInfo)}
              >
                <Save
                  className={classNames(classes.leftIcon, classes.iconSmall)}
                />
                Save
              </Button>
            )}
          </div>
          <div
            className={classNames(
              classes.actions,
              !currentPostProcess && classes.hidden,
            )}
          >
            <div className={classes.spacer} />
            <Button
              variant="contained"
              size="small"
              color="secondary"
              onClick={handlePostProcess('')}
            >
              <Cancel
                className={classNames(classes.leftIcon, classes.iconSmall)}
              />
              Cancel
            </Button>
            <Button
              variant="contained"
              size="small"
              color="primary"
              onClick={handlePostProcess('-' + currentPostProcess)}
            >
              <Done
                className={classNames(classes.leftIcon, classes.iconSmall)}
              />
              Done
            </Button>
          </div>
        </div>
      )}
    </div>
  );
}
