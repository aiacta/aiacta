import {
  createStyles,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Menu,
  MenuItem,
  TextField,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import {
  ControlPointDuplicate,
  DeleteForever,
  MoreVert,
  Search,
} from '@material-ui/icons';
import * as React from 'react';
import { AccessType, MonsterModel } from '../stores/models';
import {
  battlemaps,
  classes as classesD,
  items,
  monsters,
  spells,
} from '../stores/modules/dataStores';
import ModelStore, { BaseModel } from '../stores/modules/dataStores/ModelStore';
import { myself } from '../stores/modules/gameData';
import { openSheet, Windowable } from '../stores/modules/uiView';
import { useMemoize } from '../utils/memoize';
import useKeyCombos from './hooks/useKeyCombos';
import useObservable from './hooks/useObservable';
import VirtualList from './VirtualList';

const styles = (theme: Theme) =>
  createStyles({
    search: {
      flex: '0 0 auto',
    },
  });

export interface CompendiumProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Compendium);

function Compendium({ classes }: CompendiumProps) {
  const handleOpenSheet = useMemoize((model: Windowable) => () =>
    openSheet(model),
  );

  const handleDragItem = useMemoize(
    (obj: BaseModel<any>) => (evt: React.DragEvent<HTMLElement>) => {
      evt.dataTransfer.setData(
        'aiacta/' + obj.document.type,
        JSON.stringify(obj._id),
      );
    },
  );

  const [[menuAnchor, menuModel], updateMenu] = React.useState([
    undefined,
    null,
  ] as [undefined | HTMLElement, null | MonsterModel]);

  const handleOpenMenu = useMemoize(
    (monster: MonsterModel) => (evt: React.MouseEvent<HTMLElement>) => {
      updateMenu([evt.currentTarget, monster]);
    },
  );
  const handleCloseMenu = React.useCallback(() => {
    updateMenu([undefined, null]);
  }, []);

  const handleMenuDuplicate = React.useCallback(() => {
    if (menuModel instanceof MonsterModel) {
      monsters.add({
        ...menuModel.asDocument,
        name: `Copy of ${menuModel.name}`,
      });
    }
    updateMenu([undefined, null]);
  }, [menuModel]);
  const handleMenuDelete = React.useCallback(() => {
    if (menuModel instanceof MonsterModel) {
      monsters.remove(menuModel);
    }
    updateMenu([undefined, null]);
  }, [menuModel]);

  const [search, updateSearch] = React.useState(null as string | null);
  const handleUpdateSearch = React.useCallback(
    evt => updateSearch(evt.target.value),
    [],
  );
  useKeyCombos(
    {
      keys: ['meta', 'f'],
      callback: evt => {
        evt.preventDefault();
        updateSearch('');
      },
    },
    {
      keys: ['escape'],
      callback: evt => {
        evt.preventDefault();
        updateSearch(null);
      },
    },
  );

  const useItems = function<T extends BaseModel<any> & { name: string }>(
    store: ModelStore<T>,
  ) {
    return useObservable(
      () =>
        store
          .all()
          .filter(
            d =>
              myself().isAllowedTo(AccessType.Know, d) &&
              (!search || d.name.match(new RegExp(search, 'i'))),
          )
          .sort((a, b) => (a.name < b.name ? -1 : 1)),
      [search],
    );
  };

  const lists = [
    {
      name: 'Monsters',
      items: useItems(monsters),
      onClick: handleOpenSheet,
      onDragStart: handleDragItem,
      onMore: handleOpenMenu,
    },
    {
      name: 'Battlemaps',
      items: useItems(battlemaps),
      onDragStart: handleDragItem,
    },
    {
      name: 'Items',
      items: useItems(items),
      onClick: handleOpenSheet,
      onDragStart: handleDragItem,
    },
    {
      name: 'Spells',
      items: useItems(spells),
      onClick: handleOpenSheet,
      onDragStart: handleDragItem,
    },
    {
      name: 'Classes',
      items: useItems(classesD),
      onDragStart: handleDragItem,
    },
  ] as Array<{
    name: string;
    items: Array<BaseModel<any> & { name: string }>;
    onClick?: (dbo: any) => (evt: React.MouseEvent<HTMLElement>) => void;
    onDragStart?: (dbo: any) => (evt: React.DragEvent<HTMLElement>) => void;
    onMore?: (dbo: any) => (evt: React.MouseEvent<HTMLElement>) => void;
  }>;

  return (
    <>
      {search !== null && (
        <TextField
          type="search"
          className={classes.search}
          value={search}
          onChange={handleUpdateSearch}
          placeholder="Search"
          InputProps={{ startAdornment: <Search color="disabled" /> }}
          autoFocus
        />
      )}
      {lists
        .filter(({ items }) => items.length > 0)
        .map(({ name, items, onClick, onDragStart, onMore }) => (
          <VirtualList key={name} header={name} items={items} itemHeight={35}>
            {dbo => (
              <ListItem
                key={dbo._id}
                dense
                draggable
                button
                onClick={onClick && onClick(dbo)}
                onDragStart={onDragStart && onDragStart(dbo)}
              >
                <ListItemText primary={dbo.name} />
                {onMore && (
                  <ListItemSecondaryAction>
                    <IconButton onClick={onMore(dbo)}>
                      <MoreVert />
                    </IconButton>
                  </ListItemSecondaryAction>
                )}
              </ListItem>
            )}
          </VirtualList>
        ))}
      <Menu anchorEl={menuAnchor} open={!!menuAnchor} onClose={handleCloseMenu}>
        <MenuItem onClick={handleMenuDuplicate}>
          <ListItemIcon>
            <ControlPointDuplicate />
          </ListItemIcon>
          <ListItemText primary="Duplicate" />
        </MenuItem>
        <MenuItem onClick={handleMenuDelete}>
          <ListItemIcon>
            <DeleteForever />
          </ListItemIcon>
          <ListItemText primary="Delete" />
        </MenuItem>
      </Menu>
    </>
  );
}
