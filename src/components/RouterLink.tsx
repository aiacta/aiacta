import * as React from 'react';
import { Link } from 'react-router-dom';

export interface RouterLinkProps {
  href?: string;
}

export default RouterLink;

function RouterLink({ href, ...props }: RouterLinkProps) {
  return <Link to={href || '/'} {...props} />;
}
