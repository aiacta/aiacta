/* eslint-disable react/no-danger-with-children */
import {
  createStyles,
  Popover,
  Theme,
  Tooltip,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import { runInAction } from 'mobx';
import * as React from 'react';
import classNames from '../utils/classNames';
import useObservable from './hooks/useObservable';
import { Observer } from 'mobx-react';

const styles = (theme: Theme) =>
  createStyles({
    editBox: {
      display: 'inline-flex',
      outlineColor: theme.palette.primary.main,
      '&:hover': {
        outlineWidth: 2,
        outlineStyle: 'inset',
      },
    },
    complex: {
      cursor: 'pointer',
    },
    error: {
      outlineColor: theme.palette.error.main,
      outlineWidth: 2,
      outlineStyle: 'inset',
    },
  });

export interface EditableContentProps {
  editing: boolean;
  complex?: boolean;
  model: any;
  property: string;
  validate?: 'number' | 'boolean' | 'list' | ((value: string) => any);
  render?: (value: any, onChange?: (value: string) => void) => React.ReactNode;
  children?: (value: any, onChange: (value: string) => void) => React.ReactNode;
  className?: string;
}

export default withStyles(styles)(EditableContent);

function EditableContent({
  editing,
  complex,
  model,
  property,
  validate = v => v.trim(),
  render,
  children,
  className,
  classes,
}: EditableContentProps & WithStyles<typeof styles>) {
  const value = useObservable(
    () => property.split('.').reduce((o, k) => o[k], model),
    [model, property],
  );
  const onChange = React.useCallback(
    (newValue: string) => {
      let validValue: any = newValue;
      try {
        switch (validate) {
          case 'number':
            validValue = +validValue;
            if (isNaN(validValue)) {
              // eslint-disable-next-line no-throw-literal
              throw 'Please enter a number.';
            }
            break;
          case 'boolean':
            validValue = !!String(validValue)
              .toLowerCase()
              .match(/true|1/);
            break;
          case 'list':
            validValue = validValue.split(/\s*,\s*/g);
            if (!Array.isArray(validValue)) {
              // eslint-disable-next-line no-throw-literal
              throw 'Please enter a list.';
            }
            break;
          case undefined:
            break;
          default:
            validValue = validate(validValue);
            break;
        }
        updateError('');
      } catch (err) {
        if (typeof err === 'string') {
          updateError(err);
        }
        return;
      }

      const props = property.split('.');
      const obj = props.slice(0, -1).reduce((o, k) => o[k], model);
      runInAction(() => (obj[props.slice(-1)[0]] = validValue));
    },
    [model, property, validate],
  );
  const [error, updateError] = React.useState('');
  const handleChange = React.useCallback(
    (evt: React.FocusEvent<HTMLSpanElement>) => {
      onChange(evt.currentTarget.innerText);
    },
    [onChange],
  );

  const [popoverAnchor, updatePopoverAnchor] = React.useState(undefined as
    | HTMLElement
    | undefined);
  const handleOpenPopover = React.useCallback(
    (evt: React.MouseEvent<HTMLElement>) => {
      updatePopoverAnchor(evt.currentTarget);
    },
    [],
  );
  const handleClosePopover = React.useCallback(() => {
    updatePopoverAnchor(undefined);
  }, []);

  if (editing) {
    const node: React.ReactNode = render
      ? render(value, onChange)
      : Array.isArray(value)
      ? value.join(', ')
      : String(value);

    const contentEditable = typeof node === 'string' && !complex;

    return (
      <>
        <Tooltip title={error} open={!!error} placement="top">
          <span
            className={classNames(
              classes.editBox,
              error && classes.error,
              complex && classes.complex,
              className,
            )}
            contentEditable={contentEditable}
            onBlur={contentEditable ? handleChange : undefined}
            onClick={
              !contentEditable && complex ? handleOpenPopover : undefined
            }
            dangerouslySetInnerHTML={
              contentEditable
                ? { __html: (node as string) || '&nbsp;' }
                : undefined
            }
            children={!contentEditable ? node : undefined}
          />
        </Tooltip>
        {complex && (
          <Popover
            open={!!popoverAnchor}
            anchorEl={popoverAnchor}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            onClose={handleClosePopover}
          >
            {children ? (
              <Observer>{() => children(value, onChange)}</Observer>
            ) : (
              node
            )}
          </Popover>
        )}
      </>
    );
  }

  const r = render
    ? render(value)
    : Array.isArray(value)
    ? value.join(', ')
    : value;

  if (className) {
    return <span className={className}>{r}</span>;
  }
  return r;
}
