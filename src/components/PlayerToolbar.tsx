import {
  Avatar,
  createStyles,
  Menu,
  MenuItem,
  Theme,
  Tooltip,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { CharacterModel } from '../stores/models';
import { characters } from '../stores/modules/dataStores';
import { myself } from '../stores/modules/gameData';
import { isOnline } from '../stores/modules/network';
import { openSheet } from '../stores/modules/uiView';
import { useMemoize } from '../utils/memoize';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'fixed',
      left: 0,
      zIndex: 1050,
      display: 'grid',
      gridAutoFlow: 'column',
      gridGap: `${theme.spacing.unit}px`,
      padding: theme.spacing.unit,
    },
    avatar: {
      // width: 60,
      // height: 60,
      '& img': {
        pointerEvents: 'none',
      },
    },
  });

export interface PlayerToolbarProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(PlayerToolbar);

function PlayerToolbar({ classes }: PlayerToolbarProps) {
  const handleDragCharacter = useMemoize(
    (char: CharacterModel) => (evt: React.DragEvent<HTMLElement>) => {
      evt.dataTransfer.setData('aiacta/character', JSON.stringify(char._id));
    },
  );

  const [{ anchorEl, character }, updateAnchorEl] = React.useState({
    anchorEl: null as HTMLElement | null,
    character: null as CharacterModel | null,
  });
  const handleClick = useMemoize(
    (char: CharacterModel) => (evt: React.MouseEvent<HTMLElement>) => {
      updateAnchorEl({ anchorEl: evt.currentTarget, character: char });
    },
  );
  const handleClose = React.useCallback(() => {
    updateAnchorEl({ anchorEl: null, character: null });
  }, []);

  const handleOpenSheet = React.useCallback(() => {
    if (character) {
      openSheet(character);
    }
    handleClose();
  }, [character, handleClose]);

  const handleClaim = React.useCallback(() => {
    if (character) {
      character.owner = myself();
    }
    handleClose();
  }, [character, handleClose]);
  const handleRelease = React.useCallback(() => {
    if (character) {
      character.owner = undefined;
    }
    handleClose();
  }, [character, handleClose]);

  return (
    <div className={classes.container}>
      <Observer>
        {() => (
          <>
            {characters.all().map(char => {
              return (
                <Tooltip
                  key={char._id}
                  title={`${char.name}${
                    char.owner ? ` owned by ${char.owner.nickname}` : ''
                  }`}
                >
                  <Avatar
                    className={classes.avatar}
                    style={{
                      backgroundColor: char.owner ? char.owner.color : '#ccc',
                      filter:
                        !char.owner || !isOnline(char.owner._id)
                          ? 'grayscale(1)'
                          : undefined,
                    }}
                    src={char.tokenImageUrl}
                    draggable={myself().isDungeonMaster}
                    onDragStart={handleDragCharacter(char)}
                    onClick={handleClick(char)}
                  >
                    {!char.tokenImageUrl ? (char.name || '')[0] : null}
                  </Avatar>
                </Tooltip>
              );
            })}
          </>
        )}
      </Observer>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        <MenuItem onClick={handleOpenSheet}>Charactersheet</MenuItem>
        {character && !character.owner && (
          <MenuItem onClick={handleClaim}>Claim Character</MenuItem>
        )}
        {character && character.owner === myself() && (
          <MenuItem onClick={handleRelease}>Release Character</MenuItem>
        )}
        {character &&
          character.owner &&
          character.owner !== myself() &&
          myself().isDungeonMaster && (
            <MenuItem onClick={handleRelease}>Clear Character Owner</MenuItem>
          )}
      </Menu>
    </div>
  );
}
