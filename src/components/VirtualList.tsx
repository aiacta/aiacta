import {
  createStyles,
  List,
  // ListSubheader,
  Theme,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import * as React from 'react';

const ResizeObserver = (window as any).ResizeObserver;

const styles = (theme: Theme) =>
  createStyles({
    subheader: {
      backgroundColor: theme.palette.background.default,
      width: '100%',
      position: 'sticky',
      top: 0,
      zIndex: 1,
      background: theme.palette.background.default,
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit,
    },
    item: {
      position: 'absolute',
      width: '100%',
    },
    helper: {
      // display: 'none',
    },
  });

export interface VirtualListProps<T> {
  className?: string;
  header: React.ReactNode;
  itemHeight: number;
  items: T[];
  children: (val: T) => React.ReactNode;
}

export default withStyles(styles)(VirtualList) as <T>(
  p: VirtualListProps<T>,
) => null;

function VirtualList<T>({
  header,
  items,
  itemHeight,
  children: render,
  classes,
}: VirtualListProps<T> & WithStyles<typeof styles>) {
  const totalHeight = items.length * itemHeight;

  const [[start, end], update] = React.useReducer(
    (
      state,
      action: { containerHeight: number; scrollTop: number; offsetTop: number },
    ) => {
      const windowHeight =
        action.containerHeight -
        Math.max(0, action.offsetTop - action.scrollTop);
      const windowStart = Math.max(0, action.scrollTop - action.offsetTop);
      const startIdx = Math.ceil(windowStart / itemHeight);
      const num = Math.ceil(windowHeight / itemHeight) + 1;
      return [startIdx, startIdx + num];
    },
    [0, 0],
  );

  React.useEffect(() => {
    if (ref.current && ref.current.parentElement) {
      const target = ref.current.parentElement;
      const self = ref.current;
      const { height } = target.getBoundingClientRect();
      update({
        containerHeight: height,
        scrollTop: target.scrollTop,
        offsetTop: self.offsetTop,
      });
    }
  }, [itemHeight, items.length]);

  const ref = React.useRef<HTMLDivElement | null>(null);
  React.useEffect(() => {
    if (ref.current && ref.current.parentElement) {
      const target = ref.current.parentElement;
      const self = ref.current;
      const onScroll = () => {
        const { height } = target.getBoundingClientRect();
        update({
          containerHeight: height,
          scrollTop: target.scrollTop,
          offsetTop: self.offsetTop,
        });
      };
      target.addEventListener('scroll', onScroll);
      if (ResizeObserver) {
        const observer = new ResizeObserver(() => {
          const { height } = target.getBoundingClientRect();
          update({
            containerHeight: height,
            scrollTop: target.scrollTop,
            offsetTop: self.offsetTop,
          });
        });
        observer.observe(target);
        return () => {
          observer.disconnect();
          target.removeEventListener('scroll', onScroll);
        };
      }
    }
    return;
  }, []);

  return (
    <>
      <Typography className={classes.subheader} variant="subtitle1">
        {header}
      </Typography>
      <div ref={ref} className={classes.helper} />
      <List
        style={{ minHeight: totalHeight + 16, maxHeight: totalHeight + 16 }}
      >
        {items.slice(start, end).map((d, idx) => (
          <div
            key={idx}
            className={classes.item}
            style={{ top: start * itemHeight + idx * itemHeight }}
          >
            {render(d)}
          </div>
        ))}
      </List>
    </>
  );
}
