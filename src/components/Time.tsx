import { Button, Popover, Typography } from '@material-ui/core/es';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { time } from '../stores/modules/dataStores';
import formatTime from '../utils/formatTime';
import { useMemoize } from '../utils/memoize';

export default Time;

function Time() {
  const [controlsAnchor, updateControlsAnchor] = React.useState(undefined as
    | HTMLElement
    | undefined);

  const handleToggleTimeControls = React.useCallback(
    (evt: React.PointerEvent<HTMLElement>) => {
      const anchor = evt.currentTarget;
      updateControlsAnchor(s => (s ? undefined : anchor));
    },
    [],
  );

  const handleAdvanceTime = useMemoize((hours: number) => () => {
    time.singleton.now += hours * 3600;
  });

  return (
    <Observer>
      {() => (
        <>
          <Typography color="inherit" noWrap onClick={handleToggleTimeControls}>
            {formatTime(
              time.singleton.calendar,
              time.singleton.now,
              'hh:ii:ss DD MM yyyy',
            )}
          </Typography>
          <Popover
            open={!!controlsAnchor}
            onClose={handleToggleTimeControls}
            anchorEl={controlsAnchor}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            PaperProps={{ square: true }}
          >
            <Button onClick={handleAdvanceTime(-1)}>-1h</Button>
            <Button onClick={handleAdvanceTime(1)}>+1h</Button>
            <Button onClick={handleAdvanceTime(8)}>+8h</Button>
            <Button onClick={handleAdvanceTime(24)}>+1d</Button>
            <Button
              onClick={handleAdvanceTime(
                time.singleton.calendar.daysInWeek * 24,
              )}
            >
              +1w
            </Button>
            <Button
              onClick={handleAdvanceTime(
                time.singleton.calendar.daysInYear * 24,
              )}
            >
              +1y
            </Button>
          </Popover>
        </>
      )}
    </Observer>
  );
}
