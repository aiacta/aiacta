import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as Babylon from 'babylonjs';
import * as React from 'react';
import uuid from 'uuid/v4';
import { BattlemapModel } from '../../stores/models';
import { walls } from '../../stores/modules/dataStores';
import pickGround from '../../utils/pickGround';
import registerEventHandler from '../../utils/registerEventHandler';
import { UILayer } from '../hooks/useScene';
import { createPathArray } from '../maps/Wall';
import { ToolProps } from './Tools';
import { runInAction } from 'mobx';

const styles = (theme: Theme) => createStyles({});

export interface BuildToolProps extends ToolProps, WithStyles<typeof styles> {}

export default withStyles(styles)(BuildTool);

function BuildTool({ className, map, scene }: BuildToolProps) {
  React.useEffect(() => {
    const canvas = scene.getEngine().getRenderingCanvas();

    const vertices: Babylon.Vector2[] = [];

    const disposers = [
      registerEventHandler(canvas, 'click', evt => {
        const ground = pickGround(scene);
        if (evt.button === 0) {
          if (ground) {
            const snapToGrid = !evt.metaKey
              ? roundToNearest.bind(null, 1)
              : (v: number) => v;
            const next = new Babylon.Vector2(
              snapToGrid(ground.x),
              snapToGrid(ground.z),
            );
            if (
              vertices.length > 1 &&
              (next.subtract(vertices[0]).length() < 1 ||
                next.subtract(vertices[vertices.length - 1]).length() < 1)
            ) {
              runInAction(() => {
                (map as BattlemapModel).walls = [
                  ...(map as BattlemapModel).walls,
                  walls.add({
                    closed: next.subtract(vertices[0]).length() < 1,
                    height: 2,
                    thickness: 0.2,
                    vertices: vertices.map(v => ({
                      key: uuid(),
                      x: v.x,
                      y: v.y,
                    })),
                  }),
                ];
              });
              vertices.splice(0, vertices.length);
            } else {
              vertices.push(next);
            }
          }
        } else {
          vertices.pop();
        }
      }),
      registerEventHandler(canvas, 'contextmenu', evt => {
        evt.preventDefault();
        vertices.pop();
        const ground = pickGround(scene);
        if (vertices.length > 0) {
          if (ground) {
            const snapToGrid = !evt.metaKey
              ? roundToNearest.bind(null, 1)
              : (v: number) => v;
            renderMesh([
              ...vertices,
              new Babylon.Vector2(snapToGrid(ground.x), snapToGrid(ground.z)),
            ]);
          }
        } else if (mesh) {
          mesh.dispose();
          mesh = null;
        }
      }),
      registerEventHandler(canvas, 'pointermove', evt => {
        const ground = pickGround(scene);
        if (ground && vertices.length > 0) {
          const snapToGrid = !evt.metaKey
            ? roundToNearest.bind(null, 1)
            : (v: number) => v;
          renderMesh([
            ...vertices,
            new Babylon.Vector2(snapToGrid(ground.x), snapToGrid(ground.z)),
          ]);
        }
      }),
    ];

    let mesh: Babylon.Mesh | null = null;
    const material = new Babylon.StandardMaterial('build', scene);
    material.emissiveColor = new Babylon.Color3(0.5, 0.5, 1.0);
    function renderMesh(verts: Babylon.Vector2[]) {
      if (mesh) {
        mesh.dispose();
      }
      mesh = Babylon.MeshBuilder.CreateRibbon(
        'build',
        {
          closePath: false,
          pathArray: createPathArray(verts, false, 0.2, 2),
        },
        scene,
      );
      mesh.layerMask = UILayer;
      mesh.material = material;
    }

    return () => {
      disposers.forEach(d => d());
      if (mesh) {
        mesh.dispose(true, true);
      } else {
        material.dispose();
      }
    };
  }, [scene, map]);

  return <div className={className} />;
}

function roundToNearest(n: number, v: number) {
  return Math.round(v / n) * n;
}
