import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as Babylon from 'babylonjs';
import { reaction, runInAction } from 'mobx';
import * as React from 'react';
import { AccessType, WorldModel } from '../../stores/models';
import { myself } from '../../stores/modules/gameData';
import {
  addToSelection,
  getSelected,
  setSelectionTo,
} from '../../stores/modules/uiSelection';
import { getActiveMesh } from '../../stores/modules/uiView';
import pickGround from '../../utils/pickGround';
import pickSelectable from '../../utils/pickSelectable';
import registerEventHandler from '../../utils/registerEventHandler';
import { UILayer } from '../hooks/useScene';
import { ToolProps } from './Tools';

const styles = (theme: Theme) => createStyles({});

export interface SelectToolProps extends ToolProps, WithStyles<typeof styles> {}

export default withStyles(styles)(SelectTool);

function SelectTool({ className, map, scene, updateEditing }: SelectToolProps) {
  React.useEffect(() => {
    const mesh = createOrUpdateBox(scene, {
      topLeft: { x: 0, y: 0 },
      bottomRight: { x: 10, y: 10 },
    });
    const material = new Babylon.StandardMaterial('select', scene);
    material.emissiveColor = Babylon.Color3.White();
    mesh.material = material;
    mesh.setEnabled(false);
    mesh.layerMask = UILayer;

    const canvas = scene.getEngine().getRenderingCanvas();

    let dragRect = null as null | Babylon.Vector3;
    let dragSelected = null as null | Babylon.Vector3;
    let pointerPos = { x: 0, y: 0 };

    const disposers = [
      registerEventHandler(canvas, 'pointerdown', evt => {
        const ground = pickGround(scene);
        const selectable = pickSelectable(scene);
        if (evt.button === 1 || evt.altKey || selectable) {
          if (selectable) {
            if (!getSelected().includes(selectable)) {
              setSelectionTo(selectable);
            }
            dragSelected = ground;
          } else if (ground) {
            dragRect = ground;
            mesh.setEnabled(false);
          }
        }
        pointerPos = { x: evt.clientX, y: evt.clientY };
        updateEditing(s => (!selectable || s !== selectable._id ? null : s));
      }),
      registerEventHandler(canvas, 'pointermove', evt => {
        const ground = pickGround(scene);
        if (ground) {
          if (dragSelected) {
            const d = ground.subtract(dragSelected);
            dragSelected = ground;
            runInAction(() => {
              getSelected().forEach(selectable => {
                if (
                  hasPosition(selectable) &&
                  myself().isAllowedTo(AccessType.Write, selectable)
                ) {
                  selectable.position = selectable.position.add(
                    new Babylon.Vector2(d.x, d.z),
                  );
                }
              });
            });
          } else if (dragRect) {
            mesh.setEnabled(true);
            createOrUpdateBox(
              scene,
              {
                topLeft: {
                  x: Math.min(dragRect.x, ground.x),
                  y: Math.min(dragRect.z, ground.z),
                },
                bottomRight: {
                  x: Math.max(dragRect.x, ground.x),
                  y: Math.max(dragRect.z, ground.z),
                },
              },
              mesh,
            );
          }
        }
      }),
      registerEventHandler(canvas, 'pointerup', evt => {
        const ground = pickGround(scene);
        const selectable = pickSelectable(scene);
        const distanceMoved =
          Math.pow(pointerPos.x - evt.clientX, 2) +
          Math.pow(pointerPos.y - evt.clientY, 2);
        if (ground) {
          if (dragSelected) {
            dragSelected = null;
            const snapToGrid = !evt.metaKey
              ? roundToNearest.bind(null, 1)
              : (v: number) => v;
            if (distanceMoved > 0.1) {
              runInAction(() => {
                getSelected().forEach(selectable => {
                  if (
                    hasPosition(selectable) &&
                    myself().isAllowedTo(AccessType.Write, selectable)
                  ) {
                    selectable.position = new Babylon.Vector2(
                      snapToGrid(selectable.position.x - 0.5) + 0.5,
                      snapToGrid(selectable.position.y - 0.5) + 0.5,
                    );
                  }
                });
              });
            }
          } else if (dragRect) {
            if (map instanceof WorldModel) {
            } else {
              const bb = new Babylon.BoundingBox(
                new Babylon.Vector3(
                  Math.min(dragRect.x, ground.x),
                  -1,
                  Math.min(dragRect.z, ground.z),
                ),
                new Babylon.Vector3(
                  Math.max(dragRect.x, ground.x),
                  1,
                  Math.max(dragRect.z, ground.z),
                ),
              );
              const selection = map.tokens.filter(t =>
                bb.intersectsPoint(
                  new Babylon.Vector3(t.position.x, 0, t.position.y),
                ),
              );
              if (evt.shiftKey) {
                addToSelection(selection);
              } else {
                setSelectionTo(selection);
              }
            }
            dragRect = null;
            mesh.setEnabled(false);
          }
        }
        if (distanceMoved < 0.1 && !selectable) {
          setSelectionTo([]);
        }
      }),
      registerEventHandler(canvas, 'contextmenu', evt => {
        const selectable = pickSelectable(scene);
        if (selectable) {
          updateEditing(s => (s === selectable._id ? null : selectable._id));
        }
        evt.preventDefault();
      }),
    ];

    let selectionMeshes: Babylon.Mesh[] = [];
    const reactionDisposer = reaction(
      () =>
        getSelected()
          .map(s => getActiveMesh(s)!)
          .filter(Boolean),
      meshes => {
        selectionMeshes.forEach(m => m.dispose(true, true));
        selectionMeshes = meshes.map(mesh => {
          const width = mesh.getBoundingInfo().boundingBox.extendSizeWorld.x;
          const gizmo = createOrUpdateBox(scene, {
            topLeft: { x: -width, y: -width },
            bottomRight: { x: width, y: width },
            thickness: 0.025,
          });
          gizmo.parent = mesh;
          const material = new Babylon.StandardMaterial('select', scene);
          material.emissiveColor = new Babylon.Color3(0.5, 0.5, 1.0);
          gizmo.material = material;
          return gizmo;
        });
      },
    );

    return () => {
      disposers.forEach(d => d());
      mesh.dispose(true, true);
      selectionMeshes.forEach(m => m.dispose(true, true));
      reactionDisposer();
    };
  }, [scene, map, updateEditing]);

  return <div className={className} />;
}

function createOrUpdateBox(
  scene: Babylon.Scene,
  {
    topLeft,
    bottomRight,
    thickness,
  }: {
    topLeft: { x: number; y: number };
    bottomRight: { x: number; y: number };
    thickness?: number;
  },
  mesh?: Babylon.Mesh,
) {
  const corners = [
    { x: topLeft.x, y: topLeft.y },
    { x: bottomRight.x, y: topLeft.y },
    { x: bottomRight.x, y: bottomRight.y },
    { x: topLeft.x, y: bottomRight.y },
  ];
  if (!mesh) {
    return Babylon.MeshBuilder.CreateRibbon(
      'select',
      {
        pathArray: getPathArray(corners, thickness),
        closePath: true,
        updatable: true,
      },
      scene,
    );
  } else {
    Babylon.Mesh.CreateRibbon(
      'select',
      getPathArray(corners, thickness),
      false,
      false,
      0,
      undefined,
      undefined,
      undefined,
      mesh,
    );
    return mesh;
  }
}

function getPathArray(corners: { x: number; y: number }[], thickness = 0.05) {
  return [
    corners.map(
      ({ x, y }, i) =>
        new Babylon.Vector3(
          x - thickness * (i < 1 || i > 2 ? -1 : 1),
          1,
          y - thickness * (i < 2 ? -1 : 1),
        ),
    ),
    corners.map(
      ({ x, y }, i) =>
        new Babylon.Vector3(
          x + thickness * (i < 1 || i > 2 ? -1 : 1),
          1,
          y + thickness * (i < 2 ? -1 : 1),
        ),
    ),
  ];
}

function hasPosition(m: any): m is { position: Babylon.Vector2 } {
  return 'position' in m;
}

function roundToNearest(n: number, v: number) {
  return Math.round(v / n) * n;
}
