import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Build, Navigation } from '@material-ui/icons';
import * as Babylon from 'babylonjs';
import * as React from 'react';
import { BattlemapModel, WorldModel } from '../../stores/models';
import { myself } from '../../stores/modules/gameData';
import SpeedSelect from '../SpeedSelect';
import BuildTool from './BuildTool';
import SelectTool from './SelectTool';

const styles = (theme: Theme) =>
  createStyles({
    toolDial: {
      position: 'absolute',
      bottom: theme.spacing.unit * 2,
      left: theme.spacing.unit * 2,
    },
    toolOverlay: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      pointerEvents: 'none',
    },
  });

export interface ToolsProps extends WithStyles<typeof styles> {
  scene: Babylon.Scene;
  map: WorldModel | BattlemapModel;
  updateEditing: React.Dispatch<React.SetStateAction<string | null>>;
}

export interface ToolProps {
  className: string;
  scene: Babylon.Scene;
  map: WorldModel | BattlemapModel;
  updateEditing: React.Dispatch<React.SetStateAction<string | null>>;
}

export default withStyles(styles)(Tools);

function Tools({ scene, map, updateEditing, classes }: ToolsProps) {
  const options = React.useMemo(
    () => ({
      Select: {
        icon: <Navigation />,
        get available() {
          return true;
        },
        component: SelectTool,
      },
      Build: {
        icon: <Build />,
        get available() {
          return map instanceof BattlemapModel && myself().isDungeonMaster;
        },
        component: BuildTool,
      },
    }),
    [map],
  );

  const [currentTool, updateCurrentTool] = React.useState(options.Select);
  const onChange = React.useCallback(
    (val: { name: string }) =>
      updateCurrentTool(options[val.name as keyof typeof options]),
    [updateCurrentTool, options],
  );

  return (
    <>
      <currentTool.component
        className={classes.toolOverlay}
        scene={scene}
        map={map}
        updateEditing={updateEditing}
      />
      <SpeedSelect
        ariaLabel="Tool selection"
        className={classes.toolDial}
        icon={currentTool.icon}
        onChange={onChange}
        options={(Object.keys(options) as (keyof typeof options)[])
          .filter(key => options[key].available)
          .map(key => ({
            name: key,
            icon: options[key].icon,
          }))}
      />
    </>
  );
}
