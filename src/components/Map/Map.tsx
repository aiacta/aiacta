import {
  createStyles,
  Theme,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import * as Babylon from 'babylonjs';
import { reaction } from 'mobx';
import * as React from 'react';
import useVisibilityShaders from '../../shaders/useVisibilityShaders';
import {
  BattlemapModel,
  IBattlemap,
  isIWorld,
  IWorld,
  PartyModel,
  TokenModel,
  WorldModel,
} from '../../stores/models';
import {
  battlemaps,
  characters,
  monsters,
  worlds,
  players,
  meta,
} from '../../stores/modules/dataStores';
import { BaseModel } from '../../stores/modules/dataStores/ModelStore';
import {
  createParty,
  createPointOfInterest,
  createToken,
  joinParty,
  triggerAttack,
  triggerDamage,
  triggerSave,
} from '../../stores/modules/gameActions';
import { getFocus } from '../../stores/modules/uiView';
import pickSelectable from '../../utils/pickSelectable';
import DropZone, { DropInfo } from '../DropZone';
import useDragAndDrop from '../hooks/useDragAndDrop';
import useObservable from '../hooks/useObservable';
import useScene from '../hooks/useScene';
import MapCreationDialog from '../MapCreationDialog';
import Asset from '../maps/Asset';
import Party from '../maps/Party';
import PointOfInterest from '../maps/PointOfInterest';
import Token from '../maps/Token';
import Wall from '../maps/Wall';
import Tools from './Tools';
import { myself } from '../../stores/modules/gameData';
import Cursor from '../maps/Cursor';
import pickGround from '../../utils/pickGround';
import { isOnline } from '../../stores/modules/network';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      flex: '1 1 auto',
      overflow: 'hidden',
      position: 'relative',
    },
    menuAnchor: {
      position: 'absolute',
      visibility: 'hidden',
      pointerEvents: 'none',
    },
  });

export interface MapProps extends WithStyles<typeof styles> {
  model: BattlemapModel | WorldModel;
}

export default withStyles(styles)(Map);

function Map({ model, classes }: MapProps) {
  const canvasRef = React.useRef(null as HTMLCanvasElement | null);
  const {
    scene,
    updateCameraZoom,
    updateCameraPosition,
    updateCameraFocus,
    moveCameraPosition,
  } = useScene(canvasRef);

  React.useEffect(() => {
    updateCameraZoom(1);
    // updateCameraPosition(uiStore.cameraPosition[0], uiStore.cameraPosition[1]);
    updateCameraPosition(0, 0);

    myself().activity.map = model._id;
    return () => {
      myself().activity.map = null;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [model]);

  React.useEffect(
    () =>
      reaction(() => getFocus(), focus => focus && updateCameraFocus(focus)),
    [updateCameraFocus],
  );

  useVisibilityShaders(scene, model);

  const processDropEvent = React.useCallback(
    (evt: React.DragEvent<HTMLElement>) => {
      if (canvasRef.current && scene) {
        const { left, top } = canvasRef.current.getBoundingClientRect();
        const x = evt.clientX - left;
        const y = evt.clientY - top;

        const groundP = scene.pick(x, y, () => true);
        const ground = groundP && groundP.pickedPoint;
        const selectableP = scene.pick(x, y, p => !!p.selectable);
        const selectable =
          selectableP &&
          selectableP.pickedMesh &&
          selectableP.pickedMesh.selectable;
        return [ground, selectable] as any;
      }
      return [null, null];
    },
    [scene],
  );
  const dragAndDrop = useDragAndDrop<
    [Babylon.Nullable<Babylon.Vector3>, Babylon.Nullable<BaseModel<any>>]
  >(
    {
      'dnd5e/attack': {
        allow: (ground, selectable) => selectable instanceof TokenModel,
        onDrop: (data, ground, selectable) =>
          selectable instanceof TokenModel && triggerAttack(data, selectable),
      },
      'dnd5e/damage': {
        allow: (ground, selectable) => selectable instanceof TokenModel,
        onDrop: (data, ground, selectable) =>
          selectable instanceof TokenModel && triggerDamage(data, selectable),
      },
      'dnd5e/save': {
        allow: (ground, selectable) => selectable instanceof TokenModel,
        onDrop: (data, ground, selectable) =>
          selectable instanceof TokenModel && triggerSave(data, selectable),
      },
      'aiacta/monster': {
        allow: (ground, selectable) => !!ground,
        onDrop: (data, ground) => {
          const monster = monsters.get(data);
          if (model instanceof BattlemapModel && ground && monster) {
            createToken(model, { x: ground.x, y: ground.z }, monster);
          }
        },
      },
      'aiacta/battlemap': {
        allow: (ground, selectable) => !!ground,
        onDrop: (data, ground) => {
          const bmap = battlemaps.get(data);
          if (model instanceof WorldModel && ground && bmap) {
            createPointOfInterest(
              model,
              { x: ground.x, y: ground.z },
              { name: bmap.name, desc: '', linksTo: bmap._id },
            );
          }
        },
      },
      'aiacta/character': {
        allow: (ground, selectable) =>
          !!ground || selectable instanceof PartyModel,
        onDrop: (data, ground, selectable) => {
          const character = characters.get(data);
          if (model instanceof WorldModel && character) {
            if (selectable instanceof PartyModel) {
              joinParty(character, selectable);
            } else if (ground) {
              createParty(model, { x: ground.x, y: ground.z }, character);
            }
          } else if (model instanceof BattlemapModel && ground && character) {
            createToken(model, { x: ground.x, y: ground.z }, character);
          }
        },
      },
    },
    processDropEvent,
  );

  const [dropInfo, updateDropInfo] = React.useState(undefined as
    | { dropInfo: DropInfo; position: Babylon.Vector3 }
    | undefined);
  const handleDropImage = React.useCallback(
    (accepted: DropInfo[] | DropInfo, evt?: React.DragEvent<HTMLElement>) => {
      if (canvasRef.current && scene && evt) {
        const { left, top } = canvasRef.current.getBoundingClientRect();
        const x = evt.clientX - left;
        const y = evt.clientY - top;

        const ground = scene.pick(x, y, () => true);
        const targetPoint = ground && ground.pickedPoint;

        if (targetPoint) {
          updateDropInfo({
            dropInfo: accepted as DropInfo,
            position: targetPoint,
          });
        }
      }
    },
    [scene],
  );

  React.useEffect(() => {
    if (canvasRef.current) {
      canvasRef.current.addEventListener(
        'wheel',
        evt => {
          if (scene) {
            // touchpad zoom
            if (evt.ctrlKey) {
              updateCameraZoom(1 + evt.deltaY / 100);
            } else {
              moveCameraPosition(evt.deltaX / 2, -evt.deltaY / 2);
            }
            evt.preventDefault();
          }
        },
        { passive: false },
      );
    }
  }, [canvasRef, scene, moveCameraPosition, updateCameraZoom]);
  // const handleWheel = React.useCallback(
  //   (evt: React.WheelEvent<HTMLElement>) => {
  //     if (scene) {
  //       // touchpad zoom
  //       if (evt.ctrlKey) {
  //         updateCameraZoom(1 + evt.deltaY / 100);
  //       } else {
  //         updateCameraVelocity(evt.deltaX / 500, -evt.deltaY / 500);
  //       }
  //     }
  //   },
  //   [scene, updateCameraVelocity, updateCameraZoom],
  // );
  const [cameraDrag, updateCameraDrag] = React.useState(false);
  const handleMouseDown = React.useCallback(
    (evt: React.MouseEvent<HTMLElement>) => {
      const selectable = scene && pickSelectable(scene);
      if (evt.button === 0 && !evt.altKey && !selectable) {
        updateCameraDrag(true);
        evt.preventDefault();
      }
    },
    [scene],
  );
  const handleMouseMove = React.useMemo(() => {
    const latest = { x: null as null | number, y: null as null | number };
    return (evt: React.MouseEvent<HTMLElement>) => {
      if (cameraDrag) {
        moveCameraPosition(
          (latest.x === null ? evt.clientX : latest.x) - evt.clientX,
          -((latest.y === null ? evt.clientY : latest.y) - evt.clientY),
        );
        latest.x = evt.clientX;
        latest.y = evt.clientY;
      }
      if (meta.singleton.settings.trackCursor && scene) {
        const ground = pickGround(scene);
        if (ground) {
          myself().activity.position = { x: ground.x, y: ground.z };
        }
      }
    };
  }, [cameraDrag, moveCameraPosition, scene]);
  const handleMouseUp = React.useCallback(
    (evt: React.MouseEvent<HTMLElement>) => {
      updateCameraDrag(false);
    },
    [],
  );

  const [editing, updateEditing] = React.useState(null as string | null);

  const sceneChildren = useObservable(() => {
    if (!scene) {
      return null;
    }

    const children = [
      <Asset
        key={`${model._id}-background`}
        scene={scene}
        imageUrl={model.imageUrl}
        position={Babylon.Vector2.Zero()}
        size={model.size}
      />,
      ...players
        .filter(
          p =>
            p.activity.map === model._id && isOnline(p._id) && p !== myself(),
        )
        .map(p => <Cursor key={p._id} scene={scene} model={p} zIndex={2} />),
    ];

    if (model instanceof BattlemapModel) {
      children.push(
        ...model.walls.map(wall => (
          <Wall
            key={wall._id}
            scene={scene}
            model={wall}
            visible={true}
            editing={editing === wall._id}
          />
        )),
      );
      children.push(
        ...model.tokens.map(token => (
          <Token
            key={token._id}
            scene={scene}
            model={token}
            zIndex={1}
            editing={editing === token._id}
          />
        )),
      );
    } else {
      children.push(
        ...model.pointsOfInterest.map(poi => (
          <PointOfInterest
            key={poi._id}
            scene={scene}
            model={poi}
            zIndex={1}
            editing={editing === poi._id}
          />
        )),
      );
      children.push(
        ...model.parties.map(party => (
          <Party
            key={party._id}
            scene={scene}
            model={party}
            zIndex={1}
            editing={editing === party._id}
          />
        )),
      );
    }

    return children;
  }, [scene, model, editing]);

  const handleCreateMap = React.useCallback(
    (newMap: IWorld | IBattlemap) => {
      if (model instanceof WorldModel && dropInfo) {
        const newModel = isIWorld(newMap)
          ? worlds.add(newMap)
          : battlemaps.add(newMap);
        createPointOfInterest(
          model,
          { x: dropInfo.position.x, y: dropInfo.position.z },
          {
            name: 'A new point',
            desc: 'Here be dragons...',
            linksTo: newModel._id,
          },
        );
        updateDropInfo(undefined);
      }
    },
    [model, dropInfo],
  );
  const handleStopCreateMap = React.useCallback(
    () => updateDropInfo(undefined),
    [],
  );

  return (
    <>
      <DropZone
        allowPaste
        onDropImages={handleDropImage}
        className={classes.container}
        {...dragAndDrop}
      >
        <canvas
          ref={canvasRef}
          onPointerDown={handleMouseDown}
          onPointerMove={handleMouseMove}
          onPointerUp={handleMouseUp}
          // onWheel={handleWheel}
        />
        {scene && (
          <Tools scene={scene} map={model} updateEditing={updateEditing} />
        )}
        {sceneChildren}
        <div className={classes.menuAnchor} />
      </DropZone>
      <MapCreationDialog
        open={!!dropInfo}
        initialDrop={dropInfo && dropInfo.dropInfo}
        onCreate={handleCreateMap}
        onDismiss={handleStopCreateMap}
      />
    </>
  );
}
