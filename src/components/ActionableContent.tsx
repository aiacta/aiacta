import {
  createStyles,
  Theme,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import d20Icon from '../assets/d20-icon.png';
import { useMemoize } from '../utils/memoize';

const dragIcon = new Image();
dragIcon.src = d20Icon;

const styles = (theme: Theme) =>
  createStyles({
    action: {
      cursor: 'pointer',
      background: theme.palette.action.hover,
      borderRadius: theme.shape.borderRadius,
      '&:hover': {
        background: theme.palette.action.selected,
      },
    },
  });

export interface ActionableContentProps extends WithStyles<typeof styles> {
  instigatorId: string;
  // TODO use action-interface?
  actionlike: { name: string; desc: string } | string;
}

export default withStyles(styles)(ActionableContent);

function ActionableContent({
  actionlike,
  instigatorId,
  classes,
}: ActionableContentProps) {
  const handleDragAttack = useMemoize(
    (type: string, rollModifier: number, range: string, targets: string) => (
      evt: React.DragEvent<HTMLElement>,
    ) => {
      evt.dataTransfer.setData(
        'dnd5e/attack',
        JSON.stringify({
          instigator: instigatorId,
          type,
          rollModifier,
          range,
          targets,
        }),
      );
      evt.dataTransfer.setDragImage(dragIcon, 25, 25);
    },

    [instigatorId],
  );

  const handleDragDamage = useMemoize(
    (
      damage: string,
      damageType: string,
      damage1: string,
      damage1Type: string,
    ) => (evt: React.DragEvent<HTMLElement>) => {
      evt.dataTransfer.setData(
        'dnd5e/damage',
        JSON.stringify({
          instigator: instigatorId,
          damage,
          damageType,
          damage1,
          damage1Type,
        }),
      );
      evt.dataTransfer.setDragImage(dragIcon, 25, 25);
    },
    [instigatorId],
  );

  const handleDragSave = useMemoize(
    (
      attribute: string,
      saveDC: number,
      damage: string,
      damageType: string,
      halfOnSuccess: boolean,
    ) => (evt: React.DragEvent<HTMLElement>) => {
      evt.dataTransfer.setData(
        'dnd5e/save',
        JSON.stringify({
          instigator: instigatorId,
          attribute,
          saveDC,
          damage,
          damageType,
          halfOnSuccess,
        }),
      );
      evt.dataTransfer.setDragImage(dragIcon, 25, 25);
    },
    [instigatorId],
  );

  const desc = typeof actionlike === 'string' ? actionlike : actionlike.desc;

  const regexes = [
    '(?<type>Melee|Ranged) Weapon Attack: (?<toHit>.+?) to hit(?:, (?:reach|range) (?<range>\\d+|(?:\\d+\\/\\d+)) ft\\.)?(?:, (?<targets>\\w+) targets?\\.)?',
    '(?:(?:(?:\\d+ )?\\((?<damage>.+?)\\))|(?<staticDamage>\\d+)) (?<damageType>\\w+) damage\\.?(?: plus (?:(?:(?:\\d+ )? \\((?<damage1>.+?)\\))|(?<staticDamage1>\\d+)) (?<damage1Type>\\w+) damage\\.?)?',
    'makes? a DC (?<dc>.+?) (?<attribute>\\w+) saving throw, taking \\d+ \\((?<damage>.+?)\\) (?<damageType>\\w+) damage on a failed save(?<halfOnSuccess>, or half as much damage on a successful one)?\\.',
    'must succeed on a DC (?<dc>.+?) (?<attribute>\\w+) saving throw',
  ];

  let pos = 0;
  const blocks = [];
  while (pos < desc.length) {
    const nextMatch = regexes
      // eslint-disable-next-line no-loop-func
      .map(r => desc.substr(pos).match(r))
      .filter(Boolean)
      .sort((a, b) => a!.index! - b!.index!)[0];

    if (!nextMatch) {
      break;
    }

    if (desc.substr(pos, nextMatch.index)) {
      blocks.push(desc.substr(pos, nextMatch.index));
    }

    let dragHandler;
    if (nextMatch.groups) {
      if (nextMatch.groups.toHit) {
        dragHandler = handleDragAttack(
          nextMatch.groups.type,
          +nextMatch.groups.toHit,
          nextMatch.groups.range,
          nextMatch.groups.targets,
        );
      } else if (nextMatch.groups.attribute) {
        dragHandler = handleDragSave(
          nextMatch.groups.attribute,
          +nextMatch.groups.dc,
          nextMatch.groups.damage,
          nextMatch.groups.damageType,
          !!nextMatch.groups.halfOnSuccess,
        );
      } else if (nextMatch.groups.damage || nextMatch.groups.staticDamage) {
        dragHandler = handleDragDamage(
          nextMatch.groups.damage || nextMatch.groups.staticDamage,
          nextMatch.groups.damageType,
          nextMatch.groups.damage1 || nextMatch.groups.staticDamage1,
          nextMatch.groups.damage1Type,
        );
      }
    }

    blocks.push(
      <span
        key={nextMatch[0]}
        draggable
        onDragStart={dragHandler}
        className={classes.action}
      >
        {nextMatch[0]}
      </span>,
    );

    pos += nextMatch.index! + nextMatch[0].length;
  }

  if (pos < desc.length) {
    blocks.push(desc.substr(pos));
  }

  return <>{blocks}</>;
}
