import * as Babylon from 'babylonjs';
import * as React from 'react';

if (process.env.NODE_ENV === 'development') {
  require('babylonjs-inspector');
}

const ResizeObserver = (window as any).ResizeObserver;

export const UILayer = 0x10000000;

export default function useScene(
  canvasRef: React.RefObject<HTMLCanvasElement | null>,
) {
  const [scene, updateScene] = React.useState(null as Babylon.Scene | null);
  const cameraVelocity = React.useRef(Babylon.Vector3.Zero());
  const updateCameraZoom = React.useCallback(
    (multiplier: number = 1) => {
      if (scene) {
        const [camera] = scene.activeCameras;
        const {
          orthoTop,
          orthoLeft,
          orthoRight,
          orthoBottom,
        } = getOrthographicValues(
          camera.orthoTop! * multiplier,
          scene.getEngine().getRenderWidth() /
            scene.getEngine().getRenderHeight(),
        );
        camera.orthoTop = orthoTop;
        camera.orthoLeft = orthoLeft;
        camera.orthoRight = orthoRight;
        camera.orthoBottom = orthoBottom;
      }
    },
    [scene],
  );
  const updateCameraPosition = React.useCallback(
    (x: number, y: number) => {
      if (scene) {
        const [camera] = scene.activeCameras;
        camera.position.set(x, 20, y);
      }
    },
    [scene],
  );
  const moveCameraPosition = React.useCallback(
    (x: number, y: number) => {
      if (scene) {
        const [camera] = scene.activeCameras;
        camera.position.set(
          camera.position.x + x * (camera.orthoTop! / 250),
          20,
          camera.position.z + y * (camera.orthoTop! / 250),
        );
      }
    },
    [scene],
  );
  const updateCameraVelocity = React.useCallback(
    (x: number, y: number) => {
      if (scene) {
        const [camera] = scene.activeCameras;
        cameraVelocity.current.set(
          x * camera.orthoTop!,
          0,
          y * camera.orthoTop!,
        );
      }
    },
    [scene, cameraVelocity],
  );
  const updateCameraFocus = React.useCallback(
    (boundingBox: Babylon.BoundingBox, noZoom = false) => {
      if (scene) {
        const [camera] = scene.activeCameras;
        camera.animations.push(
          createAnimation(
            'position',
            camera.position,
            boundingBox.minimum.add(
              boundingBox.extendSize.maximizeInPlace(
                new Babylon.Vector3(
                  Number.MIN_SAFE_INTEGER,
                  20,
                  Number.MIN_SAFE_INTEGER,
                ),
              ),
            ),
            Babylon.Animation.ANIMATIONTYPE_VECTOR3,
          ),
        );
        if (!noZoom) {
          const ratio =
            scene.getEngine().getRenderWidth() /
            scene.getEngine().getRenderHeight();
          const { x: targetWidth, z: targetHeight } = boundingBox.extendSize;
          const zoom =
            targetHeight * ratio > targetWidth
              ? targetHeight
              : ((targetWidth / targetHeight) * ratio) / 2;
          const {
            orthoTop,
            orthoLeft,
            orthoRight,
            orthoBottom,
          } = getOrthographicValues(zoom, ratio);
          camera.animations.push(
            createAnimation('orthoTop', camera.orthoTop, orthoTop),
            createAnimation('orthoLeft', camera.orthoLeft, orthoLeft),
            createAnimation('orthoRight', camera.orthoRight, orthoRight),
            createAnimation('orthoBottom', camera.orthoBottom, orthoBottom),
          );
        }
        scene.beginAnimation(camera, 0, 40, false, 1);
      }
    },
    [scene],
  );

  React.useEffect(() => {
    if (canvasRef.current) {
      const engine = new Babylon.Engine(canvasRef.current, true, {}, false);
      const scn = new Babylon.Scene(engine);

      if (process.env.NODE_ENV === 'development') {
        (window as any).__scene = scn;
      }

      const camera = new Babylon.FreeCamera(
        'default camera',
        new Babylon.Vector3(0, 20, 0),
        scn,
      );
      camera.mode = Babylon.Camera.ORTHOGRAPHIC_CAMERA;
      const {
        orthoTop,
        orthoLeft,
        orthoRight,
        orthoBottom,
      } = getOrthographicValues(
        10,
        scn.getEngine().getRenderWidth() / scn.getEngine().getRenderHeight(),
      );
      camera.orthoTop = orthoTop;
      camera.orthoLeft = orthoLeft;
      camera.orthoRight = orthoRight;
      camera.orthoBottom = orthoBottom;
      camera.setTarget(Babylon.Vector3.Zero());
      camera.rotation.y = 0;

      const uiCamera = new Babylon.FreeCamera(
        'ui camera',
        new Babylon.Vector3(0, 0, 0),
        scn,
      );
      uiCamera.mode = Babylon.Camera.ORTHOGRAPHIC_CAMERA;
      uiCamera.setTarget(Babylon.Vector3.Zero());
      uiCamera.rotation.y = 0;
      uiCamera.layerMask = UILayer;
      uiCamera.parent = camera;

      scn.activeCameras = [camera, uiCamera];

      camera.position.set(0, 20, 0);

      // setup engine render loop
      engine.runRenderLoop(() => {
        // copy settings from default camera
        uiCamera.orthoTop = camera.orthoTop;
        uiCamera.orthoLeft = camera.orthoLeft;
        uiCamera.orthoRight = camera.orthoRight;
        uiCamera.orthoBottom = camera.orthoBottom;

        scn.render();

        camera.position.addInPlace(cameraVelocity.current);
        cameraVelocity.current.scaleInPlace(0.7);
      });

      updateScene(scn);

      return () => {
        scn.dispose();
        engine.dispose();
      };
    }
    return;
  }, [canvasRef]);
  React.useEffect(() => {
    // update canvas size
    let observer: any;
    if (scene && canvasRef.current && canvasRef.current.parentElement) {
      const {
        width,
        height,
      } = canvasRef.current.parentElement.getBoundingClientRect();
      canvasRef.current.width = width;
      canvasRef.current.height = height;

      if (ResizeObserver) {
        observer = new ResizeObserver(([{ contentRect }]: any) => {
          if (canvasRef.current) {
            canvasRef.current.width = contentRect.width;
            canvasRef.current.height = contentRect.height;
          }
          scene.getEngine().resize();
          updateCameraZoom();
        });
        observer.observe(canvasRef.current.parentElement);
      }
    }
    return () => observer && observer.disconnect();
  }, [canvasRef, scene, updateCameraZoom]);

  return {
    scene,
    updateCameraZoom,
    updateCameraPosition,
    updateCameraVelocity,
    updateCameraFocus,
    moveCameraPosition,
  };
}

function getOrthographicValues(height: number, whRatio: number) {
  return {
    orthoTop: height,
    orthoRight: height * whRatio,
    orthoBottom: -height,
    orthoLeft: -height * whRatio,
  };
}

const cubicEaseInOut = new Babylon.CubicEase();
cubicEaseInOut.setEasingMode(Babylon.EasingFunction.EASINGMODE_EASEINOUT);

function createAnimation(
  propertyName: string,
  from: any,
  to: any,
  animType = Babylon.Animation.ANIMATIONTYPE_FLOAT,
) {
  const anim = new Babylon.Animation(
    'anim.' + propertyName,
    propertyName,
    60,
    animType,
  );
  anim.setKeys([{ frame: 0, value: from }, { frame: 40, value: to }]);
  anim.setEasingFunction(cubicEaseInOut);
  return anim;
}
