import { autorun, comparer } from 'mobx';
import * as React from 'react';

export default function useObservable<T>(
  fn: () => T,
  inputs: any[] = [],
  compare = comparer.structural,
) {
  const [val, updateVal] = React.useState(fn());
  const ref = React.useRef(val);
  React.useEffect(
    () =>
      autorun(() => {
        const v = fn();
        if (!compare(v, ref.current)) {
          ref.current = v;
          updateVal(v);
        }
      }, {}),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    inputs,
  );
  return val;
}
