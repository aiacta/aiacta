import React, { createContext, useContext, useEffect, useMemo } from 'react';
import registerEventHandler from '../../utils/registerEventHandler';

interface KeyCombo {
  keys: string[];
  callback: (evt: KeyboardEvent) => void;
}

const KeyContext = createContext({
  window: null as null | Window,
  keyCombos: new Map<string[], (evt: KeyboardEvent) => void>(),
});

export const KeyboardHost = ({
  window,
  children,
}: {
  window: Window;
  children: React.ReactNode;
}) => {
  const value = useMemo(
    () => ({
      window,
      keyCombos: new Map<string[], (evt: KeyboardEvent) => void>(),
    }),
    [window],
  );

  useEffect(() => {
    if (value.window) {
      const pressedKeys = new Set<string>();
      const disposer = [
        registerEventHandler(value.window, 'keydown', evt => {
          if (evt.key) {
            pressedKeys.add(evt.key.toLowerCase());
            Array.from(value.keyCombos.keys())
              .filter(keys => keys.every(k => pressedKeys.has(k)))
              .forEach(c => value.keyCombos.get(c)!(evt));
          }
        }),
        registerEventHandler(value.window, 'keyup', evt => {
          if (evt.key) {
            if (evt.key === 'Meta') {
              // clear all down-keys
              pressedKeys.clear();
            } else {
              pressedKeys.delete(evt.key.toLowerCase());
            }
          }
        }),
      ];
      return () => disposer.forEach(d => d());
    }
  }, [value.window, value.keyCombos]);

  return <KeyContext.Provider value={value}>{children}</KeyContext.Provider>;
};

export default function useKeyCombos(...combos: KeyCombo[]) {
  const { keyCombos } = useContext(KeyContext);
  useEffect(() => {
    const disposer = combos.map(({ keys, callback }) => {
      keyCombos.set(keys, callback);
      return () => {
        keyCombos.delete(keys);
      };
    });
    return () => disposer.forEach(d => d());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [...combos.map(c => c.keys.join('+')), keyCombos]);
}
