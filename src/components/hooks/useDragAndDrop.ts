import { useCallback, useMemo } from 'react';

export default function useDragAndDrop<TArgs extends any[]>(
  options: {
    [key: string]: {
      allow: (...args: TArgs) => string | boolean;
      onDrop: (data: any, ...args: TArgs) => void;
    };
  },
  process: (evt: React.DragEvent<HTMLElement>) => TArgs = () =>
    ([] as any) as TArgs,
) {
  const onDragOver = useCallback(
    (evt: React.DragEvent<HTMLElement>) => {
      const dropType = evt.dataTransfer.types[0];

      const dropEffect =
        dropType in options && options[dropType].allow(...process(evt));

      evt.dataTransfer.dropEffect =
        dropEffect === false
          ? 'none'
          : dropEffect === true
          ? 'link'
          : dropEffect;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [Object.keys(options).join('+'), process],
  );
  const onDrop = useCallback(
    (evt: React.DragEvent<HTMLElement>) => {
      Object.keys(options)
        .filter(key => !!evt.dataTransfer.getData(key))
        .forEach(key => {
          let data: any = evt.dataTransfer.getData(key);
          try {
            data = JSON.parse(data);
          } catch (err) {
            //
          }
          options[key].onDrop(data, ...process(evt));
        });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [Object.keys(options).join('+'), process],
  );
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const accepts = useMemo(() => Object.keys(options), Object.keys(options));
  return { onDragOver, onDrop, accepts };
}
