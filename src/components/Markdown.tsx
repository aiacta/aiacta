import {
  createStyles,
  List,
  ListItem,
  ListItemText,
  MuiThemeProvider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import marksy from 'marksy/jsx';
import * as React from 'react';
import { markdownTheme } from '../themes';

const styles = (theme: Theme) => createStyles({});

export interface MarkdownProps extends WithStyles<typeof styles> {
  children: string;
}

export default withStyles(styles)(Markdown);

function Markdown({ children }: MarkdownProps) {
  const compiled = React.useMemo(() => compile(children), [children]);
  return (
    <MuiThemeProvider theme={markdownTheme}>
      {compiled as JSX.Element}
    </MuiThemeProvider>
  );
}

const defaultElements: marksy.DefinedElements = {
  p: ({ children }) => <Typography>{children}</Typography>,
  h1: ({ children }) => <Typography variant="h1">{children}</Typography>,
  h2: ({ children }) => <Typography variant="h2">{children}</Typography>,
  h3: ({ children }) => <Typography variant="h3">{children}</Typography>,
  h4: ({ children }) => <Typography variant="h4">{children}</Typography>,
  table: ({ children }) => <Table padding="dense">{children}</Table>,
  thead: ({ children }) => <TableHead>{children}</TableHead>,
  tbody: ({ children }) => <TableBody>{children}</TableBody>,
  tr: ({ children }) => <TableRow style={{ height: 30 }}>{children}</TableRow>,
  th: ({ children }) => <TableCell>{children}</TableCell>,
  td: ({ children }) => <TableCell>{children}</TableCell>,
  ul: ({ children }) => <List dense>{children}</List>,
  ol: ({ children }) => <List dense>{children}</List>,
  li: ({ children }) => (
    <ListItem>
      <ListItemText primary={children} />
    </ListItem>
  ),
};

function compile(
  source: string,
  components?: {
    [key: string]: React.ComponentType<any>;
  },
) {
  const c = marksy({
    createElement: React.createElement,
    elements: defaultElements,
    components,
  });
  return c(source).tree;
}
