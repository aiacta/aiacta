import {
  Avatar,
  Badge,
  Button,
  Collapse,
  createStyles,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Observer } from 'mobx-react';
import * as React from 'react';
import defaultPog from '../assets/baseline_account_circle_white_48dp.png';
import { CharacterModel, MonsterModel, TokenModel } from '../stores/models';
import { combatTracker } from '../stores/modules/dataStores';
import { myself } from '../stores/modules/gameData';
import { focusOn } from '../stores/modules/uiView';
import classNames from '../utils/classNames';
import memoize from '../utils/memoize';
import ActionableContent from './ActionableContent';

const styles = (theme: Theme) =>
  createStyles({
    incapacitatedActor: {
      background: `repeating-linear-gradient(
        -40deg,
        #ddd,
        #ddd 10px,
        #eee 10px,
        #eee 20px
      )`,
    },
    dyingActor: {
      background: `repeating-linear-gradient(
        -40deg,
        #933,
        #933 10px,
        #a66 10px,
        #a66 20px
      )`,
    },
    deadActor: {
      background: `repeating-linear-gradient(
      -40deg,
      #666,
      #666 10px,
      #777 10px,
      #777 20px
    )`,
    },
    mainLine: {
      marginLeft: theme.spacing.unit,
      width: '100%',
      display: 'grid',
      gridAutoColumns: '1fr',
      gridTemplateAreas: `
      'name armor health'
      'name armor health'`,
    },
    avatar: {
      cursor: 'zoom-in',
    },
    name: {
      gridArea: 'name',
    },
    armor: {
      gridArea: 'armor',
    },
    health: {
      gridArea: 'health',
    },
    healthBar: {
      position: 'relative',
      height: theme.spacing.unit * 2,
      margin: 2,
      background: 'red',
      '&>div': {
        position: 'absolute',
        height: '100%',
        background: 'green',
      },
    },
    initiative: {
      gridArea: 'init',
    },
    details: {
      padding: theme.spacing.unit,
      display: 'grid',
    },
    action: {
      '& > span': {
        display: 'inline',
      },
    },
    controls: {
      position: 'sticky',
      background: theme.palette.background.default,
      bottom: 0,
      zIndex: 1,
      display: 'grid',
      gridGap: `${theme.spacing.unit}px`,
      gridAutoFlow: 'column',
      gridAutoColumns: 'max-content',
      alignItems: 'center',
      paddingTop: theme.spacing.unit,
    },
  });

export interface CombatTrackerProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(CombatTracker);

function CombatTracker({ classes }: CombatTrackerProps) {
  const cb = combatTracker.singleton;

  const [toggledKeys, dispatchToggle] = React.useReducer(
    (state: string[], key: string) =>
      !state.includes(key) ? [...state, key] : state.filter(k => k !== key),
    [],
  );

  const handleClickActor = React.useCallback(
    memoize((key: string) => () => {
      dispatchToggle(key);
    }),
    [],
  );

  const handleClickAvatar = React.useCallback(
    memoize((token: TokenModel) => (evt: React.MouseEvent) => {
      evt.stopPropagation();
      focusOn(token);
    }),
    [],
  );

  const handleClickNextActor = React.useCallback(() => {
    if (cb.actors.length > 0) {
      cb.currentActor =
        cb.actors[(cb.actors.indexOf(cb.currentActor!) + 1) % cb.actors.length];
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <List>
        <Observer>
          {() =>
            cb.actors
              .filter(a => a.reference.isVisible)
              .sort((a, b) => b.initiative - a.initiative)
              .map(actor => (
                <React.Fragment key={actor.key}>
                  <ListItem
                    button
                    onClick={handleClickActor(actor.key)}
                    className={classNames(
                      actor.reference.health.current === 0 &&
                        (actor.reference.represents instanceof CharacterModel &&
                          actor.reference.represents.deathSaves.successes ===
                            3) &&
                        classes.incapacitatedActor,
                      actor.reference.health.current === 0 &&
                        (actor.reference.represents instanceof CharacterModel &&
                          actor.reference.represents.deathSaves.successes < 3 &&
                          actor.reference.represents.deathSaves.failures < 3) &&
                        classes.dyingActor,
                      actor.reference.health.current === 0 &&
                        (actor.reference.represents instanceof MonsterModel ||
                          actor.reference.represents.deathSaves.failures ===
                            3) &&
                        classes.deadActor,
                    )}
                  >
                    <Badge color="primary" badgeContent={actor.initiative}>
                      <ListItemAvatar>
                        <Avatar
                          onClick={handleClickAvatar(actor.reference)}
                          src={actor.reference.imageUrl || defaultPog}
                          className={classes.avatar}
                        />
                      </ListItemAvatar>
                    </Badge>
                    <div className={classes.mainLine}>
                      <Typography
                        component="div"
                        className={classes.name}
                        variant="h6"
                        noWrap
                      >
                        {actor.reference.name}
                      </Typography>
                      <Typography
                        component="div"
                        className={classes.health}
                        variant="body1"
                        noWrap
                      >
                        Health:{' '}
                        {`${actor.reference.health.current}${
                          actor.reference.health.temporary > 0
                            ? ` + ${actor.reference.health.temporary}`
                            : ''
                        } / ${actor.reference.healthMaximum}`}
                        <div className={classes.healthBar}>
                          <div
                            style={{
                              width: `${(actor.reference.health.current /
                                actor.reference.healthMaximum) *
                                100}%`,
                            }}
                          />
                          {actor.reference.health.temporary > 0 && (
                            <div
                              style={{
                                right: 0,
                                top: -2,
                                height: 'calc(100% + 4px)',
                                padding: 2,
                                background: 'cornflowerblue',
                                borderTop: '2px solid cornflowerblue',
                                borderBottom: '2px solid cornflowerblue',
                                borderRight: '2px solid cornflowerblue',
                                fontSize: '0.8em',
                                lineHeight: '1.2em',
                              }}
                            >
                              +{actor.reference.health.temporary}
                            </div>
                          )}
                        </div>
                      </Typography>
                    </div>
                  </ListItem>
                  <Collapse
                    in={
                      (cb.currentActor && actor.key === cb.currentActor.key) ||
                      toggledKeys.includes(actor.key)
                    }
                  >
                    <div className={classes.details}>
                      <Typography variant="overline">Actions</Typography>
                      {actor.reference.actions.map(action => (
                        <div key={action.key} className={classes.action}>
                          <Typography
                            component="span"
                            variant="subtitle2"
                            noWrap
                          >
                            {action.name}.{' '}
                          </Typography>
                          <Typography component="span">
                            <ActionableContent
                              actionlike={action}
                              instigatorId={actor.reference.represents._id}
                            />
                          </Typography>
                        </div>
                      ))}
                    </div>
                  </Collapse>
                  <Divider />
                </React.Fragment>
              ))
          }
        </Observer>
      </List>
      <Observer>
        {() => (
          <div className={classes.controls}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleClickNextActor}
              disabled={
                cb.currentActor
                  ? (cb.currentActor.reference.represents instanceof
                      CharacterModel &&
                      cb.currentActor.reference.represents.owner !==
                        myself()) ||
                    !myself().isDungeonMaster
                  : false
              }
            >
              Next Actor
            </Button>
            <Typography variant="overline">Turn {cb.round}</Typography>
          </div>
        )}
      </Observer>
    </>
  );
}
