import {
  Checkbox,
  createStyles,
  FormControlLabel,
  FormGroup,
  Radio,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { LanguageChoice, SkillChoice } from '../../stores/models';
import capitalizeKeyword from '../../utils/capitalizeKeyword';

const styles = (theme: Theme) => createStyles({});

export interface ChooseDistinctProps extends WithStyles<typeof styles> {
  choice: SkillChoice | LanguageChoice;
  currentChoice: any;
  update: (d: (current: any) => any) => void;
  options: string[];
}

export default withStyles(styles)(ChooseDistinct);

function ChooseDistinct({
  choice,
  currentChoice,
  update,
  options,
}: ChooseDistinctProps) {
  return (
    <FormGroup>
      {(choice.includes || options)
        .filter(s => !choice.excludes || !choice.excludes.includes(s as any))
        .map(skill => {
          return (
            <FormControlLabel
              key={skill}
              label={capitalizeKeyword(skill)}
              control={
                choice.num > 1 ? (
                  <Checkbox
                    checked={!!currentChoice && currentChoice.includes(skill)}
                    onChange={evt => {
                      const skill = evt.target.value;
                      const checked = evt.target.checked;
                      update(c =>
                        [...(c || []), skill].filter(
                          v => v !== skill || checked,
                        ),
                      );
                    }}
                    disabled={
                      currentChoice &&
                      currentChoice.length >= choice.num &&
                      !(currentChoice && currentChoice.includes(skill))
                    }
                    value={skill}
                    color="primary"
                  />
                ) : (
                  <Radio
                    checked={!!currentChoice && currentChoice.includes(skill)}
                    onChange={evt => {
                      const skill = evt.target.value;
                      update(() => skill);
                    }}
                    value={skill}
                    color="primary"
                  />
                )
              }
            />
          );
        })}
    </FormGroup>
  );
}
