import {
  createStyles,
  FormControlLabel,
  FormGroup,
  Radio,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { SubraceChoice } from '../../stores/models';
import { subraces } from '../../stores/modules/dataStores';

const styles = (theme: Theme) => createStyles({});

export interface ChooseSubraceProps extends WithStyles<typeof styles> {
  choice: SubraceChoice;
  currentChoice: any;
  update: (d: (current: any) => any) => void;
}

export default withStyles(styles)(ChooseSubrace);

function ChooseSubrace({ choice, currentChoice, update }: ChooseSubraceProps) {
  return (
    <FormGroup>
      {subraces
        .filter(m => m.race._id === choice.race)
        .map(spell => {
          return (
            <FormControlLabel
              key={spell._id}
              label={spell.name}
              control={
                <Radio
                  checked={currentChoice === spell._id}
                  onChange={evt => {
                    const attr = evt.target.value;
                    update(() => attr);
                  }}
                  value={spell._id}
                  color="primary"
                />
              }
            />
          );
        })}
    </FormGroup>
  );
}
