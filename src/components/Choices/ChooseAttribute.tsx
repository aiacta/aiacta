import {
  Checkbox,
  createStyles,
  FormControlLabel,
  FormGroup,
  Radio,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { AttributeChoice, attributes } from '../../stores/models';
import capitalizeKeyword from '../../utils/capitalizeKeyword';
import NumericStepper from '../NumericStepper';

const styles = (theme: Theme) => createStyles({});

export interface ChooseAttributeProps extends WithStyles<typeof styles> {
  choice: AttributeChoice;
  currentChoice: any;
  update: (d: (current: any) => any) => void;
}

export default withStyles(styles)(ChooseAttribute);

function ChooseAttribute({
  choice,
  currentChoice,
  update,
}: ChooseAttributeProps) {
  return (
    <FormGroup>
      {(choice.includes || attributes)
        .filter(a => !choice.excludes || !choice.excludes.includes(a))
        .map(attr => {
          return (
            <FormControlLabel
              key={attr}
              label={capitalizeKeyword(attr)}
              control={
                choice.points > 1 && !choice.distinct ? (
                  <NumericStepper
                    value={(currentChoice && currentChoice[attr]) || 0}
                    min={0}
                    max={
                      +Object.values(currentChoice || {}).reduce(
                        (acc, cur) => +acc - +cur,
                        choice.points,
                      ) + ((currentChoice && currentChoice[attr]) || 0)
                    }
                    onChange={val => {
                      update(c => ({
                        ...c,
                        [attr]: val,
                      }));
                    }}
                  />
                ) : choice.distinct ? (
                  <Checkbox
                    checked={!!currentChoice && currentChoice.includes(attr)}
                    onChange={evt => {
                      const attr = evt.target.value;
                      const checked = evt.target.checked;
                      update(c =>
                        [...(c || []), attr].filter(v => v !== attr || checked),
                      );
                    }}
                    disabled={
                      choice.points > 1 &&
                      currentChoice &&
                      currentChoice.length >= choice.points &&
                      !(currentChoice && currentChoice.includes(attr))
                    }
                    value={attr}
                    color="primary"
                  />
                ) : (
                  <Radio
                    checked={!!currentChoice && currentChoice.includes(attr)}
                    onChange={evt => {
                      const attr = evt.target.value;
                      update(() => attr);
                    }}
                    value={attr}
                    color="primary"
                  />
                )
              }
            />
          );
        })}
    </FormGroup>
  );
}
