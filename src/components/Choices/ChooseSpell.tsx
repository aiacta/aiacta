import {
  createStyles,
  FormControlLabel,
  FormGroup,
  Radio,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { SpellChoice } from '../../stores/models';
import { spells } from '../../stores/modules/dataStores';

const styles = (theme: Theme) => createStyles({});

export interface ChooseSpellProps extends WithStyles<typeof styles> {
  choice: SpellChoice;
  currentChoice: any;
  update: (d: (current: any) => any) => void;
}

export default withStyles(styles)(ChooseSpell);

function ChooseSpell({ choice, currentChoice, update }: ChooseSpellProps) {
  return (
    <FormGroup>
      {spells
        .filter(
          s =>
            choice.level === s.level &&
            (!choice.list ||
              !!s.classes.find(cls => cls._id === `class-${choice.list}`)),
        )
        .map(spell => {
          return (
            <FormControlLabel
              key={spell._id}
              label={spell.name}
              control={
                <Radio
                  checked={currentChoice === spell._id}
                  onChange={evt => {
                    const attr = evt.target.value;
                    update(() => attr);
                  }}
                  value={spell._id}
                  color="primary"
                />
              }
            />
          );
        })}
    </FormGroup>
  );
}
