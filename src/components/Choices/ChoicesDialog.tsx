import {
  Button,
  createStyles,
  Dialog,
  DialogActions,
  DialogContent,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { AnyChoice, skills } from '../../stores/models';
import { languages } from '../../stores/modules/dataStores';
import {
  applyChoices,
  cancelPrompt,
  useChoicesToMake,
} from '../../stores/modules/uiPrompt';
import { useMemoize } from '../../utils/memoize';
import useObservable from '../hooks/useObservable';
import Markdown from '../Markdown';
import ChooseAttribute from './ChooseAttribute';
import ChooseCustom from './ChooseCustom';
import ChooseDistinct from './ChooseDistinct';
import ChooseSpell from './ChooseSpell';
import ChooseSubclass from './ChooseSubclass';
import ChooseSubrace from './ChooseSubrace';

const styles = (theme: Theme) => createStyles({});

export interface ChoicesDialogProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(ChoicesDialog);

function ChoicesDialog({ classes }: ChoicesDialogProps) {
  const choicesToMake = useChoicesToMake();
  const languageNames = useObservable(() => languages.all().map(l => l.name));

  const [choicesMade, updateChoicesMade] = React.useState({} as {
    [key: string]: any;
  });
  const handleApplyChoice = React.useCallback(() => {
    applyChoices(choicesMade);
  }, [choicesMade]);

  React.useEffect(() => {
    updateChoicesMade({});
  }, [choicesToMake]);

  const handleChoice = useMemoize(
    (key: string) => (updater: (v: any) => any) => {
      updateChoicesMade(c => ({ ...c, [key]: updater(c[key]) }));
    },
  );

  return (
    <>
      <Dialog open={!!choicesToMake}>
        {choicesToMake ? (
          <>
            <DialogContent>
              <Markdown>
                {`# ${choicesToMake.name}

${choicesToMake.desc}`}
              </Markdown>
              {Object.entries(choicesToMake.choices).map(([key, value]) =>
                renderChoice(choicesToMake.key, key, value),
              )}
            </DialogContent>
            <DialogActions>
              <Button onClick={cancelPrompt} color="secondary">
                Abort
              </Button>
              <Button
                onClick={handleApplyChoice}
                color="primary"
                disabled={
                  Object.keys(choicesMade).length <
                  Object.keys(choicesToMake.choices).length
                }
              >
                Apply
              </Button>
            </DialogActions>
          </>
        ) : (
          'No choice to make'
        )}
      </Dialog>
    </>
  );

  function renderChoice(sourceKey: string, key: string, choice: AnyChoice) {
    switch (choice.type) {
      case 'custom':
        return (
          <ChooseCustom
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
          />
        );
      case 'attribute':
        return (
          <ChooseAttribute
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
          />
        );
      case 'subclass':
        return (
          <ChooseSubclass
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
          />
        );
      case 'subrace':
        return (
          <ChooseSubrace
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
          />
        );
      case 'spell':
        return (
          <ChooseSpell
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
          />
        );
      case 'skill':
      case 'language':
        return (
          <ChooseDistinct
            key={key}
            choice={choice}
            currentChoice={choicesMade[`${sourceKey}-${key}`]}
            update={handleChoice(`${sourceKey}-${key}`)}
            options={choice.type === 'skill' ? skills : languageNames}
          />
        );
      default:
        return 'Oops';
    }
  }
}
