import {
  createStyles,
  FormControlLabel,
  FormGroup,
  Radio,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { SubclassChoice } from '../../stores/models';
import { subclasses } from '../../stores/modules/dataStores';

const styles = (theme: Theme) => createStyles({});

export interface ChooseSubclassProps extends WithStyles<typeof styles> {
  choice: SubclassChoice;
  currentChoice: any;
  update: (d: (current: any) => any) => void;
}

export default withStyles(styles)(ChooseSubclass);

function ChooseSubclass({
  choice,
  currentChoice,
  update,
}: ChooseSubclassProps) {
  return (
    <FormGroup>
      {subclasses
        .filter(m => m.class._id === choice.class)
        .map(spell => {
          return (
            <FormControlLabel
              key={spell._id}
              label={spell.name}
              control={
                <Radio
                  checked={currentChoice === spell._id}
                  onChange={evt => {
                    const attr = evt.target.value;
                    update(() => attr);
                  }}
                  value={spell._id}
                  color="primary"
                />
              }
            />
          );
        })}
    </FormGroup>
  );
}
