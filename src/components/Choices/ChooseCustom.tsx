import { TableBody } from '@material-ui/core';
import {
  createStyles,
  FormControlLabel,
  Radio,
  Table,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { CustomChoice } from '../../stores/models';
import { useMemoize } from '../../utils/memoize';

const styles = (theme: Theme) =>
  createStyles({
    tableRow: {
      cursor: 'pointer',
    },
  });

export interface ChooseCustomProps extends WithStyles<typeof styles> {
  choice: CustomChoice;
  currentChoice: string;
  update: (d: (current: any) => any) => void;
}

export default withStyles(styles)(ChooseCustom);

function ChooseCustom({
  choice,
  currentChoice,
  update,
  classes,
}: ChooseCustomProps) {
  const handleChangeSelect = React.useCallback(
    (evt: React.ChangeEvent<any>) => {
      const val = evt.target.value;
      update(() => val);
    },
    [update],
  );
  const handleChangeTo = useMemoize((val: any) => () => {
    update(() => val);
  });
  const rows = choice.options
    .trim()
    .split('\n')
    .map(l =>
      l
        .slice(1, -1)
        .split('|')
        .map(d => d.trim()),
    );
  return (
    <Typography component="div">
      <Table padding="dense">
        <TableHead>
          <TableRow>
            {rows[0].map(data => (
              <TableCell key={data}>{data}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.slice(2).map((columns, row) => (
            <TableRow
              hover
              key={row}
              role="radio"
              selected={currentChoice === columns[0]}
              onClick={handleChangeTo(columns[0])}
              className={classes.tableRow}
            >
              {columns.map((data, column) => (
                <TableCell key={column}>
                  {column === 0 ? (
                    <FormControlLabel
                      key={data}
                      name={`choice-${data}`}
                      value={data}
                      onChange={handleChangeSelect}
                      checked={currentChoice === data}
                      control={<Radio />}
                      label={data}
                    />
                  ) : (
                    data
                  )}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Typography>
  );
}
