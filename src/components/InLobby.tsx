import {
  AppBar,
  Badge,
  Button,
  createStyles,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Drawer,
  FormControl,
  IconButton,
  Input,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Theme,
  Toolbar,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import { Dashboard, Notifications } from '@material-ui/icons';
import * as React from 'react';
import {
  createGame,
  importGame,
  joinGame,
  localGames,
} from '../stores/modules/gameData';
import classNames from '../utils/classNames';
import { useMemoize } from '../utils/memoize';
import useObservable from './hooks/useObservable';

const drawerWidth = 240;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    toolbar: {
      paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar,
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginLeft: 12,
      marginRight: 36,
    },
    menuButtonHidden: {
      display: 'none',
    },
    title: {
      flexGrow: 1,
    },
    drawerPaper: {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerPaperClose: {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing.unit * 7,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing.unit * 9,
      },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
      display: 'flex',
      flexDirection: 'column',
      flexGrow: 1,
      height: '100vh',
      overflow: 'auto',
    },
    chartContainer: {
      marginLeft: -22,
    },
    tableContainer: {
      height: 320,
    },
    gamelist: {
      display: 'flex',
      justifyContent: 'center',
    },
  });

export interface InLobbyProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(InLobby);

function InLobby({ classes }: InLobbyProps) {
  const [dialog, updateDialog] = React.useState('');
  const [inputs, updateInputs] = React.useState({
    gameLink: '',
    gameName: '',
  });

  const handleOpenDialog = useMemoize((key: string) => () => updateDialog(key));
  const handleDismiss = React.useCallback(() => updateDialog(''), []);

  const handleInputChange = useMemoize(
    (prop: keyof typeof inputs) => (
      evt: React.ChangeEvent<HTMLInputElement>,
    ) => {
      const target = evt.target;
      if (typeof inputs[prop] === 'number') {
        updateInputs(s => ({ ...s, [prop]: target.valueAsNumber }));
      } else {
        updateInputs(s => ({ ...s, [prop]: target.value }));
      }
    },
  );

  const handleJoinGame = React.useCallback(
    (evt: React.SyntheticEvent) => {
      evt.preventDefault();
      joinGame(inputs.gameLink, inputs.gameName);
    },
    [inputs.gameLink, inputs.gameName],
  );

  const handleCreateGame = React.useCallback(
    (evt: React.SyntheticEvent) => {
      evt.preventDefault();
      createGame(inputs.gameName);
    },
    [inputs.gameName],
  );

  const handleImportGame = React.useCallback(() => {
    importGame(inputs.gameName, 'DATA');
  }, [inputs.gameName]);

  const handleLoadGame = useMemoize(
    (game: { id: string; name: string }) => () => {
      joinGame(game.id, game.name);
    },
    [],
  );

  const knownGames = useObservable(() => localGames());

  return (
    <>
      <div className={classes.root}>
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, classes.appBarShift)}
        >
          <Toolbar className={classes.toolbar}>
            <Typography
              variant="h6"
              color="inherit"
              noWrap
              className={classes.title}
            >
              Aiacta
            </Typography>
            <IconButton color="inherit">
              <Badge badgeContent={4} color="secondary">
                <Notifications />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(classes.drawerPaper),
          }}
          open
        >
          <div className={classes.toolbarIcon} />
          <Divider />
          <List>
            <ListItem button>
              <ListItemIcon>
                <Dashboard />
              </ListItemIcon>
              <ListItemText primary="Games" />
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <div className={classes.gamelist}>
            {knownGames.map(game => (
              <Button
                key={game.id}
                onClick={handleLoadGame(game)}
                color="primary"
                variant="outlined"
              >
                {game.name}
              </Button>
            ))}
          </div>
          <Button onClick={handleOpenDialog('joinGame')}>Join Game</Button>
          <Button onClick={handleOpenDialog('createGame')}>Create Game</Button>
          {/* <Button onClick={handleOpenDialog('importGame')}>Import Game</Button> */}
        </main>
        <Dialog open={dialog === 'joinGame'} onClose={handleDismiss}>
          <DialogTitle>Join a game</DialogTitle>
          <DialogContent>
            <Typography>
              To join a game, you have to obtain the game-link. A dungeon master
              of that game can access this link and give it to you. You can pick
              any name for this game.
            </Typography>
            <form onSubmit={handleJoinGame}>
              <FormControl>
                <InputLabel>Link</InputLabel>
                <Input
                  required
                  value={inputs.gameLink}
                  onChange={handleInputChange('gameLink')}
                />
              </FormControl>
              <FormControl>
                <InputLabel>Name</InputLabel>
                <Input
                  required
                  value={inputs.gameName}
                  onChange={handleInputChange('gameName')}
                />
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDismiss} color="primary">
              Cancel
            </Button>
            <Button onClick={handleJoinGame} color="primary">
              Join
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog open={dialog === 'createGame'} onClose={handleDismiss}>
          <DialogTitle>Create a game</DialogTitle>
          <DialogContent>
            <Typography>
              You can create a new game that will be saved on your browser
              storage.
            </Typography>
            <form onSubmit={handleCreateGame}>
              <FormControl>
                <InputLabel>Name</InputLabel>
                <Input
                  required
                  value={inputs.gameName}
                  onChange={handleInputChange('gameName')}
                />
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDismiss} color="primary">
              Cancel
            </Button>
            <Button onClick={handleCreateGame} color="primary">
              Create
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog open={dialog === 'importGame'} onClose={handleDismiss}>
          <DialogTitle>Import a game</DialogTitle>
          <div>NOT IMPLEMENTED</div>
          <DialogActions>
            <Button onClick={handleDismiss} color="primary">
              Dismiss
            </Button>
            <Button onClick={handleImportGame} color="primary">
              Import
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  );
}
