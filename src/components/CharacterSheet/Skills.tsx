import {
  Button,
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Star, StarBorder, StarHalf, StarTwoTone } from '@material-ui/icons';
import * as React from 'react';
import { CharacterModel, SkillProficiency } from '../../stores/models';
import capitalizeKeyword from '../../utils/capitalizeKeyword';
import classNames from '../../utils/classNames';
import { useMemoize } from '../../utils/memoize';
import EditableContent from '../EditableContent';
import useObservable from '../hooks/useObservable';
import RollableContent from '../RollableContent';
import BorderBox from './BorderBox';
import BorderCircle from './BorderCircle';
import SignedValue from './SignedValue';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      // maxWidth: 'fit-content',
    },
    proficiencyBonus: {
      display: 'grid',
      alignItems: 'center',
      gridTemplateColumns: '1fr max-content',
    },
    skills: {
      display: 'grid',
      gridTemplateColumns: 'minmax(120px, 1fr) 50px 50px',
      alignItems: 'center',
      justifyItems: 'center',
    },
    name: {
      justifySelf: 'flex-start',
    },
    editing: {
      cursor: 'pointer',
    },
    expertise: {
      fill: 'goldenrod',
    },
    other: {
      gridColumn: 'span 3',
    },
    sign: {
      position: 'absolute',
      transform: 'translateX(-100%)',
    },
  });

export interface SkillsProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Skills);

function Skills({ model, editing, classes, className }: SkillsProps) {
  const profBonus = useObservable(() => model.proficiencyBonus, [model]);
  const skills = useObservable(() => ({ ...model.skills }), [model]);
  const modifiers = useObservable(() => ({ ...model.modifiers }), [model]);

  const sortedSkills = Object.entries(skillMod).sort(([a], [b]) =>
    a < b ? -1 : 1,
  );

  const [showOtherSkills, updateShowOtherSkills] = React.useState(false);
  const toggleOtherSkills = React.useCallback(
    () => updateShowOtherSkills(s => !s),
    [],
  );

  const renderProf = useMemoize((prof: SkillProficiency, onChange) => {
    switch (prof) {
      case SkillProficiency.None:
        return (
          <StarBorder
            className={onChange && classes.editing}
            onClick={onChange && onChange.bind(null, SkillProficiency.Half)}
          />
        );
      case SkillProficiency.Half:
        return (
          <StarHalf
            className={onChange && classes.editing}
            onClick={
              onChange && onChange.bind(null, SkillProficiency.Proficient)
            }
          />
        );
      case SkillProficiency.Proficient:
        return (
          <Star
            className={onChange && classes.editing}
            onClick={
              onChange && onChange.bind(null, SkillProficiency.Expertise)
            }
          />
        );
      case SkillProficiency.Expertise:
        return (
          <StarTwoTone
            className={classNames(
              classes.expertise,
              onChange && classes.editing,
            )}
            onClick={onChange && onChange.bind(null, SkillProficiency.None)}
          />
        );
      default:
        break;
    }
  });

  return (
    <div className={classNames(classes.container, className)}>
      <BorderBox className={classes.proficiencyBonus}>
        <Typography variant="h6" component="div">
          Proficiency Bonus
        </Typography>
        <BorderCircle>
          <Typography variant="h6" component="div">
            {profBonus}
          </Typography>
        </BorderCircle>
      </BorderBox>
      <BorderBox className={classes.skills}>
        <Typography variant="overline" className={classes.name}>
          Skill
        </Typography>
        <Typography variant="overline">Prof.</Typography>
        <Typography variant="overline">Bonus</Typography>
        {sortedSkills
          .filter(
            ([skill]) =>
              skills[skill as keyof typeof skills] > SkillProficiency.Half ||
              showOtherSkills,
          )
          .map(([skill, attr]) => (
            <React.Fragment key={skill}>
              <Typography className={classes.name}>
                <RollableContent
                  formula={`1d20 + this.proficiencyBonus * this.skills.${skill} + this.modifiers.${attr}`}
                  context={{ this: model }}
                  towerable
                >
                  {capitalizeKeyword(skill)}
                </RollableContent>
              </Typography>
              <EditableContent
                editing={editing}
                model={model}
                property={`skills.${skill}`}
                validate="number"
                render={renderProf}
              />
              <Typography component="div">
                <SignedValue
                  value={
                    modifiers[attr as keyof typeof modifiers] +
                    Math.floor(profBonus * skills[skill as keyof typeof skills])
                  }
                />
              </Typography>
            </React.Fragment>
          ))}
        <Button
          className={classes.other}
          size="small"
          onClick={toggleOtherSkills}
        >
          Other Skills
        </Button>
      </BorderBox>
    </div>
  );
}

const skillMod = {
  athletics: 'strength',
  acrobatics: 'dexterity',
  sleightOfHand: 'dexterity',
  stealth: 'dexterity',
  arcana: 'intelligence',
  history: 'intelligence',
  investigation: 'intelligence',
  nature: 'intelligence',
  religion: 'intelligence',
  animalHandling: 'wisdom',
  insight: 'wisdom',
  medicine: 'wisdom',
  perception: 'wisdom',
  survival: 'wisdom',
  deception: 'charisma',
  intimidation: 'charisma',
  performance: 'charisma',
  persuation: 'charisma',
};
