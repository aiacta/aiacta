import {
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { CharacterModel } from '../../stores/models';
import ActionableContent from '../ActionableContent';
import useObservable from '../hooks/useObservable';
import BorderBox from './BorderBox';

const styles = (theme: Theme) =>
  createStyles({
    container: {},
  });

export interface ActionsProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Actions);

function Actions({ model, editing, classes, className }: ActionsProps) {
  const actions = useObservable(() => model.actions);
  return (
    <BorderBox className={classes.container} outterClassName={className}>
      {actions.map(act => (
        <Typography key={act.key}>
          {act.name}.{' '}
          <ActionableContent instigatorId={model._id} actionlike={act} />
        </Typography>
      ))}
    </BorderBox>
  );
}
