import {
  Checkbox,
  createStyles,
  DialogContent,
  FormControl,
  FormControlLabel,
  Input,
  InputLabel,
  Popover,
  Radio,
  RadioGroup,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { SvgIconProps } from '@material-ui/core/es/SvgIcon';
import {
  AddCircleOutline,
  DeleteOutline,
  Forward,
  GpsFixed,
  GpsNotFixed,
  PanTool,
  PanToolOutlined,
} from '@material-ui/icons';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { CharacterModel, IItem } from '../../stores/models';
import classNames from '../../utils/classNames';
import { useMemoize } from '../../utils/memoize';
import EditableContent from '../EditableContent';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridColumnGap: `${theme.spacing.unit}px`,
      gridRowGap: `${theme.spacing.unit / 2}px`,
      gridTemplateColumns:
        '[equip]auto [quantity]auto [name]3fr [location]1fr [weight]auto [actions]auto',
      gridAutoRows: 'max-content',
    },
    pointer: {
      cursor: 'pointer',
    },
    location: {
      width: '100%',
      display: 'inline-block',
    },
    quantity: {
      justifySelf: 'flex-end',
    },
    weight: {
      justifySelf: 'flex-end',
    },
  });

export interface InventoryProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Inventory);

function Inventory({ className, model, editing, classes }: InventoryProps) {
  const [createItem, updateCreateItem] = React.useState(false);
  const handleAdd = React.useCallback(() => {
    updateCreateItem(true);
  }, []);
  const handleAbort = React.useCallback(() => {
    updateCreateItem(false);
  }, []);

  const renderIcons = useMemoize(
    (
      OnIcon: React.ComponentType<SvgIconProps>,
      OffIcon: React.ComponentType<SvgIconProps>,
    ) => (equipped: boolean, onChange?: (v: any) => void) => {
      return equipped ? (
        <OnIcon
          fontSize="small"
          className={onChange && classes.pointer}
          onClick={onChange && onChange.bind(null, false)}
        />
      ) : (
        <OffIcon
          fontSize="small"
          className={onChange && classes.pointer}
          onClick={onChange && onChange.bind(null, true)}
        />
      );
    },
  );

  const handleUseItem = useMemoize((key: string) => () => model.useItem(key));
  const handleRemoveItem = useMemoize((key: string) => () =>
    model.removeFromInventory(key, true),
  );

  const popoverRef = React.useRef(null as null | HTMLDivElement);

  return (
    <>
      <Observer>
        {() => (
          <div className={classNames(className, classes.container)}>
            <div />
            <Typography variant="overline">Qty</Typography>
            <Typography variant="overline">Item</Typography>
            <Typography variant="overline">Location</Typography>
            <div />
            <div ref={popoverRef}>
              {editing && (
                <AddCircleOutline
                  fontSize="small"
                  className={classes.pointer}
                  onClick={handleAdd}
                />
              )}
            </div>
            {model.inventory.map(inv => (
              <React.Fragment key={inv.key}>
                <div>
                  {inv.reference.equipable && (
                    <EditableContent
                      model={inv}
                      property="equipped"
                      editing={editing}
                      validate="boolean"
                      render={renderIcons(PanTool, PanToolOutlined)}
                    />
                  )}
                </div>
                <Typography className={classes.quantity}>
                  <EditableContent
                    model={inv}
                    property="quantity"
                    editing={editing}
                    validate="number"
                  />
                  x
                </Typography>
                <Typography>{inv.reference.name}</Typography>
                <Typography className={classes.location}>
                  <EditableContent
                    model={inv}
                    property="location"
                    editing={editing}
                  />
                </Typography>
                <Typography className={classes.weight}>
                  {inv.reference.weight * inv.quantity} lb.
                </Typography>
                <div>
                  {inv.reference.requiresAttunement && (
                    <EditableContent
                      model={inv}
                      property="attuned"
                      editing={editing}
                      validate="boolean"
                      render={renderIcons(GpsFixed, GpsNotFixed)}
                    />
                  )}
                  {editing && (
                    <DeleteOutline
                      fontSize="small"
                      className={classes.pointer}
                      onClick={handleRemoveItem(inv.key)}
                    />
                  )}
                  {inv.reference.use && (
                    <Forward
                      fontSize="small"
                      className={classes.pointer}
                      onClick={handleUseItem(inv.key)}
                    />
                  )}
                </div>
              </React.Fragment>
            ))}
            <div />
            <div />
            <div />
            <Typography
              className={classes.weight}
              style={{ gridColumn: 'location / span 2' }}
            >
              total{' '}
              {model.inventory.reduce(
                (acc, inv) => acc + inv.reference.weight * inv.quantity,
                0,
              )}{' '}
              lb.
            </Typography>
          </div>
        )}
      </Observer>
      <Popover
        open={createItem}
        onClose={handleAbort}
        anchorEl={popoverRef.current}
      >
        <DialogContent>
          <CreateItem />
        </DialogContent>
      </Popover>
    </>
  );
}

const defaultInputs = {
  name: '',
  desc: '',
  weight: 0,
  price: 0,
  category: '',
  subcategory: '',
  identified: true,
  rarity: 'common',
  requiresAttunement: false,
} as IItem;
function CreateItem() {
  const [itemType, updateItemType] = React.useState('generic');
  const [inputs, updateInputs] = React.useState(defaultInputs);

  const handleInputChange = React.useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>, checked?: boolean) => {
      const { name, value, valueAsNumber } = evt.target;
      const prop = name as keyof typeof defaultInputs;
      if (typeof defaultInputs[prop] === 'number') {
        updateInputs(s => ({ ...s, [prop]: valueAsNumber }));
      } else if (typeof defaultInputs[prop] === 'boolean') {
        updateInputs(s => ({ ...s, [prop]: checked }));
      } else {
        updateInputs(s => ({ ...s, [prop]: value }));
      }
    },
    [],
  );

  const handleChangeItemType = React.useCallback(
    (evt: React.ChangeEvent<any>) => {
      updateItemType(evt.target.value);
    },
    [],
  );

  return (
    <>
      <Typography>Create an item</Typography>
      <RadioGroup
        row
        name="itemType"
        value={itemType}
        onChange={handleChangeItemType}
      >
        <FormControlLabel
          value="generic"
          control={<Radio />}
          label="Generic item"
        />
        <FormControlLabel value="weapon" control={<Radio />} label="Weapon" />
        <FormControlLabel value="armor" control={<Radio />} label="Armor" />
      </RadioGroup>
      <FormControl fullWidth>
        <InputLabel>Name</InputLabel>
        <Input
          required
          name="name"
          value={inputs.name}
          onChange={handleInputChange}
        />
      </FormControl>
      <FormControl fullWidth>
        <InputLabel>Description</InputLabel>
        <Input
          name="desc"
          value={inputs.desc}
          onChange={handleInputChange}
          multiline
          rows={3}
        />
      </FormControl>
      <FormControl>
        <InputLabel>Price</InputLabel>
        <Input
          name="price"
          type="number"
          value={inputs.price}
          onChange={handleInputChange}
        />
      </FormControl>
      <FormControl>
        <InputLabel>Weight</InputLabel>
        <Input
          name="weight"
          type="number"
          value={inputs.weight}
          onChange={handleInputChange}
        />
      </FormControl>
      <FormControl>
        <InputLabel>Category</InputLabel>
        <Input
          name="category"
          value={inputs.category}
          onChange={handleInputChange}
        />
      </FormControl>
      <FormControl>
        <InputLabel>Subcategory</InputLabel>
        <Input
          name="subcategory"
          value={inputs.subcategory}
          onChange={handleInputChange}
        />
      </FormControl>
      <FormControlLabel
        label="Requires attunement"
        control={
          <Checkbox
            name="requiresAttunement"
            checked={inputs.requiresAttunement}
            onChange={handleInputChange}
            color="primary"
          />
        }
      />
      <FormControlLabel
        label="Is identified"
        control={
          <Checkbox
            name="identified"
            checked={inputs.identified}
            onChange={handleInputChange}
            color="primary"
          />
        }
      />
      {renderInputs()}
    </>
  );

  function renderInputs() {
    switch (itemType) {
      default:
        return <></>;
    }
  }
}
