import {
  createStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { comparer } from 'mobx';
import * as React from 'react';
import { CharacterModel } from '../../stores/models';
import classNames from '../../utils/classNames';
import EditableContent from '../EditableContent';
import useObservable from '../hooks/useObservable';
import RollFormulaEditor from '../RollFormulaEditor';
import RollResult from '../RollResult';
import BorderBox from './BorderBox';
import BorderCircle from './BorderCircle';
import SignedValue from './SignedValue';

const heartSize = 45;
const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateColumns: 'repeat(4, 1fr)',
      alignItems: 'center',
      justifyItems: 'center',
    },
    armorClass: {
      position: 'relative',
      '& path': {
        fill: '#fff',
        stroke: '#444',
        strokeWidth: 8,
      },
    },
    acValue: {
      position: 'absolute',
      top: '25%',
      width: '100%',
    },
    acLabel: {
      position: 'absolute',
      bottom: '15%',
      width: '100%',
    },
    hitPoints: {
      position: 'relative',
      display: 'inline-block',
    },
    healthRolls: {
      padding: theme.spacing.unit / 2,
      display: 'grid',
      gridTemplateColumns: 'repeat(5, 1fr)',
      gridColumnGap: '4px',
      justifyItems: 'center',
    },
    healthRollsText: {
      gridColumn: 'span 5',
    },
    heart: {
      display: 'inline-block',
      margin: `${heartSize * 0.375}px ${heartSize * 0.5}px ${heartSize *
        0.125}px ${heartSize * 0.5}px`,
      background: '#fff',
      height: heartSize,
      width: heartSize,
      transform: 'rotate(45deg)',
      '&:before, &:after': {
        content: '""',
        background: 'inherit',
        borderRadius: '50%',
        position: 'absolute',
        width: heartSize,
        height: heartSize,
      },
      '&:before': {
        left: -heartSize * 0.5,
        top: 0,
      },
      '&:after': {
        left: 0,
        top: -heartSize * 0.5,
      },
    },
    heartBackground: {
      background: '#444',
      transform: 'rotate(45deg) scale(1.05)',
      position: 'absolute',
      left: 0,
      top: 0,
      zIndex: -1,
    },
    heartContent: {
      position: 'absolute',
      top: '8%',
      left: '50%',
      transform: 'translateX(-50%)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
    },
    initiative: {
      position: 'relative',
      '& circle': {
        stroke: 'none',
        fill: '#fff',
      },
      '& path': {
        fill: 'none',
        stroke: '#444',
        strokeWidth: 2,
        strokeLinecap: 'round',
      },
    },
    iniLabel: {
      position: 'absolute',
      top: '35%',
      width: '60%',
      right: 0,
    },
    rollEditor: {
      padding: theme.spacing.unit / 2,
    },
  });

export interface CombatStatsProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(CombatStats);

function CombatStats({ model, editing, classes, className }: CombatStatsProps) {
  const context = useObservable(
    () => {
      return { this: model } as any;
    },
    [model],
    comparer.identity,
  );
  const renderRollFormula = React.useCallback(
    val => <RollResult sync formula={val} context={context} />,
    [context],
  );

  return (
    <BorderBox className={classes.container} outterClassName={className}>
      <div className={classes.armorClass}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1"
          height="75"
          viewBox="0 0 350.244 418.945"
        >
          <path
            d="M134.101 53.677c14.144-2.327 44.578-72.143 214.643-46.544-81.451 99.564-50.326 131.725-25.599 216.792 46.063 158.464-91.7 193.52-150.35 193.52-58.65 0-208.53-51.172-157.332-195.847 13.963-28.554-2.327-38.49-2.327-53.08 0-8.742 58.493 23.553 58.18-29.18-.313-50.406-32.58-50.065-69.816-33.208 0-8.742 31.772-110.633 132.601-52.453z"
            display="block"
          />
        </svg>
        <Typography
          className={classes.acValue}
          variant="h6"
          align="center"
          component="div"
        >
          <EditableContent
            editing={editing}
            model={model}
            property="armorClassFormula"
            render={renderRollFormula}
            complex
          >
            {(value, onChange) => (
              <RollFormulaEditor
                className={classes.rollEditor}
                value={value}
                onChange={onChange}
                presets={[
                  {
                    label: 'Default',
                    value:
                      '(this.gear.armor.armorClass ?? 10) + min(this.modifiers.dexterity, this.gear.armor.maxDexterityBonus) + this.gear.shield.armorClass',
                  },
                  {
                    label: 'Unarmored Defense',
                    value:
                      '(this.gear.armor.armorClass ?? 10) + min(this.modifiers.dexterity, this.gear.armor.maxDexterityBonus) + this.gear.shield.armorClass + (this.gear.armor ? 0 : this.modifiers.constitution)',
                  },
                ]}
              />
            )}
          </EditableContent>
        </Typography>
      </div>
      <div className={classes.hitPoints}>
        <div className={classes.heart} />
        <div className={classNames(classes.heart, classes.heartBackground)} />
        <div className={classes.heartContent}>
          <Typography component="div" variant="h6" noWrap>
            <EditableContent
              editing={editing}
              model={model}
              property="health.current"
              validate="number"
            />{' '}
            +{' '}
            <EditableContent
              editing={editing}
              model={model}
              property="health.temporary"
              validate="number"
            />
          </Typography>
          <Typography component="div" variant="h6" noWrap>
            <EditableContent
              editing={editing}
              model={model}
              property="healthMaximum"
              complex
            >
              {(value, onChange) => (
                <Table padding="dense">
                  <TableHead>
                    <TableRow>
                      <TableCell>Source</TableCell>
                      <TableCell align="right">Value</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {model.health.rolls.map((val, idx) => (
                      <TableRow key={idx}>
                        <TableCell component="th" scope="row">
                          {model.classesFlat[idx].name} (d
                          {model.hitDice[idx].faces})
                        </TableCell>
                        <TableCell align="right">
                          <EditableContent
                            editing={editing}
                            model={model}
                            property={`health.rolls.${idx}`}
                            validate="number"
                          />
                        </TableCell>
                      </TableRow>
                    ))}
                    <TableRow>
                      <TableCell component="th" scope="row">
                        Constitution
                      </TableCell>
                      <TableCell align="right">
                        <SignedValue
                          block
                          value={
                            model.health.rolls.length *
                            model.modifiers.constitution
                          }
                        />
                      </TableCell>
                    </TableRow>
                    {model.effects
                      .filter(eff => eff.affects.includes('healthMaximum'))
                      .map((eff, idx) => (
                        <TableRow key={idx}>
                          <TableCell component="th" scope="row">
                            {eff.source.name}
                          </TableCell>
                          <TableCell align="right">
                            <SignedValue block value={eff.value} />
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              )}
            </EditableContent>
          </Typography>
        </div>
      </div>
      <div className={classes.initiative}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1"
          height="70"
          viewBox="-10 15 100 70"
        >
          <circle cx={60} cy={55} r={25} />
          <path
            d={`M 0 30 l 60 0 a 25 25 0 1 1 ${Math.cos(Math.PI * 0.8) *
              25} ${Math.sin(Math.PI * 0.8) * 25 +
              25} l -15 0 M 60 55 m ${Math.cos(Math.PI * 0.975) *
              25} ${Math.sin(Math.PI * 0.975) *
              25} l -20 0 M 60 55 m ${Math.cos(Math.PI * 1.15) * 25} ${Math.sin(
              Math.PI * 1.15,
            ) * 25} l -30 0`}
            display="block"
          />
        </svg>
        <Typography className={classes.iniLabel} variant="h6" align="center">
          <EditableContent
            editing={editing}
            model={model}
            property="initiativeBonusFormula"
            render={renderRollFormula}
            complex
          >
            {(value, onChange) => (
              <RollFormulaEditor
                className={classes.rollEditor}
                value={value}
                onChange={onChange}
              />
            )}
          </EditableContent>
        </Typography>
      </div>
      <BorderCircle>
        <Typography variant="h6">
          <EditableContent
            editing={editing}
            model={model}
            property="speed.walk"
            validate="number"
          />
          ft
        </Typography>
      </BorderCircle>
    </BorderBox>
  );
}
