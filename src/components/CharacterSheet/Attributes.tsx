import {
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Star, StarBorder } from '@material-ui/icons';
import * as React from 'react';
import {
  attributes,
  Attributes,
  CharacterModel,
  SkillProficiency,
} from '../../stores/models';
import capitalizeKeyword from '../../utils/capitalizeKeyword';
import classNames from '../../utils/classNames';
import { useMemoize } from '../../utils/memoize';
import EditableContent from '../EditableContent';
import useObservable from '../hooks/useObservable';
import RollableContent from '../RollableContent';
import BorderBox from './BorderBox';
import SignedValue from './SignedValue';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridAutoFlow: 'column',
      gridAutoColumns: 'minmax(100px, 1fr)',
    },
    box: {
      display: 'grid',
      gridTemplateAreas: `
          'value'
          'modifier'
          'name'`,
      justifyItems: 'center',
    },
    value: {
      gridArea: 'value',
    },
    modifier: {
      gridArea: 'modifier',
    },
    name: {
      gridArea: 'name',
    },
    sign: {
      position: 'absolute',
      transform: 'translateX(-100%)',
    },
    proficiency: {
      position: 'absolute',
      top: theme.spacing.unit / 2,
      right: theme.spacing.unit / 2,
    },
    editing: {
      cursor: 'pointer',
    },
  });

export interface AttributesProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(AttributesFC);

function AttributesFC({ classes, model, editing, className }: AttributesProps) {
  const charAttributes = useObservable(() => ({ ...model.attributes }));
  const saves = useObservable(() => ({ ...model.saves }));

  function proficiencyStar(
    proficiency: SkillProficiency,
    onChange?: (value: string | number) => void,
  ) {
    switch (proficiency) {
      case SkillProficiency.None:
        return (
          <StarBorder
            className={onChange && classes.editing}
            onClick={
              onChange && onChange.bind(null, SkillProficiency.Proficient)
            }
          />
        );
      default:
        return (
          <Star
            className={onChange && classes.editing}
            onClick={onChange && onChange.bind(null, SkillProficiency.None)}
          />
        );
    }
  }

  const renderProf = useMemoize((prof: SkillProficiency, onChange) =>
    proficiencyStar(prof, onChange),
  );

  return (
    <div className={classNames(classes.container, className)}>
      {attributes.map(attr => (
        <BorderBox key={attr} className={classes.box}>
          <Typography variant="caption" className={classes.value}>
            <EditableContent
              editing={editing}
              model={model}
              property={`attributes.${attr}`}
              validate="number"
            />
          </Typography>
          <Typography variant="h5" component="div" className={classes.modifier}>
            <SignedValue
              value={Math.floor(
                // FIXME codesmell
                (charAttributes[attr as keyof Attributes] - 10) / 2,
              )}
            />
          </Typography>
          <Typography variant="caption" className={classes.name}>
            <RollableContent
              formula={`1d20 + this.modifiers.${attr}`}
              context={{ this: model }}
              towerable
            >
              {capitalizeKeyword(attr)}
            </RollableContent>
          </Typography>
          {/* FIXME codesmell */}
          {(saves[attr as keyof Attributes] > SkillProficiency.None ||
            editing) && (
            <EditableContent
              model={model}
              property={`saves.${attr}`}
              editing={editing}
              validate="number"
              render={renderProf}
              className={classes.proficiency}
            />
          )}
        </BorderBox>
      ))}
    </div>
  );
}
