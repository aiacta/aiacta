import {
  createStyles,
  IconButton,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Edit } from '@material-ui/icons';
import * as React from 'react';
import { AccessType, CharacterModel } from '../../stores/models';
import DropZone from '../DropZone';
import useDragAndDrop from '../hooks/useDragAndDrop';
import useObservable from '../hooks/useObservable';
import Actions from './Actions';
import Attributes from './Attributes';
import CombatStats from './CombatStats';
import Headline from './Headline';
import Inventory from './Inventory';
import Proficiencies from './Proficiencies';
import Skills from './Skills';
import Features from './Features';
import Character from './Character';
import { myself } from '../../stores/modules/gameData';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateAreas: `
        'head   head    edit'
        'attr   attr    attr'
        'profs  combat  combat'
        'feats  inv     inv'
        'char   char    char'
        'spells spells  spells'`,
    },
    headline: {
      gridArea: 'head',
    },
    edit: {
      gridArea: 'edit',
    },
    attributes: {
      gridArea: 'attr',
    },
    combat: {
      gridArea: 'combat',
    },
    proficiencies: {
      gridArea: 'profs',
      paddingRight: 10,
    },
    inventory: {
      gridArea: 'inv',
    },
    features: {
      gridArea: 'feats',
    },
    spellbook: {
      gridArea: 'spells',
    },
    character: {
      gridArea: 'char',
    },
  });

export interface CharacterSheetProps extends WithStyles<typeof styles> {
  model?: CharacterModel;
}

export default withStyles(styles)(CharacterSheet);

function CharacterSheet({ model, classes }: CharacterSheetProps) {
  const canBeRead = useObservable(() =>
    Boolean(model && myself().isAllowedTo(AccessType.Read, model)),
  );
  const canBeEdited = useObservable(() =>
    Boolean(model && myself().isAllowedTo(AccessType.Write, model)),
  );
  const [editing, updateEditing] = React.useState(canBeEdited);
  const handleToggleEdit = React.useCallback(() => updateEditing(s => !s), []);

  const dragAndDrop = useDragAndDrop({
    'aiacta/item': {
      allow: () => 'copy',
      onDrop: data => {
        model && model.addToInventory(data);
      },
    },
    'aiacta/class': {
      allow: () => 'copy',
      onDrop: data => {
        model && model.addClassLevel(data);
      },
    },
    'aiacta/spell': {
      allow: () => (model && model.canLearnSpells ? 'copy' : 'none'),
      onDrop: data => {
        model && model.learnSpell(data);
      },
    },
  });

  if (!model) {
    return <>The sheet you are looking for does not exist!</>;
  }
  if (!canBeRead) {
    // TODO maybe implement a no-access-view for chars (e.g. portrait and visible effects?)
    return <>You are not allowed to see this sheet!</>;
  }

  return (
    <DropZone {...dragAndDrop}>
      <div className={classes.container}>
        {canBeEdited && (
          <IconButton
            className={classes.edit}
            color={editing ? 'primary' : 'default'}
            onClick={handleToggleEdit}
          >
            <Edit fontSize="small" />
          </IconButton>
        )}
        <Headline
          className={classes.headline}
          model={model}
          editing={editing}
        />
        <Attributes
          className={classes.attributes}
          model={model}
          editing={editing}
        />
        <div className={classes.proficiencies}>
          <Skills model={model} editing={editing} />
          <Proficiencies model={model} editing={editing} />
        </div>
        <div className={classes.combat}>
          <CombatStats model={model} editing={editing} />
          <Actions model={model} editing={editing} />
        </div>
        <Features
          className={classes.features}
          model={model}
          editing={editing}
        />
        <Inventory
          className={classes.inventory}
          model={model}
          editing={editing}
        />
        <Character
          className={classes.character}
          model={model}
          editing={editing}
        />
        {model.isSpellcaster && (
          <div className={classes.spellbook}>
            <div>{model.spellSlots.slice(1).join(' | ')}</div>
            {model.spellsKnown.map(s => (
              <div key={s._id}>
                {s.name} ({s.level})
              </div>
            ))}
          </div>
        )}
      </div>
    </DropZone>
  );
}
