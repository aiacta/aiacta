import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '&:before': {
        content: '""',
        background: 'white',
        position: 'absolute',
        width: 60,
        height: 60,
        borderRadius: '100%',
        border: '6px double #444',
        zIndex: -1,
      },
    },
  });

export interface BorderCircleProps extends WithStyles<typeof styles> {
  children: React.ReactNode;
}

export default withStyles(styles)(BorderCircle);

function BorderCircle({ classes, children }: BorderCircleProps) {
  return <div className={classes.container}>{children}</div>;
}
