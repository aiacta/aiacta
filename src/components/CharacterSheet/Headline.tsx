import {
  createStyles,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { RemoveCircle } from '@material-ui/icons';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { CharacterModel } from '../../stores/models';
import classNames from '../../utils/classNames';
import EditableContent from '../EditableContent';
import { useMemoize } from '../../utils/memoize';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateAreas: `
        'name classes classes    classes    player'
        'name race    background alignment  exp'`,
      gridTemplateColumns: '2fr 1fr 1fr 1fr 1fr',
      alignItems: 'center',
    },
    name: {
      gridArea: 'name',
    },
    classes: {
      gridArea: 'classes',
    },
    classesGrid: {
      padding: theme.spacing.unit / 2,
      display: 'grid',
      gridTemplateColumns: '1fr 1fr auto',
      gridRowGap: theme.spacing.unit / 4,
    },
    race: {
      gridArea: 'race',
    },
    background: {
      gridArea: 'background',
    },
    alignment: {
      gridArea: 'alignment',
    },
    player: {
      gridArea: 'player',
    },
    exp: {
      gridArea: 'exp',
    },
  });

export interface HeadlineProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Headline);

function Headline({ model, editing, className, classes }: HeadlineProps) {
  const reduceClassLevel = useMemoize((id: string) => () => {
    model.removeClassLevel(id);
  });
  return (
    <Observer>
      {() => (
        <div className={classNames(classes.container, className)}>
          <Typography className={classes.name} variant="h5">
            <EditableContent editing={editing} model={model} property="name" />
          </Typography>
          <Typography className={classes.classes}>
            <EditableContent
              editing={editing}
              model={model}
              property="classes"
              render={renderClassList}
              complex
            >
              {(charClasses: typeof model.classes, onChange) => (
                <Table padding="dense">
                  <TableHead>
                    <TableRow>
                      <TableCell>Class</TableCell>
                      <TableCell align="center">Level</TableCell>
                      <TableCell align="right" />
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {charClasses.map(cls => (
                      <TableRow key={cls.key}>
                        <TableCell component="th" scope="row">
                          {cls.reference.name}
                        </TableCell>
                        <TableCell align="center">{cls.level}</TableCell>
                        <TableCell align="right">
                          <IconButton onClick={reduceClassLevel(cls.id)}>
                            <RemoveCircle fontSize="small" />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            </EditableContent>
          </Typography>
          <Typography className={classes.race}>{model.race.name}</Typography>
          <Typography className={classes.background}>
            {model.background.name}
          </Typography>
          <Typography className={classes.alignment}>
            <EditableContent
              editing={editing}
              model={model}
              property="alignment"
            />
          </Typography>
          <Typography className={classes.player}>
            {model.owner ? model.owner.nickname : 'nobody'}
          </Typography>
          <Typography className={classes.exp}>
            <EditableContent
              editing={editing}
              model={model}
              property="experiencePoints"
              validate="number"
            />{' '}
            / {model.nextExperiencePoints} xp
          </Typography>
        </div>
      )}
    </Observer>
  );
}

function renderClassList(v: CharacterModel['classes']) {
  return v.map(c => `${c.reference.name} ${c.level}`);
}
