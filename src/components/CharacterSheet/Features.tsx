import {
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { AddCircleOutline, DeleteOutline } from '@material-ui/icons';
import { runInAction } from 'mobx';
import { Observer } from 'mobx-react';
import * as React from 'react';
import uuid from 'uuid/v4';
import { CharacterModel, CommonFeature } from '../../stores/models';
import classNames from '../../utils/classNames';
import { useMemoize } from '../../utils/memoize';
import EditableContent from '../EditableContent';
import MarkdownEditor from '../MarkdownEditor';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateColumns: '1fr auto',
      gridAutoRows: 'max-content',
      paddingRight: 10,
      alignItems: 'center',
    },
    pointer: {
      cursor: 'pointer',
    },
    popout: {
      padding: theme.spacing.unit,
      maxWidth: 480,
    },
  });

export interface FeaturesProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Features);

function Features({ model, editing, className, classes }: FeaturesProps) {
  const handleAdd = useMemoize(
    (type: keyof typeof model, newItem: any) => () => {
      const arr = model[type] as { key: string }[];
      runInAction(() => {
        arr.push({
          key: uuid(),
          ...newItem,
        });
      });
    },
  );
  const handleRemove = useMemoize(
    (type: keyof typeof model, key: string) => () => {
      const arr = model[type] as { key: string }[];
      const idx = arr.findIndex(d => d.key === key);
      if (idx >= 0) {
        runInAction(() => {
          arr.splice(idx, 1);
        });
      }
    },
  );
  const handleDescChange = useMemoize(
    (item: any, prop: string) => (val: string) => (item[prop] = val),
  );
  function renderItem(
    kind: keyof typeof model,
    item: { key: string } & CommonFeature,
  ) {
    return (
      <React.Fragment key={item.key}>
        <Typography>
          <EditableContent
            editing={editing}
            model={item}
            property="name"
            complex
          >
            {(value, onChange) => (
              <div className={classes.popout}>
                <Typography variant="overline">{item.name}</Typography>
                <MarkdownEditor
                  value={item.desc}
                  onChange={handleDescChange(item, 'desc')}
                />
                <Typography variant="caption">Effects</Typography>
                {item.effects &&
                  item.effects.map(effect => (
                    <React.Fragment key={effect.key}>
                      <Typography>
                        <EditableContent
                          editing={editing}
                          model={effect}
                          property="affects"
                          validate="list"
                        />
                      </Typography>
                      <Typography>{effect.formula}</Typography>
                      <Typography>{effect.visibility}</Typography>
                    </React.Fragment>
                  ))}
              </div>
            )}
          </EditableContent>
        </Typography>
        <div>
          {editing && (
            <DeleteOutline
              fontSize="small"
              className={classes.pointer}
              onClick={handleRemove(kind, item.key)}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
  return (
    <Observer>
      {() => (
        <div className={classNames(className, classes.container)}>
          <Typography variant="overline">Traits</Typography>
          <div>
            {editing && (
              <AddCircleOutline
                fontSize="small"
                className={classes.pointer}
                onClick={handleAdd('traits', {
                  name: 'New trait',
                  desc: 'No description',
                })}
              />
            )}
          </div>
          {model.traits.map(trait => renderItem('traits', trait))}
          <Typography variant="overline">Features</Typography>
          <div>
            {editing && (
              <AddCircleOutline
                fontSize="small"
                className={classes.pointer}
                onClick={handleAdd('features', {
                  name: 'New feature',
                  desc: 'No description',
                })}
              />
            )}
          </div>
          {model.features.map(feature => renderItem('features', feature))}
          <Typography variant="overline">Feats</Typography>
          <div>
            {editing && (
              <AddCircleOutline
                fontSize="small"
                className={classes.pointer}
                onClick={handleAdd('feats', {
                  name: 'New feat',
                  desc: 'No description',
                })}
              />
            )}
          </div>
          {model.feats.map(feat => renderItem('feats', feat))}
          <Typography variant="overline">Effects</Typography>
          <div>
            {editing && (
              <AddCircleOutline
                fontSize="small"
                className={classes.pointer}
                onClick={handleAdd('customEffects', {
                  affects: 'none',
                  formula: '1',
                  source: { name: 'Unspecified' },
                  visibility: 'self',
                })}
              />
            )}
          </div>
          {model.customEffects.map(effect => (
            <React.Fragment key={effect.key}>
              <Typography>
                <EditableContent
                  editing={editing}
                  model={effect}
                  property="source.name"
                />
              </Typography>
              <div>
                {editing && (
                  <DeleteOutline
                    fontSize="small"
                    className={classes.pointer}
                    onClick={handleRemove('customEffects', effect.key)}
                  />
                )}
              </div>
            </React.Fragment>
          ))}
        </div>
      )}
    </Observer>
  );
}
