import {
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { runInAction } from 'mobx';
import { Observer } from 'mobx-react';
import * as React from 'react';
import { CharacterModel } from '../../stores/models';
import stampToken from '../../utils/stampToken';
import { DropInfo } from '../DropZone';
import EditableContent from '../EditableContent';
import ImageInput from '../ImageInput';
import BorderBox from './BorderBox';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateAreas: `
        'age  height weight'
        'eyes skin   hair'`,
      gridRowGap: `${theme.spacing.unit / 2}px`,
      gridColumnGap: `${theme.spacing.unit}px`,
      '& p': {
        display: 'flex',
        flexDirection: 'column',
      },
    },
    age: {
      gridArea: 'age',
    },
    height: {
      gridArea: 'height',
    },
    weight: {
      gridArea: 'weight',
    },
    eyes: {
      gridArea: 'eyes',
    },
    skin: {
      gridArea: 'skin',
    },
    hair: {
      gridArea: 'hair',
    },
    portrait: {
      height: 320,
    },
    images: {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr',
      gridColumnGap: `${theme.spacing.unit}px`,
    },
  });

export interface CharacterProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Character);

function Character({ editing, model, classes, className }: CharacterProps) {
  const handlePortraitChange = React.useCallback(
    (dropInfo?: DropInfo) => {
      runInAction(() => {
        model.portraitImage = dropInfo ? dropInfo.file : undefined;
        if (!model.tokenImage && dropInfo && dropInfo.image) {
          const { width, height } = dropInfo.image;
          stampToken(dropInfo.file, width, height).then(image => {
            runInAction(() => {
              model.tokenImage = image;
            });
          });
        }
      });
    },
    [model],
  );
  const handleTokenChange = React.useCallback(
    (dropInfo?: DropInfo) => {
      model.tokenImage = dropInfo ? dropInfo.file : undefined;
    },
    [model],
  );
  return (
    <div className={className}>
      <BorderBox className={classes.container}>
        <div className={classes.age}>
          <Typography variant="caption">Age</Typography>
          <Typography>
            <EditableContent editing={editing} model={model} property="age" />
          </Typography>
        </div>
        <div className={classes.height}>
          <Typography variant="caption">Height</Typography>
          <Typography>
            <EditableContent
              editing={editing}
              model={model}
              property="height"
            />
          </Typography>
        </div>
        <div className={classes.weight}>
          <Typography variant="caption">Weight</Typography>
          <Typography>
            <EditableContent
              editing={editing}
              model={model}
              property="weight"
            />
          </Typography>
        </div>
        <div className={classes.eyes}>
          <Typography variant="caption">Eyes</Typography>
          <Typography>
            <EditableContent editing={editing} model={model} property="eyes" />
          </Typography>
        </div>
        <div className={classes.skin}>
          <Typography variant="caption">Skin</Typography>
          <Typography>
            <EditableContent editing={editing} model={model} property="skin" />
          </Typography>
        </div>
        <div className={classes.hair}>
          <Typography variant="caption">Hair</Typography>
          <Typography>
            <EditableContent editing={editing} model={model} property="hair" />
          </Typography>
        </div>
      </BorderBox>
      <Observer>
        {() => (
          <div className={classes.images}>
            <ImageInput
              className={classes.portrait}
              value={model.portraitImage}
              preview={model.portraitImageUrl}
              onChange={handlePortraitChange}
              allowCrop
              allowRemove
            />
            <ImageInput
              className={classes.portrait}
              value={model.tokenImage}
              preview={model.tokenImageUrl}
              onChange={handleTokenChange}
              allowCrop
              allowRemove
            />
          </div>
        )}
      </Observer>
    </div>
  );
}
