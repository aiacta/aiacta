import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';

const styles = (theme: Theme) =>
  createStyles({
    sign: {
      position: 'absolute',
      transform: 'translateX(-100%)',
    },
  });

export interface SignedValueProps extends WithStyles<typeof styles> {
  value: number;
  block?: boolean;
}

export default withStyles(styles)(SignedValue);

function SignedValue({ value, classes, block }: SignedValueProps) {
  return (
    <>
      <span className={block ? undefined : classes.sign}>
        {value >= 0 ? '+' : '-'}
      </span>
      {Math.abs(value)}
    </>
  );
}
