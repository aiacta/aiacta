import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import classNames from '../../utils/classNames';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      border: '6px solid transparent',
      boxSizing: 'border-box',
      height: 'max-content',
      '&:before': {
        content: '""',
        position: 'absolute',
        top: 'calc(2px * -2)',
        left: 'calc(2px * -2)',
        width: 'calc(100% + calc(2px * 4))',
        height: 'calc(100% + calc(2px * 4))',
        background: '#ccc',
        clipPath:
          'polygon(0% calc(10px), calc(10px) 0%, calc(100% - 10px) 0%, 100% calc(10px), 100% calc(100% - 10px), calc(100% - 10px) 100%, calc(10px) 100%, 0% calc(100% - 10px))',
        zIndex: -1,
      },
      '&:after': {
        content: '""',
        position: 'absolute',
        top: 'calc(2px * -1)',
        left: 'calc(2px * -1)',
        width: 'calc(100% + calc(2px * 2))',
        height: 'calc(100% + calc(2px * 2))',
        background: '#fff',
        clipPath:
          'polygon(0% calc(10px), calc(10px) 0%, calc(100% - 10px) 0%, 100% calc(10px), 100% calc(100% - 10px), calc(100% - 10px) 100%, calc(10px) 100%, 0% calc(100% - 10px))',
        zIndex: -1,
      },
    },
    background: {
      clipPath:
        'polygon(0% calc(10px), calc(10px) 0%, calc(100% - 10px) 0%, 100% calc(10px), 100% calc(100% - 10px), calc(100% - 10px) 100%, calc(10px) 100%, 0% calc(100% - 10px))',
      background: '#eee',
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: 0,
    },
    content: {
      padding: 8,
      boxSizing: 'border-box',
      position: 'relative',
      zIndex: 1,
    },
  });

export interface BorderBoxProps extends WithStyles<typeof styles> {
  children: React.ReactNode;
  className?: string;
  outterClassName?: string;
}

export default withStyles(styles)(BorderBox);

function BorderBox({
  classes,
  className,
  outterClassName,
  children,
}: BorderBoxProps) {
  return (
    <div className={classNames(classes.container, outterClassName)}>
      <div className={classes.background} />
      <div className={classNames(classes.content, className)}>{children}</div>
    </div>
  );
}
