import {
  createStyles,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { CharacterModel } from '../../stores/models';
import EditableContent from '../EditableContent';
import BorderBox from './BorderBox';

const styles = (theme: Theme) => createStyles({});

export interface ProficienciesProps extends WithStyles<typeof styles> {
  model: CharacterModel;
  editing: boolean;
  className?: string;
}

export default withStyles(styles)(Proficiencies);

function Proficiencies({ model, editing, className }: ProficienciesProps) {
  return (
    <BorderBox outterClassName={className}>
      <Typography>
        <strong>Proficiencies:</strong>{' '}
        <EditableContent
          model={model}
          property="proficiencies"
          editing={editing}
          validate="list"
        />
      </Typography>
      <Typography>
        <strong>Languages:</strong>{' '}
        <EditableContent
          model={model}
          property="languages"
          editing={editing}
          validate="list"
        />
      </Typography>
    </BorderBox>
  );
}
