import {
  createStyles,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
  InputAdornment,
  Link,
  ListSubheader,
  TextField,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { FileCopy } from '@material-ui/icons';
import * as React from 'react';
import {
  AccessType,
  CharacterModel,
  IBattlemap,
  isIWorld,
  IWorld,
} from '../stores/models';
import { battlemaps, characters, worlds } from '../stores/modules/dataStores';
import { gameLink, myself } from '../stores/modules/gameData';
import { openSheet } from '../stores/modules/uiView';
import { useMemoize } from '../utils/memoize';
import DropZone, { DropInfo } from './DropZone';
import useObservable from './hooks/useObservable';
import MapCreationDialog from './MapCreationDialog';
import RouterLink from './RouterLink';

const styles = (theme: Theme) =>
  createStyles({
    dropzone: {
      paddingTop: theme.spacing.unit,
      flex: '1 1 auto',
    },
    container: {
      maxWidth: 880,
      padding: '0 20px',
      margin: 'auto',
      display: 'flex',
      flexDirection: 'column',
    },
    noWorlds: {
      flex: '1 1 auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    gridTile: {
      cursor: 'pointer',
      '& a': {
        display: 'block',
        width: '100%',
        height: '100%',
      },
      '& img': {
        objectFit: 'cover',
        width: '100%',
        height: '100%',
      },
    },
  });

export interface DashboardProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Dashboard);

function Dashboard({ classes }: DashboardProps) {
  const [dropInfo, updateDropInfo] = React.useState(undefined as
    | DropInfo
    | undefined);
  const handleDrop = React.useCallback(
    (accepted: DropInfo[] | DropInfo) =>
      myself().isDungeonMaster && updateDropInfo(accepted as DropInfo),
    [],
  );
  const handleCreateMap = React.useCallback((map: IWorld | IBattlemap) => {
    if (isIWorld(map)) {
      worlds.add(map);
    } else {
      battlemaps.add(map);
    }
    updateDropInfo(undefined);
  }, []);
  const handleStopCreateMap = React.useCallback(
    () => updateDropInfo(undefined),
    [],
  );

  const handleClickChar = useMemoize((char: CharacterModel) => () => {
    openSheet(char);
  });

  const knownWorlds = useObservable(() =>
    worlds.all().filter(d => myself().isAllowedTo(AccessType.Know, d)),
  );
  const knownBattlemaps = useObservable(() =>
    battlemaps.all().filter(d => myself().isAllowedTo(AccessType.Know, d)),
  );
  const knownCharacters = useObservable(() => characters.all());

  const handleCopyGameLink = React.useCallback(() => {
    (navigator as any).clipboard.writeText(gameLink());
  }, []);

  const connectionString = useObservable(() => gameLink() || 'no link :(');

  return (
    <>
      <DropZone
        allowPaste
        onDropImages={handleDrop}
        className={classes.dropzone}
      >
        <div className={classes.container}>
          {myself().isDungeonMaster && (
            <Typography component="div">
              <TextField
                key={connectionString}
                fullWidth
                variant="outlined"
                label="Invite others to join your game"
                disabled
                defaultValue={connectionString}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={handleCopyGameLink}>
                        <FileCopy />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Typography>
          )}
          {knownWorlds.length + knownBattlemaps.length === 0 && (
            <div className={classes.noWorlds}>
              <Typography variant="h6" gutterBottom>
                There is no world... yet!
              </Typography>
              <Typography variant="body2" gutterBottom>
                {myself().isDungeonMaster
                  ? 'Drop an image here to get started.'
                  : `Wait for your Dungeon Master to create one.${
                      myself().characters.length === 0
                        ? ' In the meantime you can create your character.'
                        : ''
                    }`}
              </Typography>
            </div>
          )}
          <GridList cellHeight={180}>
            {knownWorlds.length > 0 && (
              <GridListTile cols={2} style={{ height: 'auto' }}>
                <ListSubheader component="div">Worlds</ListSubheader>
              </GridListTile>
            )}
            {knownWorlds.map(world => (
              <GridListTile key={world._id} className={classes.gridTile}>
                <Link component={RouterLink} href={`/app/map/${world._id}`}>
                  <img src={world.imageUrl} alt={world.name} />
                  <GridListTileBar title={world.name} />
                </Link>
              </GridListTile>
            ))}
            {knownBattlemaps.length > 0 && (
              <GridListTile cols={2} style={{ height: 'auto' }}>
                <ListSubheader component="div">Battlemaps</ListSubheader>
              </GridListTile>
            )}
            {knownBattlemaps.map(battlemap => (
              <GridListTile key={battlemap._id} className={classes.gridTile}>
                <Link component={RouterLink} href={`/app/map/${battlemap._id}`}>
                  <img src={battlemap.imageUrl} alt={battlemap.name} />
                  <GridListTileBar title={battlemap.name} />
                </Link>
              </GridListTile>
            ))}
            {knownCharacters.length > 0 && (
              <GridListTile cols={2} style={{ height: 'auto' }}>
                <ListSubheader component="div">Characters</ListSubheader>
              </GridListTile>
            )}
            {knownCharacters.map(char => (
              <GridListTile
                key={char._id}
                onClick={handleClickChar(char)}
                className={classes.gridTile}
              >
                <img src={char.portraitImageUrl} alt={char.name} />
                <GridListTileBar
                  title={char.name}
                  subtitle={char.owner && char.owner.nickname}
                />
              </GridListTile>
            ))}
          </GridList>
        </div>
      </DropZone>
      <MapCreationDialog
        open={!!dropInfo}
        initialDrop={dropInfo}
        onCreate={handleCreateMap}
        onDismiss={handleStopCreateMap}
      />
    </>
  );
}
