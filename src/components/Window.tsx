import {
  createStyles,
  IconButton,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { jssPreset } from '@material-ui/core/styles';
import { Close, OpenInNew } from '@material-ui/icons';
import { create, JSS } from 'jss';
import * as React from 'react';
import { createPortal } from 'react-dom';
import JssProvider from 'react-jss/lib/JssProvider';
import classNames from '../utils/classNames';
import { useMemoize } from '../utils/memoize';
import registerEventHandler from '../utils/registerEventHandler';
import { KeyboardHost } from './hooks/useKeyCombos';

const ResizeObserver = (window as any).ResizeObserver;

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'fixed',
      zIndex: 1,
    },
    resizable: {
      display: 'grid',
      gridAutoColumns: `${theme.spacing.unit / 2}px 1fr ${theme.spacing.unit /
        2}px`,
      gridAutoRows: `${theme.spacing.unit / 2}px auto 1fr ${theme.spacing.unit /
        2}px`,
      gridTemplateAreas: `
      'nw n ne'
       'w t e'
       'w c e'
      'sw s se'`,
    },
    paper: {
      background: theme.palette.background.paper,
      boxShadow: theme.shadows[10],
      '& $toolbar': {
        background: theme.palette.background.default,
      },
    },
    content: {
      gridArea: 'c',
      overflow: 'auto',
      display: 'flex',
      flexDirection: 'column',
    },
    resizeHandle: {},
    toolbar: {
      gridArea: 't',
      display: 'flex',
      flex: '0 0 auto',
      alignItems: 'center',
      '&>*:first-child': {
        flex: '1 1 auto',
        paddingLeft: theme.spacing.unit,
      },
      zIndex: 1,
    },
    spacer: {
      flex: '1 1 auto',
    },
    draggable: {
      cursor: 'move',
    },
  });

export type Position =
  | 'top left'
  | 'top center'
  | 'top right'
  | 'center left'
  | 'center'
  | 'center right'
  | 'bottom left'
  | 'bottom center'
  | 'bottom right';

export interface WindowProps extends WithStyles<typeof styles> {
  open: boolean;
  draggable?: boolean;
  resizable?: boolean;
  detachable?: boolean;
  alignBottom?: boolean;
  paper?: boolean;
  keepPositionAndSize?: string;
  initialWidth?: number;
  initialHeight?: number;
  initialPosition?: Position | { x: number; y: number };
  title?: React.ReactNode;
  children: React.ReactNode;
  onClose?: () => void;
}

export default withStyles(styles)(Window);

const WindowContext = React.createContext({
  focused: [] as Array<{}>,
  focus: (v: {}) => {
    //
  },
});

export function WindowStack({ children }: { children: React.ReactNode }) {
  const [focused, focus] = React.useReducer(
    (state: Array<{}>, nxt: {}) => [...state.filter(d => d !== nxt), nxt],
    [],
  );
  return (
    <WindowContext.Provider value={{ focused, focus }}>
      {children}
    </WindowContext.Provider>
  );
}

function Window({
  open,
  classes,
  initialWidth,
  initialHeight,
  initialPosition = 'top left',
  keepPositionAndSize,
  draggable,
  detachable,
  resizable,
  alignBottom,
  paper,
  onClose,
  title,
  children,
}: WindowProps) {
  const wnd = React.useRef({});
  const { focus, focused } = React.useContext(WindowContext);
  const handleFocus = React.useCallback(() => focus(wnd.current), [focus]);

  React.useEffect(() => {
    if (open) {
      focus(wnd.current);
    }
  }, [focus, open, wnd]);

  const containerRef = useRefLayoutEffect<HTMLDivElement>(() => {
    const { width, height } = containerRef.current!.getBoundingClientRect();
    const {
      left: parentLeft,
      top: parentTop,
      width: parentWidth,
      height: parentHeight,
    } = containerRef.current!.parentElement!.getBoundingClientRect();

    if (!positionAndSize) {
      let left = 0;
      let top = 0;
      if (typeof initialPosition === 'string') {
        const [vertical, horizontal = 'center'] = initialPosition.split(' ');
        switch (vertical) {
          case 'center':
            top = parentHeight / 2 - height / 2;
            break;
          case 'bottom':
            top = parentHeight - height;
            break;
          default:
            break;
        }
        switch (horizontal) {
          case 'center':
            left = parentWidth / 2 - width / 2;
            break;
          case 'right':
            left = parentWidth - width;
            break;
          default:
            break;
        }
      } else {
        left = initialPosition.x;
        top = initialPosition.y;
      }
      updatePositionAndSize({
        left,
        top,
        width: initialWidth || width,
        height: initialHeight || height,
      });
    }

    updateConstraints({ maxWidth: parentWidth, maxHeight: parentHeight });
    updateOffset({ left: parentLeft, top: parentTop });

    if (ResizeObserver) {
      const observer = new ResizeObserver(([{ target, contentRect }]: any) => {
        const { left, top } = target.getBoundingClientRect();
        updateConstraints({
          maxWidth: contentRect.width,
          maxHeight: contentRect.height,
        });
        updateOffset({ left, top });
      });
      observer.observe(containerRef.current!.parentElement);
      return () => {
        observer.disconnect();
      };
    }
    return;
  });
  const [positionAndSize, updatePositionAndSize] = React.useState(
    (keepPositionAndSize
      ? JSON.parse(
          localStorage.getItem(`aiacta.window.${keepPositionAndSize}`) ||
            'null',
        )
      : null) as null | {
      left: number;
      top: number;
      width: number;
      height: number;
    },
  );
  const [constraints, updateConstraints] = React.useState(null as null | {
    maxWidth: number;
    maxHeight: number;
  });
  const [offset, updateOffset] = React.useState(null as null | {
    left: number;
    top: number;
  });
  React.useEffect(() => {
    if (positionAndSize && keepPositionAndSize) {
      localStorage.setItem(
        `aiacta.window.${keepPositionAndSize}`,
        JSON.stringify(positionAndSize),
      );
    }
  }, [keepPositionAndSize, positionAndSize]);
  const [drag, updateDrag] = React.useState(null as null | {
    clientX: number;
    clientY: number;
  });
  const [resize, updateResize] = React.useState(null as null | {
    clientX: number;
    clientY: number;
    xMod: 0 | 1 | -1;
    yMod: 0 | 1 | -1;
  });

  React.useEffect(() => {
    if (!draggable || !drag || !constraints) {
      return;
    }
    let lastPos = drag;
    return registerEventHandler(window, 'pointermove', evt => {
      evt.preventDefault();
      const dX = evt.clientX - lastPos.clientX;
      const dY = evt.clientY - lastPos.clientY;
      updatePositionAndSize(
        s =>
          constraints &&
          s && {
            ...s,
            left: Math.max(
              0,
              Math.min(constraints.maxWidth - s.width, s.left + dX),
            ),
            top: Math.max(
              0,
              Math.min(constraints.maxHeight - s.height, s.top + dY),
            ),
          },
      );
      lastPos = { clientX: evt.clientX, clientY: evt.clientY };
    });
  }, [draggable, drag, constraints]);
  React.useEffect(() => {
    if (!resizable || !resize || !constraints) {
      return;
    }
    let lastPos = resize;
    const { xMod, yMod } = resize;
    return registerEventHandler(window, 'pointermove', evt => {
      evt.preventDefault();
      const dX = (evt.clientX - lastPos.clientX) * Math.abs(xMod);
      const dY = (evt.clientY - lastPos.clientY) * Math.abs(yMod);
      updatePositionAndSize(s => {
        return (
          constraints &&
          s && {
            ...s,
            left:
              xMod < 0
                ? Math.max(
                    0,
                    Math.min(constraints.maxWidth - s.width, s.left + dX),
                  )
                : s.left,
            width: Math.max(
              50,
              Math.min(constraints.maxWidth, s.width + dX * xMod),
            ),
            top:
              yMod < 0
                ? Math.max(
                    0,
                    Math.min(constraints.maxHeight - s.height, s.top + dY),
                  )
                : s.top,
            height: Math.max(
              50,
              Math.min(constraints.maxHeight, s.height + dY * yMod),
            ),
          }
        );
      });
      lastPos = { ...lastPos, clientX: evt.clientX, clientY: evt.clientY };
    });
  }, [resizable, resize, constraints]);
  React.useEffect(() => {
    return registerEventHandler(window, 'pointerup', () => {
      updateDrag(null);
      updateResize(null);
    });
  }, []);

  const onStartDrag = React.useCallback((evt: React.PointerEvent) => {
    evt.preventDefault();
    updateDrag({
      clientX: evt.clientX,
      clientY: evt.clientY,
    });
  }, []);

  const onStartResize = useMemoize(
    (dir: string) => (evt: React.PointerEvent) => {
      const xMod = dir.includes('e') ? 1 : dir.includes('w') ? -1 : 0;
      const yMod = dir.includes('s') ? 1 : dir.includes('n') ? -1 : 0;
      evt.preventDefault();
      updateResize({
        clientX: evt.clientX,
        clientY: evt.clientY,
        xMod,
        yMod,
      });
    },
  );

  const [[detached, jss], updateDetached] = React.useState([
    null,
    undefined,
  ] as [null | HTMLElement, undefined | JSS]);

  const onDetach = React.useCallback(() => {
    if (containerRef.current) {
      const {
        top,
        left,
        width,
        height,
      } = containerRef.current.getBoundingClientRect();
      const popout = window.open(
        undefined,
        '_blank',
        [
          `top=${top + 70}`,
          `left=${left}`,
          `width=${width}`,
          `height=${height}`,
        ].join(','),
      );
      if (popout) {
        copyStyles(window.document, popout.document);
        if (typeof title === 'string') {
          popout.document.title = title;
        }
        const elem = popout.document.body.appendChild(
          popout.document.createElement('div'),
        );
        const insertionPoint = popout.document.createElement('noscript');
        popout.document.head.insertBefore(
          insertionPoint,
          popout.document.head.firstChild,
        );
        const disposeUnload = registerEventHandler(window, 'unload', () =>
          popout.close(),
        );
        popout.onunload = () => {
          disposeUnload();
          updateDetached([null, undefined]);
        };

        updateDetached([
          elem,
          create({
            ...jssPreset(),
            insertionPoint,
          }),
        ]);
      }
    }
  }, [containerRef, title]);

  if (!open) {
    return null;
  }

  if (detached) {
    return createPortal(
      <JssProvider jss={jss}>
        <KeyboardHost window={detached.ownerDocument!.defaultView!}>
          <div className={classes.content} style={{ height: '100vh' }}>
            {alignBottom && <div className={classes.spacer} />}
            {children}
          </div>
        </KeyboardHost>
      </JssProvider>,
      detached,
    );
  }

  return (
    <div
      ref={containerRef}
      className={classNames(
        classes.container,
        resizable && classes.resizable,
        paper && classes.paper,
        'window-content',
      )}
      style={{
        maxWidth: '100%',
        maxHeight: '100%',
        ...constraints,
        ...positionAndSize,
        ...(positionAndSize && offset
          ? {
              left: positionAndSize.left + offset.left,
              top: positionAndSize.top + offset.top,
            }
          : {}),
        zIndex: 1200 + focused.indexOf(wnd.current),
      }}
      onPointerDown={handleFocus}
    >
      {resizable &&
        ['nw', 'n', 'ne', 'w', 'e', 'sw', 's', 'se'].map(dir => (
          <div
            key={dir}
            style={{
              cursor: `${dir}-resize`,
              gridRow: dir === 'w' || dir === 'e' ? 'span 2' : undefined,
            }}
            className={classNames(
              classes.resizeHandle,
              classes[dir as keyof typeof classes],
            )}
            onPointerDown={onStartResize(dir)}
          />
        ))}
      <div className={classes.toolbar}>
        <Typography
          variant="h6"
          className={classNames(draggable && classes.draggable)}
          {...(draggable ? { onPointerDown: onStartDrag } : {})}
        >
          {title}
        </Typography>
        {detachable && (
          <IconButton
            color="inherit"
            aria-label="Detach into new window"
            onClick={onDetach}
          >
            <OpenInNew fontSize="small" />
          </IconButton>
        )}
        {onClose && (
          <IconButton
            color="inherit"
            aria-label="Close window"
            onClick={onClose}
          >
            <Close fontSize="small" />
          </IconButton>
        )}
      </div>
      <div className={classes.content}>
        {alignBottom && <div className={classes.spacer} />}
        {children}
      </div>
    </div>
  );
}

function isCSSStyleSheet(d: StyleSheet): d is CSSStyleSheet {
  return 'cssRules' in d;
}
function isHTMLNode(d: Node): d is HTMLElement {
  return 'dataset' in d;
}

function copyStyles(sourceDoc: Document, targetDoc: Document) {
  Array.from(sourceDoc.styleSheets).forEach(styleSheet => {
    if (isCSSStyleSheet(styleSheet)) {
      // for <style> elements
      const newStyleEl = sourceDoc.createElement('style');
      Array.from(styleSheet.cssRules).forEach(cssRule => {
        // write the text of each rule into the body of the style element
        newStyleEl.appendChild(
          sourceDoc.createTextNode((cssRule as any).cssText),
        );
      });
      const ownerNode = styleSheet.ownerNode;
      if (isHTMLNode(ownerNode)) {
        Object.keys(ownerNode.dataset).forEach(
          key => (newStyleEl.dataset[key] = ownerNode.dataset[key]),
        );
      }

      targetDoc.head.appendChild(newStyleEl);
    } else if (styleSheet.href) {
      // for <link> elements loading CSS from a URL
      const newLinkEl = sourceDoc.createElement('link');
      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = styleSheet.href;
      targetDoc.head.appendChild(newLinkEl);
    }
  });
}

function useRefLayoutEffect<T extends HTMLElement>(
  effect: React.EffectCallback,
) {
  const ref = React.useRef(null as T | null);
  const [used, updateUsed] = React.useState(null as number | null);
  const random = Math.random();
  React.useLayoutEffect(() => {
    if (ref.current) {
      updateUsed(random);
      return effect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [used === null ? random : used]);
  return ref;
}
