import {
  Button,
  createStyles,
  Input,
  Theme,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import { Send } from '@material-ui/icons';
import marksy from 'marksy/jsx';
import { reaction } from 'mobx';
import * as React from 'react';
import { ChatMessageModel } from '../stores/models';
import { chatMessages } from '../stores/modules/dataStores';
import { sendChatMessage } from '../stores/modules/gameActions';
import { myself } from '../stores/modules/gameData';
import classNames from '../utils/classNames';
import { useMemoize } from '../utils/memoize';
import Attack from './chatElements/Attack';
import Damage from './chatElements/Damage';
import Die from './chatElements/Die';

const styles = (theme: Theme) =>
  createStyles({
    group: {
      display: 'grid',
      gridTemplateRows: 'auto',
      gridTemplateColumns: 'auto minmax(min-content, 80%) 1fr',
      '&$myself': {
        gridTemplateColumns: '1fr minmax(min-content, 80%) auto',
      },
    },
    sender: {
      gridColumn: '1',
      gridRow: 'span 10000',
      alignSelf: 'flex-end',
      borderRadius: theme.shape.borderRadius,
      background: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      padding: theme.spacing.unit / 2,
      margin: theme.spacing.unit / 2,
    },
    message: {
      gridColumn: '2',
      justifySelf: 'flex-start',
      borderRadius: theme.shape.borderRadius * 3,
      background: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText,
      padding: theme.spacing.unit / 2,
      margin: theme.spacing.unit / 2,
    },
    myself: {
      '& $sender': {
        gridColumn: '3',
      },
      '& $message': {
        gridColumn: '2',
        justifySelf: 'flex-end',
      },
    },
    game: {
      '& $message': {
        gridColumn: 'span 3',
        background: 'none',
        color: theme.palette.text.primary,
        fontStyle: 'italic',
      },
    },
    messageBar: {
      display: 'flex',
      position: 'sticky',
      bottom: 0,
      background: theme.palette.background.paper,
      borderTop: `1px solid ${theme.palette.divider}`,
      flex: '0 0 auto',
    },
    input: {
      flex: '1 1 auto',
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
  });

export interface ChatProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Chat);

const compile = marksy({
  createElement: React.createElement,
  components: {
    Wrapper: props => props.children,
    Die,
    Attack,
    Damage,
  },
});

function Chat({ classes }: ChatProps) {
  const compiled = useMemoize((text: string) => compile(text).tree);
  const [input, updateInput] = React.useState('');
  const onChangeInput = React.useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
      updateInput(value);
    },
    [],
  );
  const onSubmit = React.useCallback(
    (evt: React.FormEvent) => {
      evt.preventDefault();
      sendChatMessage(input);
      updateInput('');
    },
    [input],
  );
  const formRef = React.useRef(null as HTMLFormElement | null);
  const scrolledToBottom = React.useRef(false);
  const [groups, updateGroups] = React.useState(
    chatMessages.tail(30).reduce(toMessageGroups, []),
  );
  React.useEffect(
    () =>
      reaction(
        () => chatMessages.tail(10),
        nextMsgs => {
          if (formRef.current) {
            const parent = formRef.current.parentElement!;
            const { height } = parent.getBoundingClientRect();
            scrolledToBottom.current =
              Math.round(parent.scrollTop + height) >= parent.scrollHeight;
          }
          updateGroups(
            nextMsgs
              .filter(
                msg => !groups.some(grp => grp.some(m => m._id === msg._id)),
              )
              .reduce(toMessageGroups, groups),
          );
        },
      ),
    [groups],
  );
  React.useLayoutEffect(() => {
    if (formRef.current) {
      const parent = formRef.current.parentElement!;
      const { height } = parent.getBoundingClientRect();
      if (scrolledToBottom.current) {
        parent.scrollTop = parent.scrollHeight - Math.floor(height);
      }
    }
  }, [groups]);
  React.useLayoutEffect(() => {
    if (formRef.current) {
      const parent = formRef.current.parentElement!;
      const { height } = parent.getBoundingClientRect();
      parent.scrollTop = parent.scrollHeight - Math.floor(height);
      scrolledToBottom.current = true;
    }
  }, []);

  return (
    <>
      {groups.map(msgs => (
        <Typography
          key={`${msgs[0].timestamp}:${msgs[0].document.sender}`}
          className={classNames(
            classes.group,
            !msgs[0].document.sender && classes.game,
            msgs[0].document.sender === myself()._id && classes.myself,
          )}
        >
          {msgs[0].sender ? (
            <span className={classes.sender}>{msgs[0].sender.nickname}</span>
          ) : null}
          {msgs.map(msg => (
            <span key={msg._id} className={classes.message}>
              {compiled(`<Wrapper>${msg.text}</Wrapper>`)}
            </span>
          ))}
        </Typography>
      ))}
      <form ref={formRef} onSubmit={onSubmit} className={classes.messageBar}>
        <Input
          value={input}
          onChange={onChangeInput}
          className={classes.input}
          placeholder="What would you like to say?"
        />
        <Button type="submit" variant="contained" color="primary" size="small">
          Speak
          <Send className={classes.rightIcon} />
        </Button>
      </form>
    </>
  );
}

function toMessageGroups(acc: ChatMessageModel[][], msg: ChatMessageModel) {
  if (acc.length === 0) {
    return [[msg]];
  }
  if (acc.slice(-1)[0][0].document.sender === msg.document.sender) {
    return [...acc.slice(0, -1), [...acc.slice(-1)[0], msg]];
  }
  return [...acc, [msg]];
}
