import {
  Button,
  createStyles,
  IconButton,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Delete, Edit } from '@material-ui/icons';
import { action } from 'mobx';
import * as React from 'react';
import uuid from 'uuid/v4';
import { AccessType, attributes, MonsterModel } from '../stores/models';
import { myself } from '../stores/modules/gameData';
import { useMemoize } from '../utils/memoize';
import ActionableContent from './ActionableContent';
import { DropInfo } from './DropZone';
import ImageInput from './ImageInput';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      maxWidth: 400,
    },
    nameLine: {
      display: 'flex',
      '&>h6': {
        flex: '1 1 auto',
      },
    },
    statLine: {
      display: 'flex',
    },
    statName: {
      marginRight: theme.spacing.unit,
      flex: '0 0 auto',
      fontWeight: 'bold',
    },
    attrLine: {
      display: 'grid',
      gridAutoFlow: 'column',
      gridTemplateRows: 'auto auto',
      gridAutoColumns: '1fr',
      textAlign: 'center',
      gridGap: theme.spacing.unit,
    },
    abilLine: {
      display: 'flex',
      marginBottom: theme.spacing.unit,
      '&>div': {
        flex: '1 1 auto',
      },
    },
    abilName: {
      float: 'left',
      flex: '0 0 auto',
      marginRight: theme.spacing.unit,
      fontWeight: 'bold',
    },
    errorBadge: {
      top: -1,
      right: -theme.spacing.unit * 3,
    },
  });

export interface MonsterSheetProps extends WithStyles {
  model: MonsterModel;
}

export default withStyles(styles)(MonsterSheet);

// const getViewModel = memoize(createViewModel);

function MonsterSheet({ model, classes }: MonsterSheetProps) {
  // const model = getViewModel<MonsterModel>(model);

  const [dieNr, dieSize] = model.hitDice.split('d');
  const hpBonus = model.modifiers.constitution * +dieNr;
  const hpAverage = +dieNr * (+dieSize / 2) + hpBonus;

  const canBeEdited = myself().isAllowedTo(AccessType.Write, model);

  const [editing, updateEditing] = React.useState(false);
  const handleToggleEdit = React.useCallback(() => updateEditing(s => !s), []);

  const handleAddAbility = React.useCallback(() => {
    model.specialAbilities = [
      ...model.specialAbilities,
      { key: uuid(), name: '', description: '' },
    ];
  }, [model]);
  const handleRemoveAbility = useMemoize(
    ability => () => {
      model.specialAbilities = model.specialAbilities.filter(
        a => a !== ability,
      );
    },
    [model],
  );

  const handleRemoveAction = useMemoize(
    act => () => {
      model.actions = model.actions.filter(a => a !== act);
    },
    [model],
  );

  const handleChangeTokenImage = React.useCallback(
    (dropInfo?: DropInfo) => {
      model.tokenImage = dropInfo ? dropInfo.file : undefined;
    },
    [model],
  );

  // const coerceToAttributes = useMemoize((attr: string) => (val: string) => {
  //       const value = coerceToPositiveNumber(val);
  //       return {
  //         ...model.attributes,
  //         [attr]: value,
  //       };
  //     },
  //   [model],
  // );
  // const coerceToSaves = React.useMemo(
  //   () => (val: string) => {
  //     if (val === 'none' || !val) {
  //       return Object.getOwnPropertyNames(model.saves).reduce(
  //         (acc, k) => ({ ...acc, [k]: SkillProficiency.None }),
  //         {},
  //       ) as typeof model.saves;
  //     }
  //     const regex = /([a-z][a-z ]{2,}) ([+-]\d+)/gi;
  //     let match;
  //     const abbrevs = Object.getOwnPropertyNames(model.saves).reduce(
  //       (acc, attr) => ({ ...acc, [attr.substr(0, 3)]: attr }),
  //       {},
  //     );
  //     const setSaves: Partial<typeof model.saves> = {};
  //     // tslint:disable-next-line:no-conditional-assignment
  //     while ((match = regex.exec(val))) {
  //       const [, name, value] = match;
  //       const attr = abbrevs[name.substr(0, 3).toLowerCase()];
  //       const mod = +value;
  //       if (!attr) {
  //         // tslint:disable-next-line:no-string-throw
  //         throw `Unrecognized attribute '${name}'.`;
  //       }
  //       if (isNaN(mod)) {
  //         // tslint:disable-next-line:no-string-throw
  //         throw `Modifier for skill ${name} has to be a number.`;
  //       }
  //       const prof =
  //         (mod - model.modifiers[attr]) / model.proficiencyBonus;
  //       switch (prof) {
  //         case SkillProficiency.Proficient:
  //         case SkillProficiency.Expertise:
  //           break;
  //         default:
  //           // tslint:disable-next-line:no-string-throw
  //           throw `Modifier for skill ${name} has to be a valid proficient modulo.`;
  //       }
  //       setSaves[attr] = prof;
  //     }
  //     return {
  //       ...model.saves,
  //       ...setSaves,
  //     };
  //   },
  //   [model],
  // );
  // const coerceToSkills = React.useMemo(
  //   () => (val: string) => {
  //     if (val === 'none' || !val) {
  //       return Object.getOwnPropertyNames(model.skills).reduce(
  //         (acc, k) => ({ ...acc, [k]: SkillProficiency.None }),
  //         {},
  //       ) as typeof model.skills;
  //     }
  //     const regex = /([a-z][a-z ]+) ([+-]\d+)/gi;
  //     let match;
  //     const setSkills: Partial<typeof model.skills> = {};
  //     // tslint:disable-next-line:no-conditional-assignment
  //     while ((match = regex.exec(val))) {
  //       const [, name, value] = match;
  //       const skill = decapitalize(name.replace(/ ([A-Z])/g, '$1'));
  //       const mod = +value;
  //       if (!(skill in model.skills)) {
  //         // tslint:disable-next-line:no-string-throw
  //         throw `Unrecognized skill '${skill}'.`;
  //       }
  //       if (isNaN(mod)) {
  //         // tslint:disable-next-line:no-string-throw
  //         throw `Modifier for skill ${skill} has to be a number.`;
  //       }
  //       const prof =
  //         (mod - model.modifiers[skillMod[skill]]) /
  //         model.proficiencyBonus;
  //       switch (prof) {
  //         case SkillProficiency.Proficient:
  //         case SkillProficiency.Expertise:
  //           break;
  //         default:
  //           // tslint:disable-next-line:no-string-throw
  //           throw `Modifier for skill ${skill} has to be a valid proficient modulo.`;
  //       }
  //       setSkills[skill] = prof;
  //     }
  //     return {
  //       ...model.skills,
  //       ...setSkills,
  //     };
  //   },
  //   [model],
  // );
  // const coerceToAbilities = useMemoize((key: string, prop: string) => (val: string) => {
  //       return model.specialAbilities.map(ab =>
  //         ab.key !== key
  //           ? ab
  //           : {
  //               ...ab,
  //               [prop]: val,
  //             },
  //       );
  //     },
  //   [model],
  // );
  // const coerceToActions = useMemoize((key: string, prop: string) => (val: string) => {
  //       return model.actions.map(ac =>
  //         ac.key !== key
  //           ? ac
  //           : {
  //               ...ac,
  //               [prop]: val,
  //             },
  //       );
  //     },
  //   [model],
  // );

  return (
    <div className={classes.container}>
      <div className={classes.nameLine}>
        <Typography noWrap variant="h6">
          {editing ? (
            <EditableContent2 model={model} property="name" />
          ) : (
            model.name
          )}
        </Typography>
        {canBeEdited && (
          <IconButton
            color={editing ? 'primary' : 'default'}
            onClick={handleToggleEdit}
          >
            <Edit fontSize="small" />
          </IconButton>
        )}
      </div>
      <Typography noWrap>
        <em>
          {model.size}{' '}
          {editing ? (
            <EditableContent2 model={model} property="mtype" />
          ) : (
            model.mtype
          )}
          {editing ? (
            <>
              {' '}
              (
              <EditableContent2 model={model} property="subtype" />)
            </>
          ) : model.subtype ? (
            ` (${model.subtype})`
          ) : (
            ''
          )}
          ,{' '}
          {editing ? (
            <EditableContent2 model={model} property="alignment" />
          ) : (
            model.alignment
          )}
        </em>
      </Typography>
      <hr />
      <div className={classes.statLine}>
        <Typography noWrap className={classes.statName}>
          Armor Class
        </Typography>
        <Typography>{model.armorClass}</Typography>
      </div>
      <div className={classes.statLine}>
        <Typography noWrap className={classes.statName}>
          Hit Points
        </Typography>
        <Typography>
          {hpAverage} ({model.hitDice}
          {hpBonus > 0 ? ` + ${hpBonus}` : ''})
        </Typography>
      </div>
      <div className={classes.statLine}>
        <Typography noWrap className={classes.statName}>
          Speed
        </Typography>
        <Typography>
          {
            'TODO'
            // [
            //   `${model.speed.walk}ft`,
            //   ...['swim', 'climb', 'hover', 'fly']
            //     .filter(k => model.speed[k] > 0)
            //     .map(k => `${k} ${model.speed[k]}ft`),
            // ].join(', ')
          }
        </Typography>
      </div>
      <hr />
      <div className={classes.attrLine}>
        {attributes.map(attr => (
          <React.Fragment key={attr}>
            <Typography noWrap key={attr} className={classes.attrName}>
              {attr.substr(0, 3).toUpperCase()}
            </Typography>
            <Typography noWrap />
          </React.Fragment>
        ))}
      </div>
      <hr />
      {editing && (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Saving Throws
          </Typography>
        </div>
      )}
      {editing ? (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Skills
          </Typography>
        </div>
      ) : (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Skills
          </Typography>
        </div>
      )}
      {editing ? (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Damage Resistances
          </Typography>
          <Typography>
            <EditableContent2 model={model} property="resistances" />
          </Typography>
        </div>
      ) : (
        model.resistances.length > 0 && (
          <div className={classes.statLine}>
            <Typography noWrap className={classes.statName}>
              Damage Resistances
            </Typography>
            <Typography>{model.resistances}</Typography>
          </div>
        )
      )}
      {editing ? (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Damage Immunities
          </Typography>
          <Typography>
            <EditableContent2 model={model} property="immunities" />
          </Typography>
        </div>
      ) : (
        model.immunities.length > 0 && (
          <div className={classes.statLine}>
            <Typography noWrap className={classes.statName}>
              Damage Immunities
            </Typography>
            <Typography>{model.immunities}</Typography>
          </div>
        )
      )}
      {editing ? (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Condition Immunities
          </Typography>
          <Typography>
            <EditableContent2 model={model} property="conditionImmunities" />
          </Typography>
        </div>
      ) : (
        model.conditionImmunities.length > 0 && (
          <div className={classes.statLine}>
            <Typography noWrap className={classes.statName}>
              Condition Immunities
            </Typography>
            <Typography>{model.conditionImmunities}</Typography>
          </div>
        )
      )}
      <div className={classes.statLine}>
        <Typography noWrap className={classes.statName}>
          Senses
        </Typography>
        <Typography />
      </div>
      {editing ? (
        <div className={classes.statLine}>
          <Typography noWrap className={classes.statName}>
            Languages
          </Typography>
          <Typography>
            <EditableContent2 model={model} property="languages" />
          </Typography>
        </div>
      ) : (
        model.languages.length > 0 && (
          <div className={classes.statLine}>
            <Typography noWrap className={classes.statName}>
              Languages
            </Typography>
            <Typography>{model.languages}</Typography>
          </div>
        )
      )}
      <div className={classes.statLine}>
        <Typography noWrap className={classes.statName}>
          Challenge
        </Typography>
        <Typography>
          {`${model.challengeRating} (${model.xpWorth} XP)`}
        </Typography>
      </div>
      <hr />
      {editing
        ? model.specialAbilities.map(ability => (
            <div key={ability.key} className={classes.abilLine}>
              <div>
                <Typography
                  noWrap
                  className={classes.abilName}
                  component="span"
                >
                  .
                </Typography>
                <Typography component="span" />
              </div>
              <IconButton
                color="primary"
                onClick={handleRemoveAbility(ability)}
              >
                <Delete />
              </IconButton>
            </div>
          ))
        : model.specialAbilities.map(ability => (
            <div key={ability.key} className={classes.abilLine}>
              <div>
                <Typography
                  noWrap
                  className={classes.abilName}
                  component="span"
                >
                  {ability.name}.
                </Typography>
                <Typography component="span">{ability.description}</Typography>
              </div>
            </div>
          ))}
      {editing && (
        <Button color="primary" onClick={handleAddAbility}>
          Add Ability
        </Button>
      )}
      <hr />
      {editing
        ? model.actions.map(ac => (
            <div key={ac.key} className={classes.abilLine}>
              <div>
                <Typography
                  noWrap
                  className={classes.abilName}
                  component="span"
                >
                  .
                </Typography>
                <Typography component="span" />
              </div>
              <IconButton color="primary" onClick={handleRemoveAction(action)}>
                <Delete />
              </IconButton>
            </div>
          ))
        : model.actions.map(ac => (
            <div key={ac.key} className={classes.abilLine}>
              <div>
                <Typography
                  noWrap
                  className={classes.abilName}
                  component="span"
                >
                  {ac.name}.
                </Typography>
                <Typography component="span">
                  <ActionableContent instigatorId={model._id} actionlike={ac} />
                </Typography>
              </div>
            </div>
          ))}
      {editing && (
        <ImageInput
          value={model.tokenImage}
          onChange={handleChangeTokenImage}
          allowCrop
          cropRatio={1}
        />
      )}
    </div>
  );
}

const EditableContent2 = (props: any) => null;
