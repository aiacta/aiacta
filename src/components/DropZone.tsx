import {
  createStyles,
  Omit,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import classNames from '../utils/classNames';
import registerEventHandler from '../utils/registerEventHandler';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      '&:before': {
        content: '""',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderWidth: 4,
        borderRadius: 8,
        borderStyle: 'dashed',
        borderColor: 'transparent',
        transition: theme.transitions.create('border-color'),
        pointerEvents: 'none',
      },
    },
    target: {
      '&:before': {
        borderColor: 'rgba(42, 42, 213, .4)',
      },
    },
    active: {
      '&:before': {
        borderColor: 'rgba(42, 213, 42, .4)',
      },
    },
    hint: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: 8,
      transition: theme.transitions.create('background'),
      background: 'rgba(42, 42, 213, 0)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      pointerEvents: 'none',
    },
    hintTarget: {
      background: 'rgba(42, 42, 213, 0.2)',
    },
    hintActive: {
      background: 'rgba(42, 213, 42, 0.2)',
    },
  });

export interface DropZoneProps
  extends WithStyles<typeof styles>,
    Omit<React.HTMLProps<HTMLDivElement>, 'onDrop'> {
  multiple?: boolean;
  allowPaste?: boolean;
  accepts?: string | string[];
  onDrop?: (evt: React.DragEvent<HTMLElement>) => void;
  onDropImages?: (
    accepted: DropInfo | DropInfo[],
    evt?: React.DragEvent<HTMLElement>,
  ) => void;
}

export interface DropInfo {
  readonly file: File;
  readonly preview: string;
  readonly image?: {
    readonly width: number;
    readonly height: number;
  };
}

export default withStyles(styles)(DropZone);

const dropZoneContext = React.createContext({
  isDragging: false,
  types: [] as ReadonlyArray<string>,
});

function DropZone({
  allowPaste,
  multiple,
  accepts,
  onDrop,
  onDropImages,
  children,
  onDragOver,
  classes,
  ...rest
}: DropZoneProps) {
  const [active, updateActive] = React.useState(false);
  const { isDragging, types: dragTypes } = React.useContext(dropZoneContext);

  const handleFiles = React.useCallback(
    (files: FileList, evt?: React.DragEvent<HTMLElement>) => {
      if (onDropImages) {
        Promise.all(
          Array.from(files)
            .filter(
              file =>
                !accepts ||
                (Array.isArray(accepts)
                  ? accepts.some(t => !!file.type.match(new RegExp(`^${t}`)))
                  : file.type.match(new RegExp(`^${accepts}`))),
            )
            .slice(0, multiple ? undefined : 1)
            .map(async file => {
              const preview = window.URL.createObjectURL(file);
              return {
                preview,
                file,
                ...(file.type.match(/^image\/.*$/)
                  ? { image: await resolveImageProperties(preview) }
                  : {}),
              };
            }),
        ).then(accepted => {
          if (!multiple && accepted.length > 1) {
            accepted.splice(1, accepted.length - 1);
          }

          onDropImages(multiple ? accepted : accepted[0], evt);
        });
      }
    },
    [onDropImages, multiple, accepts],
  );
  const handlePaste = React.useCallback(
    (evt: React.ClipboardEvent<HTMLDivElement>) => {
      if (evt.clipboardData.files.length > 0) {
        handleFiles(evt.clipboardData.files);
      }
    },
    [handleFiles],
  );
  const handleDrop = React.useCallback(
    (evt: React.DragEvent<HTMLDivElement>) => {
      evt.preventDefault();
      evt.persist();

      updateActive(false);

      if (evt.dataTransfer.files.length > 0) {
        handleFiles(evt.dataTransfer.files, evt);
      }

      if (onDrop) {
        onDrop(evt);
      }
    },
    [handleFiles, onDrop],
  );

  const handleDragEnter = React.useCallback(
    (evt: React.DragEvent<HTMLDivElement>) => {
      evt.preventDefault();
      updateActive(true);
    },
    [],
  );
  const handleDragLeave = React.useCallback(() => updateActive(false), []);
  const handleDragOver = React.useCallback(
    (evt: React.DragEvent<HTMLDivElement>) => {
      evt.preventDefault();
      evt.stopPropagation();
      evt.dataTransfer.dropEffect =
        onDrop || (onDropImages && evt.dataTransfer.types.includes('Files'))
          ? 'copy'
          : 'none';
      if (onDragOver) {
        onDragOver(evt);
      }
      if (!active && evt.dataTransfer.dropEffect !== 'none') {
        updateActive(true);
      }
      if (active && evt.dataTransfer.dropEffect === 'none') {
        updateActive(false);
      }
      return false;
    },
    [onDrop, onDropImages, onDragOver, active],
  );

  const acceptedList = [
    ...(onDropImages ? ['Files'] : []),
    ...(accepts ? (Array.isArray(accepts) ? accepts : [accepts]) : []),
  ];
  const acceptsCurrent =
    acceptedList.length === 0 ||
    acceptedList.some(t => {
      const regex = new RegExp(`^${t}`);
      return dragTypes.some(d => !!d.match(regex));
    });

  return (
    <div
      {...rest}
      onDragEnter={handleDragEnter}
      onDragOver={handleDragOver}
      onDragLeave={handleDragLeave}
      onDrop={handleDrop}
      {...(allowPaste ? { onPaste: handlePaste } : {})}
      className={classNames(
        classes.container,
        isDragging && acceptsCurrent && classes.target,
        active && classes.active,
        rest.className,
      )}
    >
      <div
        className={classNames(
          classes.hint,
          isDragging && acceptsCurrent && classes.hintTarget,
          active && classes.hintActive,
        )}
      />
      {children}
    </div>
  );
}

async function resolveImageProperties(src: string) {
  const image = new Image();
  image.src = src;
  return new Promise<{ width: number; height: number }>((resolve, reject) => {
    image.onload = () => {
      resolve({ width: image.width, height: image.height });
    };
  });
}

export const Context = ({ children }: { children: React.ReactNode }) => {
  const [ctx, updateCtx] = React.useState({
    isDragging: false,
    types: [] as ReadonlyArray<string>,
  });
  React.useEffect(() => {
    const disposer = [
      registerEventHandler(window, 'dragenter', evt => {
        updateCtx({ isDragging: true, types: evt.dataTransfer!.types });
      }),
      registerEventHandler(window, 'dragover', evt => {
        evt.preventDefault();
        evt.stopPropagation();
        evt.dataTransfer!.dropEffect = 'none';
        return false;
      }),
      registerEventHandler(window, 'dragexit', () =>
        updateCtx({ isDragging: false, types: [] }),
      ),
      registerEventHandler(window, 'dragend', () =>
        updateCtx({ isDragging: false, types: [] }),
      ),
      registerEventHandler(window, 'drop', () =>
        updateCtx({ isDragging: false, types: [] }),
      ),
      registerEventHandler(window, 'dragleave', evt => {
        if (!evt.clientX && !evt.clientY) {
          updateCtx({ isDragging: false, types: [] });
        }
      }),
    ];
    return () => disposer.forEach(d => d());
  }, []);
  return (
    <dropZoneContext.Provider value={ctx}>{children}</dropZoneContext.Provider>
  );
};
