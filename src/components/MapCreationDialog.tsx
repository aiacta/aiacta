import {
  Button,
  createStyles,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormLabel,
  Input,
  InputAdornment,
  InputLabel,
  Radio,
  RadioGroup,
  Theme,
  Tooltip,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import { Info } from '@material-ui/icons';
import { Vector2 } from 'babylonjs';
import * as React from 'react';
import { IBattlemap, IWorld } from '../stores/models';
import { useMemoize } from '../utils/memoize';
import { DropInfo } from './DropZone';
import ImageInput from './ImageInput';

const styles = (theme: Theme) =>
  createStyles({
    grid: {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr',
      gridGap: `${theme.spacing.unit}px`,
      margin: theme.spacing.unit * 2,
    },
    inputSpan: {
      gridColumn: 'span 2',
    },
    measureInputs: {
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
    },
  });

export interface MapCreationDialogProps extends WithStyles<typeof styles> {
  open: boolean;
  initialDrop?: DropInfo;
  noWorld?: boolean;
  onCreate: (d: IWorld | IBattlemap) => void;
  onDismiss: () => void;
}

export default withStyles(styles)(MapCreationDialog);

function MapCreationDialog({
  open,
  noWorld,
  classes,
  onCreate,
  onDismiss,
  initialDrop,
}: MapCreationDialogProps) {
  const [inputs, updateInputs] = React.useState({
    name: 'A new world',
    type: noWorld ? 'battlemap' : 'world',
    width: initialDrop && initialDrop.image ? initialDrop.image.width : 10,
    height: initialDrop && initialDrop.image ? initialDrop.image.height : 10,
    milesPerPixel: 1,
    measuredPixel: 0,
    measuredMiles: 0,
  });
  const {
    name,
    type,
    width,
    height,
    milesPerPixel,
    measuredPixel,
    measuredMiles,
  } = inputs;

  const handleInputChange = useMemoize(
    (prop: keyof typeof inputs) => (
      evt: React.ChangeEvent<HTMLInputElement>,
    ) => {
      const target = evt.target;
      if (typeof inputs[prop] === 'number') {
        updateInputs(s => ({ ...s, [prop]: target.valueAsNumber }));
      } else {
        updateInputs(s => ({ ...s, [prop]: target.value }));
      }
    },
  );

  const handleDismiss = React.useCallback(() => {
    onDismiss();
  }, [onDismiss]);

  const [showMeasurement, updateShowMeasurement] = React.useState(false);

  const handleBeginMeasurement = React.useCallback(
    () => updateShowMeasurement(true),
    [],
  );
  const handleDismissMeasurement = React.useCallback(
    () => updateShowMeasurement(false),
    [],
  );

  const imageRef = React.useRef<HTMLImageElement>(null);
  const [imageDrop, updateImageDrop] = React.useState(undefined as
    | DropInfo
    | undefined);
  const [dmImageDrop, updateDMImageDrop] = React.useState(undefined as
    | DropInfo
    | undefined);
  const handleImageChange = React.useCallback((dropInfo?: DropInfo) => {
    updateImageDrop(dropInfo);
  }, []);
  const handleDMImageChange = React.useCallback((dropInfo?: DropInfo) => {
    updateDMImageDrop(dropInfo);
  }, []);

  React.useEffect(() => {
    if (initialDrop && initialDrop.image) {
      const { width: iw, height: ih } = initialDrop.image;
      updateInputs(s => ({ ...s, width: iw, height: ih }));
      updateImageDrop(initialDrop);
    }
  }, [initialDrop]);

  const measureCanvasRef = React.useRef<HTMLCanvasElement>(null);
  const measurementStart = React.useRef(null as {
    top: number;
    left: number;
  } | null);
  const drawMeasurementCanvas = React.useCallback(
    (measurementTargetX?: number, measurementTargetY?: number) => {
      if (measureCanvasRef.current && imageRef.current) {
        const canvas = measureCanvasRef.current;
        const image = imageRef.current;
        canvas.width = 400;
        canvas.height = 400 * (image.height / image.width);

        const ctx = canvas.getContext('2d')!;
        ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

        if (
          typeof measurementTargetX !== 'undefined' &&
          typeof measurementTargetY !== 'undefined' &&
          measurementStart.current
        ) {
          const { left, top } = canvas.getBoundingClientRect();
          ctx.strokeStyle = '#000';
          ctx.lineWidth = 4;
          ctx.moveTo(
            measurementStart.current.left - left,
            measurementStart.current.top - top,
          );
          ctx.lineTo(measurementTargetX - left, measurementTargetY - top);
          ctx.stroke();
        }
      }
    },
    [measureCanvasRef, imageRef, measurementStart],
  );
  const handleBeginMeasure = React.useCallback(
    (evt: React.PointerEvent<HTMLCanvasElement>) => {
      measurementStart.current = { left: evt.clientX, top: evt.clientY };
    },
    [],
  );
  const handleMeasure = React.useCallback(
    (evt: React.PointerEvent<HTMLCanvasElement>) => {
      drawMeasurementCanvas(evt.clientX, evt.clientY);
    },
    [drawMeasurementCanvas],
  );
  const handleEndMeasure = React.useCallback(
    (evt: React.PointerEvent<HTMLCanvasElement>) => {
      if (
        imageDrop &&
        imageDrop.image &&
        measureCanvasRef.current &&
        measurementStart.current
      ) {
        const dist =
          new Vector2(
            evt.clientX - measurementStart.current.left,
            evt.clientY - measurementStart.current.top,
          ).length() *
          (imageDrop.image.width / measureCanvasRef.current.width);
        updateInputs(s => ({ ...s, measuredPixel: dist }));
        measurementStart.current = null;
      }
    },
    [imageDrop, measureCanvasRef],
  );
  const handleApplyMeasurement = React.useCallback(() => {
    const image = imageDrop && imageDrop.image;
    if (image) {
      updateInputs(s => ({
        ...s,
        milesPerPixel: s.measuredPixel / s.measuredMiles,
        width: image.width / (s.measuredPixel / s.measuredMiles),
        height: image.height / (s.measuredPixel / s.measuredMiles),
      }));
      updateShowMeasurement(false);
    }
  }, [imageDrop]);

  const handleCreate = React.useCallback(() => {
    if (imageDrop) {
      if (type === 'world') {
        onCreate({
          name,
          width,
          height,
          pointsOfInterest: [],
          _attachments: {
            image: {
              content_type: imageDrop.file.type,
              data: imageDrop.file,
              size: imageDrop.file.size,
            },
          },
        });
      } else {
        onCreate({
          name,
          width,
          height,
          tokens: [],
          assets: [],
          walls: [],
          globalIllumination: 0,
          _attachments: {
            image: {
              content_type: imageDrop.file.type,
              data: imageDrop.file,
              size: imageDrop.file.size,
            },
          },
        });
      }
      updateInputs({
        name: 'A new world',
        type: noWorld ? 'battlemap' : 'world',
        width: initialDrop && initialDrop.image ? initialDrop.image.width : 10,
        height:
          initialDrop && initialDrop.image ? initialDrop.image.height : 10,
        milesPerPixel: 1,
        measuredPixel: 0,
        measuredMiles: 0,
      });
    }
  }, [imageDrop, type, noWorld, initialDrop, onCreate, name, width, height]);

  React.useEffect(() => {
    if (showMeasurement) {
      drawMeasurementCanvas();
    }
  }, [showMeasurement, drawMeasurementCanvas]);

  return (
    <Dialog open={open} onClose={handleDismiss}>
      <DialogTitle id="token-creation-dialog">Create a new world</DialogTitle>
      <DialogContent className={classes.grid}>
        <FormControl className={classes.inputSpan}>
          <InputLabel>Name</InputLabel>
          <Input value={name} onChange={handleInputChange('name')} />
        </FormControl>
        {!noWorld && (
          <FormControl component="div" className={classes.inputSpan}>
            <FormLabel component="label">Type</FormLabel>
            <RadioGroup
              value={type}
              onChange={handleInputChange('type') as any}
            >
              <FormControlLabel
                value="world"
                control={<Radio />}
                label="Worldmap"
              />
              <FormControlLabel
                value="battlemap"
                control={<Radio />}
                label="Battlemap"
              />
            </RadioGroup>
          </FormControl>
        )}
        <FormControl>
          <Typography gutterBottom>Image</Typography>
          <ImageInput
            imageRef={imageRef}
            value={imageDrop && imageDrop.file}
            preview={imageDrop && imageDrop.preview}
            onChange={handleImageChange}
          />
        </FormControl>
        <FormControl>
          <Typography gutterBottom>
            DM-Image&nbsp;
            <Tooltip title="This image will replace the normal image for all DMs playing. If you leave this blank, the DM will see the default image.">
              <Info color="primary" fontSize="inherit" />
            </Tooltip>
          </Typography>
          <ImageInput
            value={dmImageDrop && dmImageDrop.file}
            preview={dmImageDrop && dmImageDrop.preview}
            onChange={handleDMImageChange}
          />
        </FormControl>
        <FormControl>
          <InputLabel>Width</InputLabel>
          <Input
            type="number"
            value={width}
            onChange={handleInputChange('width')}
            readOnly={type === 'world'}
            endAdornment={
              <InputAdornment position="end">
                {type === 'world' ? 'miles' : 'cells'}
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl>
          <InputLabel>Height</InputLabel>
          <Input
            type="number"
            value={height}
            onChange={handleInputChange('height')}
            readOnly={type === 'world'}
            endAdornment={
              <InputAdornment position="end">
                {type === 'world' ? 'miles' : 'cells'}
              </InputAdornment>
            }
          />
        </FormControl>
        {type === 'world' && (
          <FormControl>
            <InputLabel>Miles per Pixel</InputLabel>
            <Input
              type="number"
              value={milesPerPixel}
              readOnly
              onChange={handleInputChange('milesPerPixel')}
              endAdornment={
                <>
                  <InputAdornment position="end">mi/p</InputAdornment>
                  <Button
                    onClick={handleBeginMeasurement}
                    disabled={!imageDrop}
                  >
                    Measure
                  </Button>
                </>
              }
            />
          </FormControl>
        )}
        <Dialog open={showMeasurement} onClose={handleDismissMeasurement}>
          <canvas
            ref={measureCanvasRef}
            onPointerDown={handleBeginMeasure}
            onPointerMove={handleMeasure}
            onPointerUp={handleEndMeasure}
          />
          <div className={classes.measureInputs}>
            <FormControl>
              <InputLabel>Pixel</InputLabel>
              <Input
                type="number"
                value={measuredPixel}
                onChange={handleInputChange('measuredPixel')}
                inputProps={{ min: 0 }}
              />
            </FormControl>
            <FormControl>
              <InputLabel>Miles</InputLabel>
              <Input
                type="number"
                value={measuredMiles}
                onChange={handleInputChange('measuredMiles')}
                inputProps={{ min: 0 }}
              />
            </FormControl>
          </div>
          <DialogActions>
            <Button onClick={handleDismissMeasurement} color="primary">
              Abort
            </Button>
            <Button onClick={handleApplyMeasurement} color="primary">
              Apply
            </Button>
          </DialogActions>
        </Dialog>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDismiss} color="primary">
          Cancel
        </Button>
        <Button onClick={handleCreate} color="primary">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}
