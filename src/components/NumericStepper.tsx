import {
  createStyles,
  IconButton,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { Add, Remove } from '@material-ui/icons';
import * as React from 'react';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      alignItems: 'center',
    },
  });

export interface NumericStepperProps extends WithStyles<typeof styles> {
  value: number;
  min?: number;
  max?: number;
  onChange: (val: number) => void;
}

export default withStyles(styles)(NumericStepper);

function NumericStepper({
  value,
  min = Number.MIN_SAFE_INTEGER,
  max = Number.MAX_SAFE_INTEGER,
  onChange,
  classes,
}: NumericStepperProps) {
  return (
    <div className={classes.container}>
      <IconButton
        disabled={value + 1 > max}
        onClick={() => onChange(Math.min(max, value + 1))}
        color="primary"
      >
        <Add fontSize="small" />
      </IconButton>
      <Typography component="span">{value}</Typography>
      <IconButton
        disabled={value - 1 < min}
        onClick={() => onChange(Math.max(min, value - 1))}
        color="primary"
      >
        <Remove fontSize="small" />
      </IconButton>
    </div>
  );
}
