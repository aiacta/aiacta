import {
  Button,
  Collapse,
  createStyles,
  ExpansionPanel,
  ExpansionPanelActions,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  FormControl,
  FormControlLabel,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Radio,
  RadioGroup,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core/es';
import { ExpandMore, Loop } from '@material-ui/icons';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab/es';
import * as React from 'react';
import {
  attributes,
  BackgroundModel,
  ClassModel,
  RaceModel,
  SkillProficiency,
  skills,
} from '../stores/models';
import {
  backgrounds,
  characters,
  classes as classesData,
  races,
  subraces,
} from '../stores/modules/dataStores';
import { rollDice } from '../stores/modules/gameActions';
import { myself } from '../stores/modules/gameData';
import { promptForChoice } from '../stores/modules/uiPrompt';
import { closeWindow } from '../stores/modules/uiView';
import capitalizeKeyword from '../utils/capitalizeKeyword';
import classNames from '../utils/classNames';
import { useMemoize } from '../utils/memoize';
import Markdown from './Markdown';

const styles = (theme: Theme) =>
  createStyles({
    actionsContainer: {
      marginTop: theme.spacing.unit * 2,
    },
    choiceName: {
      flex: '1 1 auto',
      alignSelf: 'center',
    },
    choiceDetails: {
      display: 'flex',
      flexDirection: 'column',
    },
    trait: {
      '& > p:first-child': {
        fontWeight: 'bold',
      },
      '& > p': {
        display: 'inline',
      },
      marginBottom: theme.spacing.unit,
    },
    selected: {
      border: `2px solid ${theme.palette.primary.main}`,
    },
    attributeGrid: {
      display: 'grid',
      gridTemplateRows: 'auto auto',
      gridAutoColumns: '1fr',
      gridAutoFlow: 'column',
      justifyItems: 'center',
    },
    attribute: {
      cursor: 'grab',
    },
    pointBuyGrid: {
      gridTemplateRows: 'auto auto auto',
      '& > div': {
        display: 'inherit',
      },
    },
    form: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      flex: '0 0 200px',
      margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit}px`,
    },
    alignmentGrid: {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr 1fr',
    },
  });

export interface CharacterCreatorProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(CharacterCreator);

function CharacterCreator({ classes }: CharacterCreatorProps) {
  const [activeStep, updateActiveStep] = React.useState(0);
  const [character, updateCharacter] = React.useState({
    race: undefined as RaceModel | undefined,
    class: undefined as ClassModel | undefined,
    background: undefined as BackgroundModel | undefined,
    choicesMade: {},
    attributes: {
      strength: 8,
      dexterity: 8,
      constitution: 8,
      wisdom: 8,
      intelligence: 8,
      charisma: 8,
    },
    name: '',
    gender: '',
    age: '',
    height: '',
    weight: '',
    eyes: '',
    skin: '',
    hair: '',
    alignment: '',
  });
  const [attributeMode, updateAttributeMode] = React.useState('random');

  const attributeCosts = {
    8: 0,
    9: 1,
    10: 2,
    11: 3,
    12: 4,
    13: 5,
    14: 7,
    15: 9,
  };

  const handleClickBack = React.useCallback(() => {
    updateActiveStep(s => s - 1);
  }, []);
  const handleClickNext = React.useCallback(() => {
    updateActiveStep(s => s + 1);
  }, []);
  const handleFinish = React.useCallback(async () => {
    if (character.race && character.class && character.background) {
      const char = characters.add({
        name: character.name,
        bio: '',
        desc: '',
        notes: [],
        owner: myself().isDungeonMaster ? '' : myself()._id,
        size: character.race.size,
        health: {
          current: 0,
          temporary: 0,
          rolls: [],
        },
        deathSaves: {
          successes: 0,
          failures: 0,
        },
        speed: {
          walk: 30,
          climb: 0,
          fly: 0,
          hover: 0,
          swim: 0,
          ...character.race.speed,
        },
        attributes: {
          strength:
            character.attributes.strength +
            (character.race.attributeBonuses.strength || 0),
          dexterity:
            character.attributes.dexterity +
            (character.race.attributeBonuses.dexterity || 0),
          constitution:
            character.attributes.constitution +
            (character.race.attributeBonuses.constitution || 0),
          intelligence:
            character.attributes.intelligence +
            (character.race.attributeBonuses.intelligence || 0),
          wisdom:
            character.attributes.wisdom +
            (character.race.attributeBonuses.wisdom || 0),
          charisma:
            character.attributes.charisma +
            (character.race.attributeBonuses.charisma || 0),
        },
        saves: {
          strength: SkillProficiency.None,
          dexterity: SkillProficiency.None,
          constitution: SkillProficiency.None,
          intelligence: SkillProficiency.None,
          wisdom: SkillProficiency.None,
          charisma: SkillProficiency.None,
          ...character.race.saves,
          ...character.class.saves,
        },
        skills: {
          athletics: SkillProficiency.None,
          acrobatics: SkillProficiency.None,
          sleightOfHand: SkillProficiency.None,
          stealth: SkillProficiency.None,
          arcana: SkillProficiency.None,
          history: SkillProficiency.None,
          investigation: SkillProficiency.None,
          nature: SkillProficiency.None,
          religion: SkillProficiency.None,
          animalHandling: SkillProficiency.None,
          insight: SkillProficiency.None,
          medicine: SkillProficiency.None,
          perception: SkillProficiency.None,
          survival: SkillProficiency.None,
          deception: SkillProficiency.None,
          intimidation: SkillProficiency.None,
          performance: SkillProficiency.None,
          persuation: SkillProficiency.None,
          ...character.race.skills,
          ...character.class.skills,
          ...character.background.skills,
        },
        vulnerabilities: [
          ...character.race.vulnerabilities,
          ...character.class.vulnerabilities,
        ],
        resistances: [
          ...character.race.resistances,
          ...character.class.resistances,
        ],
        immunities: [
          ...character.race.immunities,
          ...character.class.immunities,
        ],
        conditionImmunities: [
          ...character.race.conditionImmunities,
          ...character.class.conditionImmunities,
        ],
        sight: {
          normal: true,
          darkvision: 0,
          blindsight: 0,
          truesight: 0,
          ...character.race.sight,
          ...character.class.sight,
        },
        languages: [...character.race.languages, ...character.class.languages],
        proficiencies: [
          ...character.race.proficiencies,
          ...character.class.proficiencies,
          ...character.background.proficiencies,
        ],
        gender: character.gender,
        age: character.age,
        height: character.height,
        weight: character.weight,
        eyes: character.eyes,
        skin: character.skin,
        hair: character.hair,
        alignment: character.alignment,
        race: character.race._id,
        choicesMade: { ...character.choicesMade },
        classes: [],
        hitDice: [],
        background: character.background._id,
        traits: [],
        features: [],
        feats: [],
        resources: [],
        customEffects: [],
        customActions: [],
        spellSlotsSpent: {
          pact: 0,
          0: 0,
          1: 0,
          2: 0,
          3: 0,
          4: 0,
          5: 0,
          6: 0,
          7: 0,
          8: 0,
          9: 0,
        },
        spellsLearned: [],
        experiencePoints: 0,
        initiativeBonusFormula: 'this.modifiers.dexterity',
        armorClassFormula:
          '(this.gear.armor.armorClass ?? 10) + min(this.modifiers.dexterity, this.gear.armor.maxDexterityBonus) + this.gear.shield.armorClass',
        inventory: [],
        _attachments: {},
      });
      closeWindow('characterCreator');
      try {
        await char.addRacialTraits(character.race._id);
        await char.addClassLevel(character.class._id);
        await char.addBackgroundFeatures(character.background._id);
      } catch (err) {
        characters.remove(char);
      }
    }
  }, [character]);

  const handleSelectRace = useMemoize((race: RaceModel) => () => {
    race.traits
      .filter(trait => Boolean(trait.choices))
      .reduce(async (prom, trait) => {
        const acc = await prom;
        const res = await promptForChoice(race.name, trait as Required<
          typeof trait
        >);
        return {
          ...acc,
          ...res,
        };
      }, Promise.resolve({}))
      .then(async choicesMade => {
        if (subraces.filter(s => s.race === race)) {
          const subraceChoice = await promptForChoice(race.name, {
            name: race.name,
            desc: 'Choose your subrace.',
            choices: { subrace: { type: 'subrace', race: race._id } },
          });

          const subrace = race.subraces.find(
            sc => sc._id === subraceChoice[`${race.name}-subrace`],
          )!;
          return subrace.traits
            .filter(trait => Boolean(trait.choices))
            .reduce(async (prom, trait) => {
              const acc = await prom;
              const res = await promptForChoice(race.name, trait as Required<
                typeof trait
              >);
              return {
                ...acc,
                ...res,
              };
            }, Promise.resolve({ ...choicesMade, ...subraceChoice }));
        }
        return choicesMade;
      })
      .then(
        choicesMade => {
          updateCharacter(c => ({
            ...c,
            race,
            choicesMade: { ...c.choicesMade, ...choicesMade },
          }));
          handleClickNext();
        },
        () => {},
      );
  });
  const handleSelectClass = useMemoize((cls: ClassModel) => () => {
    const skillChoice = promptForChoice(cls.name, {
      name: cls.name,
      desc: `Choose ${
        cls.choices.skills.num
      } skills from the following options.`,
      choices: cls.choices,
    });
    cls.features
      .filter(feature => Boolean(feature.choices && feature.level === 1))
      .reduce(async (prom, feature) => {
        const acc = await prom;
        const res = await promptForChoice(cls.name, feature as Required<
          typeof feature
        >);
        return {
          ...acc,
          ...res,
        };
      }, skillChoice)
      .then(choicesMade => {
        if (choicesMade[`${cls.name}-subclass`]) {
          const subclass = cls.subclasses.find(
            sc => sc._id === choicesMade[`${cls.name}-subclass`],
          )!;
          return subclass.features
            .filter(feature => Boolean(feature.choices && feature.level === 1))
            .reduce(async (prom, feature) => {
              const acc = await prom;
              const res = await promptForChoice(cls.name, feature as Required<
                typeof feature
              >);
              return {
                ...acc,
                ...res,
              };
            }, Promise.resolve(choicesMade));
        }
        return choicesMade;
      })
      .then(
        choicesMade => {
          updateCharacter(c => ({
            ...c,
            class: cls,
            choicesMade: { ...c.choicesMade, ...choicesMade },
          }));
          handleClickNext();
        },
        () => {},
      );
  });
  const handleSelectBackground = useMemoize((bg: BackgroundModel) => () => {
    promptForChoice(bg.name, {
      name: bg.name,
      desc: `Make the following choices for your background.`,
      choices: bg.choices,
    }).then(
      choicesMade => {
        updateCharacter(c => ({
          ...c,
          background: bg,
          choicesMade: { ...c.choicesMade, ...choicesMade },
        }));
        handleClickNext();
      },
      () => {},
    );
  });
  const handleChangeCharacter = useMemoize(
    (key: keyof typeof character, value?: any, advance?: boolean) => (
      evt:
        | React.ChangeEvent<
            HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
          >
        | React.MouseEvent<HTMLElement>,
      data?: any,
    ) => {
      if (typeof value === 'object') {
      }

      const evtValue = (evt as React.ChangeEvent<HTMLInputElement>).target
        ? (evt as React.ChangeEvent<HTMLInputElement>).target.value
        : undefined;
      updateCharacter(c => ({
        ...c,
        [key]: value || data || evtValue,
      }));
      if (advance) {
        handleClickNext();
      }
    },
  );
  const handleRandomizeCharacter = useMemoize(
    (key: keyof typeof character) => {
      switch (key) {
        case 'name':
          return () =>
            updateCharacter(c => {
              const names = c.race
                ? c.gender === 'male'
                  ? c.race.maleNames
                  : c.race.femaleNames
                : [];
              const surnames = c.race ? c.race.surnames : [];
              return {
                ...c,
                name:
                  [random(names), random(surnames)].filter(Boolean).join(' ') ||
                  'Not implemented',
              };
            });
        case 'age':
          return () =>
            updateCharacter(c => ({
              ...c,
              age: c.race
                ? (
                    c.race.adultAge +
                    Math.round(
                      Math.random() * (c.race.lifeExpectancy - c.race.adultAge),
                    )
                  ).toString()
                : '',
            }));
        case 'height':
          return () =>
            rollDice(character.race ? character.race.heightRoll : '2d6').then(
              res =>
                updateCharacter(c => ({
                  ...c,
                  height: c.race
                    ? inchToStr(c.race.baseHeight + (res.value as number))
                    : '',
                })),
            );
        case 'weight':
          return () =>
            rollDice(character.race ? character.race.weightRoll : '2d6').then(
              res =>
                updateCharacter(c => ({
                  ...c,
                  weight: c.race
                    ? `${c.race.baseWeight +
                        (strToInch(c.height) - c.race.baseHeight) *
                          (res.value as number)} lb.`
                    : '',
                })),
            );
        default:
          return () =>
            updateCharacter(c => ({
              ...c,
              [key]:
                c.race && Array.isArray(c.race[key as keyof typeof c['race']])
                  ? random(c.race[key as keyof typeof c['race']])
                  : 'Not implemented',
            }));
      }
    },
    [character.race],
  );

  const handleChangeAttributeMode = React.useCallback(
    (evt: React.ChangeEvent<HTMLSelectElement>) => {
      updateAttributeMode(evt.target.value);
    },
    [],
  );
  const handleRollAttributes = React.useCallback(() => {
    Promise.all(
      Array(6)
        .fill(null)
        .map(() => rollDice('4d6kh3').then(res => res.value as number)),
    ).then(
      ([strength, dexterity, constitution, intelligence, wisdom, charisma]) => {
        updateCharacter(c => ({
          ...c,
          attributes: {
            strength,
            dexterity,
            constitution,
            intelligence,
            wisdom,
            charisma,
          },
        }));
      },
    );
  }, []);
  const handleChangeAttribute = useMemoize(
    (attr: keyof typeof character['attributes'], chg?: number) => (
      evt: any,
    ) => {
      if (chg) {
        updateCharacter(c => ({
          ...c,
          attributes: {
            ...c.attributes,
            [attr]: c.attributes[attr] += chg,
          },
        }));
      } else {
        const val = (evt as React.ChangeEvent<HTMLInputElement>).target
          .valueAsNumber;
        updateCharacter(c => ({
          ...c,
          attributes: {
            ...c.attributes,
            [attr]: val,
          },
        }));
      }
    },
  );
  const remainingPointBuyPoints = attributes.reduce(
    (acc, attr) =>
      acc -
      attributeCosts[character.attributes[attr] as keyof typeof attributeCosts],
    29,
  );
  const handleDragAttributeStart = useMemoize(
    (attribute: string) => (evt: React.DragEvent<HTMLSpanElement>) => {
      evt.dataTransfer.setData('aiacta/attribute', attribute);
    },
  );
  const handleDragOverAttribute = React.useCallback(
    (evt: React.DragEvent<HTMLSpanElement>) => {
      evt.preventDefault();
      evt.stopPropagation();
      evt.dataTransfer.dropEffect = 'link';
      return false;
    },
    [],
  );
  const handleDropOnAttribute = useMemoize(
    (target: keyof typeof character['attributes']) => (
      evt: React.DragEvent<HTMLSpanElement>,
    ) => {
      const origin = evt.dataTransfer.getData(
        'aiacta/attribute',
      ) as keyof typeof character['attributes'];
      updateCharacter(c => ({
        ...c,
        attributes: {
          ...c.attributes,
          [target]: c.attributes[origin],
          [origin]: c.attributes[target],
        },
      }));
    },
  );

  const steps = [
    'Choose a Race',
    'Choose a Class',
    'Determine Ability Scores',
    'Choose a Background',
    'Describe your Character',
  ];
  const stepContent = (num: number) => {
    switch (num) {
      case 0:
        return (
          <>
            {races.all().map(race => (
              <ExpansionPanel
                key={race.name}
                className={classNames(
                  character.race === race && classes.selected,
                )}
              >
                <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                  <Typography className={classes.choiceName} variant="body1">
                    {race.name}
                  </Typography>
                  <Button
                    onClick={handleSelectRace(race)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.choiceDetails}>
                  <Markdown>{race.desc}</Markdown>
                  {race.traits.map(trait => (
                    <div key={trait.key} className={classes.trait}>
                      <Typography>{trait.name}. </Typography>
                      <Markdown>{trait.desc}</Markdown>
                    </div>
                  ))}
                </ExpansionPanelDetails>
                <ExpansionPanelActions>
                  <Button
                    onClick={handleSelectRace(race)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelActions>
              </ExpansionPanel>
            ))}
            <div className={classes.actionsContainer}>
              <Button
                disabled={!character.race}
                onClick={handleClickNext}
                variant="contained"
                color="primary"
              >
                Next
              </Button>
            </div>
          </>
        );
      case 1:
        return (
          <>
            {classesData.all().map(cls => (
              <ExpansionPanel
                key={cls.name}
                className={classNames(
                  character.class === cls && classes.selected,
                )}
              >
                <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                  <Typography className={classes.choiceName} variant="body1">
                    {cls.name}
                  </Typography>
                  <Button
                    onClick={handleSelectClass(cls)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.choiceDetails}>
                  <Markdown>{cls.desc}</Markdown>
                  <Typography gutterBottom>
                    As a {cls.name}, you gain the following class features.
                  </Typography>
                  <div className={classes.trait}>
                    <Typography>Hit dice: </Typography>
                    <Typography>
                      1d{cls.hitDie} per {cls.name} level
                    </Typography>
                  </div>
                  {cls.features.find(f =>
                    Boolean(
                      f.choices &&
                        Object.values(f.choices).some(
                          c => c.type === 'subclass',
                        ),
                    ),
                  ) && (
                    <div className={classes.trait}>
                      <Typography>Variations: </Typography>
                      <Typography>
                        Choose at {cls.name} level{' '}
                        {
                          cls.features.find(f =>
                            Boolean(
                              f.choices &&
                                Object.values(f.choices).some(
                                  c => c.type === 'subclass',
                                ),
                            ),
                          )!.level
                        }{' '}
                        from {cls.subclasses.map(sub => sub.name).join(', ')}
                      </Typography>
                    </div>
                  )}
                  <div className={classes.trait}>
                    <Typography>Proficiencies: </Typography>
                    <Typography>{cls.proficiencies.join(', ')}</Typography>
                  </div>
                  <div className={classes.trait}>
                    <Typography>Saving Throws: </Typography>
                    <Typography>
                      {Object.keys(cls.saves)
                        .map(capitalizeKeyword)
                        .join(', ')}
                    </Typography>
                  </div>
                  <div className={classes.trait}>
                    <Typography>Skills: </Typography>
                    <Typography>
                      {Object.keys(cls.skills).length > 0
                        ? `Gain ${Object.keys(cls.skills)
                            .map(capitalizeKeyword)
                            .join(', ')} and choose `
                        : 'Choose '}
                      {`${cls.choices.skills.num} from  ${(
                        cls.choices.skills.includes || skills
                      )
                        .map(capitalizeKeyword)
                        .join(', ')}`}
                    </Typography>
                  </div>
                  {cls.features
                    .filter(feature => feature.level <= 1)
                    .map(feature => (
                      <div key={feature.key} className={classes.trait}>
                        <Typography>{feature.name}. </Typography>
                        <Markdown>{feature.desc}</Markdown>
                      </div>
                    ))}
                  <BeyondFirstLevel>
                    {cls.features
                      .filter(feature => feature.level > 1)
                      .map(feature => (
                        <div key={feature.key} className={classes.trait}>
                          <Typography>{feature.name}. </Typography>
                          <Markdown>{feature.desc}</Markdown>
                        </div>
                      ))}
                  </BeyondFirstLevel>
                </ExpansionPanelDetails>
                <ExpansionPanelActions>
                  <Button
                    onClick={handleSelectClass(cls)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelActions>
              </ExpansionPanel>
            ))}
            <div className={classes.actionsContainer}>
              <Button onClick={handleClickBack}>Back</Button>
              <Button
                disabled={!character.class}
                onClick={handleClickNext}
                variant="contained"
                color="primary"
              >
                Next
              </Button>
            </div>
          </>
        );
      case 2:
        return (
          <>
            <RadioGroup
              row
              name="attributeMode"
              value={attributeMode}
              onChange={handleChangeAttributeMode as any}
            >
              <FormControlLabel
                value="random"
                control={<Radio />}
                label="Dice roll"
              />
              <FormControlLabel
                value="pointBuy"
                control={<Radio />}
                label="Point-Buy"
              />
              <FormControlLabel
                value="manual"
                control={<Radio />}
                label="Manual entry"
              />
            </RadioGroup>
            <div
              className={classNames(
                classes.attributeGrid,
                attributeMode === 'pointBuy' && classes.pointBuyGrid,
              )}
            >
              {attributes.map(attr => (
                <React.Fragment key={attr}>
                  <Typography variant="caption">
                    {capitalizeKeyword(attr)}
                  </Typography>
                  <Typography
                    variant="button"
                    onDragOver={handleDragOverAttribute}
                    onDrop={handleDropOnAttribute(attr)}
                  >
                    <span
                      draggable
                      className={classes.attribute}
                      onDragStart={handleDragAttributeStart(attr)}
                    >
                      {character.attributes[attr]}
                    </span>
                    {character.race && character.race.attributeBonuses[attr]
                      ? ` ${
                          character.race!.attributeBonuses[attr]! > 0
                            ? '+'
                            : '-'
                        } ${Math.abs(character.race!.attributeBonuses[attr]!)}`
                      : ''}
                  </Typography>
                  {attributeMode === 'pointBuy' && (
                    <div>
                      <Button
                        onClick={handleChangeAttribute(attr, 1)}
                        variant="outlined"
                        color="primary"
                      >
                        +
                      </Button>
                      <Button
                        onClick={handleChangeAttribute(attr, -1)}
                        variant="outlined"
                        color="primary"
                      >
                        -
                      </Button>
                    </div>
                  )}
                </React.Fragment>
              ))}
            </div>
            <div className={classes.actionsContainer}>
              <Button onClick={handleClickBack}>Back</Button>
              <Button
                disabled={
                  attributeMode === 'pointBuy'
                    ? remainingPointBuyPoints !== 0
                    : false
                }
                onClick={handleClickNext}
                variant="contained"
                color="primary"
              >
                Next
              </Button>
              {attributeMode === 'random' && (
                <Button
                  onClick={handleRollAttributes}
                  variant="contained"
                  color="primary"
                >
                  Roll
                </Button>
              )}
              {attributeMode === 'pointBuy' && (
                <Typography>
                  Remaining points: {remainingPointBuyPoints}
                </Typography>
              )}
            </div>
          </>
        );
      case 3:
        return (
          <>
            {backgrounds.all().map(bg => (
              <ExpansionPanel
                key={bg.name}
                className={classNames(
                  character.background === bg && classes.selected,
                )}
              >
                <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                  <Typography className={classes.choiceName} variant="body1">
                    {bg.name}
                  </Typography>
                  <Button
                    onClick={handleSelectBackground(bg)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.choiceDetails}>
                  <Markdown>{bg.desc}</Markdown>
                  {bg.features.map(feature => (
                    <div key={feature.key} className={classes.trait}>
                      <Typography>{feature.name}. </Typography>
                      <Markdown>{feature.desc}</Markdown>
                    </div>
                  ))}
                </ExpansionPanelDetails>
                <ExpansionPanelActions>
                  <Button
                    onClick={handleSelectBackground(bg)}
                    size="small"
                    color="primary"
                  >
                    Select
                  </Button>
                </ExpansionPanelActions>
              </ExpansionPanel>
            ))}
            <div className={classes.actionsContainer}>
              <Button onClick={handleClickBack}>Back</Button>
              <Button
                disabled={!character.background}
                onClick={handleClickNext}
                variant="contained"
                color="primary"
              >
                Next
              </Button>
            </div>
          </>
        );
      case 4:
        return (
          <>
            <div>
              <div className={classes.form}>
                {[
                  'name',
                  'age',
                  'height',
                  'weight',
                  'eyes',
                  'skin',
                  'hair',
                ].map(attr => (
                  <FormControl key={attr} className={classes.textField}>
                    <InputLabel>{capitalizeKeyword(attr)}</InputLabel>
                    <Input
                      value={character[attr as keyof typeof character]}
                      onChange={handleChangeCharacter(
                        // FIXME codesmell
                        attr as keyof typeof character,
                      )}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            onClick={handleRandomizeCharacter(
                              // FIXME codesmell
                              attr as keyof typeof character,
                            )}
                          >
                            <Loop />
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                ))}
              </div>
              <Typography variant="h6">Alignment</Typography>
              <ToggleButtonGroup
                className={classes.alignmentGrid}
                value={character.alignment}
                onChange={handleChangeCharacter('alignment')}
                exclusive
              >
                <ToggleButton value="lawful good">Lawful good</ToggleButton>
                <ToggleButton value="neutral good">Neutral good</ToggleButton>
                <ToggleButton value="chaotic good">Chaotic good</ToggleButton>
                <ToggleButton value="lawful neutral">
                  Lawful neutral
                </ToggleButton>
                <ToggleButton value="neutral">Neutral</ToggleButton>
                <ToggleButton value="chaotic neutral">
                  Chaotic neutral
                </ToggleButton>
                <ToggleButton value="lawful evil">Lawful evil</ToggleButton>
                <ToggleButton value="neutral evil">Neutral evil</ToggleButton>
                <ToggleButton value="chaotic evil">Chaotic evil</ToggleButton>
              </ToggleButtonGroup>
            </div>
            <div className={classes.actionsContainer}>
              <Button onClick={handleClickBack}>Back</Button>
              <Button
                onClick={handleFinish}
                variant="contained"
                color="primary"
              >
                Finish
              </Button>
            </div>
          </>
        );
      default:
        return (
          <>
            N/A
            <div className={classes.actionsContainer}>
              <Button onClick={handleClickBack} disabled={num === 0}>
                Back
              </Button>
            </div>
          </>
        );
    }
  };

  return (
    <>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, idx) => (
          <Step key={step}>
            <StepLabel>{step}</StepLabel>
            <StepContent>{stepContent(idx)}</StepContent>
          </Step>
        ))}
      </Stepper>
    </>
  );
}

function inchToStr(inch: number) {
  const feet = Math.floor(inch / 12);
  return `${feet > 0 ? `${feet}'` : ''}${
    inch % 12 > 0 ? ` ${inch % 12}''` : ''
  }`;
}

function strToInch(str: string) {
  const res = /(?:(?<feet>\d)+')?\s?(?:(?<inch>\d)+'')?/.exec(str);
  if (res) {
    return (
      (res.groups!.feet ? +res.groups!.feet * 12 : 0) + (+res.groups!.inch || 0)
    );
  }
  return NaN;
}

function random<T>(arr: T[]) {
  return arr[Math.round((arr.length - 1) * Math.random())];
}

function BeyondFirstLevel({ children }: { children: React.ReactNode }) {
  const [show, updateShow] = React.useState(false);
  const handleToggle = React.useCallback(() => {
    updateShow(s => !s);
  }, []);

  return (
    <>
      <Button onClick={handleToggle}>Beyond 1st level...</Button>
      <Collapse in={show} unmountOnExit>
        {children}
      </Collapse>
    </>
  );
}
