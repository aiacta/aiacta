import { AbstractMesh, BoundingBox, Nullable } from 'babylonjs';
import { action, observable } from 'mobx';
import {
  AssetModel,
  PartyModel,
  PointOfInterestModel,
  TokenModel,
  WallModel,
  CharacterModel,
  MonsterModel,
  ItemModel,
} from '../models';
import useObservable from '../../components/hooks/useObservable';

const store = observable({
  active: new Map<Renderable, AbstractMesh>(),
  focus: null as Nullable<BoundingBox>,
  windows: new Set<string>(),
});

export type Renderable =
  | TokenModel
  | PointOfInterestModel
  | AssetModel
  | WallModel
  | PartyModel;

export type Windowable =
  | CharacterModel
  | MonsterModel
  | PointOfInterestModel
  | ItemModel;

export const setActive = action(
  (model: Renderable, mesh: AbstractMesh, active: boolean) => {
    if (active) {
      store.active.set(model, mesh);
    } else {
      store.active.delete(model);
    }
  },
);

export const focusOn = action((focus: Renderable | BoundingBox) => {
  if (!(focus instanceof BoundingBox)) {
    const mesh = store.active.get(focus);
    if (mesh && mesh.isEnabled()) {
      const bb = mesh.getBoundingInfo().boundingBox;
      store.focus = new BoundingBox(bb.minimumWorld, bb.maximumWorld).scale(10);
    }
  } else {
    store.focus = focus;
  }
});

export const openSheet = action((model: Windowable, toggle = false) => {
  const prefix = `${model.document.type}sheet`;
  if (store.windows.has(`${prefix}-${model._id}`)) {
    if (toggle) {
      store.windows.delete(`${prefix}-${model._id}`);
    }
  } else {
    store.windows.add(`${prefix}-${model._id}`);
  }
});

export const openWindow = action((key: string) => {
  store.windows.add(key);
});

export const closeSheet = action((model: Windowable) => {
  store.windows.delete(`${model.document.type}sheet-${model._id}`);
});

export const closeWindow = action((key: string) => {
  store.windows.delete(key);
});

export function getActiveModels() {
  return Array.from(store.active.keys());
}

export function getActiveMesh(model: Renderable) {
  return store.active.get(model);
}

export function getActiveMeshes() {
  return Array.from(store.active.values());
}

export function getFocus() {
  return store.focus;
}

export function useOpenWindows() {
  return useObservable(() => Array.from(store.windows));
}
