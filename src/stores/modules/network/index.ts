import { Nullable } from 'babylonjs';
import { action, observable, runInAction } from 'mobx';
import PeerJS from 'peerjs';
import { getData, meta, putData, updateData, players } from '../dataStores';
import { myself } from '../gameData';

const store = observable({
  peer: null as Nullable<PeerJS>,
  connections: new Map<string, PeerJS.DataConnection>(),
  playerIdToPeer: new Map<string, string>(),
});

export async function start(gameId: string) {
  if (store.peer) {
    store.peer.destroy();
  }
  const host = localStorage.getItem('aiacta.peerHost') || 'peer.aiacta.com';

  const peer = new PeerJS({
    host,
    port: 9000,
    key: gameId,
    secure: true,
  });
  peer.on('open', id => {
    runInAction(() => {
      store.peer = peer;
      store.connections.clear();
    });
    peer.listAllPeers(async peers => {
      let hasRegistered = false;
      peers
        .filter(peer => peer !== id)
        .map(peer =>
          connectWith(peer).then(
            () => {
              if (!hasRegistered) {
                hasRegistered = true;
                sendTo(peer, {
                  type: 'register',
                  player: myself().document,
                  lastSync: meta.singleton.lastSync,
                });
              }
            },
            () => {
              console.log(`connection to ${peer} timed out`);
            },
          ),
        );
    });
  });
  peer.on('connection', connection => {
    addConnection(connection);
  });
}

export const stop = action(() => {
  if (store.peer) {
    store.peer.destroy();
    store.peer = null;
    store.connections.clear();
  }
});

export function isConnected() {
  return !!store.peer;
}

export function isOnline(playerId: string) {
  return (
    store.connections.has(store.playerIdToPeer.get(playerId) || 'noop') ||
    playerId === myself()._id
  );
}

export function networkId() {
  return store.peer && store.peer.id;
}

function flushChanges() {
  broadcast({ type: 'dataChange', changes });
  changes = [];
}

const addConnection = action((connection: PeerJS.DataConnection) => {
  console.log('new connection');
  store.connections.set(connection.peer, connection);
  connection.on('data', (data: Message) => {
    console.log('received', data);

    switch (data.type) {
      case 'announce': {
        onAnnounce(connection.peer, data);
        break;
      }
      case 'register': {
        onRegister(connection.peer, data);
        break;
      }
      case 'setup': {
        onSetup(connection.peer, data);
        break;
      }
      case 'dataChange': {
        onDataChange(connection.peer, data);
        break;
      }
      default:
        throw new Error('Unexpected Message');
    }
  });
  connection.on('close', () => {
    closeConnection(connection);
  });
  connection.on('error', err => {
    console.log('connection error', err);
    closeConnection(connection);
  });
});

function broadcast(message: Message, ...except: string[]) {
  store.connections.forEach(connection => {
    if (!except.includes(connection.peer)) {
      connection.send(message);
    }
  });
}

function sendTo(id: string, message: Message) {
  const connection = store.connections.get(id);
  if (connection) {
    connection.send(message);
  }
}

async function connectWith(id: string) {
  console.log(`Trying to connect to ${id}`);
  const peerConnection = await new Promise<PeerJS.DataConnection>(
    (resolve, reject) => {
      const connection = store.peer!.connect(id);
      const timeout = setTimeout(() => {
        connection.close();
        reject();
      }, 5000);
      connection.on('open', () => {
        clearTimeout(timeout);
        addConnection(connection);
        resolve(connection);
      });
    },
  );
  sendTo(id, { type: 'announce', playerId: myself()._id, requestAnswer: true });
  return peerConnection;
}

const closeConnection = action((connection: PeerJS.DataConnection) => {
  connection.close();
  console.log('connection closed');
  store.connections.delete(
    typeof connection === 'string' ? connection : connection.peer,
  );
  // if (store.connections.size === 0) {
  //   disconnect();
  // }
});

type Message =
  | AnnounceMessage
  | RegisterMessage
  | SetupMessage
  | DataChangeMessage;

type Document<T> = import('../dataStores/ModelStore').Document<T>;

// ***************************************************************** //

interface AnnounceMessage {
  type: 'announce';
  playerId: string;
  requestAnswer: boolean;
}

function onAnnounce(from: string, message: AnnounceMessage) {
  runInAction(() => store.playerIdToPeer.set(message.playerId, from));

  if (message.requestAnswer) {
    sendTo(from, {
      type: 'announce',
      playerId: myself()._id,
      requestAnswer: false,
    });
  }
}

// ***************************************************************** //

interface RegisterMessage {
  type: 'register';
  player: Document<import('../../models').IPlayer>;
  lastSync: number;
}

function onRegister(from: string, message: RegisterMessage) {
  // suspend network send
  sendChanges = false;
  runInAction(() => {
    players.add(message.player);
  });
  // resume network send
  sendChanges = true;
  // send information since last sync to player
  sendTo(from, {
    type: 'setup',
    since: Date.now(),
    documentGroups: getData(message.lastSync),
  });
}

// ***************************************************************** //

interface SetupMessage {
  type: 'setup';
  since: number;
  documentGroups: {
    [key: string]: Document<any>[] | undefined;
  };
}

function onSetup(from: string, message: SetupMessage) {
  // p2p transforms blobs into arraybuffers, so we need to transform them back
  Object.values(message.documentGroups).forEach(documents => {
    if (documents) {
      documents.forEach(document => {
        if ('_attachments' in document) {
          Object.keys(document._attachments).forEach(key => {
            if (document._attachments[key].data instanceof ArrayBuffer) {
              document._attachments[key].data = new Blob([
                document._attachments[key].data as ArrayBuffer,
              ]);
            }
          });
        }
      });
    }
  });
  // suspend network send
  sendChanges = false;
  runInAction(() => {
    putData(message.documentGroups);
    meta.singleton.lastSync = message.since;
  });
  // resume network send
  sendChanges = true;
}

// ***************************************************************** //

interface DataChangeMessage {
  type: 'dataChange';
  changes: { id: string; type: string; diff: any }[];
}

let sendChanges = true;
let changes: { id: string; type: string; diff: any }[] = [];
export function sendChange(id: string, type: string, diff: any) {
  if (sendChanges) {
    changes.push({ id, type, diff });
    setImmediate(flushChanges);
  }
}

function onDataChange(from: string, message: DataChangeMessage) {
  // p2p transforms blobs into arraybuffers, so we need to transform them back
  message.changes.forEach(change => {
    if (change.diff) {
      if ('_attachments' in change.diff) {
        Object.keys(change.diff._attachments).forEach(key => {
          if (change.diff._attachments[key].data instanceof ArrayBuffer) {
            change.diff._attachments[key].data = new Blob([
              change.diff._attachments[key].data as ArrayBuffer,
            ]);
          }
        });
      }
    }
  });
  // suspend network send
  sendChanges = false;
  runInAction(() => {
    message.changes.forEach(change =>
      updateData(change.id, change.type, change.diff),
    );
  });
  // resume network send
  sendChanges = true;
}
