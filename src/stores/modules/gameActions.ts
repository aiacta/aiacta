import Program, { Context, Resolution } from '@aiacta/dicelang';
import { runInAction } from 'mobx';
import ReactGA from 'react-ga';
import { Action, Category } from '../../utils/analytics';
import {
  BattlemapModel,
  CharacterModel,
  IPointOfInterest,
  MonsterModel,
  PartyModel,
  TokenModel,
  TravelSpeed,
  WorldModel,
} from '../models';
import {
  characters,
  chatMessages,
  monsters,
  parties,
  pointsOfInterest,
  tokens,
} from './dataStores';
import { myself } from './gameData';

export async function rollDice(
  programOrString: string | Program,
  context?: Context,
  towerRoll = false,
) {
  const program =
    typeof programOrString === 'string'
      ? new Program(programOrString)
      : programOrString;
  if (program.errors.length > 0) {
    throw new Error(program.errors.join(', '));
  }
  ReactGA.event({ category: Category.Game, action: Action.Roll });
  return program.run(
    {
      // TODO apply real randomness here?
    },
    context,
  );
}

export function sendChatMessage(text: string, sender = myself()._id) {
  const rollCommand = /^\/r(?:oll)? (?<rollString>.+)$/.exec(text);
  if (rollCommand) {
    rollDice(rollCommand.groups!.rollString).then(res =>
      sendChatMessage(res.toChatMessage(true)),
    );
    return;
  }
  runInAction(() =>
    chatMessages.add({
      sender,
      text,
      timestamp: Date.now(),
    }),
  );
}

export function triggerAttack(
  attack: {
    instigator: string;
    type: string;
    rollModifier: number;
    range: string;
    targets: string;
  },
  target: TokenModel | CharacterModel | null,
) {
  rollDice(
    `d20 ${
      attack.rollModifier < 0 ? attack.rollModifier : '+' + attack.rollModifier
    }`,
  ).then(res => {
    let isCritical = false;
    let isAutomaticMiss = false;
    if (res.of) {
      const [d20] = res.of;
      if ((d20 as any).dice) {
        isCritical = !!(d20 as any).dice[0].critical;
        isAutomaticMiss = (d20 as any).dice[0].value === 1;
      }
    }
    const actor =
      characters.get(attack.instigator) ||
      monsters.get(attack.instigator) ||
      tokens.get(attack.instigator);
    ReactGA.event({ category: Category.Game, action: Action.Attack });
    sendChatMessage(
      `<Attack instigator={\`${attack.instigator}\`} instigatorName={\`${
        actor ? actor.name : 'Unknown'
      }\`} target={${target ? `\`${target._id}\`` : 'null'}} targetName={${
        target ? `\`${target.name}\`` : 'null'
      }} type={\`${attack.type}\`} range={\`${attack.range}\`} targets={\`${
        attack.targets
      }\`} rolled={${res.value}}${isCritical ? ' criticalHit' : ''}${
        isAutomaticMiss ? ' automaticMiss' : ''
      }>${res.toChatMessage(true)}</Attack>`,
      '',
    );
  });
}

export function triggerDamage(
  damage: {
    instigator: string; // TokenID | MonsterID | CharacterID
    damage: string;
    damageType: string;
    damage1: string;
    damage1Type: string;
  },
  target: TokenModel | CharacterModel | null,
) {
  Promise.all([
    rollDice(damage.damage),
    damage.damage1 ? rollDice(damage.damage1) : Promise.resolve(null),
  ]).then(([d0, d1]) => {
    runInAction(() => {
      if (target) {
        target.damage(d0.value as number, damage.damageType);
        if (d1) {
          target.damage(d1.value as number, damage.damage1Type);
        }
      }
    });
    ReactGA.event({ category: Category.Game, action: Action.Damage });
    sendChatMessage(
      `${damageToChat(damage.instigator, damage.damageType, target, d0)}${
        d1
          ? damageToChat(damage.instigator, damage.damage1Type, target, d1)
          : ''
      }`,
      '',
    );
  });
}

export function triggerSave(
  save: {
    instigator: string;
    attribute: string;
    saveDC: number;
    damage: string;
    damageType: string;
    halfOnSuccess: boolean;
  },
  target: TokenModel | CharacterModel | null,
) {
  console.log('triggered save', save, target);
  ReactGA.event({ category: Category.Game, action: Action.Save });
}

export function createToken(
  onto: BattlemapModel,
  { x, y }: { x: number; y: number },
  from: CharacterModel | MonsterModel,
) {
  const sizes = {
    tiny: 0.5,
    small: 1,
    medium: 1,
    large: 2,
    huge: 4,
    gargantuan: 8,
  };
  if (from instanceof MonsterModel) {
    const [dieNr] = from.hitDice.split('d');
    const hpBonus = from.modifiers.constitution * +dieNr;

    return rollDice(`${from.hitDice} + ${hpBonus}`).then(res => {
      return runInAction(() => {
        const newToken = tokens.add({
          ...{ x, y },
          width: sizes[from.size],
          height: sizes[from.size],
          name: from.name,
          represents: from._id,
          color: '#fff',
          overloads: {
            health: {
              current: res.value as number,
              temporary: 0,
            },
            healthMaximum: res.value as number,
          },
          waypoints: [],
          _attachments: {},
        });
        onto.tokens = [...onto.tokens, newToken];
        return newToken;
      });
    });
  } else {
    return runInAction(() => {
      const newToken = tokens.add({
        ...{ x, y },
        width: sizes[from.size],
        height: sizes[from.size],
        name: from.name,
        represents: from._id,
        color: '#fff',
        overloads: {},
        waypoints: [],
        _attachments: {},
      });
      onto.tokens = [...onto.tokens, newToken];
      return newToken;
    });
  }
}

export function createParty(
  onto: WorldModel,
  { x, y }: { x: number; y: number },
  character: CharacterModel,
) {
  return parties.add({
    ...{ x, y },
    currentMap: onto._id,
    members: [character._id],
    milesPerHour: 24,
    travelSpeed: TravelSpeed.Normal,
    waypoints: [],
    sight: {
      normal: true,
      blindsight: 0,
      darkvision: 0,
      truesight: 0,
    },
  });
}

export function joinParty(character: CharacterModel, party: PartyModel) {
  party.members = [...party.members, character];
}

export function createPointOfInterest(
  onto: WorldModel,
  { x, y }: { x: number; y: number },
  from: Partial<IPointOfInterest>,
) {
  const newPoI = pointsOfInterest.add({
    name: '',
    desc: '',
    color: '#ffffff',
    width: 5,
    height: 5,
    ...{ x, y },
    ...from,
    _attachments: {},
  });
  onto.pointsOfInterest = [...onto.pointsOfInterest, newPoI];
  return newPoI;
}

function damageToChat(
  instigator: string, // TokenID | MonsterID | CharacterID
  damageType: string,
  target: TokenModel | CharacterModel | null,
  roll: Resolution,
) {
  const actor =
    characters.get(instigator) ||
    monsters.get(instigator) ||
    tokens.get(instigator);
  return `<Damage instigator={\`${instigator}\`} instigatorName={\`${
    actor ? actor.name : 'Unknown'
  }\`} target={${target ? `\`${target._id}\`` : 'null'}} targetName={${
    target ? `\`${target.name}\`` : 'null'
  }} type={\`${damageType}\`} rolled={${roll.value}}>${roll.toChatMessage(
    true,
  )}</Damage>`;
}
