import ModelStore from './ModelStore';
import { defaultSettings } from '../../models';

export const assets = new ModelStore(
  import('../../models').then(m => m.AssetModel),
);
export const backgrounds = new ModelStore(
  import('../../models').then(m => m.BackgroundModel),
);
export const battlemaps = new ModelStore(
  import('../../models').then(m => m.BattlemapModel),
);
export const characters = new ModelStore(
  import('../../models').then(m => m.CharacterModel),
);
export const chatMessages = new ModelStore(
  import('../../models').then(m => m.ChatMessageModel),
);
export const classes = new ModelStore(
  import('../../models').then(m => m.ClassModel),
);
export const items = new ModelStore(
  import('../../models').then(m => m.ItemModel),
);
export const languages = new ModelStore(
  import('../../models').then(m => m.LanguageModel),
);
export const logs = new ModelStore(
  import('../../models').then(m => m.LogModel),
);
export const monsters = new ModelStore(
  import('../../models').then(m => m.MonsterModel),
);
export const parties = new ModelStore(
  import('../../models').then(m => m.PartyModel),
);
export const players = new ModelStore(
  import('../../models').then(m => m.PlayerModel),
);
export const pointsOfInterest = new ModelStore(
  import('../../models').then(m => m.PointOfInterestModel),
);
export const races = new ModelStore(
  import('../../models').then(m => m.RaceModel),
);
export const spells = new ModelStore(
  import('../../models').then(m => m.SpellModel),
);
export const subclasses = new ModelStore(
  import('../../models').then(m => m.SubclassModel),
);
export const subraces = new ModelStore(
  import('../../models').then(m => m.SubraceModel),
);
export const tokens = new ModelStore(
  import('../../models').then(m => m.TokenModel),
);
export const walls = new ModelStore(
  import('../../models').then(m => m.WallModel),
);
export const worlds = new ModelStore(
  import('../../models').then(m => m.WorldModel),
);

export const combatTracker = new ModelStore(
  import('../../models').then(m => m.CombatTrackerModel),
).asSingleton({
  actors: [],
  currentActor: '',
  round: 0,
});
export const time = new ModelStore(
  import('../../models').then(m => m.TimeModel),
).asSingleton({
  calendar: 'HarptosCalendar',
  now: 0,
});

export const meta = new ModelStore(
  import('../../models').then(m => m.MetaModel),
  { networkSync: false },
).asSingleton({
  settings: { ...defaultSettings },
  lastSync: 0,
  _attachments: {},
});
