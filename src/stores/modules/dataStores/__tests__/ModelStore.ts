import ModelStore, { BaseModel } from '../ModelStore';
import { runInAction } from 'mobx';

describe('ModelStore', () => {
  it('correctly calls the update callback with changed data', async () => {
    jest.useFakeTimers();
    const ogDateNow = Date.now;
    Date.now = jest.fn(() => 1337);
    const update = jest.fn(
      async (docs: { _id: string; _rev?: string; _deleted?: boolean }[]) =>
        docs.map(
          doc =>
            ({ id: doc._id, rev: doc._rev + 'NEW' } as PouchDB.Core.Response),
        ),
    );
    const change = jest.fn(async (id: string, type: string, diff: any) => {});

    const store = new ModelStore((async () => TestModel)());
    await store.initialize(
      {
        test: [
          {
            _id: 'id0',
            _attachments: {},
            _rev: 'prevId0',
            type: 'test',
            lastChanged: 0,
            name: 'test0',
          },
          {
            _id: 'id2',
            _attachments: {},
            _rev: 'prevId2',
            type: 'test',
            lastChanged: 0,
            name: 'test2',
          },
          {
            _id: 'id3',
            _attachments: {},
            _rev: 'prevId3',
            type: 'test',
            lastChanged: 0,
            name: 'test3',
          },
          {
            _id: 'id4',
            _attachments: {},
            _rev: 'prevId4',
            type: 'test',
            lastChanged: 0,
            name: 'test4',
          },
        ],
      },
      update,
      change,
      3000,
    );

    runInAction(() => {
      store.get('id0')!.document.name = 'new test0';
      store.get('id2')!.document.name = 'new test2';
      store.add(
        {
          name: 'test1',
        },
        'id1',
      );
      store.remove('id3');
    });

    expect(update).not.toHaveBeenCalled();

    jest.advanceTimersByTime(10000);
    await new Promise(resolve => setImmediate(resolve));

    expect(update).toHaveBeenCalledWith([
      {
        _id: 'id0',
        _attachments: {},
        _rev: 'prevId0',
        type: 'test',
        lastChanged: 1337,
        name: 'new test0',
      },
      {
        _id: 'id2',
        _attachments: {},
        _rev: 'prevId2',
        type: 'test',
        lastChanged: 1337,
        name: 'new test2',
      },
      {
        _id: 'id1',
        _attachments: {},
        type: 'test',
        lastChanged: 1337,
        name: 'test1',
      },
      {
        _id: 'id3',
        _rev: 'prevId3',
        _deleted: true,
      },
    ]);
    expect(store.get('id0')!.document.lastChanged).toBe(1337);
    expect(store.get('id1')).not.toBe(undefined);
    expect(store.get('id3')).toBe(undefined);

    Date.now = ogDateNow;
  });
});

interface ITest {
  name: string;
}

class TestModel extends BaseModel<ITest> {
  public static readonly type = 'test';
}
