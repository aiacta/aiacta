import { Nullable } from 'babylonjs';
import { action, observable, runInAction } from 'mobx';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import groupBy from '../../../utils/groupBy';
import {
  assets,
  backgrounds,
  battlemaps,
  characters,
  chatMessages,
  classes,
  combatTracker,
  items,
  languages,
  logs,
  meta,
  monsters,
  parties,
  players,
  pointsOfInterest,
  races,
  spells,
  time,
  tokens,
  walls,
  worlds,
  subclasses,
  subraces,
} from './dataStores';
import ModelStore from './ModelStore';

export * from './dataStores';

export const allStores: ModelStore<any, any>[] = [
  assets,
  backgrounds,
  battlemaps,
  characters,
  chatMessages,
  classes,
  items,
  languages,
  logs,
  monsters,
  parties,
  players,
  pointsOfInterest,
  races,
  spells,
  subclasses,
  subraces,
  tokens,
  walls,
  worlds,

  combatTracker,
  time,

  meta,
];

PouchDB.plugin(PouchDBFind);

const store = observable({
  databaseName: null as Nullable<string>,
});

let database: PouchDB.Database;
export async function loadData(
  databaseName: string,
  sendChange: (id: string, type: string, diff: any) => void,
) {
  if (store.databaseName === databaseName) {
    return;
  }
  if (database) {
    database.close();
  }
  database = new PouchDB(databaseName);

  // ensure index exists
  const { indexes } = await database.getIndexes();
  if (!indexes.some(i => i.name === 'type')) {
    await database.createIndex({
      index: { name: 'type', fields: ['type'] },
    });
  }

  // initialize all stores with their data
  const { rows } = await database.allDocs<{ type: string }>({
    include_docs: true,
    attachments: true,
    binary: true,
  });
  const documentGroups = groupBy(rows, 'type');
  await Promise.all(
    allStores.map(store =>
      store.initialize(
        documentGroups,
        docs => database.bulkDocs(docs),
        sendChange,
      ),
    ),
  );

  runInAction(() => {
    store.databaseName = databaseName;
  });
}

export function isReady() {
  return !!store.databaseName;
}

export function hasPendingChanges() {
  return allStores.some(s => s.hasPendingChanges);
}

export async function getRawData() {
  return database.allDocs({
    include_docs: true,
    attachments: true,
    binary: true,
  });
}

export function getData(since?: number, includeAll = false) {
  return allStores
    .filter(s => includeAll || s.networkSync)
    .reduce(
      (acc, store) => ({
        ...acc,
        [store.type]: store.all(since).map(m => m.document),
      }),
      {} as { [key: string]: any[] | undefined },
    );
}

export const putData = action(
  (documentGroups: { [key: string]: any[] | undefined }) => {
    allStores.forEach(store => {
      const documents = documentGroups[store.type];
      if (documents) {
        store.addAll(documents);
      }
    });
  },
);

export function updateData(id: string, type: string, diff: any) {
  const store = allStores.find(store => store.type === type);
  if (store) {
    store.update(id, diff);
  }
}
