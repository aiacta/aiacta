import assignDeep from 'assign-deep';
import { Nullable } from 'babylonjs';
import {
  action,
  computed,
  decorate,
  IReactionDisposer,
  observable,
  reaction,
  toJS,
} from 'mobx';
import uuid from 'uuid/v4';
import deepDiff from '../../../utils/deepDiff';
import { AccessType } from '../../models/types';

export default class ModelStore<
  TModel extends BaseModel<any>,
  TDocument = TModel extends BaseModel<infer U> ? U : never
> {
  private data = observable.map<string, TModel>();
  private changed = observable.set<string>();
  private lastRevs = observable.map<string, string | undefined>(); // id<>rev
  private disposeNetworkUpdate: Nullable<IReactionDisposer> = null;
  private disposeDbUpdate: Nullable<IReactionDisposer> = null;

  private networkSyncSuspended = false;

  private Model: (new (doc: Document<TDocument>) => TModel) & {
    readonly type: string;
  } = null as any;
  public type: string = null as any;

  // eslint-disable-next-line no-useless-constructor
  constructor(
    public readonly promisedModel: Promise<
      (new (doc: Document<TDocument>) => TModel) & {
        readonly type: string;
      }
    >,
    private readonly options = {
      networkSync: true,
    },
  ) {}

  public get networkSync() {
    return this.options.networkSync;
  }

  public suspendNetworkSync(val: boolean) {
    this.networkSyncSuspended = val;
  }

  public get hasPendingChanges() {
    return this.changed.size > 0;
  }

  public get singleton() {
    return this.get(this.Model.type + '-one')!;
  }

  public asSingleton(initialValue: TDocument) {
    const initialize = this.initialize.bind(this);
    const store = this;
    this.initialize = ((async (...args) => {
      await initialize(...args);
      if (!this.get(this.Model.type + '-one')) {
        this.add(initialValue, this.Model.type + '-one');
      }
    }) as typeof store.initialize).bind(this);

    return this;
  }

  public async initialize(
    documentsGroups: { [key: string]: Document<TDocument>[] | undefined },
    update: (
      documents: {
        _id: string;
        _rev?: string;
        _deleted?: boolean;
      }[],
    ) => Promise<(PouchDB.Core.Response | PouchDB.Core.Error)[]>,
    sendChange: (id: string, type: string, diff: any) => void,
    updateDelay = 3000,
    networkDelay = undefined,
  ) {
    this.Model = await this.promisedModel;
    this.type = this.Model.type;

    this.data.clear();
    this.changed.clear();
    this.lastRevs.clear();
    if (this.disposeNetworkUpdate) {
      this.disposeNetworkUpdate();
    }
    if (this.disposeDbUpdate) {
      this.disposeDbUpdate();
    }

    (documentsGroups[this.type] || []).forEach(document => {
      this.data.set(document._id, new this.Model(document));
    });

    this.disposeNetworkUpdate = reaction(
      () => Array.from(this.data.values()).map(d => d.asDocument),
      newDocs => {
        newDocs.forEach(doc => {
          // bail if same object existed before
          if (oldDocs.includes(doc)) {
            return;
          }
          const old = oldDocs.find(d => d._id === doc._id);
          const diff = deepDiff(doc, old);
          if (!old || Object.keys(diff).length > 0) {
            this.changed.add(doc._id);
            if (this.options.networkSync && !this.networkSyncSuspended) {
              sendChange(doc._id, this.type, diff);
            }
          }
        });
        oldDocs.forEach(doc => {
          if (!newDocs.find(d => d._id === doc._id)) {
            this.changed.add(doc._id);
            if (this.options.networkSync && !this.networkSyncSuspended) {
              sendChange(doc._id, this.type, null);
            }
          }
        });

        oldDocs = newDocs;
      },
      { delay: networkDelay },
    );
    this.disposeDbUpdate = reaction(
      () => Array.from(this.changed.values()),
      changedIds => {
        if (changedIds.length > 0) {
          const now = Date.now();
          update(
            changedIds.map(id => {
              const model = this.data.get(id);
              if (model) {
                return {
                  ...toJS(model.document),
                  lastChanged: now,
                };
              } else {
                return {
                  _id: id,
                  _rev: this.lastRevs.get(id),
                  _deleted: true,
                };
              }
            }),
          ).then(responses =>
            responses.forEach(response => {
              if (response.id) {
                const model = this.data.get(response.id);
                if (model) {
                  (model.document as any)._rev = response.rev;
                  model.document.lastChanged = now;
                }
              }
            }),
          );

          this.changed.clear();
        }
      },
      { delay: updateDelay },
    );
    let oldDocs = Array.from(this.data.values()).map(d => d.asDocument);
  }

  public add(
    document: TDocument & { _id?: string; _rev?: string },
    id = document._id || uuid(),
  ) {
    if (this.data.has(id)) {
      const model = this.data.get(id)!;
      apply(model.document, document);
      return model;
    } else {
      const model = new this.Model({
        _attachments: {} as any,
        lastChanged: Date.now(),
        ...document,
        type: this.Model.type,
        _id: id,
        _rev: undefined,
      });
      this.data.set(id, model);
      return model;
    }
  }

  public addAll(documents: (TDocument & { _id?: string; _rev?: string })[]) {
    return documents.map(document => this.add(document));
  }

  public update(id: string, diff: any) {
    if (diff === null) {
      this.remove(id);
    } else {
      const model = this.data.get(id)!;
      if (!model) {
        this.add(diff, id);
      } else {
        apply(model.document, diff);
        model.document.lastChanged = Date.now();
      }
    }
  }

  public get(id: string) {
    return this.data.get(id);
  }

  public remove(idOrModel: string | TModel) {
    const id = typeof idOrModel === 'string' ? idOrModel : idOrModel._id;
    if (this.data.has(id)) {
      const model = this.data.get(id)!;
      model.onDelete();
      this.lastRevs.set(id, model.document._rev);
      this.data.delete(id);
    }
  }

  public all(since?: number) {
    if (typeof since !== 'undefined') {
      return Array.from(this.data.values()).filter(
        d => !d.document.lastChanged || d.document.lastChanged >= since,
      );
    }
    return Array.from(this.data.values());
  }

  public filter(predicate: (model: TModel) => boolean) {
    return this.all().filter(predicate);
  }

  public find(predicate: (model: TModel) => boolean) {
    for (let m of this.data.values()) {
      if (predicate(m)) {
        return m;
      }
    }
    return undefined;
  }

  public tail(num: number) {
    return Array.from(this.data.values())
      .sort(
        (a, b) => (a.document.lastChanged || 0) - (b.document.lastChanged || 0),
      )
      .slice(-num);
  }
}

decorate(ModelStore, {
  hasPendingChanges: computed,
  singleton: computed.struct,
  add: action,
  addAll: action,
  update: action,
  remove: action,
});

type DocumentValue =
  | number
  | string
  | boolean
  | null
  | undefined
  | DocumentArray
  | { [key: string]: DocumentValue };
interface DocumentArray
  extends Array<number | string | boolean | { key: string }> {}

export type Document<
  T extends {
    _attachments?: {
      [key: string]: Attachment;
    };
  }
> = Pick<
  T,
  { [Key in keyof T]: T[Key] extends DocumentValue ? Key : never }[keyof T]
> & {
  readonly _id: string;
  readonly _rev?: string;
  readonly _attachments: T['_attachments'];
  readonly type: string;
  lastChanged: number;
};
export type Attachment = {
  content_type: string;
  data: Blob;
  size: number;
};

export class BaseModel<TDocument> {
  public readonly document: Document<TDocument>;

  constructor(document: Document<TDocument>) {
    this.document = document;
  }

  get _id() {
    return this.document._id;
  }

  public clone() {
    return new (this.constructor as any)(toJS(this.document));
  }

  public onDelete() {}

  public playerIsAllowedTo(
    type: AccessType,
    player: import('../../models').PlayerModel,
  ) {
    if (player.isDungeonMaster && type !== AccessType.SeeThrough) {
      return true;
    }
    return false;
  }

  public get asDocument() {
    const { _rev, lastChanged, ...props } = toJS(this.document);
    return props;
  }
}

decorate(BaseModel, {
  document: observable,
  asDocument: computed.struct,
});

function apply(target: any, changes: any) {
  Object.keys(changes).forEach(key => {
    if (key !== '_id') {
      switch (typeof changes[key]) {
        case 'number':
        case 'string':
        case 'boolean':
          target[key] = changes[key];
          break;
        default:
          if (Array.isArray(changes[key])) {
            if ((changes[key] as any[]).some(d => typeof d !== 'object')) {
              target[key] = changes[key];
            } else {
              target[key] = (changes[key] as Array<{
                key: string;
                add?: any;
                change?: any;
                oldIndex?: any;
                delete?: boolean;
              }>)
                .filter(val => !val.delete)
                .map((val, idx) => {
                  if (val.add) {
                    return val.add;
                  }
                  if (val.change) {
                    return apply(target[key][idx], val.change);
                  }
                  if (val.oldIndex >= 0) {
                    if (val.change) {
                      return apply(target[key][val.oldIndex], val.change);
                    }
                    return target[key][val.oldIndex];
                  }
                  // fallback for 'default arrays'
                  return val;
                });
            }
          } else {
            target[key] = assignDeep(toJS(target[key]), changes[key]);
            // apply(target[key], changes[key]);
          }
          break;
      }
    }
  });
  return target;
}
