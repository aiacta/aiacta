import { action, observable } from 'mobx';

interface Notification {
  message: string;
  closable: boolean;
  autoHideDuration?: number;
  actions: React.ReactNode[];
}

const store = observable({
  notifications: [] as Notification[],
});

export function getNotification(): Notification | undefined {
  return store.notifications.length ? store.notifications[0] : undefined;
}

export const shiftNotification = action(() => {
  store.notifications.shift();
});

export const notify = action(
  (message: string, closable = true, autoHideDuration?: number) => {
    store.notifications.push({
      message,
      closable,
      autoHideDuration,
      actions: [],
    });
  },
);

export const notifyWithActions = action(
  (
    message: string,
    actions: React.ReactNode[],
    closable = true,
    autoHideDuration?: number,
  ) => {
    store.notifications.push({
      message,
      closable,
      autoHideDuration,
      actions,
    });
  },
);
