import { observable } from 'mobx';
import browserHistory from '../../browserHistory';
import { WorldModel, BattlemapModel } from '../models';

const store = observable({
  location: browserHistory.location,
});

browserHistory.listen(location => {
  store.location = location;
});

export function navigateTo(map: WorldModel | BattlemapModel) {
  if (map instanceof WorldModel || map instanceof BattlemapModel) {
    browserHistory.push(`/app/map/${map._id}`);
  }
}
