import { action, observable } from 'mobx';
import useObservable from '../../components/hooks/useObservable';
import {
  AssetModel,
  PartyModel,
  PointOfInterestModel,
  TokenModel,
  WallModel,
} from '../models';

const store = observable({
  selected: new Set<Selectable>(),
});

export type Selectable =
  | TokenModel
  | PointOfInterestModel
  | AssetModel
  | WallModel
  | PartyModel;

export const setSelectionTo = action((models: Selectable | Selectable[]) => {
  store.selected = new Set(Array.isArray(models) ? models : [models]);
});

export const addToSelection = action((models: Selectable | Selectable[]) => {
  (Array.isArray(models) ? models : [models]).forEach(model =>
    store.selected.add(model),
  );
});

export const removeFromSelection = action((model: Selectable) => {
  store.selected.delete(model);
});

export const clearSelection = action(() => {
  store.selected.clear();
});

export function getSelected() {
  return Array.from(store.selected);
}

export function useSelected() {
  return useObservable(() => Array.from(store.selected));
}
