import { Nullable } from 'babylonjs';
import { action, observable, runInAction, when } from 'mobx';
import useObservable from '../../components/hooks/useObservable';
import { CommonChoices } from '../models';

const store = observable({
  choicesToMake: null as Nullable<{
    key: string;
    name: string;
    desc: string;
    choices: CommonChoices;
    choosen: { [key: string]: any };
  }>,
});

export async function promptForChoice(
  key: string,
  choice: {
    name: string;
    desc: string;
    choices: CommonChoices;
  },
) {
  if (store.choicesToMake) {
    throw new Error('A choice is already being made...');
  }

  runInAction(() => {
    store.choicesToMake = { ...choice, key, choosen: {} };
  });

  await when(
    () =>
      !store.choicesToMake ||
      Object.keys(store.choicesToMake.choosen).length ===
        Object.keys(choice.choices).length,
  );

  if (!store.choicesToMake) {
    throw new Error('Choice was rejected');
  }

  const choosen = store.choicesToMake!.choosen;
  runInAction(() => {
    store.choicesToMake = null;
  });

  return choosen;
}

export const applyChoices = action((choices: { [key: string]: any }) => {
  if (store.choicesToMake) {
    store.choicesToMake.choosen = choices;
  }
});

export const cancelPrompt = action(() => {
  store.choicesToMake = null;
});

export function useChoicesToMake() {
  return useObservable(() => store.choicesToMake);
}
