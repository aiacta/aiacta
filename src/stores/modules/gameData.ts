import { action, computed, observable, reaction, runInAction } from 'mobx';
import ReactGA from 'react-ga';
import uuid from 'uuid/v4';
import { Action, Category } from '../../utils/analytics';
import { PlayerModel } from '../models';
import {
  backgrounds,
  classes,
  items,
  languages,
  loadData,
  monsters,
  players,
  races,
  spells,
  subclasses,
  subraces,
} from './dataStores';
import { sendChange, start } from './network';

const colorNames = [
  '#FF0000',
  '#800000',
  '#FFFF00',
  '#808000',
  '#00FF00',
  '#008000',
  '#00FFFF',
  '#008080',
  '#0000FF',
  '#000080',
  '#FF00FF',
  '#800080',
];

const store = observable({
  user: parseUser(window.localStorage.getItem('aiacta.user') || 'null'),
  game: parseGame(window.localStorage.getItem('aiacta.game') || 'null'),
  localGames: parseLocalGames(
    window.localStorage.getItem('aiacta.games') || '[]',
  ),
});

const temporaryPlayer = computed(
  () =>
    new PlayerModel({
      _id: '',
      nickname: 'Annonymous',
      color: '#ff0000',
      ...(store.user || {}),
      accessControl: [],
      activity: {
        map: null,
        position: null,
      },
      isDungeonMaster: false,
      lastChanged: undefined as any,
      type: 'player',
      _attachments: {},
    }),
);

export function myself() {
  return (store.user && players.get(store.user._id)) || temporaryPlayer.get();
}

export const createUser = action((_id: string, nickname: string) => {
  store.user = {
    _id,
    nickname,
    color: colorNames[Math.floor(Math.random() * colorNames.length)],
  };
});

export function game() {
  return store.game;
}

export function localGames() {
  return store.localGames;
}

export async function createGame(name: string) {
  await loadGame(uuid(), name);
  runInAction(() => {
    players.add({ ...myself().document, isDungeonMaster: true });
  });
  await loadSRDContent();
  ReactGA.event({ category: Category.Game, action: Action.Create });
}

export async function importGame(name: string, data: string) {
  await loadGame(uuid(), name);
  players.add({ ...myself().document, isDungeonMaster: true });
  // TODO insert data
  ReactGA.event({ category: Category.Game, action: Action.Import });
}

export async function joinGame(id: string, name: string) {
  await loadGame(id, name);
  ReactGA.event({ category: Category.Game, action: Action.Join });
}

export function gameLink() {
  return store.game && store.game.id;
}

export async function loadSRDContent() {
  const {
    SRDBackgrounds,
    SRDClasses,
    SRDItems,
    SRDLanguages,
    SRDMonsters,
    SRDRaces,
    SRDSpells,
    SRDSubclasses,
    SRDSubraces,

    OtherBackgrounds,
    OtherClasses,
    OtherItems,
    OtherLanguages,
    OtherMonsters,
    OtherRaces,
    OtherSpells,
    OtherSubclasses,
    OtherSubraces,
  } = await import('../data/seeds');
  runInAction(() => {
    SRDBackgrounds.forEach(({ id, doc }) =>
      backgrounds.add(doc, `background-${id}`),
    );
    SRDClasses.forEach(({ id, doc }) => classes.add(doc, `class-${id}`));
    SRDItems.forEach(({ id, doc }) => items.add(doc, `item-${id}`));
    SRDLanguages.forEach(({ id, doc }) => languages.add(doc, `language-${id}`));
    SRDMonsters.forEach(({ id, doc }) => monsters.add(doc, `monster-${id}`));
    SRDRaces.forEach(({ id, doc }) => races.add(doc, `race-${id}`));
    SRDSpells.forEach(({ id, doc }) => spells.add(doc as any, `spell-${id}`));
    SRDSubclasses.forEach(({ id, doc }) =>
      subclasses.add(doc as any, `subclass-${id}`),
    );
    SRDSubraces.forEach(({ id, doc }) =>
      subraces.add(doc as any, `subrace-${id}`),
    );

    OtherBackgrounds.forEach(({ id, doc }) =>
      backgrounds.add(doc, `background-${id}`),
    );
    OtherClasses.forEach(({ id, doc }) => classes.add(doc, `class-${id}`));
    OtherItems.forEach(({ id, doc }) => items.add(doc, `item-${id}`));
    OtherLanguages.forEach(({ id, doc }) =>
      languages.add(doc, `language-${id}`),
    );
    OtherMonsters.forEach(({ id, doc }) => monsters.add(doc, `monster-${id}`));
    OtherRaces.forEach(({ id, doc }) => races.add(doc, `race-${id}`));
    OtherSpells.forEach(({ id, doc }) => spells.add(doc as any, `spell-${id}`));
    OtherSubclasses.forEach(({ id, doc }) =>
      subclasses.add(doc as any, `subclass-${id}`),
    );
    OtherSubraces.forEach(({ id, doc }) =>
      subraces.add(doc as any, `subrace-${id}`),
    );
  });
}

let currentGameId = '';
async function loadGame(id: string, name: string) {
  currentGameId = id;
  runInAction(() => {
    if (!store.localGames.some(g => g.id === id)) {
      store.localGames.push({ id, name });
    }
    store.game = { id, name };
  });
  await loadData(id, sendChange);
  await start(id);
}

reaction(
  () => store.user && { ...store.user },
  user => {
    if (user) {
      window.localStorage.setItem('aiacta.user', JSON.stringify(user));
      ReactGA.set({ userId: user._id });
    } else {
      window.localStorage.removeItem('aiacta.user');
      ReactGA.set({ userId: undefined });
    }
  },
  { fireImmediately: true },
);

reaction(
  () => store.game && { ...store.game },
  game => {
    if (game) {
      window.localStorage.setItem('aiacta.game', JSON.stringify(game));
      if (game.id !== currentGameId) {
        loadGame(game.id, game.name);
      }
    } else {
      window.localStorage.removeItem('aiacta.game');
    }
  },
  { fireImmediately: true },
);

reaction(
  () => store.localGames.map(g => g),
  games => {
    window.localStorage.setItem('aiacta.games', JSON.stringify(games));
  },
  { fireImmediately: true },
);

function parseUser(userStr: string) {
  const user = JSON.parse(userStr) as { _id?: string };
  if (user && user._id) {
    return {
      _id: '',
      nickname: 'Annonymous',
      color: colorNames[Math.floor(Math.random() * colorNames.length)],
      ...user,
    };
  }
  return null;
}

function parseGame(gameStr: string) {
  const game = JSON.parse(gameStr) as { id?: string };
  if (game && game.id) {
    return {
      id: '',
      name: 'Game',
      ...game,
    };
  }
  return null;
}

function parseLocalGames(gamesStr: string) {
  const games = JSON.parse(gamesStr) as { id?: string }[];
  if (Array.isArray(games)) {
    return games.map(g => parseGame(JSON.stringify(g))!).filter(Boolean);
  }
  return [];
}
