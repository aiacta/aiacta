import { computed, decorate } from 'mobx';
import * as calendars from '../data/Calendar';
import { BaseModel } from '../modules/dataStores/ModelStore';

export interface ITime {
  calendar: keyof typeof calendars;
  now: number;
}

export class TimeModel extends BaseModel<ITime> {
  public static readonly type = 'time';

  // @computed.struct
  get calendar() {
    return new calendars[this.document.calendar]();
  }
  set calendar(value) {
    if (value instanceof calendars.HarptosCalendar) {
      this.document.calendar = 'HarptosCalendar';
    }
  }

  // @computed
  get now() {
    return this.document.now;
  }
  set now(value) {
    this.document.now = value;
  }
}

decorate(TimeModel, {
  calendar: computed.struct,
  now: computed,
});
