import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import uuid from 'uuid/v4';
import { characters, worlds } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { TravelSpeed, TokenSight, AccessType } from './types';
import { PlayerModel } from './PlayerModel';

export interface IParty {
  x: number;
  y: number;
  sight: TokenSight;
  milesPerHour: number;
  travelSpeed: TravelSpeed;
  waypoints: Array<{ key: string; x: number; y: number }>;
  currentMap: string;
  members: string[];
}

export class PartyModel extends BaseModel<IParty> {
  public static readonly type = 'party';

  // @computed.struct
  get position() {
    return new Vector2(this.document.x, this.document.y);
  }
  set position(value) {
    this.document.x = value.x;
    this.document.y = value.y;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }
  set sight(value) {
    this.document.sight = value;
  }

  // @computed
  get milesPerHour() {
    return this.document.milesPerHour;
  }
  set milesPerHour(value) {
    this.document.milesPerHour = value;
  }

  // @computed
  get travelSpeed() {
    return this.document.travelSpeed;
  }
  set travelSpeed(value) {
    this.document.travelSpeed = value;
  }

  // @computed.struct
  get waypoints() {
    return this.document.waypoints.map(p => new Vector2(p.x, p.y));
  }
  set waypoints(value) {
    this.document.waypoints = value.map(v => {
      // TODO better resolve existing points?
      const prev = this.document.waypoints.find(
        w => w.x === v.x && w.y === v.y,
      );
      return { key: prev ? prev.key : uuid(), x: v.x, y: v.y };
    });
  }

  // @computed
  get currentMap() {
    return worlds.get(this.document.currentMap)!;
  }
  set currentMap(value) {
    this.document.currentMap = value._id;
  }

  // @computed
  get members() {
    return this.document.members.map(id => characters.get(id)!).filter(Boolean);
  }
  set members(value) {
    this.document.members = Array.from(new Set(value.map(m => m._id)));
  }

  public playerIsAllowedTo(type: AccessType, player: PlayerModel) {
    if (super.playerIsAllowedTo(type, player)) {
      return true;
    }
    if (this.members.some(m => m.owner === player)) {
      return (
        (type & (AccessType.Read | AccessType.Know | AccessType.SeeThrough)) > 0
      );
    }
    return false;
  }
}

decorate(PartyModel, {
  position: computed.struct,
  sight: computed,
  milesPerHour: computed,
  travelSpeed: computed,
  waypoints: computed.struct,
  currentMap: computed,
  members: computed,
});
