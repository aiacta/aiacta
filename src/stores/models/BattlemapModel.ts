import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import { memoizeMulti } from '../../utils/memoize';
import { assets, meta, tokens, walls } from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import { CharacterModel } from './CharacterModel';
import { PlayerModel } from './PlayerModel';
import { AccessType } from './types';

export interface IBattlemap {
  name: string;
  width: number;
  height: number;
  tokens: string[];
  assets: string[];
  walls: string[];
  globalIllumination: number;
  _attachments: {
    image: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class BattlemapModel extends BaseModel<IBattlemap> {
  public static readonly type = 'battlemap';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get image() {
    return (
      this.document._attachments.image && this.document._attachments.image.data
    );
  }
  set image(value) {
    this.document._attachments.image.data = value;
  }
  // @computed
  get imageUrl() {
    return memoizedImageUrls(this.image);
  }

  // @computed.struct
  get size() {
    return new Vector2(this.document.width, this.document.height);
  }
  set size(value) {
    this.document.width = value.x;
    this.document.height = value.y;
  }

  // @computed.struct
  get tokens() {
    return this.document.tokens.map(id => tokens.get(id)!);
  }
  set tokens(value) {
    this.document.tokens = value.map(token => token._id);
  }

  // @computed.struct
  get assets() {
    return this.document.assets.map(id => assets.get(id)!);
  }
  set assets(value) {
    this.document.assets = value.map(asset => asset._id);
  }

  // @computed.struct
  get walls() {
    return this.document.walls.map(id => walls.get(id)!);
  }
  set walls(value) {
    this.document.walls = value.map(wall => wall._id);
  }

  // @computed.struct
  get lightSources() {
    return [...this.tokens, ...this.assets].filter(
      m => m.lightSource,
    ) as Array<{
      position: Vector2;
      lightSource: { dim: number; bright: number };
    }>;
  }

  // @computed
  get globalIllumination() {
    return this.document.globalIllumination;
  }
  set globalIllumination(value) {
    this.document.globalIllumination = value;
  }

  // @computed
  get exploration() {
    const key = `battlemap-exploration-${this._id}`;
    if (key in meta.singleton.document._attachments) {
      return meta.singleton.document._attachments[key].data;
    }
    return null;
  }
  set exploration(value) {
    const key = `battlemap-exploration-${this._id}`;
    if (value) {
      meta.singleton.document._attachments[key] = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (meta.singleton.document._attachments as any)[key] = undefined;
    }
  }
  // @computed
  get explorationUrl() {
    const explorationImg = this.exploration;
    if (explorationImg) {
      return memoizedImageUrls(explorationImg);
    }
    return null;
  }

  public playerIsAllowedTo(type: AccessType, player: PlayerModel) {
    if (super.playerIsAllowedTo(type, player)) {
      return true;
    }
    if (
      this.tokens.some(
        t =>
          t.represents instanceof CharacterModel &&
          t.represents.owner === player,
      )
    ) {
      return (type & (AccessType.Read | AccessType.Know)) > 0;
    }
    return false;
  }
}

decorate(BattlemapModel, {
  name: computed,
  image: computed,
  imageUrl: computed,
  size: computed.struct,
  tokens: computed.struct,
  assets: computed.struct,
  walls: computed.struct,
  lightSources: computed.struct,
  globalIllumination: computed,
  exploration: computed,
  explorationUrl: computed,
});
