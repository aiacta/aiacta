import { computed, decorate } from 'mobx';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { classes } from '../modules/dataStores';

export interface ISpell {
  name: string;
  desc: string;
  higherLevel: string;
  range: string;
  components: 'V' | 'S' | 'M' | 'VS' | 'VM' | 'SM' | 'VSM';
  materials: string;
  ritual: boolean;
  duration: string;
  concentration: boolean;
  castingTime: string;
  level: number;
  school: string;
  classes: string[];
}

export class SpellModel extends BaseModel<ISpell> {
  public static readonly type = 'spell';

  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  get range() {
    return this.document.range;
  }
  set range(value) {
    this.document.range = value;
  }

  get components() {
    return this.document.components;
  }
  set components(value) {
    this.document.components = value;
  }

  get materials() {
    return this.document.materials;
  }
  set materials(value) {
    this.document.materials = value;
  }

  get ritual() {
    return this.document.ritual;
  }
  set ritual(value) {
    this.document.ritual = value;
  }

  get duration() {
    return this.document.duration;
  }
  set duration(value) {
    this.document.duration = value;
  }

  get concentration() {
    return this.document.concentration;
  }
  set concentration(value) {
    this.document.concentration = value;
  }

  get castingTime() {
    return this.document.castingTime;
  }
  set castingTime(value) {
    this.document.castingTime = value;
  }

  get level() {
    return this.document.level;
  }
  set level(value) {
    this.document.level = value;
  }

  get school() {
    return this.document.school;
  }
  set school(value) {
    this.document.school = value;
  }

  get classes() {
    return this.document.classes.map(cls => classes.get(cls)!).filter(Boolean);
  }
  set classes(value) {
    this.document.classes = value.map(cls => cls._id);
  }
}

decorate(SpellModel, {
  name: computed,
  desc: computed,
  range: computed,
  components: computed,
  materials: computed,
  ritual: computed,
  duration: computed,
  concentration: computed,
  castingTime: computed,
  level: computed,
  school: computed,
  classes: computed,
});
