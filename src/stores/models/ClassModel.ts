import { computed, decorate } from 'mobx';
import { subclasses } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';
import {
  Attributes,
  CommonChoices,
  CommonFeature,
  Ressource,
  Saves,
  SkillChoice,
  Skills,
  TokenSight,
} from './types';

export interface IClass {
  name: string;
  desc: string;
  hitDie: number;
  saves: Partial<Saves>;
  skills: Partial<Skills>;
  sight: Partial<TokenSight>;
  vulnerabilities: string[];
  resistances: string[];
  immunities: string[];
  conditionImmunities: string[];
  languages: string[];
  proficiencies: string[];
  choices: CommonChoices & {
    skills: SkillChoice;
  };
  features: Array<
    {
      key: string;
      level: number;
      choices?: CommonChoices;
    } & CommonFeature
  >;
  resources: Array<
    {
      key: string;
      maxByLevel: { [key: number]: number };
    } & Ressource
  >;
  spellcasting:
    | {
        type: 'half' | 'full' | 'pact';
        attribute: keyof Attributes;
        knowsAll: boolean;
      }
    | {
        type: 'none';
        knowsAll?: boolean;
      };
}

export class ClassModel extends BaseModel<IClass> {
  public static readonly type = 'class';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get saves() {
    return this.document.saves;
  }
  set saves(value) {
    this.document.saves = value;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }
  set skills(value) {
    this.document.skills = value;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }
  set sight(value) {
    this.document.sight = value;
  }

  // @computed
  get vulnerabilities() {
    return this.document.vulnerabilities;
  }
  set vulnerabilities(value) {
    this.document.vulnerabilities = value;
  }

  // @computed
  get resistances() {
    return this.document.resistances;
  }
  set resistances(value) {
    this.document.resistances = value;
  }

  // @computed
  get immunities() {
    return this.document.immunities;
  }
  set immunities(value) {
    this.document.immunities = value;
  }

  // @computed
  get conditionImmunities() {
    return this.document.conditionImmunities;
  }
  set conditionImmunities(value) {
    this.document.conditionImmunities = value;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }
  set languages(value) {
    this.document.languages = value;
  }

  // @computed
  get proficiencies() {
    return this.document.proficiencies;
  }
  set proficiencies(value) {
    this.document.proficiencies = value;
  }

  // @computed
  get spellcasting() {
    return this.document.spellcasting;
  }
  set spellcasting(value) {
    this.document.spellcasting = value;
  }

  // @computed
  get choices() {
    return this.document.choices;
  }
  set choices(value) {
    this.document.choices = value;
  }

  // @computed
  get resources() {
    return this.document.resources;
  }
  set resources(value) {
    this.document.resources = value;
  }

  // @computed
  get features() {
    return this.document.features;
  }
  set features(value) {
    this.document.features = value;
  }

  // @computed
  get hitDie() {
    return this.document.hitDie;
  }
  set hitDie(value) {
    this.document.hitDie = value;
  }

  // @computed
  get subclasses() {
    return subclasses.filter(sc => sc.class === this);
  }
}

decorate(ClassModel, {
  name: computed,
  desc: computed,
  saves: computed,
  skills: computed,
  sight: computed,
  vulnerabilities: computed,
  resistances: computed,
  immunities: computed,
  conditionImmunities: computed,
  languages: computed,
  proficiencies: computed,
  spellcasting: computed,
  choices: computed,
  features: computed,
  hitDie: computed,
  subclasses: computed,
});
