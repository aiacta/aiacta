import { computed, decorate } from 'mobx';
import { players } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';

export interface IChatMessage {
  timestamp: number;
  sender: string;
  text: string;
}

export class ChatMessageModel extends BaseModel<IChatMessage> {
  public static readonly type = 'chatMessage';

  // @computed
  get sender() {
    return players.get(this.document.sender);
  }
  set sender(value) {
    this.document.sender = value ? value._id : '';
  }

  // @computed
  get text() {
    return this.document.text;
  }
  set text(value) {
    this.document.text = value;
  }

  // @computed
  get timestamp() {
    return this.document.timestamp;
  }
  set timestamp(value) {
    this.document.timestamp = value;
  }
}

decorate(ChatMessageModel, {
  sender: computed,
  text: computed,
  timestamp: computed,
});
