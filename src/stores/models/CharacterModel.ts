import Program from '@aiacta/dicelang';
import { action, computed, decorate, runInAction } from 'mobx';
import uuid from 'uuid/v4';
import { memoizeMulti } from '../../utils/memoize';
import {
  backgrounds,
  classes,
  items,
  parties,
  players,
  races,
  tokens,
  spells,
} from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import { rollDice } from '../modules/gameActions';
import { promptForChoice } from '../modules/uiPrompt';
import { PlayerModel } from './PlayerModel';
import {
  AccessType,
  Attributes,
  CommonChoices,
  CommonFeature,
  Effect,
  Ressource,
  Saves,
  Skills,
  Speeds,
  TokenAction,
  TokenSight,
} from './types';
import { augmentArray } from './util';

export interface ICharacter {
  name: string;
  desc: string;
  bio: string;
  notes: Array<{ key: string; subject: string; desc: string }>;
  owner: string;
  size: 'tiny' | 'small' | 'medium' | 'large' | 'huge' | 'gargantuan';
  race: string;
  classes: Array<{ key: string; id: string; level: number }>;
  hitDice: Array<{ key: string; faces: number; used: boolean }>;
  choicesMade: { [key: string]: string };
  background: string;
  health: {
    current: number;
    temporary: number;
    rolls: number[];
  };
  deathSaves: {
    successes: number;
    failures: number;
  };
  speed: Speeds;
  attributes: Attributes;
  saves: Saves;
  skills: Skills;
  sight: TokenSight;
  vulnerabilities: string[];
  resistances: string[];
  immunities: string[];
  conditionImmunities: string[];
  languages: string[];
  proficiencies: string[];
  traits: Array<{ key: string } & CommonFeature>;
  features: Array<{ key: string } & CommonFeature>;
  feats: Array<{ key: string } & CommonFeature>;
  customEffects: Array<{ key: string; source: { name: string } } & Effect>;
  resources: Array<
    {
      key: string;
      used: number;
    } & Ressource
  >;
  customActions: Array<
    TokenAction & {
      resource?: string;
    }
  >;
  spellSlotsSpent: {
    pact: number;
    0: 0;
    1: number;
    2: number;
    3: number;
    4: number;
    5: number;
    6: number;
    7: number;
    8: number;
    9: number;
  };
  spellsLearned: Array<{
    key: string;
    spell: string;
    class?: string;
    attribute?: keyof Attributes;
  }>;
  gender: string;
  age: string;
  height: string;
  weight: string;
  eyes: string;
  skin: string;
  hair: string;
  alignment: string;
  experiencePoints: number;
  inventory: Array<{
    key: string;
    item: string;
    location: string;
    equipped: boolean;
    attuned: boolean;
    quantity: number;
  }>;

  initiativeBonusFormula: string;
  armorClassFormula: string;

  _attachments: {
    tokenImage?: Attachment;
    portraitImage?: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class CharacterModel extends BaseModel<ICharacter> {
  public static readonly type = 'character';

  onDelete() {
    tokens
      .filter(token => token.represents === this)
      .forEach(token => tokens.remove(token));
    parties.all().forEach(party => {
      party.members = party.members.filter(m => m !== this);
    });
  }

  // @computed
  get owner() {
    return players.get(this.document.owner);
  }
  set owner(value) {
    this.document.owner = value ? value._id : '';
  }

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get bio() {
    return this.document.bio;
  }
  set bio(value) {
    this.document.bio = value;
  }

  // @computed
  get notes() {
    return this.document.notes;
  }
  set notes(value) {
    this.document.notes = value;
  }

  // @computed
  get level() {
    return this.document.classes.reduce((acc, cls) => acc + cls.level, 0);
  }

  // @computed
  get hitDice() {
    return this.document.hitDice;
  }

  // @computed
  get classes() {
    const augmented = augmentArray(
      this.document.classes,
      cls => classes.get(cls.id)!,
    );
    return this.document.classes as typeof augmented;
  }
  // @computed
  get classesFlat() {
    return this.classes.flatMap(cls =>
      Array<typeof cls.reference>(cls.level).fill(cls.reference),
    );
  }

  public async addClassLevel(id: string) {
    const cls = classes.get(id);
    const docCls = this.document.classes.find(c => c.id === id);
    const currentLevel = docCls ? docCls.level : 0;
    if (cls) {
      const { value: rolledHP } = await rollDice(
        this.document.classes.length ? `d${cls.hitDie}` : `${cls.hitDie}`,
      );

      const subclass = cls.subclasses.find(
        sc => sc._id === this.document.choicesMade[`${cls.name}-subclass`],
      );
      const newFeatures = [
        ...cls.features.filter(f => f.level === currentLevel + 1),
        ...(subclass
          ? subclass.features.filter(f => f.level === currentLevel + 1)
          : []),
      ];

      const choicesMade = await this.resolveChoices(cls.name, newFeatures);

      if (choicesMade[`${cls.name}-subclass`]) {
        const newSubclassFeatures = cls.subclasses
          .find(sc => sc._id === choicesMade[`${cls.name}-subclass`])!
          .features.filter(f => f.level === currentLevel + 1);

        const subclassChoicesMade = await this.resolveChoices(
          cls.name,
          newSubclassFeatures,
        );

        Object.assign(choicesMade, subclassChoicesMade);
      }

      runInAction(() => {
        this.document.choicesMade = {
          ...this.document.choicesMade,
          ...choicesMade,
        };

        cls.resources
          .filter(f =>
            Object.keys(f.maxByLevel).some(k => +k <= currentLevel + 1),
          )
          .forEach(res => {
            const curRes = this.document.resources.find(r => r.key === res.key);
            if (curRes) {
              curRes.max = res.maxByLevel[currentLevel + 1] || curRes.max;
            } else {
              const { maxByLevel, ...newRes } = res;
              this.document.resources.push({ ...newRes, used: 0 });
            }
          });

        this.document.features.push(
          ...newFeatures.map(f => ({
            key: uuid(),
            name: f.name,
            desc: this.replaceDescriptionWithChoices(
              cls.name,
              f.desc,
              cls.features,
            ),
          })),
        );

        if (docCls) {
          docCls.level += 1;
        } else {
          this.document.classes.push({ key: uuid(), id, level: 1 });
        }
        this.document.hitDice.push({
          key: uuid(),
          faces: cls.hitDie,
          used: false,
        });
        this.document.health.rolls.push(rolledHP as number);
        this.document.health.current +=
          (rolledHP as number) + this.modifiers.constitution;

        this.applyChoices(cls.name, newFeatures);
      });
    }
  }

  public async addRacialTraits(id: string) {
    const race = races.get(id);
    if (race) {
      const choicesMade = await this.resolveChoices(race.name, race.traits);

      runInAction(() => {
        this.document.choicesMade = {
          ...this.document.choicesMade,
          ...choicesMade,
        };
        this.document.size = race.size;
        this.document.speed = {
          ...this.document.speed,
          ...race.speed,
        };

        this.document.traits.push(
          ...race.traits.map(t => ({
            key: uuid(),
            name: t.name,
            desc: this.replaceDescriptionWithChoices(
              race.name,
              t.desc,
              race.traits,
            ),
          })),
        );

        this.applyChoices(race.name, race.traits);
      });
    }
  }

  public async addBackgroundFeatures(id: string) {
    const background = backgrounds.get(id);
    if (background) {
      const choicesMade = await this.resolveChoices(
        background.name,
        background.features,
      );

      runInAction(() => {
        this.document.choicesMade = {
          ...this.document.choicesMade,
          ...choicesMade,
        };

        this.document.features.push(
          ...background.features.map(f => ({
            key: uuid(),
            name: f.name,
            desc: this.replaceDescriptionWithChoices(
              background.name,
              f.desc,
              background.features,
            ),
          })),
        );

        this.applyChoices(background.name, background.features);
      });
    }
  }

  private replaceDescriptionWithChoices(
    key: string,
    desc: string,
    arr: Array<{ choices?: CommonChoices }>,
  ) {
    return desc.replace(
      /c\{(?<choiceKey>\w+?)(?::(?<mappingKey>\w+?))?\}/g,
      (match, choiceKey, mappingKey) => {
        if (mappingKey) {
          const arrChoices = arr.find(d =>
            Boolean(d.choices && d.choices[choiceKey]),
          )!.choices![choiceKey];
          return String(
            arrChoices.type === 'custom' &&
              arrChoices.maps![mappingKey][
                this.document.choicesMade[`${key}-${choiceKey}`]
              ],
          );
        }
        return this.document.choicesMade[`${key}-${choiceKey}`];
      },
    );
  }

  private async resolveChoices(
    key: string,
    arr: Array<{
      name: string;
      desc: string;
      choices?: CommonChoices;
    }>,
  ) {
    return arr
      .filter(d =>
        Boolean(
          d.choices &&
            Object.keys(d.choices).some(
              k => !this.document.choicesMade[`${key}-${k}`],
            ),
        ),
      )
      .reduce(async (prom, d) => {
        const acc = await prom;
        const res = await promptForChoice(key, d as Required<typeof d>);
        return { ...acc, ...res };
      }, Promise.resolve({} as { [key: string]: string }));
  }

  private applyChoices(
    key: string,
    arr: Array<{
      name: string;
      desc: string;
      choices?: CommonChoices;
    }>,
  ) {
    arr
      .filter(d =>
        Boolean(
          d.choices &&
            Object.keys(d.choices).some(
              k => !!this.document.choicesMade[`${key}-${k}`],
            ),
        ),
      )
      .forEach(({ choices }) => {
        Object.keys(choices!).forEach(k => {
          const choice = choices![k];
          const choosen = this.document.choicesMade[`${key}-${k}`];
          switch (choice.type) {
            case 'spell':
              this.document.spellsLearned.push({
                key: uuid(),
                spell: choosen,
                attribute: choice.attribute,
              });
              break;
            default:
              break;
          }
        });
      });
  }

  // @action
  public removeClassLevel(id: string) {
    const clsIdx = this.classes.findIndex(cls => cls.id === id);
    if (clsIdx >= 0) {
      const cls = this.classes[clsIdx];
      this.hitDice.splice(clsIdx + cls.level - 1, 1);
      this.health.rolls.splice(clsIdx + cls.level - 1, 1);
      if (cls.level === 1) {
        this.classes.splice(clsIdx, 1);
      } else {
        --cls.level;
      }
    }
  }

  // @computed
  get race() {
    return races.get(this.document.race)!;
  }
  set race(value) {
    this.document.race = value._id;
  }

  // @computed
  get background() {
    return backgrounds.get(this.document.background)!;
  }
  set background(value) {
    this.document.background = value._id;
  }

  // @computed
  get alignment() {
    return this.document.alignment;
  }
  set alignment(value) {
    this.document.alignment = value;
  }

  // @computed
  get experiencePoints() {
    return this.document.experiencePoints;
  }
  set experiencePoints(value) {
    this.document.experiencePoints = value;
  }

  // @computed
  get nextExperiencePoints() {
    return [
      0,
      300,
      900,
      2700,
      6500,
      14000,
      23000,
      34000,
      48000,
      64000,
      85000,
      100000,
      120000,
      140000,
      165000,
      195000,
      225000,
      265000,
      305000,
      355000,
    ][this.level];
  }

  // @computed
  get attributes() {
    return this.document.attributes;
  }

  // @computed.struct
  get modifiers() {
    const m = {
      strength: Math.floor((this.document.attributes.strength - 10) / 2),
      dexterity: Math.floor((this.document.attributes.dexterity - 10) / 2),
      constitution: Math.floor(
        (this.document.attributes.constitution - 10) / 2,
      ),
      wisdom: Math.floor((this.document.attributes.wisdom - 10) / 2),
      intelligence: Math.floor(
        (this.document.attributes.intelligence - 10) / 2,
      ),
      charisma: Math.floor((this.document.attributes.charisma - 10) / 2),
    };
    return m as Readonly<typeof m>;
  }

  // @computed
  get saves() {
    return this.document.saves;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }

  // @computed
  get vulnerabilities() {
    return this.document.vulnerabilities;
  }

  // @computed
  get resistances() {
    return this.document.resistances;
  }

  // @computed
  get immunities() {
    return this.document.immunities;
  }

  // @computed
  get conditionImmunities() {
    return this.document.conditionImmunities;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }

  // @computed
  get speed() {
    return this.document.speed;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }

  // @computed
  get proficiencies() {
    return this.document.proficiencies;
  }

  // @computed
  get health() {
    return this.document.health;
  }
  // @computed
  get healthMaximum() {
    return (
      this.document.health.rolls.reduce(
        (acc, val) =>
          acc +
          Math.max(
            1,
            val + Math.floor((this.document.attributes.constitution - 10) / 2),
          ),
        0,
      ) +
      this.effects
        .filter(eff => eff.affects.includes('healthMaximum'))
        .reduce((acc, cur) => acc + cur.value, 0)
    );
  }

  // @computed
  get deathSaves() {
    return this.document.deathSaves;
  }

  // @computed
  get resources() {
    return this.document.resources;
  }

  // @computed.struct
  get actions() {
    return [
      ...this.inventory
        .filter(inv => inv.equipped && inv.reference.category === 'Weapon')
        .reduce(
          (acc, inv) => {
            const attr = inv.reference.isMelee ? 'strength' : 'dexterity';
            const thrown = inv.reference
              .properties!.map(p =>
                p.match(/Thrown \(range (?<range0>\d+)\/(?<range1>\d+)\)/i),
              )
              .find(m => !!m);

            return [
              ...acc,
              {
                key: inv.key,
                name: `Attack: ${inv.reference.name}`,
                desc: `${
                  inv.reference.isMelee ? 'Melee' : 'Ranged'
                } Weapon Attack: +${this.modifiers[attr] +
                  this.proficiencyBonus} to hit, ${
                  inv.reference.isMelee
                    ? `reach ${inv.reference.range} ft.`
                    : `range ${(inv.reference.range as [number, number])![0]}/${
                        (inv.reference.range as [number, number])![1]
                      } ft.`
                }, one target. Hit: ${inv.reference
                  .damage!.map(
                    (dmg, idx) =>
                      `(${dmg.formula}${
                        idx === 0 ? ` + ${this.modifiers[attr]}` : ''
                      }) ${dmg.type} damage`,
                  )
                  .join(' plus ')}.`,
                type: 'action' as 'action',
              },
              ...(thrown
                ? [
                    {
                      key: inv.key,
                      name: `Attack: ${inv.reference.name}`,
                      desc: `Ranged Weapon Attack: +${this.modifiers[attr] +
                        this.proficiencyBonus} to hit, range ${
                        thrown.groups!.range0
                      }/${
                        thrown.groups!.range1
                      } ft., one target. Hit: ${inv.reference
                        .damage!.map(
                          (dmg, idx) =>
                            `(${dmg.formula}${
                              idx === 0 ? ` + ${this.modifiers[attr]}` : ''
                            }) ${dmg.type} damage`,
                        )
                        .join(' plus ')}.`,
                      type: 'action' as 'action',
                    },
                  ]
                : []),
            ];
          },
          [] as TokenAction[],
        ),
      ...this.document.customActions,
    ] as Readonly<TokenAction>[];
  }

  // @computed
  get proficiencyBonus() {
    return (
      1 +
      Math.ceil(
        this.document.classes.reduce((acc, cls) => acc + cls.level, 0) / 4,
      )
    );
  }

  // @computed
  get customActions() {
    return this.document.customActions;
  }

  // @computed
  get isSpellcaster() {
    return this.classes.some(cls => cls.reference.spellcasting.type !== 'none');
  }

  // @computed
  get canLearnSpells() {
    return this.classes.some(
      cls =>
        cls.reference.spellcasting.type !== 'none' &&
        !cls.reference.spellcasting.knowsAll,
    );
  }

  // @computed
  get spellSlots() {
    const castingLevel = this.classes.reduce(
      (acc, { reference, level }) =>
        acc +
        Math.floor(
          level *
            (reference.spellcasting.type === 'full'
              ? 1
              : reference.spellcasting.type === 'half'
              ? 0.5
              : 0),
        ),
      0,
    ) as keyof typeof slotsPerLevel;

    return slotsPerLevel[castingLevel];
  }

  // @computed
  get spellSlotsPerClass() {
    return this.classes.reduce(
      (acc, { reference, level }) => ({
        ...acc,
        [reference._id]:
          slotsPerLevel[
            Math.floor(
              level *
                (reference.spellcasting.type === 'full'
                  ? 1
                  : reference.spellcasting.type === 'half'
                  ? 0.5
                  : 0),
            ) as keyof typeof slotsPerLevel
          ],
      }),
      {} as { [key: string]: number[] },
    );
  }

  // @computed
  get spellsLearned() {
    return this.document.spellsLearned;
  }
  set spellsLearned(value) {
    this.document.spellsLearned = value;
  }

  // @action
  public learnSpell(id: string) {
    const spell = spells.get(id);
    // TODO ask for the class that should learn this spell?
    const cls = this.classes.find(
      cls =>
        cls.reference.spellcasting.type !== 'none' &&
        !cls.reference.spellcasting.knowsAll,
    );
    if (spell && cls && !this.spellsLearned.some(s => s.spell === id)) {
      this.spellsLearned.push({
        key: uuid(),
        spell: id,
        class: cls.id,
      });
    }
  }

  // @computed
  get spellsKnown() {
    return [
      ...this.spellsLearned.map(s => spells.get(s.spell)!),
      ...this.classes.flatMap(({ reference }) =>
        reference.spellcasting.knowsAll
          ? spells.filter(
              s =>
                s.level > 0 &&
                s.classes.includes(reference) &&
                s.level <=
                  // highest slot for any valid class
                  s.classes.reduce(
                    (acc, cls) =>
                      Math.max(
                        acc,
                        this.spellSlotsPerClass[cls._id]
                          ? 9 -
                              [...this.spellSlotsPerClass[cls._id]]
                                .reverse()
                                .findIndex(d => d > 0)
                          : -1,
                      ),
                    -1,
                  ),
            )
          : [],
      ),
    ];
  }

  // @computed
  get initiativeBonusFormula() {
    return this.document.initiativeBonusFormula;
  }
  set initiativeBonusFormula(value) {
    this.document.initiativeBonusFormula = value;
  }

  // @computed
  get armorClassFormula() {
    return this.document.armorClassFormula;
  }
  set armorClassFormula(value) {
    this.document.armorClassFormula = value;
  }

  // @computed
  get hair() {
    return this.document.hair;
  }
  set hair(value) {
    this.document.hair = value;
  }

  // @computed
  get skin() {
    return this.document.skin;
  }
  set skin(value) {
    this.document.skin = value;
  }

  // @computed
  get gender() {
    return this.document.gender;
  }
  set gender(value) {
    this.document.gender = value;
  }

  // @computed
  get age() {
    return this.document.age;
  }
  set age(value) {
    this.document.age = value;
  }

  // @computed
  get height() {
    return this.document.height;
  }
  set height(value) {
    this.document.height = value;
  }

  // @computed
  get weight() {
    return this.document.weight;
  }
  set weight(value) {
    this.document.weight = value;
  }

  // @computed
  get eyes() {
    return this.document.eyes;
  }
  set eyes(value) {
    this.document.eyes = value;
  }

  // @computed
  get inventory() {
    const augmented = augmentArray(
      this.document.inventory,
      d => items.get(d.item)!,
    );
    return this.document.inventory as typeof augmented;
  }

  // @action
  public addToInventory(id: string) {
    const item = items.get(id);
    if (item) {
      this.document.inventory.push({
        key: uuid(),
        item: item._id,
        equipped: false,
        attuned: false,
        location: '',
        quantity: 1,
      });
    }
  }

  // @action
  public removeFromInventory(key: string, removeAll = false) {
    const idx = this.document.inventory.findIndex(i => i.key === key);
    if (idx >= 0) {
      if (this.document.inventory[idx].quantity > 1) {
        --this.document.inventory[idx].quantity;
      } else {
        this.document.inventory.splice(idx, 1);
      }
    }
  }

  public useItem(key: string) {
    const inv = this.inventory.find(i => i.key === key);
    if (inv && inv.reference.use) {
      switch (inv.reference.use.action) {
        case 'heal':
          rollDice(inv.reference.use.formula).then(res => {
            runInAction(() => {
              this.removeFromInventory(key);
              this.heal(res.value as number);
            });
          });
          break;
        default:
          break;
      }
    }
  }

  // @action
  public heal(amount: number) {
    this.health.current = Math.min(
      this.health.current + amount,
      this.healthMaximum,
    );
  }

  // @action
  public damage(amount: number, type: string, ignoreResistance = false) {
    amount *= this.immunities.includes(type)
      ? 0 // immune
      : this.resistances.includes(type) && !ignoreResistance
      ? 0.5 // resistant
      : 1;
    const newTemporary = this.health.temporary - amount;
    amount -= this.health.temporary;
    const newCurrent = this.health.current - Math.max(0, amount);
    this.health.current = Math.max(0, newCurrent);
    this.health.temporary = Math.max(0, newTemporary);
  }

  // @computed
  get size() {
    const race = races.get(this.document.race);
    return (race && race.size) || 'medium';
  }

  // @computed.struct
  get gear() {
    return {
      armor: this.inventory
        .filter(
          inv =>
            inv.reference.category === 'Armor' &&
            inv.reference.subcategory !== 'Shield' &&
            inv.equipped,
        )
        .map(inv => inv.reference)
        .find(() => true),
      shield: this.inventory
        .filter(
          inv =>
            inv.reference.category === 'Armor' &&
            inv.reference.subcategory === 'Shield' &&
            inv.equipped,
        )
        .map(inv => inv.reference)
        .find(() => true),
      primaryWeapon: this.inventory
        .filter(inv => inv.reference.category === 'Weapon' && inv.equipped)
        .map(inv => inv.reference)
        .find(() => true),
    };
  }

  // @computed
  get traits() {
    return this.document.traits;
  }

  // @computed
  get feats() {
    return this.document.feats;
  }

  // @computed
  get features() {
    return this.document.features;
  }

  // @computed
  get lightSource() {
    return this.effects
      .filter(
        effect =>
          effect.affects.includes('lightSource') &&
          typeof effect.value === 'object',
      )
      .reduce(
        (acc, effect) => ({
          dim: Math.max(effect.value.dim, acc ? acc.dim : 0),
          bright: Math.max(effect.value.bright, acc ? acc.bright : 0),
        }),
        undefined as ({ dim: number; bright: number } | undefined),
      );
  }

  get customEffects() {
    return this.document.customEffects;
  }

  // @computed
  get effects(): Array<Effect & { value: number; source: { name: string } }> {
    return [
      ...this.traits,
      ...this.features,
      ...this.feats,
      ...this.inventory
        .filter(inv => inv.equipped && inv.reference.effects)
        .map(inv => inv.reference),
    ]
      .flatMap(source =>
        (source.effects || []).map(effect => ({
          ...effect,
          source: source as { name: string },
        })),
      )
      .concat(this.customEffects)
      .map(effect => ({
        ...effect,
        value:
          typeof effect.value !== 'undefined'
            ? effect.value
            : (new Program(effect.formula).runSync({}, { this: this } as any)
                .value as number),
      }));
  }

  // @computed
  get tokenImage() {
    return this.document._attachments.tokenImage
      ? this.document._attachments.tokenImage.data
      : undefined;
  }
  set tokenImage(value) {
    if (value) {
      this.document._attachments.tokenImage = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).tokenImage = undefined;
    }
  }
  // @computed
  get tokenImageUrl() {
    return this.document._attachments.tokenImage
      ? memoizedImageUrls(this.document._attachments.tokenImage.data)
      : undefined;
  }

  // @computed
  get portraitImage() {
    return this.document._attachments.portraitImage
      ? this.document._attachments.portraitImage.data
      : undefined;
  }
  set portraitImage(value) {
    if (value) {
      this.document._attachments.portraitImage = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).portraitImage = undefined;
    }
  }
  // @computed
  get portraitImageUrl() {
    return this.document._attachments.portraitImage
      ? memoizedImageUrls(this.document._attachments.portraitImage.data)
      : undefined;
  }

  // @computed
  get party() {
    return parties.find(p => p.members.includes(this));
  }

  public playerIsAllowedTo(type: AccessType, player: PlayerModel) {
    if (super.playerIsAllowedTo(type, player)) {
      return true;
    }
    if (this.owner === player) {
      return true;
    }
    return false;
  }
}

decorate(CharacterModel, {
  owner: computed,
  name: computed,
  desc: computed,
  bio: computed,
  notes: computed,
  level: computed,
  classes: computed,
  classesFlat: computed,
  race: computed,
  background: computed,
  alignment: computed,
  experiencePoints: computed,
  nextExperiencePoints: computed,
  attributes: computed,
  modifiers: computed.struct,
  saves: computed,
  skills: computed,
  vulnerabilities: computed,
  resistances: computed,
  immunities: computed,
  conditionImmunities: computed,
  sight: computed,
  speed: computed,
  languages: computed,
  proficiencies: computed,
  hitDice: computed,
  health: computed,
  healthMaximum: computed,
  deathSaves: computed,
  resources: computed,
  actions: computed.struct,
  proficiencyBonus: computed,
  customActions: computed,
  isSpellcaster: computed,
  canLearnSpells: computed,
  spellSlots: computed,
  spellSlotsPerClass: computed,
  spellsKnown: computed,
  spellsLearned: computed,
  learnSpell: action,
  initiativeBonusFormula: computed,
  armorClassFormula: computed,
  inventory: computed,
  size: computed,
  gear: computed.struct,
  tokenImage: computed,
  tokenImageUrl: computed,
  portraitImage: computed,
  portraitImageUrl: computed,
  party: computed,
  hair: computed,
  skin: computed,
  gender: computed,
  age: computed,
  weight: computed,
  eyes: computed,
  lightSource: computed,
  effects: computed,

  removeClassLevel: action,
  addToInventory: action,
  removeFromInventory: action,
  heal: action,
  damage: action,
});

const slotsPerLevel = {
  1: [0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
  2: [0, 3, 0, 0, 0, 0, 0, 0, 0, 0],
  3: [0, 4, 2, 0, 0, 0, 0, 0, 0, 0],
  4: [0, 4, 3, 0, 0, 0, 0, 0, 0, 0],
  5: [0, 4, 3, 2, 0, 0, 0, 0, 0, 0],
  6: [0, 4, 3, 3, 0, 0, 0, 0, 0, 0],
  7: [0, 4, 3, 3, 1, 0, 0, 0, 0, 0],
  8: [0, 4, 3, 3, 2, 0, 0, 0, 0, 0],
  9: [0, 4, 3, 3, 3, 1, 0, 0, 0, 0],
  10: [0, 4, 3, 3, 3, 2, 0, 0, 0, 0],
  11: [0, 4, 3, 3, 3, 2, 1, 0, 0, 0],
  12: [0, 4, 3, 3, 3, 2, 1, 0, 0, 0],
  13: [0, 4, 3, 3, 3, 2, 1, 1, 0, 0],
  14: [0, 4, 3, 3, 3, 2, 1, 1, 0, 0],
  15: [0, 4, 3, 3, 3, 2, 1, 1, 1, 0],
  16: [0, 4, 3, 3, 3, 2, 1, 1, 1, 0],
  17: [0, 4, 3, 3, 3, 2, 1, 1, 1, 1],
  18: [0, 4, 3, 3, 3, 3, 1, 1, 1, 1],
  19: [0, 4, 3, 3, 3, 3, 2, 1, 1, 1],
  20: [0, 4, 3, 3, 3, 3, 2, 2, 1, 1],
};
