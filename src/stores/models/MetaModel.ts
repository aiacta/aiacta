import { computed, decorate } from 'mobx';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';

export const defaultSettings: IMeta['settings'] = {
  trackCursor: true,
};

export interface IMeta {
  lastSync: number;
  settings: {
    trackCursor: boolean;
  };

  _attachments: {
    [key: string]: Attachment;
  };
}

export class MetaModel extends BaseModel<IMeta> {
  public static readonly type = 'meta';

  // @computed
  get settings() {
    return this.document.settings;
  }
  set settings(value) {
    this.document.settings = value;
  }

  // @computed
  get lastSync() {
    return this.document.lastSync;
  }
  set lastSync(value) {
    this.document.lastSync = value;
  }
}

decorate(MetaModel, {
  settings: computed,
  lastSync: computed,
});
