import { computed, decorate } from 'mobx';
import { races } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';
import {
  CommonChoices,
  CommonFeature,
  Speeds,
  Attributes,
  Saves,
  TokenSight,
  Skills,
} from './types';

export interface ISubrace {
  race: string;
  name: string;
  desc: string;
  traits: Array<{ key: string; choices?: CommonChoices } & CommonFeature>;
  speed: Partial<Speeds>;
  attributeBonuses: Partial<Attributes>;
  saves: Partial<Saves>;
  skills: Partial<Skills>;
  sight: Partial<TokenSight>;
  vulnerabilities: string[];
  resistances: string[];
  immunities: string[];
  conditionImmunities: string[];
  languages: string[];
  proficiencies: string[];
}

export class SubraceModel extends BaseModel<ISubrace> {
  public static readonly type = 'subrace';

  // @computed
  get race() {
    return races.get(this.document.race)!;
  }
  set race(value) {
    this.document.race = value._id;
  }

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get traits() {
    return this.document.traits;
  }
  set traits(value) {
    this.document.traits = value;
  }

  // @computed
  get attributeBonuses() {
    return this.document.attributeBonuses;
  }
  set attributeBonuses(value) {
    this.document.attributeBonuses = value;
  }

  // @computed
  get saves() {
    return this.document.saves;
  }
  set saves(value) {
    this.document.saves = value;
  }

  // @computed
  get speed() {
    return this.document.speed;
  }
  set speed(value) {
    this.document.speed = value;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }
  set skills(value) {
    this.document.skills = value;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }
  set sight(value) {
    this.document.sight = value;
  }

  // @computed
  get vulnerabilities() {
    return this.document.vulnerabilities;
  }
  set vulnerabilities(value) {
    this.document.vulnerabilities = value;
  }

  // @computed
  get resistances() {
    return this.document.resistances;
  }
  set resistances(value) {
    this.document.resistances = value;
  }

  // @computed
  get immunities() {
    return this.document.immunities;
  }
  set immunities(value) {
    this.document.immunities = value;
  }

  // @computed
  get conditionImmunities() {
    return this.document.conditionImmunities;
  }
  set conditionImmunities(value) {
    this.document.conditionImmunities = value;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }
  set languages(value) {
    this.document.languages = value;
  }

  // @computed
  get proficiencies() {
    return this.document.proficiencies;
  }
  set proficiencies(value) {
    this.document.proficiencies = value;
  }
}

decorate(SubraceModel, {
  name: computed,
  desc: computed,
  traits: computed,
  attributeBonuses: computed,
  saves: computed,
  speed: computed,
  skills: computed,
  sight: computed,
  vulnerabilities: computed,
  resistances: computed,
  immunities: computed,
  conditionImmunities: computed,
  languages: computed,
  proficiencies: computed,
});
