export type Attributes = {
  strength: number;
  dexterity: number;
  constitution: number;
  wisdom: number;
  intelligence: number;
  charisma: number;
};

export const attributes: (keyof Attributes)[] = [
  'strength',
  'dexterity',
  'constitution',
  'wisdom',
  'intelligence',
  'charisma',
];

export const skills: (keyof Skills)[] = [
  'athletics',
  'acrobatics',
  'sleightOfHand',
  'stealth',
  'arcana',
  'history',
  'investigation',
  'nature',
  'religion',
  'animalHandling',
  'insight',
  'medicine',
  'perception',
  'survival',
  'deception',
  'intimidation',
  'performance',
  'persuation',
];

export type Saves = { [p in keyof Attributes]: SkillProficiency };

export type Skills = {
  athletics: SkillProficiency;
  acrobatics: SkillProficiency;
  sleightOfHand: SkillProficiency;
  stealth: SkillProficiency;
  arcana: SkillProficiency;
  history: SkillProficiency;
  investigation: SkillProficiency;
  nature: SkillProficiency;
  religion: SkillProficiency;
  animalHandling: SkillProficiency;
  insight: SkillProficiency;
  medicine: SkillProficiency;
  perception: SkillProficiency;
  survival: SkillProficiency;
  deception: SkillProficiency;
  intimidation: SkillProficiency;
  performance: SkillProficiency;
  persuation: SkillProficiency;
};

export type Speeds = {
  walk: number;
  swim: number;
  hover: number;
  fly: number;
  climb: number;
};

export enum SkillProficiency {
  None = 0,
  Half = 0.5,
  Proficient = 1,
  Expertise = 2,
}

export enum Rest {
  Short,
  Long,
}

export interface CommonFeature {
  name: string;
  desc: string;
  effects?: Array<{ key: string } & Effect>;
}

export interface Ressource {
  name: string;
  desc: string;
  max: number;
  refresh: Rest;
}

export interface Effect {
  affects: string[];
  formula: string;
  value?: any;
  visibility: 'hidden' | 'self' | 'everyone';
  icon?: string;
}

export enum AccessType {
  Read = 2 ** 0, // read values (e.g. character sheets / monster stats)
  Write = 2 ** 1, // write values (e.g. change character sheet / monster stats)
  Control = 2 ** 2, // control object (e.g. move token)
  SeeThrough = 2 ** 3, // have vision from object (e.g. can see from token)
  Know = 2 ** 4, // knows an object (e.g. visible in listings)
}

export enum TravelSpeed {
  Slow = 0.5,
  Normal = 1,
  Fast = 2,
}

export interface CommonChoices {
  [choiceKey: string]: AnyChoice;
}

export type AnyChoice =
  | CustomChoice
  | AttributeChoice
  | SkillChoice
  | SpellChoice
  | LanguageChoice
  | EquipmentChoice
  | SubclassChoice
  | SubraceChoice;

export type CustomChoice = {
  type: 'custom';
  options: string;
  maps?: {
    [alternateName: string]: {
      [choice: string]: string | boolean | number;
    };
  };
};

export type AttributeChoice = {
  type: 'attribute';
  points: number;
  includes?: (keyof Attributes)[];
  excludes?: (keyof Attributes)[];
  distinct?: boolean;
};

export type SkillChoice = {
  type: 'skill';
  num: number;
  expertise?: boolean;
  includes?: (keyof Skills)[];
  excludes?: (keyof Skills)[];
};

export type SpellChoice = {
  type: 'spell';
  level: number;
  list?: string;
  attribute?: keyof Attributes;
};

export type LanguageChoice = {
  type: 'language';
  num: number;
  includes?: string[];
  excludes?: string[];
};

export type EquipmentChoice = {
  type: 'equipment';
  // options: Array<{key: string; }>;
};

export type SubclassChoice = {
  type: 'subclass';
  class: string;
};

export type SubraceChoice = {
  type: 'subrace';
  race: string;
};

export interface TokenStats {
  health: {
    current: number;
    temporary: number;
  };
  healthMaximum: number;
}

export type TokenSight = {
  normal: boolean;
  darkvision: number;
  blindsight: number;
  truesight: number;
};

export type TokenAction = {
  key: string;
  name: string;
  desc: string;
  type: 'action' | 'bonusAction' | 'reaction' | 'special' | 'legendary';
};
