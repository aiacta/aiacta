import { computed, decorate } from 'mobx';
import { BaseModel } from '../modules/dataStores/ModelStore';

export interface ILanguage {
  name: string;
  script: string;
}

export class LanguageModel extends BaseModel<ILanguage> {
  public static readonly type = 'language';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(val) {
    this.document.name = val;
  }

  // @computed
  get script() {
    return this.document.script;
  }
  set script(val) {
    this.document.script = val;
  }
}

decorate(LanguageModel, {
  name: computed,
  script: computed,
});
