import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import uuid from 'uuid/v4';
import { battlemaps } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';

export interface IWall {
  thickness: number;
  height: number;
  closed: boolean;
  vertices: Array<{ key: string; x: number; y: number }>;
}

export class WallModel extends BaseModel<IWall> {
  public static readonly type = 'wall';

  onDelete() {
    battlemaps
      .filter(map => map.walls.includes(this))
      .forEach(map => (map.walls = map.walls.filter(w => w !== this)));
  }

  // @computed
  get battlemap() {
    return battlemaps.find(bm => bm.walls.includes(this));
  }

  // @computed
  get thickness() {
    return this.document.thickness;
  }
  set thickness(value) {
    this.document.thickness = value;
  }

  // @computed
  get height() {
    return this.document.height;
  }
  set height(value) {
    this.document.height = value;
  }

  // @computed
  get closed() {
    return this.document.closed;
  }
  set closed(value) {
    this.document.closed = value;
  }

  // @computed.struct
  get vertices() {
    return this.document.vertices.map(v => new Vector2(v.x, v.y));
  }
  set vertices(value) {
    this.document.vertices = value.map(v => {
      // TODO better resolve existing points?
      const prev = this.document.vertices.find(w => w.x === v.x && w.y === v.y);
      return { key: prev ? prev.key : uuid(), x: v.x, y: v.y };
    });
  }
}

decorate(WallModel, {
  battlemap: computed,
  thickness: computed,
  height: computed,
  closed: computed,
  vertices: computed.struct,
});
