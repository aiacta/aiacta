import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import { memoizeMulti } from '../../utils/memoize';
import { battlemaps, worlds } from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';

export interface IPointOfInterest {
  name: string;
  desc: string;
  color: string;
  x: number;
  y: number;
  width: number;
  height: number;
  linksTo?: string;

  _attachments: {
    image?: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class PointOfInterestModel extends BaseModel<IPointOfInterest> {
  public static readonly type = 'poi';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get color() {
    return this.document.color;
  }
  set color(value) {
    this.document.color = value;
  }

  // @computed
  get linksTo() {
    return this.document.linksTo
      ? (worlds.get(this.document.linksTo) ||
          battlemaps.get(this.document.linksTo))!
      : null;
  }
  set linksTo(value) {
    this.document.linksTo = value ? value._id : '';
  }

  // @computed
  get image() {
    return this.document._attachments.image
      ? this.document._attachments.image.data
      : undefined;
  }
  set image(value) {
    if (value) {
      this.document._attachments.image = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).image = undefined;
    }
  }
  // @computed
  get imageUrl() {
    if (this.document._attachments.image) {
      return memoizedImageUrls(this.document._attachments.image.data);
    }
    return null;
  }

  // @computed.struct
  get position() {
    return new Vector2(this.document.x, this.document.y);
  }
  set position(value) {
    this.document.x = value.x;
    this.document.y = value.y;
  }

  // @computed.struct
  get size() {
    return new Vector2(this.document.width, this.document.height);
  }
  set size(value) {
    this.document.width = value.x;
    this.document.height = value.y;
  }
}

decorate(PointOfInterestModel, {
  name: computed,
  desc: computed,
  color: computed,
  linksTo: computed,
  image: computed,
  imageUrl: computed,
  position: computed.struct,
  size: computed.struct,
});
