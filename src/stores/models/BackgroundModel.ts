import { computed, decorate } from 'mobx';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { CommonChoices, CommonFeature, Skills } from './types';

export interface IBackground {
  name: string;
  desc: string;
  skills: Partial<Skills>;
  languages: string[];
  proficiencies: string[];
  choices: CommonChoices;
  features: Array<{ key: string; choices?: CommonChoices } & CommonFeature>;
}

export class BackgroundModel extends BaseModel<IBackground> {
  public static readonly type = 'background';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }
  set skills(value) {
    this.document.skills = value;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }
  set languages(value) {
    this.document.languages = value;
  }

  // @computed
  get proficiencies() {
    return this.document.proficiencies;
  }
  set proficiencies(value) {
    this.document.proficiencies = value;
  }

  // @computed
  get choices() {
    return this.document.choices;
  }
  set choices(value) {
    this.document.choices = value;
  }

  // @computed
  get features() {
    return this.document.features;
  }
  set features(value) {
    this.document.features = value;
  }
}

decorate(BackgroundModel, {
  name: computed,
  desc: computed,
  skills: computed,
  languages: computed,
  proficiencies: computed,
  choices: computed,
  features: computed,
});
