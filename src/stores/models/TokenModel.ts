import { Vector2 } from 'babylonjs';
import { action, computed, decorate, observable } from 'mobx';
import uuid from 'uuid/v4';
import { memoizeMulti } from '../../utils/memoize';
import {
  battlemaps,
  characters,
  combatTracker,
  monsters,
} from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import { CharacterModel } from './CharacterModel';
import { PlayerModel } from './PlayerModel';
import { AccessType, TokenStats } from './types';

export interface IToken {
  name: string;
  x: number;
  y: number;
  width: number;
  height: number;
  color: string;
  represents: string;
  overloads: Partial<TokenStats>;
  lightSource?: {
    dim: number;
    bright: number;
  };
  waypoints: Array<{ key: string; x: number; y: number }>;

  _attachments: {
    image?: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class TokenModel extends BaseModel<IToken> {
  public static readonly type = 'token';

  // @action
  onDelete() {
    const idx = combatTracker.singleton.actors.findIndex(
      a => a.reference === this,
    );
    if (idx > 0) {
      combatTracker.singleton.actors.splice(idx, 1);
    }
    battlemaps.all().forEach(map => {
      map.tokens = map.tokens.filter(t => t !== this);
    });
  }

  // @computed
  get battlemap() {
    return battlemaps.find(bm => bm.tokens.includes(this));
  }

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get image() {
    if (this.document._attachments.image) {
      return this.document._attachments.image.data;
    }
    const representee = (characters.get(this.document.represents) ||
      monsters.get(this.document.represents))!;
    return representee.tokenImage;
  }
  set image(value) {
    if (value) {
      this.document._attachments.image = {
        content_type: value instanceof File ? value.type : 'unknown',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).image = undefined;
    }
  }
  // @computed
  get imageUrl() {
    if (this.document._attachments.image) {
      return memoizedImageUrls(this.document._attachments.image.data);
    }
    const representee = (characters.get(this.document.represents) ||
      monsters.get(this.document.represents))!;
    if (representee.tokenImage) {
      return memoizedImageUrls(representee.tokenImage);
    }
    return null;
  }

  // @computed.struct
  get position() {
    return new Vector2(this.document.x, this.document.y);
  }
  set position(value) {
    this.document.x = value.x;
    this.document.y = value.y;
  }

  // @computed.struct
  get size() {
    return new Vector2(this.document.width, this.document.height);
  }
  set size(value) {
    this.document.width = value.x;
    this.document.height = value.y;
  }

  // @computed
  get represents() {
    return (characters.get(this.document.represents) ||
      monsters.get(this.document.represents))!;
  }
  set represents(value) {
    this.document.represents = value._id;
  }

  // @computed
  get sight() {
    return this.represents.sight;
  }

  // @computed
  get health() {
    return this.represents instanceof CharacterModel
      ? this.represents.health
      : this.document.overloads.health!;
  }
  // @computed
  get healthMaximum() {
    return this.represents instanceof CharacterModel
      ? this.represents.healthMaximum
      : this.document.overloads.healthMaximum!;
  }

  // @computed
  get actions() {
    if (this.represents instanceof CharacterModel) {
      return this.represents.actions;
    } else {
      return this.represents.actions;
    }
  }

  // @computed
  get lightSource() {
    return (
      this.document.lightSource ||
      (this.represents instanceof CharacterModel
        ? this.represents.lightSource
        : undefined)
    );
  }
  set lightSource(value) {
    this.document.lightSource = value;
  }

  // @computed.struct
  get waypoints() {
    return this.document.waypoints.map(p => new Vector2(p.x, p.y));
  }
  set waypoints(value) {
    this.document.waypoints = value.map(v => {
      // TODO better resolve existing points?
      const prev = this.document.waypoints.find(
        w => w.x === v.x && w.y === v.y,
      );
      return { key: prev ? prev.key : uuid(), x: v.x, y: v.y };
    });
  }

  // @action
  public damage(amount: number, type: string, ignoreResistance = false) {
    amount *= this.represents.immunities.includes(type)
      ? 0 // immune
      : this.represents.resistances.includes(type) && !ignoreResistance
      ? 0.5 // resistant
      : 1;
    const newTemporary = this.health.temporary - amount;
    amount -= this.health.temporary;
    const newCurrent = this.health.current - Math.max(0, amount);
    this.health.current = Math.max(0, newCurrent);
    this.health.temporary = Math.max(0, newTemporary);
  }

  // @observable
  public isVisible = false;

  public playerIsAllowedTo(type: AccessType, player: PlayerModel) {
    if (super.playerIsAllowedTo(type, player)) {
      return true;
    }
    if (this.represents instanceof CharacterModel) {
      if (this.represents.owner === player) {
        return true;
      }
    }
    return false;
  }
}

decorate(TokenModel, {
  battlemap: computed,
  name: computed,
  image: computed,
  imageUrl: computed,
  position: computed.struct,
  size: computed.struct,
  represents: computed,
  sight: computed,
  health: computed,
  healthMaximum: computed,
  actions: computed,
  lightSource: computed,
  waypoints: computed.struct,

  damage: action,

  isVisible: observable,
});
