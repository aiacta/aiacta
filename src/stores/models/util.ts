import { extendObservable } from 'mobx';

export function augmentArray<T, K, U extends string = 'reference'>(
  arr: T[],
  aug: (d: T) => K,
  prop?: U,
): Array<T & { readonly [P in U]: K }> {
  return arr.map((d: any) =>
    d[prop || 'reference']
      ? d
      : extendObservable(d, {
          get [prop || 'reference']() {
            return aug(d);
          },
        }),
  );
}
