import { action, computed, decorate } from 'mobx';
import uuid from 'uuid/v4';
import { memoizeMulti } from '../../utils/memoize';
import { characters } from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import { AccessType } from './types';

export interface IPlayer {
  nickname: string;
  isDungeonMaster: boolean;
  color: string;
  accessControl: Array<{
    key: string;
    id: string;
    mode: number;
  }>;
  activity: {
    map: string | null;
    position: { x: number; y: number } | null;
  };
  _attachments: {
    avatar?: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class PlayerModel extends BaseModel<IPlayer> {
  public static readonly type = 'player';

  // @computed
  get nickname() {
    return this.document.nickname;
  }
  set nickname(value) {
    this.document.nickname = value;
  }

  // @computed
  get avatar() {
    return this.document._attachments.avatar
      ? this.document._attachments.avatar.data
      : null;
  }
  set avatar(value) {
    if (value) {
      this.document._attachments.avatar = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).avatar = undefined;
    }
  }
  // @computed
  get avatarUrl() {
    if (this.document._attachments.avatar) {
      return memoizedImageUrls(this.document._attachments.avatar.data);
    }
    return null;
  }

  // @computed
  get isDungeonMaster() {
    if (window.localStorage.getItem('aiacta.forceAsPlayer')) {
      return false;
    }
    return this.document.isDungeonMaster;
  }
  set isDungeonMaster(value) {
    this.document.isDungeonMaster = value;
  }

  // @computed
  get color() {
    return this.document.color;
  }
  set color(value) {
    this.document.color = value;
  }

  // @computed
  get activity() {
    return this.document.activity;
  }
  set activity(value) {
    this.document.activity = value;
  }

  // @computed.struct
  get characters() {
    return characters.filter(c => c.owner === this);
  }

  // @computed.struct
  get parties() {
    return this.characters.map(c => c.party!).filter(Boolean);
  }

  public isAllowedTo(type: AccessType, model: BaseModel<any>) {
    const ac = this.document.accessControl.find(a => a.id === model._id);
    // tslint:disable-next-line:no-bitwise
    return (
      Boolean(ac && (ac.mode & type) > 0) || model.playerIsAllowedTo(type, this)
    );
  }

  // @action
  public allowTo(type: AccessType, id: string) {
    const ac = this.document.accessControl.find(a => a.id === id);
    if (ac) {
      // tslint:disable-next-line:no-bitwise
      ac.mode |= type;
    } else {
      this.document.accessControl.push({ key: uuid(), id, mode: type });
    }
  }
  // @action
  public disallowTo(type: AccessType, id: string) {
    const ac = this.document.accessControl.find(a => a.id === id);
    if (ac) {
      // tslint:disable-next-line:no-bitwise
      ac.mode ^= type;
    }
  }
}

decorate(PlayerModel, {
  nickname: computed,
  avatar: computed,
  avatarUrl: computed,
  isDungeonMaster: computed,
  color: computed,
  activity: computed,
  characters: computed.struct,
  parties: computed.struct,

  allowTo: action,
  disallowTo: action,
});
