import { computed, decorate } from 'mobx';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { Effect } from './types';

export interface IItem {
  name: string;
  desc: string;
  weight: number;
  price: number;
  category: string;
  subcategory: string;
  identified: boolean;
  rarity:
    | 'common'
    | 'uncommon'
    | 'rare'
    | 'very rare'
    | 'legendary'
    | 'artifact';
  requiresAttunement: boolean;
  damage?: Array<{ key: string; formula: string; type: string }>;
  range?: number | [number, number];
  armorClass?: number;
  maxDexterityBonus?: number;
  stealthDisadvantage?: boolean;
  minimumStrength?: number;
  properties?: string[];
  effects?: Array<{ key: string } & Effect>;
  use?: {
    action: 'heal';
    formula: string;
  };
}

export class ItemModel extends BaseModel<IItem> {
  public static readonly type = 'item';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(val) {
    this.document.name = val;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(val) {
    this.document.desc = val;
  }

  // @computed
  get weight() {
    return this.document.weight;
  }
  set weight(val) {
    this.document.weight = val;
  }

  // @computed
  get price() {
    return this.document.price;
  }
  set price(val) {
    this.document.price = val;
  }

  // @computed
  get category() {
    return this.document.category;
  }
  set category(val) {
    this.document.category = val;
  }

  // @computed
  get subcategory() {
    return this.document.subcategory;
  }
  set subcategory(val) {
    this.document.subcategory = val;
  }

  // @computed
  get equipable() {
    return Boolean(this.damage || this.armorClass || this.effects);
  }

  // @computed
  get identified() {
    return this.document.identified;
  }
  set identified(val) {
    this.document.identified = val;
  }

  // @computed
  get rarity() {
    return this.document.rarity;
  }
  set rarity(val) {
    this.document.rarity = val;
  }

  // @computed
  get requiresAttunement() {
    return this.document.requiresAttunement;
  }
  set requiresAttunement(val) {
    this.document.requiresAttunement = val;
  }

  // @computed
  get damage() {
    return this.document.damage;
  }
  set damage(val) {
    this.document.damage = val;
  }

  // @computed
  get range() {
    return this.document.range;
  }
  set range(val) {
    this.document.range = val;
  }

  // @computed
  get isMelee() {
    return this.document.range && !Array.isArray(this.document.range);
  }

  // @computed
  get armorClass() {
    return this.document.armorClass;
  }
  set armorClass(val) {
    this.document.armorClass = val;
  }

  // @computed
  get maxDexterityBonus() {
    return this.document.maxDexterityBonus;
  }
  set maxDexterityBonus(val) {
    this.document.maxDexterityBonus = val;
  }

  // @computed
  get stealthDisadvantage() {
    return this.document.stealthDisadvantage;
  }
  set stealthDisadvantage(val) {
    this.document.stealthDisadvantage = val;
  }

  // @computed
  get properties() {
    return this.document.properties;
  }
  set properties(val) {
    this.document.properties = val;
  }

  // @computed
  get effects() {
    return this.document.effects;
  }
  set effects(val) {
    this.document.effects = val;
  }

  // @computed
  get use() {
    return this.document.use;
  }
  set use(val) {
    this.document.use = val;
  }
}

decorate(ItemModel, {
  name: computed,
  desc: computed,
  weight: computed,
  price: computed,
  category: computed,
  subcategory: computed,
  equipable: computed,
  identified: computed,
  rarity: computed,
  requiresAttunement: computed,
  damage: computed,
  range: computed,
  isMelee: computed,
  armorClass: computed,
  maxDexterityBonus: computed,
  stealthDisadvantage: computed,
  properties: computed,
  effects: computed,
  use: computed,
});
