import { computed, decorate } from 'mobx';
import { classes } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { CommonChoices, CommonFeature } from './types';

export interface ISubclass {
  class: string;
  name: string;
  desc: string;
  features: Array<
    {
      key: string;
      level: number;
      choices?: CommonChoices;
    } & CommonFeature
  >;
}

export class SubclassModel extends BaseModel<ISubclass> {
  public static readonly type = 'subclass';

  // @computed
  get class() {
    return classes.get(this.document.class)!;
  }
  set class(value) {
    this.document.class = value._id;
  }

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get features() {
    return this.document.features;
  }
  set features(value) {
    this.document.features = value;
  }
}

decorate(SubclassModel, {
  name: computed,
  desc: computed,
  features: computed,
});
