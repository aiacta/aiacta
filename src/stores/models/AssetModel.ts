import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';

export interface IAsset {
  name: string;
  x: number;
  y: number;
  width: number;
  height: number;
  color: string;
  lightSource: {
    dim: number;
    bright: number;
  };
  _attachments: {
    image: Attachment;
  };
}

export class AssetModel extends BaseModel<IAsset> {
  public static readonly type = 'asset';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get image() {
    return this.document._attachments.image.data;
  }
  set image(value) {
    this.document._attachments.image.data = value;
  }

  // @computed.struct
  get position() {
    return new Vector2(this.document.x, this.document.y);
  }
  set position(value) {
    this.document.x = value.x;
    this.document.y = value.y;
  }

  // @computed.struct
  get size() {
    return new Vector2(this.document.width, this.document.height);
  }
  set size(value) {
    this.document.width = value.x;
    this.document.height = value.y;
  }

  // @computed.struct
  get lightSource() {
    return this.document.lightSource;
  }
  set lightSource(value) {
    this.document.lightSource = value;
  }
}

decorate(AssetModel, {
  name: computed,
  image: computed,
  position: computed.struct,
  size: computed.struct,
  lightSource: computed.struct,
});
