import { computed, decorate } from 'mobx';
import { tokens } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';
import { augmentArray } from './util';

export interface ICombatTracker {
  actors: Array<{
    key: string;
    token: string;
    initiative: number;
  }>;
  currentActor: string;
  round: number;
}

export class CombatTrackerModel extends BaseModel<ICombatTracker> {
  public static readonly type = 'combatTracker';

  // @computed
  get actors() {
    const augmented = augmentArray(
      this.document.actors,
      d => tokens.get(d.token)!,
    );
    return this.document.actors as typeof augmented;
  }

  // @computed
  get currentActor() {
    return this.actors.find(a => a.key === this.document.currentActor);
  }
  set currentActor(value) {
    this.document.currentActor = value ? value.key : '';
  }

  // @computed
  get round() {
    return this.document.round;
  }
  set round(value) {
    this.document.round = value;
  }
}

decorate(CombatTrackerModel, {
  actors: computed,
  currentActor: computed,
  round: computed,
});
