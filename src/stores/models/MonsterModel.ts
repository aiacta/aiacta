import { computed, decorate } from 'mobx';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import {
  Attributes,
  Saves,
  SkillProficiency,
  TokenAction,
  TokenSight,
} from './types';

export interface IMonster {
  name: string;
  size: 'tiny' | 'small' | 'medium' | 'large' | 'huge' | 'gargantuan';
  mtype: string;
  subtype: string;
  alignment: string;
  armorClass: number;
  hitDice: string;
  speed: {
    walk: number;
    swim: number;
    hover: number;
    fly: number;
    climb: number;
  };
  attributes: Attributes;
  saves: Saves;
  skills: {
    athletics: SkillProficiency;
    acrobatics: SkillProficiency;
    sleightOfHand: SkillProficiency;
    stealth: SkillProficiency;
    arcana: SkillProficiency;
    history: SkillProficiency;
    investigation: SkillProficiency;
    nature: SkillProficiency;
    religion: SkillProficiency;
    animalHandling: SkillProficiency;
    insight: SkillProficiency;
    medicine: SkillProficiency;
    perception: SkillProficiency;
    survival: SkillProficiency;
    deception: SkillProficiency;
    intimidation: SkillProficiency;
    performance: SkillProficiency;
    persuation: SkillProficiency;
  };
  vulnerabilities: string;
  resistances: string;
  immunities: string;
  conditionImmunities: string;
  sight: TokenSight;
  languages: string;
  challengeRating: number;
  specialAbilities: Array<{
    key: string;
    name: string;
    description: string;
  }>;
  actions: TokenAction[];
  legendaryActions: TokenAction[];

  _attachments: {
    tokenImage?: Attachment;
    portraitImage?: Attachment;
  };
}

export class MonsterModel extends BaseModel<IMonster> {
  public static readonly type = 'monster';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get size() {
    return this.document.size;
  }
  set size(value) {
    this.document.size = value;
  }

  // @computed
  get mtype() {
    return this.document.mtype;
  }
  set mtype(value) {
    this.document.mtype = value;
  }

  // @computed
  get subtype() {
    return this.document.subtype;
  }
  set subtype(value) {
    this.document.subtype = value;
  }

  // @computed
  get alignment() {
    return this.document.alignment;
  }
  set alignment(value) {
    this.document.alignment = value;
  }

  // @computed
  get armorClass() {
    return this.document.armorClass;
  }
  set armorClass(value) {
    this.document.armorClass = value;
  }

  // @computed
  get hitDice() {
    return this.document.hitDice;
  }
  set hitDice(value) {
    this.document.hitDice = value;
  }

  // @computed
  get speed() {
    return this.document.speed;
  }
  set speed(value) {
    this.document.speed = value;
  }

  // @computed
  get attributes() {
    return this.document.attributes;
  }
  set attributes(value) {
    this.document.attributes = value;
  }

  // @computed.struct
  get modifiers() {
    return {
      strength: Math.floor((this.document.attributes.strength - 10) / 2),
      dexterity: Math.floor((this.document.attributes.dexterity - 10) / 2),
      constitution: Math.floor(
        (this.document.attributes.constitution - 10) / 2,
      ),
      wisdom: Math.floor((this.document.attributes.wisdom - 10) / 2),
      intelligence: Math.floor(
        (this.document.attributes.intelligence - 10) / 2,
      ),
      charisma: Math.floor((this.document.attributes.charisma - 10) / 2),
    };
  }

  // @computed
  get saves() {
    return this.document.saves;
  }
  set saves(value) {
    this.document.saves = value;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }
  set skills(value) {
    this.document.skills = value;
  }

  // @computed
  get vulnerabilities() {
    return this.document.vulnerabilities;
  }
  set vulnerabilities(value) {
    this.document.vulnerabilities = value;
  }

  // @computed
  get resistances() {
    return this.document.resistances;
  }
  set resistances(value) {
    this.document.resistances = value;
  }

  // @computed
  get immunities() {
    return this.document.immunities;
  }
  set immunities(value) {
    this.document.immunities = value;
  }

  // @computed
  get conditionImmunities() {
    return this.document.conditionImmunities;
  }
  set conditionImmunities(value) {
    this.document.conditionImmunities = value;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }
  set sight(value) {
    this.document.sight = value;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }
  set languages(value) {
    this.document.languages = value;
  }

  // @computed
  get challengeRating() {
    return this.document.challengeRating;
  }
  set challengeRating(value) {
    this.document.challengeRating = value;
  }

  // @computed
  get proficiencyBonus() {
    return 2 + Math.max(0, Math.floor((this.document.challengeRating - 1) / 4));
  }

  // @computed
  get xpWorth() {
    return challengeRatingToXP[
      this.challengeRating as keyof typeof challengeRatingToXP
    ];
  }

  // @computed
  get specialAbilities() {
    return this.document.specialAbilities;
  }
  set specialAbilities(value) {
    this.document.specialAbilities = value;
  }

  // @computed
  get actions() {
    return this.document.actions;
  }
  set actions(value) {
    this.document.actions = value;
  }

  // @computed
  get legendaryActions() {
    return this.document.legendaryActions;
  }
  set legendaryActions(value) {
    this.document.legendaryActions = value;
  }

  // @computed
  get tokenImage() {
    return this.document._attachments.tokenImage
      ? this.document._attachments.tokenImage.data
      : undefined;
  }
  set tokenImage(value) {
    if (value) {
      this.document._attachments.tokenImage = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).tokenImage = undefined;
    }
  }

  // @computed
  get portraitImage() {
    return this.document._attachments.portraitImage
      ? this.document._attachments.portraitImage.data
      : undefined;
  }
  set portraitImage(value) {
    if (value) {
      this.document._attachments.portraitImage = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (this.document._attachments as any).portraitImage = undefined;
    }
  }
}

const challengeRatingToXP = {
  0: 10,
  '0.125': 25,
  '0.25': 50,
  '0.5': 100,
  1: 200,
  2: 450,
  3: 700,
  4: 1100,
  5: 1800,
  6: 2300,
  7: 2900,
  8: 3900,
  14: 11500,
  15: 13000,
  16: 15000,
  17: 18000,
  18: 20000,
  19: 22000,
  20: 25000,
  21: 33000,
  22: 41000,
  23: 50000,
  24: 62000,
  25: 75000,
};

decorate(MonsterModel, {
  name: computed,
  size: computed,
  mtype: computed,
  subtype: computed,
  alignment: computed,
  armorClass: computed,
  hitDice: computed,
  speed: computed,
  attributes: computed,
  modifiers: computed,
  saves: computed,
  skills: computed,
  vulnerabilities: computed,
  resistances: computed,
  immunities: computed,
  conditionImmunities: computed,
  sight: computed,
  languages: computed,
  challengeRating: computed,
  proficiencyBonus: computed,
  xpWorth: computed,
  specialAbilities: computed,
  actions: computed,
  legendaryActions: computed,
  tokenImage: computed,
  portraitImage: computed,
});
