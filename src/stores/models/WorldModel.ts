import { Vector2 } from 'babylonjs';
import { computed, decorate } from 'mobx';
import { memoizeMulti } from '../../utils/memoize';
import { meta, parties, pointsOfInterest } from '../modules/dataStores';
import { Attachment, BaseModel } from '../modules/dataStores/ModelStore';
import { PlayerModel } from './PlayerModel';
import { AccessType } from './types';
import { WallModel } from './WallModel';

export interface IWorld {
  name: string;
  width: number;
  height: number;
  pointsOfInterest: string[];
  _attachments: {
    image: Attachment;
  };
}

const memoizedImageUrls = memoizeMulti((blob: Blob) =>
  URL.createObjectURL(blob),
);

export class WorldModel extends BaseModel<IWorld> {
  public static readonly type = 'world';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get image() {
    return this.document._attachments.image.data;
  }
  set image(value) {
    this.document._attachments.image.data = value;
  }
  // @computed
  get imageUrl() {
    return memoizedImageUrls(this.document._attachments.image.data);
  }

  // @computed.struct
  get size() {
    return new Vector2(this.document.width, this.document.height);
  }
  set size(value) {
    this.document.width = value.x;
    this.document.height = value.y;
  }

  // @computed.struct
  get pointsOfInterest() {
    return this.document.pointsOfInterest
      .map(id => pointsOfInterest.get(id)!)
      .filter(Boolean);
  }
  set pointsOfInterest(value) {
    this.document.pointsOfInterest = value.map(poi => poi._id);
  }

  // @computed.struct
  get parties() {
    return parties.all().filter(p => p.currentMap === this);
  }

  // @computed.struct
  get lightSources() {
    return this.parties.map(m => ({
      position: m.position,
      lightSource: { dim: 550, bright: 500 },
    }));
  }

  get walls() {
    return [] as WallModel[];
  }

  // @computed
  get exploration() {
    const key = `world-exploration-${this._id}`;
    if (key in meta.singleton.document._attachments) {
      return meta.singleton.document._attachments[key].data;
    }
    return null;
  }
  set exploration(value) {
    const key = `world-exploration-${this._id}`;
    if (value) {
      meta.singleton.document._attachments[key] = {
        content_type: value instanceof File ? value.type : 'image/png',
        data: value,
        size: value.size,
      };
    } else {
      (meta.singleton.document._attachments as any)[key] = undefined;
    }
  }
  // @computed
  get explorationUrl() {
    const explorationImg = this.exploration;
    if (explorationImg) {
      return memoizedImageUrls(explorationImg);
    }
    return null;
  }

  public playerIsAllowedTo(type: AccessType, player: PlayerModel) {
    if (super.playerIsAllowedTo(type, player)) {
      return true;
    }
    if (this.parties.some(p => p.members.some(m => m.owner === player))) {
      return (type & (AccessType.Read | AccessType.Know)) > 0;
    }
    return false;
  }
}

decorate(WorldModel, {
  name: computed,
  image: computed,
  imageUrl: computed,
  size: computed.struct,
  pointsOfInterest: computed.struct,
  parties: computed.struct,
  lightSources: computed.struct,
  exploration: computed,
  explorationUrl: computed,
});
