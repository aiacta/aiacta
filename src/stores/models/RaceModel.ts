import { computed, decorate } from 'mobx';
import { BaseModel } from '../modules/dataStores/ModelStore';
import {
  Attributes,
  CommonChoices,
  CommonFeature,
  Saves,
  Skills,
  Speeds,
  TokenSight,
} from './types';
import { subraces } from '../modules/dataStores';

export interface IRace {
  name: string;
  desc: string;
  traits: Array<{ key: string; choices?: CommonChoices } & CommonFeature>;
  size: 'tiny' | 'small' | 'medium' | 'large' | 'huge' | 'gargantuan';
  speed: Partial<Speeds>;
  attributeBonuses: Partial<Attributes>;
  saves: Partial<Saves>;
  skills: Partial<Skills>;
  sight: Partial<TokenSight>;
  vulnerabilities: string[];
  resistances: string[];
  immunities: string[];
  conditionImmunities: string[];
  languages: string[];
  proficiencies: string[];
  maleNames: string[];
  femaleNames: string[];
  surnames: string[];
  skinColors: string[];
  hairColors: string[];
  eyeColors: string[];
  adultAge: number;
  lifeExpectancy: number;
  baseHeight: number;
  heightRoll: string;
  baseWeight: number;
  weightRoll: string;
}

export class RaceModel extends BaseModel<IRace> {
  public static readonly type = 'race';

  // @computed
  get name() {
    return this.document.name;
  }
  set name(value) {
    this.document.name = value;
  }

  // @computed
  get desc() {
    return this.document.desc;
  }
  set desc(value) {
    this.document.desc = value;
  }

  // @computed
  get traits() {
    return this.document.traits;
  }
  set traits(value) {
    this.document.traits = value;
  }

  // @computed
  get size() {
    return this.document.size;
  }
  set size(value) {
    this.document.size = value;
  }

  // @computed
  get attributeBonuses() {
    return this.document.attributeBonuses;
  }
  set attributeBonuses(value) {
    this.document.attributeBonuses = value;
  }

  // @computed
  get saves() {
    return this.document.saves;
  }
  set saves(value) {
    this.document.saves = value;
  }

  // @computed
  get speed() {
    return this.document.speed;
  }
  set speed(value) {
    this.document.speed = value;
  }

  // @computed
  get skills() {
    return this.document.skills;
  }
  set skills(value) {
    this.document.skills = value;
  }

  // @computed
  get sight() {
    return this.document.sight;
  }
  set sight(value) {
    this.document.sight = value;
  }

  // @computed
  get vulnerabilities() {
    return this.document.vulnerabilities;
  }
  set vulnerabilities(value) {
    this.document.vulnerabilities = value;
  }

  // @computed
  get resistances() {
    return this.document.resistances;
  }
  set resistances(value) {
    this.document.resistances = value;
  }

  // @computed
  get immunities() {
    return this.document.immunities;
  }
  set immunities(value) {
    this.document.immunities = value;
  }

  // @computed
  get conditionImmunities() {
    return this.document.conditionImmunities;
  }
  set conditionImmunities(value) {
    this.document.conditionImmunities = value;
  }

  // @computed
  get languages() {
    return this.document.languages;
  }
  set languages(value) {
    this.document.languages = value;
  }

  // @computed
  get proficiencies() {
    return this.document.proficiencies;
  }
  set proficiencies(value) {
    this.document.proficiencies = value;
  }

  // @computed
  get maleNames() {
    return this.document.maleNames;
  }
  set maleNames(value) {
    this.document.maleNames = value;
  }

  // @computed
  get femaleNames() {
    return this.document.femaleNames;
  }
  set femaleNames(value) {
    this.document.femaleNames = value;
  }

  // @computed
  get surnames() {
    return this.document.surnames;
  }
  set surnames(value) {
    this.document.surnames = value;
  }

  // @computed
  get adultAge() {
    return this.document.adultAge;
  }
  set adultAge(value) {
    this.document.adultAge = value;
  }

  // @computed
  get lifeExpectancy() {
    return this.document.lifeExpectancy;
  }
  set lifeExpectancy(value) {
    this.document.lifeExpectancy = value;
  }

  // @computed
  get baseHeight() {
    return this.document.baseHeight;
  }
  set baseHeight(value) {
    this.document.baseHeight = value;
  }

  // @computed
  get heightRoll() {
    return this.document.heightRoll;
  }
  set heightRoll(value) {
    this.document.heightRoll = value;
  }

  // @computed
  get baseWeight() {
    return this.document.baseWeight;
  }
  set baseWeight(value) {
    this.document.baseWeight = value;
  }

  // @computed
  get weightRoll() {
    return this.document.weightRoll;
  }
  set weightRoll(value) {
    this.document.weightRoll = value;
  }

  // @computed
  get subraces() {
    return subraces.filter(sr => sr.race === this);
  }
}

decorate(RaceModel, {
  name: computed,
  desc: computed,
  traits: computed,
  size: computed,
  attributeBonuses: computed,
  saves: computed,
  speed: computed,
  skills: computed,
  sight: computed,
  vulnerabilities: computed,
  resistances: computed,
  immunities: computed,
  conditionImmunities: computed,
  languages: computed,
  proficiencies: computed,
  maleNames: computed,
  femaleNames: computed,
  surnames: computed,
  adultAge: computed,
  lifeExpectancy: computed,
  baseHeight: computed,
  heightRoll: computed,
  baseWeight: computed,
  weightRoll: computed,
  subraces: computed,
});
