import { IBattlemap } from './BattlemapModel';
import { IWorld } from './WorldModel';

export * from './AssetModel';
export * from './BackgroundModel';
export * from './BattlemapModel';
export * from './CharacterModel';
export * from './ChatMessageModel';
export * from './ClassModel';
export * from './CombatTrackerModel';
export * from './ItemModel';
export * from './LanguageModel';
export * from './LogModel';
export * from './MetaModel';
export * from './MonsterModel';
export * from './PartyModel';
export * from './PlayerModel';
export * from './PointOfInterestModel';
export * from './RaceModel';
export * from './SpellModel';
export * from './SubclassModel';
export * from './SubraceModel';
export * from './TimeModel';
export * from './TokenModel';
export * from './types';
export * from './WallModel';
export * from './WorldModel';

export function isIWorld(d: IWorld | IBattlemap): d is IWorld {
  return 'pointsOfInterest' in d;
}
