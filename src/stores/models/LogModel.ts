import { computed, decorate } from 'mobx';
import { players } from '../modules/dataStores';
import { BaseModel } from '../modules/dataStores/ModelStore';

export interface ILog {
  message: string;
  category: string;
  timestamp: number;
  origin: string;
  player: string;
}

export class LogModel extends BaseModel<ILog> {
  public static readonly type = 'log';

  get message() {
    return this.document.message;
  }
  set message(value) {
    this.document.message = value;
  }

  get category() {
    return this.document.category;
  }
  set category(value) {
    this.document.category = value;
  }

  get timestamp() {
    return this.document.timestamp;
  }
  set timestamp(value) {
    this.document.timestamp = value;
  }

  get origin() {
    return this.document.origin;
  }
  set origin(value) {
    this.document.origin = value;
  }

  get player() {
    return players.get(this.document.player);
  }
  set player(value) {
    this.document.player = value ? value._id : '';
  }
}

decorate(LogModel, {
  message: computed,
  category: computed,
  timestamp: computed,
  origin: computed,
  player: computed,
});
