import { ICalendar } from '../Calendar';

export class HarptosCalendar implements ICalendar {
  public readonly days = [
    'First',
    'Second',
    'Third',
    'Forth',
    'Fifth',
    'Sixth',
    'Seventh',
    'Eighth',
    'Ninth',
    'Tenth',
  ];

  public readonly months = [
    ['Hammer', 30],
    ['Midwinter', 1],
    ['Alturiak', 30],
    ['Ches', 30],
    ['Tarsakh', 30],
    ['Greengrass', 1],
    ['Mirtul', 30],
    ['Kythorn', 30],
    ['Flamerule', 30],
    ['Midsummer', 1],
    ['Eleasis', 30],
    ['Eleint', 30],
    ['Highharvestide', 1],
    ['Marpenoth', 30],
    ['Uktar', 30],
    ['Feast of the Moon', 1],
    ['Nightal', 30],
  ] as Array<[string, number]>;
  private cdaysInYear = this.months.reduce((acc, [, days]) => acc + days, 0);

  get daysInWeek() {
    return this.days.length;
  }

  get daysInYear() {
    return this.cdaysInYear;
  }

  public dateToTimestamp(
    year: number,
    month: number,
    day: number,
    hour: number,
    minute: number,
    second: number,
  ) {
    const daysSinceStartOfYear = this.months
      .slice(0, month)
      .reduce((acc, [, days]) => acc + days, 0);
    const daysSinceYear0 =
      this.daysInYear * year + daysSinceStartOfYear + (day - 1);
    return daysSinceYear0 * 86400 + hour * 3600 + minute * 60 + second;
  }

  public timestampToDate(time: number) {
    const year = Math.floor(time / 86400 / this.daysInYear);
    const [month, timeLeft] = (this.months.reduce<string | number>(
      (acc, [, days], i) =>
        typeof acc === 'number'
          ? acc - days * 86400 > 0
            ? acc - days * 86400
            : `${i}:${acc}`
          : acc,
      time % (this.daysInYear * 86400),
    ) as string)
      .split(':')
      .map(d => +d);
    const day = Math.floor(timeLeft / 86400) + 1;
    const hour = Math.floor((timeLeft % 86400) / 3600);
    const minute = Math.floor(((timeLeft % 86400) % 3600) / 60);
    const second = ((timeLeft % 86400) % 3600) % 60;
    return {
      year,
      month,
      day,
      hour,
      minute,
      second,
    };
  }
}
