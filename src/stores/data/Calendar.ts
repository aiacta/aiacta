export interface ICalendar {
  months: Array<[string, number]>;

  daysInWeek: number;
  daysInYear: number;

  dateToTimestamp(
    year: number,
    month: number,
    day: number,
    hour: number,
    minute: number,
    second: number,
  ): number;
  timestampToDate(
    ts: number,
  ): {
    year: number;
    month: number;
    day: number;
    hour: number;
    minute: number;
    second: number;
  };
}

export { HarptosCalendar } from './impl/HarptosCalendar';
