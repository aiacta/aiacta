import { IRace, SkillProficiency } from '../../models';

export default [
  {
    id: 'dragonborn',
    doc: {
      name: 'Dragonborn',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        strength: 2,
        charisma: 1,
      },
      sight: {},
      speed: { walk: 30 },
      saves: {},
      skills: {},
      languages: ['Common', 'Draconic'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 15,
      lifeExpectancy: 80,
      baseHeight: 66,
      heightRoll: '2d8',
      baseWeight: 175,
      weightRoll: '2d6',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc:
            'Your Strength score increases by 2, and your Charisma score increses by 1.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Young dragonborn grow quickly. They walk hours after hatching, attain the size and development of a 10-year-old human child by the age of 3, and reach adulthood by 15. They live to be around 80.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Dragonborn tend to extremes, making a conscious choice for one side or the other in the cosmic war between good and evil. Most dragonborn are good, but those who side with evil can be terrible villains.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Dragonborn are taller and heavier than humans, standing well over 6 feet tall and averaging almost 250 pounds. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'draconic',
          name: 'Draconic Ancestry',
          desc:
            'You have draconic ancestry. Choose one type of dragon from the Draconic Ancestry table. Your breath weapon and damage resistance are determined by the dragon type, as shown in the table.',
          choices: {
            draconicAncestry: {
              type: 'custom',
              options: `
| Dragon | Damage Type | Breath Weapon                |
|--------|-------------|------------------------------|
| Black  | Acid        | 5 by 30 ft. line (Dex. save) |
| Blue   | Lightning   | 5 by 30 ft. line (Dex. save) |
| Brass  | Fire        | 5 by 30 ft. line (Dex. save) |
| Bronze | Lightning   | 5 by 30 ft. line (Dex. save) |
| Copper | Acid        | 5 by 30 ft. line (Dex. save) |
| Gold   | Fire        | 15 ft. cone (Dex. save)      |
| Green  | Poison      | 15 ft. cone (Con. save)      |
| Red    | Fire        | 15 ft. cone (Dex. save)      |
| Silver | Cold        | 15 ft. cone (Con. save)      |
| White  | Cold        | 15 ft. cone (Con. save)      |`,
              maps: {
                damageType: {
                  Black: 'acid',
                  Blue: 'lightning',
                  Brass: 'fire',
                  Bronze: 'lightning',
                  Copper: 'acid',
                  Gold: 'fire',
                  Green: 'poison',
                  Red: 'fire',
                  Silver: 'cold',
                  White: 'cold',
                },
                breathShape: {
                  Black: '5 by 30 ft. line',
                  Blue: '5 by 30 ft. line',
                  Brass: '5 by 30 ft. line',
                  Bronze: '5 by 30 ft. line',
                  Copper: '5 by 30 ft. line',
                  Gold: '15 ft. cone',
                  Green: '15 ft. cone',
                  Red: '15 ft. cone',
                  Silver: '15 ft. cone',
                  White: '15 ft. cone',
                },
                breathSave: {
                  Black: 'Dexterity',
                  Blue: 'Dexterity',
                  Brass: 'Dexterity',
                  Bronze: 'Dexterity',
                  Copper: 'Dexterity',
                  Gold: 'Dexterity',
                  Green: 'Constitution',
                  Red: 'Dexterity',
                  Silver: 'Constitution',
                  White: 'Constitution',
                },
              },
            },
          },
        },
        {
          key: 'breathWeapon',
          name: 'Breath Weapon',
          desc: `You can use your action to exhale destructive energy. Your draconic ancestry determines the size, shape, and damage type of the exhalation.

When you use your breath weapon, each creature in a c{draconicAncestry:breathShape} must make a c{draconicAncestry:breathSave} saving throw. The DC for this saving throw equals 8 + your Constitution modifier + your proficiency bonus. A creature takes 2d6 c{draconicAncestry:damageType} damage on a failed save, and half as much damage on a successful one. The damage increases to 3d6 at 6th level, 4d6 at 11th level, and 5d6 at 16th level.

After you use your breath weapon, you can’t use it again until you complete a short or long rest.`,
        },
        {
          key: 'dmgResist',
          name: 'Damage Resistance',
          desc: 'You have resistance to c{draconicAncestry:damageType} damage.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and Draconic. Draconic is thought to be one of the oldest languages and is often used in the study of magic. The language sounds harsh to most other creatures and includes numerous hard consonants and sibilants.',
        },
      ],
    } as IRace,
  },
  {
    id: 'dwarf',
    doc: {
      name: 'Dwarf',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        constitution: 2,
      },
      sight: { darkvision: 60 },
      speed: { walk: 25 },
      saves: {},
      skills: {},
      languages: ['Common', 'Dwarvish'],
      conditionImmunities: [],
      immunities: [],
      resistances: ['poison'],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 50,
      lifeExpectancy: 350,
      baseHeight: 44,
      heightRoll: '2d4',
      baseWeight: 115,
      weightRoll: '2d6',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Increase',
          desc: 'Your Constitution score increases by 2.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Dwarves mature at the same rate as humans, but they’re considered young until they reach the age of 50. On average, they live about 350 years.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Most dwarves are lawful, believing firmly in the benefits of a well-ordered society. They tend toward good as well, with a strong sense of fair play and a belief that everyone deserves to share in the benefits of a just order.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Dwarves stand between 4 and 5 feet tall and average about 150 pounds. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc:
            'Your base walking speed is 25 feet. Your speed is not reduced by wearing heavy armor.',
        },
        {
          key: 'vision',
          name: 'Darkvision',
          desc:
            'You have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You cannot discern color in darkness, only shades of gray.',
        },
        {
          key: 'dwarvenRes',
          name: 'Dwarven Resilience',
          desc:
            'You have advantage on saving throws against poison, and you have resistance against poison damage.',
        },
        {
          key: 'stonecunning',
          name: 'Stonecunning',
          desc:
            'Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and Dwarvish. Dwarvish is full of hard consonants and guttural sounds, and those characteristics spill over into whatever other language a dwarf might speak.',
        },
      ],
    } as IRace,
  },
  {
    id: 'elf',
    doc: {
      name: 'Elf',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        dexterity: 2,
      },
      sight: { darkvision: 60 },
      speed: { walk: 30 },
      saves: {},
      skills: { perception: SkillProficiency.Proficient },
      languages: ['Common', 'Elvish'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 100,
      lifeExpectancy: 750,
      baseHeight: 54,
      heightRoll: '2d10',
      baseWeight: 90,
      weightRoll: '1d4',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Dexterity score increases by 2.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Although elves reach physical maturity at about the same age as humans, the elven understanding of adulthood goes beyond physical growth to encompass worldly experience. An elf typically claims adulthood and an adult name around the age of 100 and can live to be 750 years old.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Elves love freedom, variety, and self- expression, so they lean strongly toward the gentler aspects of chaos. They value and protect others’ freedom as well as their own, and they are more often good than not. The drow are an exception; their exile has made them vicious and dangerous. Drow are more often evil than not.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Elves range from under 5 to over 6 feet tall and have slender builds. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'darkvision',
          name: 'Darkvision',
          desc:
            'Accustomed to twilit forests and the night sky, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.',
        },
        {
          key: 'keenSenses',
          name: 'Keen Senses',
          desc: 'You have proficiency in the Perception skill.',
        },
        {
          key: 'feyAncestry',
          name: 'Fey Ancestry',
          desc:
            'You have advantage on saving throws against being charmed, and magic can’t put you to sleep.',
        },
        {
          key: 'trance',
          name: 'Trance',
          desc:
            'Elves don’t need to sleep. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. (The Common word for such meditation is “trance”) While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common, and Elvish. Elvish is fluid, with subtle intonations and intricate grammar. Elven literature is rich and varied, and their songs and poems are famous among other races. Many bards learn their language so they can add Elvish ballads to their repertoires.',
        },
      ],
    } as IRace,
  },
  {
    id: 'gnome',
    doc: {
      name: 'Gnome',
      desc: '',
      size: 'small',
      attributeBonuses: {
        intelligence: 2,
      },
      sight: { darkvision: 60 },
      speed: { walk: 25 },
      saves: {},
      skills: {},
      languages: ['Common', 'Gnomish'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 40,
      lifeExpectancy: 500,
      baseHeight: 0,
      heightRoll: '0',
      baseWeight: 0,
      weightRoll: '0',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Intelligence score increases by 2.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Gnomes mature at the same rate humans do, and most are expected to settle down into an adult life by around age 40. They can live 350 to almost 500 years.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Gnomes are most often good. Those who tend toward law are sages, engineers, researchers, scholars, investigators, or inventors. Those who tend toward chaos are minstrels, tricksters, wanderers, or fanciful jewelers. Gnomes are good-hearted, and even the tricksters among them are more playful than vicious.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Gnomes are between 3 and 4 feet tall and average about 40 pounds. Your size is Small.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 25 feet.',
        },
        {
          key: 'darkvision',
          name: 'Darkvision',
          desc:
            'Accustomed to life underground, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.',
        },
        {
          key: 'cunning',
          name: 'Gnome Cunning',
          desc:
            'You have advantage on all Intelligence, Wisdom, and Charisma saving throws against magic.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and Gnomish. The Gnomish language, which uses the Dwarvish script, is renowned for its technical treatises and its catalogs of knowledge about the natural world.',
        },
      ],
    } as IRace,
  },
  {
    id: 'halfling',
    doc: {
      name: 'Halfling',
      desc: '',
      size: 'small',
      attributeBonuses: {
        dexterity: 2,
      },
      sight: {},
      speed: { walk: 25 },
      saves: {},
      skills: {},
      languages: ['Common', 'Halfling'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 20,
      lifeExpectancy: 250,
      baseHeight: 31,
      heightRoll: '2d4',
      baseWeight: 35,
      weightRoll: '1',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Dexterity score increases by 2.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'A halfling reaches adulthood at the age of 20 and generally lives into the middle of his or her second century.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Most halflings are lawful good. As a rule, they are good-hearted and kind, hate to see others in pain, and have no tolerance for oppression. They are also very orderly and traditional, leaning heavily on the support of their community and the comfort of their old ways.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Halflings average about 3 feet tall and weigh about 40 pounds. Your size is Small.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 25 feet.',
        },
        {
          key: 'lucky',
          name: 'Lucky',
          desc:
            'When you roll a 1 on the d20 for an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll.',
        },
        {
          key: 'brave',
          name: 'Brave',
          desc: 'You have advantage on saving throws against being frightened.',
        },
        {
          key: 'nimbleness',
          name: 'Halfling Nimbleness',
          desc:
            'You can move through the space of any creature that is of a size larger than yours.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and Halfling. The Halfling language isn’t secret, but halflings are loath to share it with others. They write very little, so they don’t have a rich body of literature. Their oral tradition, however, is very strong. Almost all halflings speak Common to converse with the people in whose lands they dwell or through which they are traveling.',
        },
      ],
    } as IRace,
  },
  {
    id: 'halfElf',
    doc: {
      name: 'Half-Elf',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        charisma: 2,
      },
      sight: { darkvision: 60 },
      speed: { walk: 30 },
      saves: {},
      skills: {},
      languages: ['Common', 'Elvish', '[your Choice]'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 20,
      lifeExpectancy: 180,
      baseHeight: 57,
      heightRoll: '2d8',
      baseWeight: 110,
      weightRoll: '2d4',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc:
            'Your Charisma score increases by 2, and two other ability scores of your choice increase by 1.',
          choices: {
            baseAsi: {
              type: 'attribute',
              points: 2,
              distinct: true,
            },
          },
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Half-elves mature at the same rate humans do and reach adulthood around the age of 20. They live much longer than humans, however, often exceeding 180 years.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Half-elves share the chaotic bent of their elven heritage. They value both personal freedom and creative expression, demonstrating neither love of leaders nor desire for followers. They chafe at rules, resent others’ demands, and sometimes prove unreliable, or at least unpredictable.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Half-elves are about the same size as humans, ranging from 5 to 6 feet tall. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'darkvision',
          name: 'Darkvision',
          desc:
            'Thanks to your elf blood, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.',
        },
        {
          key: 'feyAncestry',
          name: 'Fey Ancestry',
          desc:
            'You have advantage on saving throws against being charmed, and magic can’t put you to sleep.',
        },
        {
          key: 'skillVersatility',
          name: 'Skill Versatility',
          desc: 'You gain proficiency in two skills of your choice.',
          choices: {
            skillVersatility: {
              type: 'skill',
              num: 2,
            },
          },
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common, Elvish, and one extra language of your choice.',
          choices: {
            language: {
              type: 'language',
              excludes: ['Common', 'Elvish'],
            },
          },
        },
      ],
    } as IRace,
  },
  {
    id: 'halfOrc',
    doc: {
      name: 'Half-Orc',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        strength: 2,
        constitution: 1,
      },
      sight: { darkvision: 60 },
      speed: { walk: 30 },
      saves: {},
      skills: {
        intimidation: SkillProficiency.Proficient,
      },
      languages: ['Common', 'Orc'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 14,
      lifeExpectancy: 75,
      baseHeight: 58,
      heightRoll: '2d10',
      baseWeight: 140,
      weightRoll: '2d6',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc:
            'Your Strength score increases by 2, and your Constitution score increses by 1.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Half-orcs mature a little faster than humans, reaching adulthood around age 14. They age noticeably faster and rarely live longer than 75 years.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Half-orcs inherit a tendency toward chaos from their orc parents and are not strongly inclined toward good. Half-orcs raised among orcs and willing to live out their lives among them are usually evil.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Half-orcs are somewhat larger and bulkier than humans, and they range from 5 to well over 6 feet tall. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'darkvision',
          name: 'Darkvision',
          desc:
            'Thanks to your orc blood, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.',
        },
        {
          key: 'menacing',
          name: 'Menacing',
          desc: 'You gain proficiency in the Intimidation skill.',
        },
        {
          key: 'relentlessEndurance',
          name: 'Relentless Endurance',
          desc:
            'When you are reduced to 0 hit points but not killed outright, you can drop to 1 hit point instead. You can’t use this feature again until you finish a long rest.',
        },
        {
          key: 'savageAttacks',
          name: 'Savage Attacks',
          desc:
            'When you score a critical hit with a melee weapon attack, you can roll one of the weapon’s damage dice one additional time and add it to the extra damage of the critical hit.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and Orc. Orc is a harsh, grating language with hard consonants. It has no script of its own but is written in the Dwarvish script.',
        },
      ],
    } as IRace,
  },
  {
    id: 'human',
    doc: {
      name: 'Human',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        strength: 1,
        dexterity: 1,
        constitution: 1,
        intelligence: 1,
        wisdom: 1,
        charisma: 1,
      },
      sight: {},
      speed: { walk: 30 },
      saves: {},
      skills: {},
      languages: ['Common', '[your Choice]'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 16,
      lifeExpectancy: 80,
      baseHeight: 56,
      heightRoll: '2d10',
      baseWeight: 110,
      weightRoll: '2d4',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your ability scores each increase by 1.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Humans reach adulthood in their late teens and live less than a century.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Humans tend toward no particular alignment. The best and the worst are found among them.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Humans vary widely in height and build, from barely 5 feet to well over 6 feet tall. Regardless of your position in that range, your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc:
            'You can speak, read, and write Common and one extra language of your choice. Humans typically learn the languages of other peoples they deal with, including obscure dialects. They are fond of sprinkling their speech with words borrowed from other tongues: Orc curses, Elvish musical expressions, Dwarvish military phrases, and so on.',
          choices: {
            language: {
              type: 'language',
              excludes: ['Common'],
            },
          },
        },
      ],
    } as IRace,
  },
  {
    id: 'tiefling',
    doc: {
      name: 'Tiefling',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        intelligence: 1,
        charisma: 2,
      },
      sight: { darkvision: 60 },
      speed: { walk: 30 },
      saves: {},
      skills: {},
      languages: ['Common', 'Infernal'],
      conditionImmunities: [],
      immunities: [],
      resistances: ['fire'],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 16,
      lifeExpectancy: 90,
      baseHeight: 57,
      heightRoll: '2d8',
      baseWeight: 110,
      weightRoll: '2d4',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc:
            'Your Charisma score increases by 2, and your Intelligence score increses by 1.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            'Tieflings mature at the same rate as humans but live a few years longer.',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            'Tieflings might not have an innate tendency toward evil, but many of them end up there. Evil or not, an independent nature inclines many tieflings toward a chaotic alignment.',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            'Tieflings are about the same size and build as humans. Your size is Medium.',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: 'darkvision',
          name: 'Darkvision',
          desc:
            'Thanks to your infernal heritage, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.',
        },
        {
          key: 'hellishResistance',
          name: 'Hellish Resistance',
          desc: 'You have resistance to fire damage.',
        },
        {
          key: 'infernalLegacy',
          name: 'Infernal Legacy',
          desc:
            'You know the thaumaturgy cantrip. When you reach 3rd level, you can cast the hellish rebuke spell as a 2nd-level spell once with this trait and regain the ability to do so when you finish a long rest. When you reach 5th level, you can cast the darkness spell once with this trait and regain the ability to do so when you finish a long rest. Charisma is your spellcasting ability for these spells.',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc: 'You can speak, read, and write Common and Infernal.',
        },
      ],
    } as IRace,
  },
];

/*
{
    id: 'dragonborn',
    doc: {
      name: 'Dragonborn',
      desc: '',
      size: 'medium',
      attributeBonuses: {
        strength: 2,
        charisma: 1,
      },
      sight: { },
      speed: { walk: 30 },
      saves: {},
      skills: {},
      languages: ['Common', 'Draconic'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      maleNames: [],
      femaleNames: [],
      surnames: [],
      skinColors: [],
      hairColors: [],
      eyeColors: [],
      adultAge: 15,
      lifeExpectancy: 80,
      baseHeight: 0, // TODO
      heightRoll: '0',
      baseWeight: 0,
      weightRoll: '0',
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc:
            'Your Strength score increases by 2, and your Charisma score increses by 1.',
        },
        {
          key: 'age',
          name: 'Age',
          desc:
            '',
        },
        {
          key: 'alignment',
          name: 'Alignment',
          desc:
            '',
        },
        {
          key: 'size',
          name: 'Size',
          desc:
            '',
        },
        {
          key: 'speed',
          name: 'Speed',
          desc: 'Your base walking speed is 30 feet.',
        },
        {
          key: '',
          name: '',
          desc: '',
        },
        {
          key: 'languages',
          name: 'Languages',
          desc: '',
        },
      ],
    } as IRace,
  },
*/
