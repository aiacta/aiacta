import { IClass, Rest, SkillProficiency } from '../../models';

export default [
  {
    id: 'barbarian',
    doc: {
      name: 'Barbarian',
      desc: '',
      hitDie: 12,
      saves: {
        strength: SkillProficiency.Proficient,
        constitution: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'animalHandling',
            'athletics',
            'intimidation',
            'nature',
            'perception',
            'survival',
          ],
        },
      },
      spellcasting: { type: 'none' },
      sight: {},
      languages: [],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Shields',
        'Simple Weapons',
        'Martial Weapons',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [
        {
          key: 'rage',
          name: 'Rage',
          desc: '',
          max: 0,
          maxByLevel: {
            1: 2,
            3: 3,
            6: 4,
            12: 5,
            17: 6,
            20: -1,
          },
          refresh: Rest.Long,
        },
      ],
      features: [
        {
          key: 'rage',
          level: 1,
          name: 'Rage',
          desc: `In battle, you fight with primal ferocity. On your Turn, you can enter a rage as a bonus action.

While raging, you gain the following benefits if you aren't wearing heavy armor:

 - You have advantage on Strength checks and Strength saving throws.
 - When you make a melee weapon attack using Strength, you gain a bonus to the damage roll that increases as you gain levels as a barbarian, as shown in the Rage Damage column of the Barbarian table.
 - You have resistance to bludgeoning, piercing, and slashing damage.

If you are able to cast spells, you can’t cast them or concentrate on them while raging.

Your rage lasts for 1 minute. It ends early if you are knocked unconscious or if your turn ends and you haven’t attacked a hostile creature since your last turn or taken damage since then. You can also end your rage on your turn as a bonus action.

Once you have raged the number of times shown for your barbarian level in the Rages column of the Barbarian table, you must finish a long rest before you can rage again.`,
        },
        {
          key: 'unarmoredDefense',
          level: 1,
          name: 'Unarmored Defense',
          desc:
            'While you are not wearing any armor, your Armor Class equals 10 + your Dexterity modifier + your Constitution modifier. You can use a shield and still gain this benefit.',
        },
        {
          key: 'dangerSense',
          level: 2,
          name: 'Danger Sense',
          desc: `At 2nd level, you gain an uncanny sense of when things nearby aren’t as they should be, giving you an edge when you dodge away from danger.

You have advantage on Dexterity saving throws against effects that you can see, such as traps and spells. To gain this benefit, you can’t be blinded, deafened, or incapacitated.`,
        },
        {
          key: 'recklessAttack',
          level: 2,
          name: 'Reckless Attack',
          desc:
            'Starting at 2nd level, you can throw aside all concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn.',
        },
        {
          key: 'primalPath',
          level: 3,
          name: 'Primal Path',
          desc:
            'At 3rd level, you choose a path that shapes the nature of your rage.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-barbarian',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'extraAttack',
          level: 5,
          name: 'Extra Attack',
          desc:
            'Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.',
        },
        {
          key: 'fastMovement',
          level: 5,
          name: 'Fast Movement',
          desc:
            'Starting at 5th level, your speed increases by 10 feet while you aren’t wearing heavy armor',
        },
        {
          key: 'feralInstinct',
          level: 7,
          name: 'Feral Instinct',
          desc: `By 7th level, your instincts are so honed that you have advantage on initiative rolls.

Additionally, if you are surprised at the beginning of combat and aren’t incapacitated, you can act normally on your first turn, but only if you enter your rage before doing anything else on that turn.`,
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'brutalCrit',
          level: 9,
          name: 'Brutal Critical',
          desc: `Beginning at 9th level, you can roll one additional weapon damage die when determining the extra damage for a critical hit with a melee attack.

This increases to two additional dice at 13th level and three additional dice at 17th level.`,
        },
        {
          key: 'relentlessRage',
          level: 11,
          name: 'Relentless Rage',
          desc: `Starting at 11th level, your rage can keep you fighting despite grievous wounds. If you drop to 0 hit points while you’re raging and don’t die outright, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead.

Each time you use this feature after the first, the DC increases by 5. When you finish a short or long rest, the DC resets to 10.`,
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'persistRage',
          level: 15,
          name: 'Persistent Rage',
          desc:
            'Beginning at 15th level, your rage is so fierce that it ends early only if you fall unconscious or if you choose to end it.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'indomMight',
          level: 18,
          name: 'Indomitable Might',
          desc:
            'Beginning at 18th level, if your total for a Strength check is less than your Strength score, you can use that score in place of the total.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'primalChampion',
          level: 20,
          name: 'Primal Champion',
          desc:
            'At 20th level, you embody the power of the wilds. Your Strength and Constitution scores increase by 4. Your maximum for those scores is now 24.',
        },
      ],
    } as IClass,
  },
  {
    id: 'bard',
    doc: {
      name: 'Bard',
      desc: '',
      hitDie: 8,
      saves: {
        dexterity: SkillProficiency.Proficient,
        charisma: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 3,
          includes: [
            'athletics',
            'acrobatics',
            'sleightOfHand',
            'stealth',
            'arcana',
            'history',
            'investigation',
            'nature',
            'religion',
            'animalHandling',
            'insight',
            'medicine',
            'perception',
            'survival',
            'deception',
            'intimidation',
            'performance',
            'persuation',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'full', attribute: 'charisma', knowsAll: false },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Simple Weapons',
        'hand crossbow',
        'longsword',
        'rapier',
        'shortsword',
        '[musical instrument of your choice]',
        '[musical instrument of your choice]',
        '[musical instrument of your choice]',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'spellcasting',
          level: 1,
          name: 'Spellcasting',
          desc: `You have learned to untangle and reshape the fabric of reality in harmony with your wishes and music.

Your spells are part of your vast repertoire, magic that you can tune to different situations.

## Cantrips
You know two cantrips of your choice from the bard spell list. You learn additional bard cantrips of your choice at higher levels, as shown in the Cantrips Known column of the Bard table.

## Spell Slots
The Bard table shows how many spell slots you have to cast your bard spells of 1st level and higher. To cast one of these spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

For example, if you know the 1st-level spell cure wounds and have a 1st-level and a 2nd-level spell slot available, you can cast cure wounds using either slot.

## Spells Known of 1st Level and Higher
You know four 1st-level spells of your choice from the bard spell list.

The Spells Known column of the Bard table shows when you learn more bard spells of your choice.

Each of these spells must be of a level for which you have spell slots, as shown on the table. For instance, when you reach 3rd level in this class, you can learn one new spell of 1st or 2nd level.

Additionally, when you gain a level in this class, you can choose one of the bard spells you know and replace it with another spell from the bard spell list, which also must be of a level for which you have spell slots.

## Spellcasting Ability
Charisma is your spellcasting ability for your bard spells. Your magic comes from the heart and soul you pour into the performance of your music or oration. You use your Charisma whenever a spell refers to your spellcasting ability. In addition, you use your Charisma modifier when setting the saving throw DC for a bard spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Charisma modifier

Spell attack modifier = your proficiency bonus + your Charisma modifier

## Ritual Casting
You can cast any bard spell you know as a ritual if that spell has the ritual tag.

## Spellcasting Focus
You can use a musical instrument (see Equipment) as a spellcasting focus for your bard spells.`,
        },
        {
          key: 'bardicInspi',
          level: 1,
          name: 'Bardic Inspiration',
          desc: `You can inspire others through stirring words or music. To do so, you use a bonus action on your turn to choose one creature other than yourself within 60 feet of you who can hear you. That creature gains one Bardic Inspiration die, a d6.

Once within the next 10 minutes, the creature can roll the die and add the number rolled to one ability check, attack roll, or saving throw it makes. The creature can wait until after it rolls the d20 before deciding to use the Bardic Inspiration die, but must decide before the GM says whether the roll succeeds or fails. Once the Bardic Inspiration die is rolled, it is lost. A creature can have only one Bardic Inspiration die at a time.

You can use this feature a number of times equal to your Charisma modifier (a minimum of once). You regain any expended uses when you finish a long rest.

Your Bardic Inspiration die changes when you reach certain levels in this class. The die becomes a d8 at 5th level, a d10 at 10th level, and a d12 at 15th level.`,
        },
        {
          key: 'jackOTrades',
          level: 2,
          name: 'Jack of All Trades',
          desc:
            'Starting at 2nd level, you can add half your proficiency bonus, rounded down, to any ability check you make that doesn’t already include your proficiency bonus.',
        },
        {
          key: 'songOfRest',
          level: 2,
          name: 'Song of Rest',
          desc: `Beginning at 2nd level, you can use soothing music or oration to help revitalize your wounded allies during a short rest. If you or any friendly creatures who can hear your performance regain hit points at the end of the short rest by spending one or more Hit Dice, each of those creatures regains an extra 1d6 hit points.

The extra hit points increase when you reach certain levels in this class: to 1d8 at 9th level, to 1d10 at 13th level, and to 1d12 at 17th level.`,
        },
        {
          key: 'bardCollege',
          level: 3,
          name: 'Bard College',
          desc:
            'At 3rd level, you delve into the advanced techniques of a Bardic College.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-bard',
            },
          },
        },
        {
          key: 'expertise',
          level: 3,
          name: 'Expertise',
          desc:
            'At 3rd level, choose two of your skill proficiencies. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies.',
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'fontOInspi',
          level: 5,
          name: 'Font of Inspiration',
          desc:
            'Beginning when you reach 5th level, you regain all of your expended uses of Bardic Inspiration when you finish a short or long rest.',
        },
        {
          key: 'countercharm',
          level: 6,
          name: 'Countercharm',
          desc:
            'At 6th level, you gain the ability to use musical notes or words of power to disrupt mind-influencing effects. As an action, you can start a performance that lasts until the end of your next turn. During that time, you and any friendly creatures within 30 feet of you have advantage on saving throws against being frightened or charmed. A creature must be able to hear you to gain this benefit. The performance ends early if you are incapacitated or silenced or if you voluntarily end it (no action required).',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'magicalSecrets',
          level: 10,
          name: 'Magical Secrets',
          desc: `By 10th level, you have plundered magical knowledge from a wide spectrum of disciplines. Choose two spells from any classes, including this one. A spell you choose must be of a level you can cast, as shown on the bard table, or a cantrip.

The chosen spells count as bard spells for you and are included in the number in the Spells Known column of the bard table.

You learn two additional spells from any classes at 14th level and again at 18th level.`,
        },
        {
          key: 'expertise1',
          level: 10,
          name: 'Expertise',
          desc:
            'At 10th level, choose two of your skill proficiencies. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'supInspi',
          level: 20,
          name: 'Superior Inspiration',
          desc:
            'At 20th level, when you roll initiative and have no uses of Bardic Inspiration left, you regain one use.',
        },
      ],
    } as IClass,
  },
  {
    id: 'cleric',
    doc: {
      name: 'Cleric',
      desc: '',
      hitDie: 8,
      saves: {
        wisdom: SkillProficiency.Proficient,
        charisma: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'history',
            'insight',
            'medicine',
            'persuation',
            'religion',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'full', attribute: 'wisdom', knowsAll: true },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Shields',
        'Simple Weapons',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'spellcasting',
          level: 1,
          name: 'Spellcasting',
          desc: `As a conduit for divine power, you can cast cleric spells.

## Cantrips
At 1st level, you know three cantrips of your choice from the cleric spell list. You learn additional cleric cantrips of your choice at higher levels, as shown in the Cantrips Known column of the Cleric table.

## Preparing and Casting Spells
The Cleric table shows how many spell slots you have to cast your spells of 1st level and higher. To cast one of these spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

You prepare the list of cleric spells that are available for you to cast, choosing from the cleric spell list. When you do so, choose a number of cleric spells equal to your Wisdom modifier + your cleric level (minimum of one spell). The spells must be of a level for which you have spell slots.

For example, if you are a 3rd-level cleric, you have four 1st-level and two 2nd-level spell slots. With a Wisdom of 16, your list of prepared spells can include six spells of 1st or 2nd level, in any combination. If you prepare the 1st-level spell cure wounds, you can cast it using a 1st-level or 2nd-level slot. Casting the spell doesn’t remove it from your list of prepared spells.

You can change your list of prepared spells when you finish a long rest. Preparing a new list of cleric spells requires time spent in prayer and meditation: at least 1 minute per spell level for each spell on your list.

## Spellcasting Ability
Wisdom is your spellcasting ability for your cleric spells. The power of your spells comes from your devotion to your deity. You use your Wisdom whenever a cleric spell refers to your spellcasting ability. In addition, you use your Wisdom modifier when setting the saving throw DC for a cleric spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Wisdom modifier

Spell attack modifier = your proficiency bonus + your Wisdom modifier

## Ritual Casting
You can cast a cleric spell as a ritual if that spell has the ritual tag and you have the spell prepared.

## Spellcasting Focus
You can use a holy symbol (see Equipment) as a spellcasting focus for your cleric spells.`,
        },
        {
          key: 'domain',
          level: 1,
          name: 'Divine Domain',
          desc: `Choose one domain related to your deity. Your chosen domain grants you domain spells and other features when you choose it at 1st level. It also grants you additional ways to use Channel Divinity when you gain that feature at 2nd level, and additional benefits at 6th, 8th, and 17th levels.

## Domain Spells
Each domain has a list of spells—its domain spells— that you gain at the cleric levels noted in the domain description. Once you gain a domain spell, you always have it prepared, and it doesn’t count against the number of spells you can prepare each day.

If you have a domain spell that doesn’t appear on the cleric spell list, the spell is nonetheless a cleric spell for you.`,
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-cleric',
            },
          },
        },
        {
          key: 'channelDivinity',
          level: 2,
          name: 'Channel Divinity',
          desc: `At 2nd level, you gain the ability to channel divine energy directly from your deity, using that energy to fuel magical effects. You start with two such effects: Turn Undead and an effect determined by your domain. Some domains grant you additional effects as you advance in levels, as noted in the domain description.

When you use your Channel Divinity, you choose which effect to create. You must then finish a short or long rest to use your Channel Divinity again.

Some Channel Divinity effects require saving throws. When you use such an effect from this class, the DC equals your cleric spell save DC.

Beginning at 6th level, you can use your Channel Divinity twice between rests, and beginning at 18th level, you can use it three times between rests. When you finish a short or long rest, you regain your expended uses.`,
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'divineIntervention',
          level: 10,
          name: 'Divine Intervention',
          desc: `Beginning at 10th level, you can call on your deity to intervene on your behalf when your need is great.

Imploring your deity’s aid requires you to use your action. Describe the assistance you seek, and roll percentile dice. If you roll a number equal to or lower than your cleric level, your deity intervenes. The GM chooses the nature of the intervention; the effect of any cleric spell or cleric domain spell would be appropriate.

If your deity intervenes, you can’t use this feature again for 7 days. Otherwise, you can use it again after you finish a long rest.

At 20th level, your call for intervention succeeds automatically, no roll required.`,
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
      ],
    } as IClass,
  },
  {
    id: 'druid',
    doc: {
      name: 'Druid',
      desc: '',
      hitDie: 8,
      saves: {
        intelligence: SkillProficiency.Proficient,
        wisdom: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'arcana',
            'animalHandling',
            'insight',
            'medicine',
            'nature',
            'perception',
            'religion',
            'survival',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'full', attribute: 'wisdom', knowsAll: true },
      languages: ['Druidic'],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Shields',
        'clubs',
        'daggers',
        'darts',
        'javelins',
        'maces',
        'quarterstaffs',
        'scimitars',
        'sickles',
        'slings',
        'spears',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'druidic',
          level: 1,
          name: 'Druidic',
          desc:
            'You know Druidic, the secret language of druids. You can speak the language and use it to leave hidden messages. You and others who know this language automatically spot such a message. Others spot the message’s presence with a successful DC 15 Wisdom (Perception) check but can’t decipher it without magic.',
        },
        {
          key: 'spellcasting',
          level: 1,
          name: 'Spellcasting',
          desc: `Drawing on the divine essence of nature itself, you can cast spells to shape that essence to your will.

## Cantrips
At 1st level, you know two cantrips of your choice from the druid spell list. You learn additional druid cantrips of your choice at higher levels, as shown in the Cantrips Known column of Table: The Druid.

## Preparing and Casting Spells
Table: The Druid shows how many spell slots you have to cast your spells of 1st level and higher. To cast one of these druid spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

You prepare the list of druid spells that are available for you to cast, choosing from the druid spell list. When you do so, choose a number of druid spells equal to your Wisdom modifier + your druid level (minimum of one spell). The spells must be of a level for which you have spell slots.

For example, if you are a 3rd-level druid, you have four 1st-level and two 2nd-level spell slots. With a Wisdom of 16, your list of prepared spells can include six spells of 1st or 2nd level, in any combination. If you prepare the 1st-level spell cure wounds, you can cast it using a 1st-level or 2nd-level slot. Casting the spell doesn’t remove it from your list of prepared spells.

You can also change your list of prepared spells when you finish a long rest. Preparing a new list of druid spells requires time spent in prayer and meditation: at least 1 minute per spell level for each spell on your list.

## Spellcasting Ability
Wisdom is your spellcasting ability for your druid spells, since your magic draws upon your devotion and attunement to nature. You use your Wisdom whenever a spell refers to your spellcasting ability. In addition, you use your Wisdom modifier when setting the saving throw DC for a druid spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Wisdom modifier

Spell attack modifier = your proficiency bonus + your Wisdom modifier

## Ritual Casting
You can cast a druid spell as a ritual if that spell has the ritual tag and you have the spell prepared.

## Spellcasting Focus
You can use a druidic focus (see Equipment) as a spellcasting focus for your druid spells.`,
        },
        {
          key: 'wildshape',
          level: 2,
          name: 'Wildshape',
          desc: `Starting at 2nd level, you can use your action to magically assume the shape of a beast that you have seen before. You can use this feature twice. You regain expended uses when you finish a short or long rest.

Your druid level determines the beasts you can transform into, as shown in Table: Beast Shapes. At 2nd level, for example, you can transform into any beast that has a challenge rating of 1/4 or lower that doesn’t have a flying or swimming speed.

| Level | Max. CR | Limitations                 | Example     |
|-------|---------|-----------------------------|-------------|
| 2nd   | 1/4     | No flying or swimming speed | Wolf        |
| 4th   | 1/2     | No flying speed             | Crocodile   |
| 8th   | 1       | —                           | Giant eagle |

You can stay in a beast shape for a number of hours equal to half your druid level (rounded down). You then revert to your normal form unless you expend another use of this feature. You can revert to your normal form earlier by using a bonus action on your turn. You automatically revert if you fall unconscious, drop to 0 hit points, or die.

While you are transformed, the following rules apply:

Your game statistics are replaced by the statistics of the beast, but you retain your alignment, personality, and Intelligence, Wisdom, and Charisma scores. You also retain all of your skill and saving throw proficiencies, in addition to gaining those of the creature. If the creature has the same proficiency as you and the bonus in its stat block is higher than yours, use the creature’s bonus instead of yours. If the creature has any legendary or lair actions, you can’t use them.
When you transform, you assume the beast’s hit points and Hit Dice. When you revert to your normal form, you return to the number of hit points you had before you transformed. However, if you revert as a result of dropping to 0 hit points, any excess damage carries over to your normal form. For example, if you take 10 damage in animal form and have only 1 hit point left, you revert and take 9 damage. As long as the excess damage doesn’t reduce your normal form to 0 hit points, you aren’t knocked unconscious.
You can’t cast spells, and your ability to speak or take any action that requires hands is limited to the capabilities of your beast form. Transforming doesn’t break your concentration on a spell you’ve already cast, however, or prevent you from taking actions that are part of a spell, such as call lightning, that you’ve already cast.
You retain the benefit of any features from your class, race, or other source and can use them if the new form is physically capable of doing so. However, you can’t use any of your special senses, such as darkvision, unless your new form also has that sense.
You choose whether your equipment falls to the ground in your space, merges into your new form, or is worn by it. Worn equipment functions as normal, but the GM decides whether it is practical for the new form to wear a piece of equipment, based on the creature’s shape and size. Your equipment doesn’t change size or shape to match the new form, and any equipment that the new form can’t wear must either fall to the ground or merge with it. Equipment that merges with the form has no effect until you leave the form.`,
        },
        {
          key: 'druidCircle',
          level: 2,
          name: 'Druid Circle',
          desc: 'At 2nd level, you choose to identify with a circle of druids.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-druid',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'timelessBody',
          level: 18,
          name: 'Timeless Body',
          desc:
            'Starting at 18th level, the primal magic that you wield causes you to age more slowly. For every 10 years that pass, your body ages only 1 year.',
        },
        {
          key: 'beastSpells',
          level: 18,
          name: 'Beast Spells',
          desc:
            'Beginning at 18th level, you can cast many of your druid spells in any shape you assume using Wild Shape. You can perform the somatic and verbal components of a druid spell while in a beast shape, but you aren’t able to provide material components.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'archdruid',
          level: 20,
          name: 'Archdruid',
          desc: `At 20th level, you can use your Wild Shape an unlimited number of times.

Additionally, you can ignore the verbal and somatic components of your druid spells, as well as any material components that lack a cost and aren’t consumed by a spell. You gain this benefit in both your normal shape and your beast shape from Wild Shape.`,
        },
      ],
    } as IClass,
  },
  {
    id: 'fighter',
    doc: {
      name: 'Fighter',
      desc: '',
      hitDie: 10,
      saves: {
        strength: SkillProficiency.Proficient,
        constitution: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'acrobatics',
            'animalHandling',
            'athletics',
            'history',
            'insight',
            'intimidation',
            'perception',
            'survival',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'none' },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Heavy Armor',
        'Shields',
        'Simple Weapons',
        'Martial Weapons',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'fightingStyle',
          level: 1,
          name: 'Fighting Style',
          desc: `You adopt a particular style of fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.`,
          choices: {
            fightingStyle: {
              type: 'custom',
              options: `
| Style                   | Effects                                                                                                                                                                                                                                                                                             |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Archery	                | You gain a +2 bonus to attack rolls you make with ranged weapons.                                                                                                                                                                                                                                   |
| Defense	                | While you are wearing armor you gain a +1 bonus to AC.                                                                                                                                                                                                                                              |
| Dueling	                | When you are wielding a melee weapon in one hand and no other weapons you gain a +2 bonus to damage rolls with that weapon.you gain a +2 bonus to damage rolls with that weapon.                                                                                                                    |
| Great Weapon Fighting	  | When you roll a 1 or 2 on a damage die for an attack you make with a melee weapon that you are wielding with two hands you can reroll the die and must use the new roll even if the new roll is a 1 or a 2. The weapon must have the two-handed or versatile property for you to gain this benefit. |
| Protection		          | When a creature you can see attacks a target other than you that is within 5 feet of you you can use your reaction to impose disadvantage on the attack roll. You must be wielding a shield.                                                                                                        |
| Two-Weapon Fighting	    | When you engage in two-weapon fighting you can add your ability modifier to the damage of the second attack.                                                                                                                                                                                        |`,
            },
          },
        },
        {
          key: 'sndWind',
          level: 1,
          name: 'Second Wind',
          desc:
            'You have a limited well of stamina that you can draw on to protect yourself from harm. On your turn, you can use a bonus action to regain hit points equal to 1d10 + your fighter level. Once you use this feature, you must finish a short or long rest before you can use it again.',
        },
        {
          key: 'actionSurge',
          level: 2,
          name: 'Action Surge',
          desc: `Starting at 2nd level, you can push yourself beyond your normal limits for a moment. On your turn, you can take one additional action on top of your regular action and a possible bonus action.

Once you use this feature, you must finish a short or long rest before you can use it again. Starting at 17th level, you can use it twice before a rest, but only once on the same turn.`,
        },
        {
          key: 'martialArchetype',
          level: 3,
          name: 'Martial Archetype',
          desc:
            'At 3rd level, you choose an archetype that you strive to emulate in your combat styles and techniques.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-fighter',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'extraAttack',
          level: 5,
          name: 'Extra Attack',
          desc: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.

The number of attacks increases to three when you reach 11th level in this class and to four when you reach 20th level in this class.`,
        },
        {
          key: 'asi1',
          level: 6,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 6th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi2',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'indomitable',
          level: 9,
          name: 'Indomitable',
          desc: `Beginning at 9th level, you can reroll a saving throw that you fail. If you do so, you must use the new roll, and you can’t use this feature again until you finish a long rest.

You can use this feature twice between long rests starting at 13th level and three times between long rests starting at 17th level.`,
        },
        {
          key: 'asi3',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi5',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
      ],
    } as IClass,
  },
  {
    id: 'monk',
    doc: {
      name: 'Monk',
      desc: '',
      hitDie: 8,
      saves: {
        strength: SkillProficiency.Proficient,
        dexterity: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'acrobatics',
            'athletics',
            'history',
            'insight',
            'religion',
            'stealth',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'none' },
      languages: [],
      proficiencies: ['Simple Weapons', 'shortswords'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'unarmoredDefense',
          level: 1,
          name: 'Unarmored Defense',
          desc:
            'Beginning at 1st level, while you are wearing no armor and not wielding a shield, your AC equals 10 + your Dexterity modifier + your Wisdom modifier',
        },
        {
          key: 'martialArts',
          level: 1,
          name: 'Martial Arts',
          desc: `At 1st level, your practice of martial arts gives you mastery of combat styles that use unarmed strikes and monk weapons, which are shortswords and any simple melee weapons that don’t have the two- handed or heavy property.

You gain the following benefits while you are unarmed or wielding only monk weapons and you aren’t wearing armor or wielding a shield:

 - You can use Dexterity instead of Strength for the attack and damage rolls of your unarmed strikes and monk weapons.
 - You can roll a d4 in place of the normal damage of your unarmed strike or monk weapon. This die changes as you gain monk levels, as shown in the Martial Arts column of Table: The Monk.
 - When you use the Attack action with an unarmed strike or a monk weapon on your turn, you can make one unarmed strike as a bonus action. For example, if you take the Attack action and attack with a quarterstaff, you can also make an unarmed strike as a bonus action, assuming you haven’t already taken a bonus action this turn.

Certain monasteries use specialized forms of the monk weapons. For example, you might use a club that is two lengths of wood connected by a short chain (called a nunchaku) or a sickle with a shorter, straighter blade (called a kama). Whatever name you use for a monk weapon, you can use the game statistics provided for the weapon.`,
        },
        {
          key: 'ki',
          level: 2,
          name: 'KI',
          desc: `Starting at 2nd level, your training allows you to harness the mystic energy of ki. Your access to this energy is represented by a number of ki points. Your monk level determines the number of points you have, as shown in the Ki Points column of Table: The Monk.

You can spend these points to fuel various ki features. You start knowing three such features: Flurry of Blows, Patient Defense, and Step of the Wind. You learn more ki features as you gain levels in this class.

When you spend a ki point, it is unavailable until you finish a short or long rest, at the end of which you draw all of your expended ki back into yourself. You must spend at least 30 minutes of the rest meditating to regain your ki points.

Some of your ki features require your target to make a saving throw to resist the feature’s effects. The saving throw DC is calculated as follows:

Ki save DC = 8 + your proficiency bonus + your Wisdom modifier.

## Flurry of Blows
Immediately after you take the Attack action on your turn, you can spend 1 ki point to make two unarmed strikes as a bonus action.

## Patient Defense
You can spend 1 ki point to take the Dodge action as a bonus action on your turn.

## Step of the Wind
You can spend 1 ki point to take the Disengage or Dash action as a bonus action on your turn, and your jump distance is doubled for the turn.`,
        },
        {
          key: 'unarmoredMovement',
          level: 2,
          name: 'Unarmored Movement',
          desc: `Starting at 2nd level, your speed increases by 10 feet while you are not wearing armor or wielding a shield. This bonus increases when you reach certain monk levels, as shown in Table: The Monk.

At 9th level, you gain the ability to move along vertical surfaces and across liquids on your turn without falling during the move.`,
        },
        {
          key: 'monasticTradition',
          level: 3,
          name: 'Monastic Tradition',
          desc:
            'When you reach 3rd level, you commit yourself to a monastic tradition.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-monk',
            },
          },
        },
        {
          key: 'deflectMissiles',
          level: 0,
          name: 'Deflect Missiles',
          desc: `Starting at 3rd level, you can use your reaction to deflect or catch the missile when you are hit by a ranged weapon attack. When you do so, the damage you take from the attack is reduced by 1d10 + your Dexterity modifier + your monk level.

If you reduce the damage to 0, you can catch the missile if it is small enough for you to hold in one hand and you have at least one hand free. If you catch a missile in this way, you can spend 1 ki point to make a ranged attack with the weapon or piece of ammunition you just caught, as part of the same reaction. You make this attack with proficiency, regardless of your weapon proficiencies, and the missile counts as a monk weapon for the attack, which has a normal range of 20 feet and a long range of 60 feet.`,
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'slowFall',
          level: 4,
          name: 'Slow Fall',
          desc:
            'Beginning at 4th level, you can use your reaction when you fall to reduce any falling damage you take by an amount equal to five times your monk level.',
        },
        {
          key: 'extraAttack',
          level: 5,
          name: 'Extra Attack',
          desc:
            'Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn',
        },
        {
          key: 'stunningStrike',
          level: 5,
          name: 'Stunning Strike',
          desc:
            'Starting at 5th level, you can interfere with the flow of ki in an opponent’s body. When you hit another creature with a melee weapon attack, you can spend 1 ki point to attempt a stunning strike. The target must succeed on a Constitution saving throw or be stunned until the end of your next turn.',
        },
        {
          key: 'kiEmpored',
          level: 6,
          name: 'KI-Empowered Strike',
          desc:
            'Starting at 6th level, your unarmed strikes count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage.',
        },
        {
          key: 'evasion',
          level: 7,
          name: 'Evasion',
          desc:
            'At 7th level, your instinctive agility lets you dodge out of the way of certain area effects, such as a blue dragon’s lightning breath or a fireball spell. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.',
        },
        {
          key: 'stillnessOfMind',
          level: 7,
          name: 'Stillness of Mind',
          desc:
            'Starting at 7th level, you can use your action to end one effect on yourself that is causing you to be charmed or frightened',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'purityOfBody',
          level: 10,
          name: 'Purity of Body',
          desc:
            'At 10th level, your mastery of the ki flowing through you makes you immune to disease and poison',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'tongueOfSunMoon',
          level: 13,
          name: 'Tongue of Sun and Moon',
          desc:
            'Starting at 13th level, you learn to touch the ki of other minds so that you understand all spoken languages. Moreover, any creature that can understand a language can understand what you say',
        },
        {
          key: 'diamondSoul',
          level: 14,
          name: 'Diamond Soul',
          desc: `Beginning at 14th level, your mastery of ki grants you proficiency in all saving throws.

Additionally, whenever you make a saving throw and fail, you can spend 1 ki point to reroll it and take the second result`,
        },
        {
          key: 'timelessBody',
          level: 15,
          name: 'Timeless Body',
          desc:
            'At 15th level, your ki sustains you so that you suffer none of the frailty of old age, and you can’t be aged magically. You can still die of old age, however. In addition, you no longer need food or water.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'emptyBody',
          level: 18,
          name: 'Empty Body',
          desc: `Beginning at 18th level, you can use your action to spend 4 ki points to become invisible for 1 minute. During that time, you also have resistance to all damage but force damage.

Additionally, you can spend 8 ki points to cast the astral projection spell, without needing material components. When you do so, you can’t take any other creatures with you`,
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'perfectSelf',
          level: 20,
          name: 'Perfect Self',
          desc:
            'At 20th level, when you roll for initiative and have no ki points remaining, you regain 4 ki points.',
        },
      ],
    } as IClass,
  },
  {
    id: 'paladin',
    doc: {
      name: 'Paladin',
      desc: '',
      hitDie: 10,
      saves: {
        wisdom: SkillProficiency.Proficient,
        charisma: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'athletics',
            'insight',
            'intimidation',
            'medicine',
            'persuation',
            'religion',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'half', attribute: 'charisma', knowsAll: true },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Heavy Armor',
        'Shields',
        'Simple Weapons',
        'Martial Weapons',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'divineSense',
          level: 1,
          name: 'Divine Sense',
          desc: `The presence of strong evil registers on your senses like a noxious odor, and powerful good rings like heavenly music in your ears. As an action, you can open your awareness to detect such forces. Until the end of your next turn, you know the location of any celestial, fiend, or undead within 60 feet of you that is not behind total cover. You know the type (celestial, fiend, or undead) of any being whose presence you sense, but not its identity. Within the same radius, you also detect the presence of any place or object that has been consecrated or desecrated, as with the hallow spell.

You can use this feature a number of times equal to 1 + your Charisma modifier. When you finish a long rest, you regain all expended uses.`,
        },
        {
          key: 'layOnHands',
          level: 1,
          name: 'Lay on Hands',
          desc: `Your blessed touch can heal wounds. You have a pool of healing power that replenishes when you take a long rest. With that pool, you can restore a total number of hit points equal to your paladin level × 5.

As an action, you can touch a creature and draw power from the pool to restore a number of hit points to that creature, up to the maximum amount remaining in your pool.

Alternatively, you can expend 5 hit points from your pool of healing to cure the target of one disease or neutralize one poison affecting it. You can cure multiple diseases and neutralize multiple poisons with a single use of Lay on Hands, expending hit points separately for each one.

This feature has no effect on undead and constructs.`,
        },
        {
          key: 'fightingStyle',
          level: 2,
          name: 'Fighting Style',
          desc: `At 2nd level, you adopt a style of fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.`,
          choices: {
            fightingStyle: {
              type: 'custom',
              options: `
| Style                   | Effects                                                                                                                                                                                                                                                                                             |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Defense	                | While you are wearing armor you gain a +1 bonus to AC.                                                                                                                                                                                                                                              |
| Dueling	                | When you are wielding a melee weapon in one hand and no other weapons you gain a +2 bonus to damage rolls with that weapon.you gain a +2 bonus to damage rolls with that weapon.                                                                                                                    |
| Great Weapon Fighting	  | When you roll a 1 or 2 on a damage die for an attack you make with a melee weapon that you are wielding with two hands you can reroll the die and must use the new roll even if the new roll is a 1 or a 2. The weapon must have the two-handed or versatile property for you to gain this benefit. |
| Protection		          | When a creature you can see attacks a target other than you that is within 5 feet of you you can use your reaction to impose disadvantage on the attack roll. You must be wielding a shield.                                                                                                        |`,
            },
          },
        },
        {
          key: 'spellcasting',
          level: 2,
          name: 'Spellcasting',
          desc: `By 2nd level, you have learned to draw on divine magic through meditation and prayer to cast spells as a cleric does.

## Preparing and Casting Spells
The Paladin table shows how many spell slots you have to cast your spells. To cast one of your paladin spells of 1st level or higher, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

You prepare the list of paladin spells that are available for you to cast, choosing from the paladin spell list. When you do so, choose a number of paladin spells equal to your Charisma modifier + half your paladin level, rounded down (minimum of one spell). The spells must be of a level for which you have spell slots.

For example, if you are a 5th-level paladin, you have four 1st-level and two 2nd-level spell slots. With a Charisma of 14, your list of prepared spells can include four spells of 1st or 2nd level, in any combination. If you prepare the 1st-level spell cure wounds, you can cast it using a 1st-level or a 2nd- level slot. Casting the spell doesn’t remove it from your list of prepared spells.

You can change your list of prepared spells when you finish a long rest. Preparing a new list of paladin spells requires time spent in prayer and meditation: at least 1 minute per spell level for each spell on your list.

## Spellcasting Ability
Charisma is your spellcasting ability for your paladin spells, since their power derives from the strength of your convictions. You use your Charisma whenever a spell refers to your spellcasting ability. In addition, you use your Charisma modifier when setting the saving throw DC for a paladin spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Charisma modifier

Spell attack modifier = your proficiency bonus + your Charisma modifier

## Spellcasting Focus
You can use a holy symbol as a spellcasting focus for your paladin spells.`,
        },
        {
          key: 'divineSmite',
          level: 2,
          name: 'Divine Smite',
          desc:
            'Starting at 2nd level, when you hit a creature with a melee weapon attack, you can expend one spell slot to deal radiant damage to the target, in addition to the weapon’s damage. The extra damage is 2d8 for a 1st-level spell slot, plus 1d8 for each spell level higher than 1st, to a maximum of 5d8. The damage increases by 1d8 if the target is an undead or a fiend.',
        },
        {
          key: 'divineHealth',
          level: 3,
          name: 'Divine Health',
          desc:
            'By 3rd level, the divine magic flowing through you makes you immune to disease.',
        },
        {
          key: 'sacredOath',
          level: 3,
          name: 'Sacred Oath',
          desc: `When you reach 3rd level, you swear the oath that binds you as a paladin forever. Up to this time you have been in a preparatory stage, committed to the path but not yet sworn to it.

## Oath Spells
Each oath has a list of associated spells. You gain access to these spells at the levels specified in the oath description. Once you gain access to an oath spell, you always have it prepared. Oath spells don’t count against the number of spells you can prepare each day.

If you gain an oath spell that doesn’t appear on the paladin spell list, the spell is nonetheless a paladin spell for you.

## Channel Divinity
Your oath allows you to channel divine energy to fuel magical effects. Each Channel Divinity option provided by your oath explains how to use it.

When you use your Channel Divinity, you choose which option to use. You must then finish a short or long rest to use your Channel Divinity again.

Some Channel Divinity effects require saving throws. When you use such an effect from this class, the DC equals your paladin spell save DC.`,
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-paladin',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'extraAttack',
          level: 5,
          name: 'Extra Attack',
          desc:
            'Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.',
        },
        {
          key: 'auraOfProtection',
          level: 6,
          name: 'Aura of Protection',
          desc: `Starting at 6th level, whenever you or a friendly creature within 10 feet of you must make a saving throw, the creature gains a bonus to the saving throw equal to your Charisma modifier (with a minimum bonus of +1). You must be conscious to grant this bonus.

At 18th level, the range of this aura increases to 30 feet.`,
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'auraOfCourage',
          level: 10,
          name: 'Aura of Courage',
          desc: '',
        },
        {
          key: 'improvedDivineSmite',
          level: 11,
          name: 'Improved Divine Smite',
          desc:
            'By 11th level, you are so suffused with righteous might that all your melee weapon strikes carry divine power with them. Whenever you hit a creature with a melee weapon, the creature takes an extra 1d8 radiant damage. If you also use your Divine Smite with an attack, you add this damage to the extra damage of your Divine Smite.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'cleansingTouch',
          level: 14,
          name: 'Cleansing Touch',
          desc: `Beginning at 14th level, you can use your action to end one spell on yourself or on one willing creature that you touch.

You can use this feature a number of times equal to your Charisma modifier (a minimum of once). You regain expended uses when you finish a long rest.`,
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
      ],
    } as IClass,
  },
  {
    id: 'ranger',
    doc: {
      name: 'Ranger',
      desc: '',
      hitDie: 10,
      saves: {
        strength: SkillProficiency.Proficient,
        dexterity: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 3,
          includes: [
            'animalHandling',
            'athletics',
            'insight',
            'investigation',
            'nature',
            'perception',
            'stealth',
            'survival',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'half', attribute: 'wisdom', knowsAll: true },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Medium Armor',
        'Shields',
        'Simple Weapons',
        'Martial Weapons',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'FavoredEnemy',
          level: 1,
          name: 'Favored Enemy',
          desc: `Beginning at 1st level, you have significant experience studying, tracking, hunting, and even talking to a certain type of enemy.

Choose a type of favored enemy: aberrations, beasts, celestials, constructs, dragons, elementals, fey, fiends, giants, monstrosities, oozes, plants, or undead. Alternatively, you can select two races of humanoid (such as gnolls and orcs) as favored enemies.

You have advantage on Wisdom (Survival) checks to track your favored enemies, as well as on Intelligence checks to recall information about them.

When you gain this feature, you also learn one language of your choice that is spoken by your favored enemies, if they speak one at all.

You choose one additional favored enemy, as well as an associated language, at 6th and 14th level. As you gain levels, your choices should reflect the types of monsters you have encountered on your adventures.`,
        },
        {
          key: 'naturalExplorer',
          level: 1,
          name: 'Natural Explorer',
          desc: `You are particularly familiar with one type of natural environment and are adept at traveling and surviving in such regions. Choose one type of favored terrain: arctic, coast, desert, forest, grassland, mountain, or swamp. When you make an Intelligence or Wisdom check related to your favored terrain, your proficiency bonus is doubled if you are using a skill that you’re proficient in.

While traveling for an hour or more in your favored terrain, you gain the following benefits:

 - Difficult terrain doesn’t slow your group’s travel.
 - Your group can’t become lost except by magical means.
 - Even when you are engaged in another activity while traveling (such as foraging, navigating, or tracking), you remain alert to danger.
 - If you are traveling alone, you can move stealthily at a normal pace.
 - When you forage, you find twice as much food as you normally would.
 - While tracking other creatures, you also learn their exact number, their sizes, and how long ago they passed through the area.

You choose additional favored terrain types at 6th and 10th level`,
        },
        {
          key: 'fightingStyle',
          level: 2,
          name: 'Fighting Style',
          desc: `At 2nd level, you adopt a particular style of fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.`,
          choices: {
            fightingStyle: {
              type: 'custom',
              options: `
| Style                   | Effects                                                                                                                                                                                                                                                                                             |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Archery	                | You gain a +2 bonus to attack rolls you make with ranged weapons.                                                                                                                                                                                                                                   |
| Defense	                | While you are wearing armor you gain a +1 bonus to AC.                                                                                                                                                                                                                                              |
| Dueling	                | When you are wielding a melee weapon in one hand and no other weapons you gain a +2 bonus to damage rolls with that weapon.you gain a +2 bonus to damage rolls with that weapon.                                                                                                                    |
| Two-Weapon Fighting	    | When you engage in two-weapon fighting you can add your ability modifier to the damage of the second attack.                                                                                                                                                                                        |`,
            },
          },
        },
        {
          key: 'spellcasting',
          level: 2,
          name: 'Spellcasting',
          desc: `By the time you reach 2nd level, you have learned to use the magical essence of nature to cast spells, much as a druid does.

## Spell Slots
Table: The Ranger shows how many spell slots you have to cast your spells of 1st level and higher. To cast one of these spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

For example, if you know the 1st-level spell animal friendship and have a 1st-level and a 2nd-level spell slot available, you can cast animal friendship using either slot.

## Spells Known of 1st Level and Higher
You know two 1st-level spells of your choice from the ranger spell list.

The Spells Known column of Table: The Ranger shows when you learn more ranger spells of your choice. Each of these spells must be of a level for which you have spell slots. For instance, when you reach 5th level in this class, you can learn one new spell of 1st or 2nd level.

Additionally, when you gain a level in this class, you can choose one of the ranger spells you know and replace it with another spell from the ranger spell list, which also must be of a level for which you have spell slots.

## Spellcasting Ability
Wisdom is your spellcasting ability for your ranger spells, since your magic draws on your attunement to nature. You use your Wisdom whenever a spell refers to your spellcasting ability. In addition, you use your Wisdom modifier when setting the saving throw DC for a ranger spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Wisdom modifier

Spell attack modifier = your proficiency bonus + your Wisdom modifier.`,
        },
        {
          key: 'primevalAwareness',
          level: 3,
          name: 'Primeval Awareness',
          desc:
            'Beginning at 3rd level, you can use your action and expend one ranger spell slot to focus your awareness on the region around you. For 1 minute per level of the spell slot you expend, you can sense whether the following types of creatures are present within 1 mile of you (or within up to 6 miles if you are in your favored terrain): aberrations, celestials, dragons, elementals, fey, fiends, and undead. This feature doesn’t reveal the creatures’ location or number.',
        },
        {
          key: 'rangerArchetype',
          level: 3,
          name: 'Ranger Archetype',
          desc:
            'At 3rd level, you choose an archetype that you strive to emulate.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-ranger',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'extraAttack',
          level: 5,
          name: 'Extra Attack',
          desc:
            'Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.',
        },
        {
          key: 'landsStride',
          level: 8,
          name: 'Land’s Stride',
          desc: `Starting at 8th level, moving through nonmagical difficult terrain costs you no extra movement. You can also pass through nonmagical plants without being slowed by them and without taking damage from them if they have thorns, spines, or a similar hazard.

In addition, you have advantage on saving throws against plants that are magically created or manipulated to impede movement, such those created by the entangle spell.`,
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'hideInPlainSight',
          level: 10,
          name: 'Hide in Plain Sight',
          desc: `Starting at 10th level, you can spend 1 minute creating camouflage for yourself. You must have access to fresh mud, dirt, plants, soot, and other naturally occurring materials with which to create your camouflage.

Once you are camouflaged in this way, you can try to hide by pressing yourself up against a solid surface, such as a tree or wall, that is at least as tall and wide as you are. You gain a +10 bonus to Dexterity (Stealth) checks as long as you remain there without moving or taking actions. Once you move or take an action or a reaction, you must camouflage yourself again to gain this benefit.`,
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'vanish',
          level: 14,
          name: 'Vanish',
          desc:
            'Starting at 14th level, you can use the Hide action as a bonus action on your turn. Also, you can’t be tracked by nonmagical means, unless you choose to leave a trail.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'feralSenses',
          level: 18,
          name: 'Feral Senses',
          desc: `At 18th level, you gain preternatural senses that help you fight creatures you can’t see. When you attack a creature you can’t see, your inability to see it doesn’t impose disadvantage on your attack rolls against it.

You are also aware of the location of any invisible creature within 30 feet of you, provided that the creature isn’t hidden from you and you aren’t blinded or deafened.`,
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'foeSlayer',
          level: 20,
          name: 'Foe Slayer',
          desc:
            'At 20th level, you become an unparalleled hunter of your enemies. Once on each of your turns, you can add your Wisdom modifier to the attack roll or the damage roll of an attack you make against one of your favored enemies. You can choose to use this feature before or after the roll, but before any effects of the roll are applied.',
        },
      ],
    } as IClass,
  },
  {
    id: 'rogue',
    doc: {
      name: 'Rogue',
      desc: '',
      hitDie: 8,
      saves: {
        dexterity: SkillProficiency.Proficient,
        intelligence: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 4,
          includes: [
            'acrobatics',
            'athletics',
            'deception',
            'insight',
            'intimidation',
            'investigation',
            'perception',
            'performance',
            'persuasion',
            'sleightOfHand',
            'stealth',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'none' },
      languages: [],
      proficiencies: [
        'Light Armor',
        'Simple Weapons',
        'hand crossbows',
        'longswords',
        'rapiers',
        'shortswords',
        "thieves' tools",
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'expertise',
          level: 1,
          name: 'Expertise',
          desc: `At 1st level, choose two of your skill proficiencies, or one of your skill proficiencies and your proficiency with thieves’ tools. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies.

At 6th level, you can choose two more of your proficiencies (in skills or with thieves’ tools) to gain this benefit.`,
        },
        {
          key: 'sneakAttack',
          level: 1,
          name: 'Sneak Attack',
          desc: `Beginning at 1st level, you know how to strike subtly and exploit a foe’s distraction. Once per turn, you can deal an extra 1d6 damage to one creature you hit with an attack if you have advantage on the attack roll. The attack must use a finesse or a ranged weapon.

You don’t need advantage on the attack roll if another enemy of the target is within 5 feet of it, that enemy isn’t incapacitated, and you don’t have disadvantage on the attack roll.

The amount of the extra damage increases as you gain levels in this class, as shown in the Sneak Attack column of the Rogue table.`,
        },
        {
          key: 'thievesCant',
          level: 1,
          name: 'Thieves’ Cant',
          desc: `During your rogue training you learned thieves’ cant, a secret mix of dialect, jargon, and code that allows you to hide messages in seemingly normal conversation. Only another creature that knows thieves’ cant understands such messages. It takes four times longer to convey such a message than it does to speak the same idea plainly.

In addition, you understand a set of secret signs and symbols used to convey short, simple messages, such as whether an area is dangerous or the territory of a thieves’ guild, whether loot is nearby, or whether the people in an area are easy marks or will provide a safe house for thieves on the run.`,
        },
        {
          key: 'cunningAction',
          level: 2,
          name: 'Cunning Action',
          desc:
            'Starting at 2nd level, your quick thinking and agility allow you to move and act quickly. You can take a bonus action on each of your turns in combat. This action can be used only to take the Dash, Disengage, or Hide action.',
        },
        {
          key: 'archetype',
          level: 3,
          name: 'Archetype',
          desc:
            'At 3rd level, you choose an archetype that you emulate in the exercise of your rogue abilities.',
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'uncannyDodge',
          level: 5,
          name: 'Uncanny Dodge',
          desc:
            'Starting at 5th level, when an attacker that you can see hits you with an attack, you can use your reaction to halve the attack’s damage against you.',
        },
        {
          key: 'evasion',
          level: 7,
          name: 'Evasion',
          desc:
            'Beginning at 7th level, you can nimbly dodge out of the way of certain area effects, such as a red dragon’s fiery breath or an ice storm spell. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'reliableTalent',
          level: 11,
          name: 'Reliable Talent',
          desc:
            'By 11th level, you have refined your chosen skills until they approach perfection. Whenever you make an ability check that lets you add your proficiency bonus, you can treat a d20 roll of 9 or lower as a 10.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'blindsense',
          level: 14,
          name: 'Blindsense',
          desc:
            'Starting at 14th level, if you are able to hear, you are aware of the location of any hidden or invisible creature within 10 feet of you.',
        },
        {
          key: 'slipperyMind',
          level: 15,
          name: 'Slippery Mind',
          desc:
            'By 15th level, you have acquired greater mental strength. You gain proficiency in Wisdom saving throws.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'elusive',
          level: 18,
          name: 'Elusive',
          desc:
            'Beginning at 18th level, you are so evasive that attackers rarely gain the upper hand against you. No attack roll has advantage against you while you aren’t incapacitated.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'strokeOfLuck',
          level: 20,
          name: 'Stroke of Luck',
          desc: `At 20th level, you have an uncanny knack for succeeding when you need to. If your attack misses a target within range, you can turn the miss into a hit. Alternatively, if you fail an ability check, you can treat the d20 roll as a 20.

Once you use this feature, you can’t use it again until you finish a short or long rest.`,
        },
      ],
    } as IClass,
  },
  {
    id: 'sorcerer',
    doc: {
      name: 'Sorcerer',
      desc: '',
      hitDie: 6,
      saves: {
        constitution: SkillProficiency.Proficient,
        charisma: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'arcana',
            'deception',
            'insight',
            'intimidation',
            'persuation',
            'religion',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'full', attribute: 'charisma', knowsAll: false },
      languages: [],
      proficiencies: [
        'daggers',
        'darts',
        'slings',
        'quarterstaffs',
        'light crossbows',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'spellcasting',
          level: 1,
          name: 'Spellcasting',
          desc: `An event in your past, or in the life of a parent or ancestor, left an indelible mark on you, infusing you with arcane magic. This font of magic, whatever its origin, fuels your spells.

## Cantrips
At 1st level, you know four cantrips of your choice from the sorcerer spell list. You learn additional sorcerer cantrips of your choice at higher levels, as shown in the Cantrips Known column of Table: The Sorcerer.

## Spell Slots
Table: The Sorcerer shows how many spell slots you have to cast your spells of 1st level and higher. To cast one of these sorcerer spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

For example, if you know the 1st-level spell burning hands and have a 1st-level and a 2nd-level spell slot available, you can cast burning hands using either slot.

## Spells Known of 1st Level and Higher
You know two 1st-level spells of your choice from the sorcerer spell list.

The Spells Known column of Table: The Sorcerer shows when you learn more sorcerer spells of your choice. Each of these spells must be of a level for which you have spell slots. For instance, when you reach 3rd level in this class, you can learn one new spell of 1st or 2nd level.

Additionally, when you gain a level in this class, you can choose one of the sorcerer spells you know and replace it with another spell from the sorcerer spell list, which also must be of a level for which you have spell slots.

## Spellcasting Ability
Charisma is your spellcasting ability for your sorcerer spells, since the power of your magic relies on your ability to project your will into the world. You use your Charisma whenever a spell refers to your spellcasting ability. In addition, you use your Charisma modifier when setting the saving throw DC for a sorcerer spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Charisma modifier

## Spellcasting Focus
You can use an arcane focus as a spellcasting focus for your sorcerer spells.`,
        },
        {
          key: 'sorcerousOrigin',
          level: 1,
          name: 'Sorcerous Origin',
          desc:
            'Choose a sorcerous origin, which describes the source of your innate magical power.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-sorcerer',
            },
          },
        },
        {
          key: 'fontOfMagic',
          level: 2,
          name: 'Font of Magic',
          desc: `At 2nd level, you tap into a deep wellspring of magic within yourself. This wellspring is represented by sorcery points, which allow you to create a variety of magical effects.

## Sorcery Points
You have 2 sorcery points, and you gain more as you reach higher levels, as shown in the Sorcery Points column of Table: The Sorcerer. You can never have more sorcery points than shown on the table for your level. You regain all spent sorcery points when you finish a long rest.

## Flexible Casting
You can use your sorcery points to gain additional spell slots, or sacrifice spell slots to gain additional sorcery points. You learn other ways to use your sorcery points as you reach higher levels.

**Creating Spell Slots.** You can transform unexpended sorcery points into one spell slot as a bonus action on your turn. The Creating Spell Slots table shows the cost of creating a spell slot of a given level. You can create spell slots no higher in level than 5th.

Any spell slot you create with this feature vanishes when you finish a long rest.

| Spell Slot Level | Sorcery Point Cost |
|------------------|--------------------|
| 1st              | 2                  |
| 2nd              | 3                  |
| 3rd              | 5                  |
| 4th              | 6                  |
| 5th              | 7                  |

**Converting a Spell Slot to Sorcery Points.** As a bonus action on your turn, you can expend one spell slot and gain a number of sorcery points equal to the slot’s level.`,
        },
        {
          key: 'metamagic',
          level: 3,
          name: 'Metamagic',
          desc: `At 3rd level, you gain the ability to twist your spells to suit your needs. You gain two of the following Metamagic options of your choice. You gain another one at 10th and 17th level.

You can use only one Metamagic option on a spell when you cast it, unless otherwise noted.

## Careful Spell
When you cast a spell that forces other creatures to make a saving throw, you can protect some of those creatures from the spell’s full force. To do so, you spend 1 sorcery point and choose a number of those creatures up to your Charisma modifier (minimum of one creature). A chosen creature automatically succeeds on its saving throw against the spell.

## Distant Spell
When you cast a spell that has a range of 5 feet or greater, you can spend 1 sorcery point to double the range of the spell.

When you cast a spell that has a range of touch, you can spend 1 sorcery point to make the range of the spell 30 feet.

## Empowered Spell
When you roll damage for a spell, you can spend 1 sorcery point to reroll a number of the damage dice up to your Charisma modifier (minimum of one). You must use the new rolls.

You can use Empowered Spell even if you have already used a different Metamagic option during the casting of the spell.

## Extended Spell
When you cast a spell that has a duration of 1 minute or longer, you can spend 1 sorcery point to double its duration, to a maximum duration of 24 hours.

## Heightened Spell
When you cast a spell that forces a creature to make a saving throw to resist its effects, you can spend 3 sorcery points to give one target of the spell disadvantage on its first saving throw made against the spell.

## Quickened Spell
When you cast a spell that has a casting time of 1 action, you can spend 2 sorcery points to change the casting time to 1 bonus action for this casting.

## Subtle Spell
When you cast a spell, you can spend 1 sorcery point to cast it without any somatic or verbal components.

## Twinned Spell
When you cast a spell that targets only one creature and doesn’t have a range of self, you can spend a number of sorcery points equal to the spell’s level to target a second creature in range with the same spell (1 sorcery point if the spell is a cantrip).

To be eligible, a spell must be incapable of targeting more than one creature at the spell’s current level. For example, magic missile and scorching ray aren’t eligible, but ray of frost and chromatic orb are.`,
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'sorcerousRestoration',
          level: 20,
          name: 'Sorcerous Restoration',
          desc:
            'At 20th level, you regain 4 expended sorcery points whenever you finish a short rest.',
        },
      ],
    } as IClass,
  },
  {
    id: 'warlock',
    doc: {
      name: 'Warlock',
      desc: '',
      hitDie: 8,
      saves: {
        wisdom: SkillProficiency.Proficient,
        charisma: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'arcana',
            'deception',
            'history',
            'intimidation',
            'investigation',
            'nature',
            'religion',
          ],
        },
      },
      sight: {},
      spellcasting: { type: 'none' },
      languages: [],
      proficiencies: ['Light Armor', 'Simple Weapons'],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'patron',
          level: 1,
          name: 'Patron',
          desc: `At 1st level, you have struck a bargain with an otherworldly being of your choice. Only details for the Fiend were released as Open Game Content by Wizards of the Coast. Additional options are listed below.

Your choice grants you features at 1st level and again at 6th, 10th, and 14th level.

The beings that serve as patrons for warlocks are mighty inhabitants of other planes of existence—not gods, but almost godlike in their power. Various patrons give their warlocks access to different powers and invocations, and expect significant favors in return. Some patrons collect warlocks, doling out mystic knowledge relatively freely or boasting of their ability to bind mortals to their will. Other patrons bestow their power only grudgingly, and might make a pact with only one warlock. Warlocks who serve the same patron might view each other as allies, siblings, or rivals.`,
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-warlock',
            },
          },
        },
        {
          key: 'pactMagic',
          level: 1,
          name: 'Pact Magic',
          desc: `Your arcane research and the magic bestowed on you by your patron have given you facility with spells.

## Cantrips
You know two cantrips of your choice from the warlock spell list. You learn additional warlock cantrips of your choice at higher levels, as shown in the Cantrips Known column of the Warlock table.

## Spell Slots
Table: The Warlock shows how many spell slots you have. The table also shows what the level of those slots is; all of your spell slots are the same level. To cast one of your warlock spells of 1st level or higher, you must expend a spell slot. You regain all expended spell slots when you finish a short or long rest. For example, when you are 5th level, you have two 3rd-level spell slots. To cast the 1st-level spell thunderwave, you must spend one of those slots, and you cast it as a 3rd-level spell.

## Spells Known of 1st Level and Higher
At 1st level, you know two 1st-level spells of your choice from the warlock spell list. The Spells Known column of Table: The Warlock shows when you learn more warlock spells of your choice of 1st level and higher. A spell you choose must be of a level no higher than what’s shown in the table’s Slot Level column for your level. When you reach 6th level, for example, you learn a new warlock spell, which can be 1st, 2nd, or 3rd level. Additionally, when you gain a level in this class, you can choose one of the warlock spells you know and replace it with another spell from the warlock spell list, which also must be of a level for which you have spell slots.

## Spellcasting Ability
Charisma is your spellcasting ability for your warlock spells, so you use your Charisma whenever a spell refers to your spellcasting ability. In addition, you use your Charisma modifier when setting the saving throw DC for a warlock spell you cast and when making an attack roll with one. Spell save DC = 8 + your proficiency bonus + your Charisma modifier Spell attack modifier = your proficiency bonus + your Charisma modifier

## Spellcasting Focus
You can use an arcane focus as a spellcasting focus for your warlock spells.`,
        },
        {
          key: 'eldritchInvocations',
          level: 2,
          name: 'Eldritch Invocations',
          desc:
            'In your study of occult lore, you have unearthed eldritch invocations, fragments of forbidden knowledge that imbue you with an abiding magical ability. At 2nd level, you gain two eldritch invocations of your choice. Your invocation options are detailed at the end of the class description. When you gain certain warlock levels, you gain additional invocations of your choice, as shown in the Invocations Known column of the Warlock table. Additionally, when you gain a level in this class, you can choose one of the invocations you know and replace it with another invocation that you could learn at that level',
        },
        {
          key: 'pactBoon',
          level: 3,
          name: 'Pact Boon',
          desc: `At 3rd level, your otherworldly patron bestows a gift upon you for your loyal service. You gain one of the following features.

## Pact of the Chain
You learn the find familiar spell and can cast it as a ritual. The spell doesn’t count against your number of spells known. When you cast the spell, you can choose one of the normal forms for your familiar or one of the following special forms: imp, pseudodragon, quasit, or sprite. Additionally, when you take the Attack action, you can forgo one of your own attacks to allow your familiar to make one attack of its own with its reaction.

## Pact of the Blade
You can use your action to create a pact weapon in your empty hand. You can choose the form that this melee weapon takes each time you create it. You are proficient with it while you wield it. This weapon counts as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage. Your pact weapon disappears if it is more than 5 feet away from you for 1 minute or more. It also disappears if you use this feature again, if you dismiss the weapon (no action required), or if you die. You can transform one magic weapon into your pact weapon by performing a special ritual while you hold the weapon. You perform the ritual over the course of 1 hour, which can be done during a short rest. You can then dismiss the weapon, shunting it into an extradimensional space, and it appears whenever you create your pact weapon thereafter. You can’t affect an artifact or a sentient weapon in this way. The weapon ceases being your pact weapon if you die, if you perform the 1-hour ritual on a different weapon, or if you use a 1-hour ritual to break your bond to it. The weapon appears at your feet if it is in the extradimensional space when the bond breaks.

## Pact of the Tome
Your patron gives you a grimoire called a Book of Shadows. When you gain this feature, choose three cantrips from any class’s spell list (the three needn’t be from the same list). While the book is on your person, you can cast those cantrips at will. They don’t count against your number of cantrips known. If they don’t appear on the warlock spell list, they are nonetheless warlock spells for you. If you lose your Book of Shadows, you can perform a 1-hour ceremony to receive a replacement from your patron. This ceremony can be performed during a short or long rest, and it destroys the previous book. The book turns to ash when you die.`,
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'mythicArcanum',
          level: 11,
          name: 'Mythic Arcanum',
          desc:
            'At 11th level, your patron bestows upon you a magical secret called an arcanum. Choose one 6th- level spell from the warlock spell list as this arcanum. You can cast your arcanum spell once without expending a spell slot. You must finish a long rest before you can do so again. At higher levels, you gain more warlock spells of your choice that can be cast in this way: one 7th- level spell at 13th level, one 8th-level spell at 15th level, and one 9th-level spell at 17th level. You regain all uses of your Mystic Arcanum when you finish a long rest.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'eldritchMaster',
          level: 20,
          name: 'Eldritch Master',
          desc:
            'At 20th level, you can draw on your inner reserve of mystical power while entreating your patron to regain expended spell slots. You can spend 1 minute entreating your patron for aid to regain all your expended spell slots from your Pact Magic feature. Once you regain spell slots with this feature, you must finish a long rest before you can do so again.',
        },
      ],
    } as IClass,
  },
  {
    id: 'wizard',
    doc: {
      name: 'Wizard',
      desc: '',
      hitDie: 6,
      saves: {
        intelligence: SkillProficiency.Proficient,
        wisdom: SkillProficiency.Proficient,
      },
      skills: {},
      choices: {
        skills: {
          type: 'skill',
          num: 2,
          includes: [
            'arcana',
            'history',
            'insight',
            'investigation',
            'medicine',
            'religion',
          ],
        },
      },
      sight: {},
      spellcasting: {
        type: 'full',
        attribute: 'intelligence',
        knowsAll: false,
      },
      languages: [],
      proficiencies: [
        'daggers',
        'darts',
        'slings',
        'quarterstaffs',
        'light crossbows',
      ],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      resources: [],
      features: [
        {
          key: 'spellcasting',
          level: 1,
          name: 'Spellcasting',
          desc: `As a student of arcane magic, you have a spellbook containing spells that show the first glimmerings of your true power.

## Cantrips
At 1st level, you know three cantrips of your choice from the wizard spell list. You learn additional wizard cantrips of your choice at higher levels, as shown in the Cantrips Known column of the Wizard table.

## Spellbook
At 1st level, you have a spellbook containing six 1st- level wizard spells of your choice. Your spellbook is the repository of the wizard spells you know, except your cantrips, which are fixed in your mind.

## Preparing and Casting Spells
Table: The Wizard shows how many spell slots you have to cast your spells of 1st level and higher. To cast one of these spells, you must expend a slot of the spell’s level or higher. You regain all expended spell slots when you finish a long rest.

You prepare the list of wizard spells that are available for you to cast. To do so, choose a number of wizard spells from your spellbook equal to your Intelligence modifier + your wizard level (minimum of one spell). The spells must be of a level for which you have spell slots.

For example, if you’re a 3rd-level wizard, you have four 1st-level and two 2nd-level spell slots. With an Intelligence of 16, your list of prepared spells can include six spells of 1st or 2nd level, in any combination, chosen from your spellbook. If you prepare the 1st-level spell magic missile, you can cast it using a 1st-level or a 2nd-level slot. Casting the spell doesn’t remove it from your list of prepared spells.

You can change your list of prepared spells when you finish a long rest. Preparing a new list of wizard spells requires time spent studying your spellbook and memorizing the incantations and gestures you must make to cast the spell: at least 1 minute per spell level for each spell on your list.

## Spellcasting Ability
Intelligence is your spellcasting ability for your wizard spells, since you learn your spells through dedicated study and memorization. You use your Intelligence whenever a spell refers to your spellcasting ability. In addition, you use your Intelligence modifier when setting the saving throw DC for a wizard spell you cast and when making an attack roll with one.

Spell save DC = 8 + your proficiency bonus + your Intelligence modifier

Spell attack modifier = your proficiency bonus + your Intelligence modifier

## Ritual Casting
You can cast a wizard spell as a ritual if that spell has the ritual tag and you have the spell in your spellbook. You don’t need to have the spell prepared.

## Spellcasting Focus
You can use an arcane focus as a spellcasting focus for your wizard spells.

## Learning Spells of 1st Level and Higher
Each time you gain a wizard level, you can add two wizard spells of your choice to your spellbook for free. Each of these spells must be of a level for which you have spell slots, as shown on the Wizard table. On your adventures, you might find other spells that you can add to your spellbook (see “Your Spellbook”).`,
        },
        {
          key: 'arcaneRecovery',
          level: 1,
          name: 'Arcane Recovery',
          desc: `You have learned to regain some of your magical energy by studying your spellbook. Once per day when you finish a short rest, you can choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your wizard level (rounded up), and none of the slots can be 6th level or higher.

For example, if you’re a 4th-level wizard, you can recover up to two levels worth of spell slots. You can recover either a 2nd-level spell slot or two 1st-level spell slots.`,
        },
        {
          key: 'arcaneTradtion',
          level: 2,
          name: 'Arcane Tradtion',
          desc:
            'When you reach 2nd level, you choose an arcane tradition, shaping your practice of magic.',
          choices: {
            subclass: {
              type: 'subclass',
              class: 'class-wizard',
            },
          },
        },
        {
          key: 'asi',
          level: 4,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi1',
          level: 8,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi2',
          level: 12,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'asi3',
          level: 16,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'spellMastery',
          level: 18,
          name: 'Spell Mastery',
          desc: `At 18th level, you have achieved such mastery over certain spells that you can cast them at will. Choose a 1st-level wizard spell and a 2nd-level wizard spell that are in your spellbook. You can cast those spells at their lowest level without expending a spell slot when you have them prepared. If you want to cast either spell at a higher level, you must expend a spell slot as normal.

By spending 8 hours in study, you can exchange one or both of the spells you chose for different spells of the same levels.`,
        },
        {
          key: 'asi4',
          level: 19,
          name: 'Ability Score Improvement',
          desc:
            'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
        },
        {
          key: 'signatureSpells',
          level: 20,
          name: 'Signature Spells',
          desc: `When you reach 20th level, you gain mastery over two powerful spells and can cast them with little effort. Choose two 3rd-level wizard spells in your spellbook as your signature spells. You always have these spells prepared, they don’t count against the number of spells you have prepared, and you can cast each of them once at 3rd level without expending a spell slot. When you do so, you can’t do so again until you finish a short or long rest.

If you want to cast either spell at a higher level, you must expend a spell slot as normal.`,
        },
      ],
    } as IClass,
  },
  //   {
  //     id: '',
  //     doc: {
  //       name: '',
  //       desc: '',
  //       hitDie: 0,
  //       saves: {},
  //       skills: {},
  //       choices: {
  //         skills: {
  //           type: 'skill',
  //           num: 0,
  //           includes: [
  //           ]
  //         }
  //       },
  //       sight: {},
  //       spellcasting: { type: 'none' },
  //       languages: [],
  //       proficiencies: [],
  //       conditionImmunities: [],
  //       immunities: [],
  //       resistances: [],
  //       vulnerabilities: [],
  //       resources: [],
  //       features: [
  // {
  //     key: '',
  //     level: 0,
  //     name: '',
  //     desc: '',
  //   },
  // {
  //     key: 'asi',
  //     level: 4,
  //     name: 'Ability Score Improvement',
  //     desc:
  //       'When you reach 4th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
  //   },
  //   {
  //     key: 'asi1',
  //     level: 8,
  //     name: 'Ability Score Improvement',
  //     desc:
  //       'When you reach 8th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
  //   },
  //   {
  //     key: 'asi2',
  //     level: 12,
  //     name: 'Ability Score Improvement',
  //     desc:
  //       'When you reach 12th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
  //   },
  //   {
  //     key: 'asi3',
  //     level: 16,
  //     name: 'Ability Score Improvement',
  //     desc:
  //       'When you reach 16th you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
  //   },
  //   {
  //     key: 'asi4',
  //     level: 19,
  //     name: 'Ability Score Improvement',
  //     desc:
  //       'When you reach 19th level you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature.',
  //   },
  //   ],
  //   }],
  //     },
  //   },
];
