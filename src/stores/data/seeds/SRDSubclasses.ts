import { ISubclass } from '../../models';

export default [
  {
    id: 'barbarian-berserker',
    doc: {
      class: 'class-barbarian',
      name: 'Path of the Berserker',
      desc:
        'For some barbarians, rage is a means to an end— that end being violence. The Path of the Berserker is a path of untrammeled fury, slick with blood. As you enter the berserker’s rage, you thrill in the chaos of battle, heedless of your own health or well-being.',
      features: [
        {
          key: 'frenzy',
          level: 3,
          name: 'Frenzy',
          desc:
            'Starting when you choose this path at 3rd level, you can go into a frenzy when you rage. If you do so, for the duration of your rage you can make a single melee weapon attack as a bonus action on each of your turns after this one. When your rage ends, you suffer one level of exhaustion.',
        },
        {
          key: 'mindlessRage',
          level: 6,
          name: 'Mindless Rage',
          desc:
            'Beginning at 6th level, you can’t be charmed or frightened while raging. If you are charmed or frightened when you enter your rage, the effect is suspended for the duration of the rage.',
        },
        {
          key: 'intimiPresence',
          level: 10,
          name: 'Intimidating Presence',
          desc: `Beginning at 10th level, you can use your action to frighten someone with your menacing presence. When you do so, choose one creature that you can see within 30 feet of you. If the creature can see or hear you, it must succeed on a Wisdom saving throw (DC equal to 8 + your proficiency bonus + your Charisma modifier) or be frightened of you until the end of your next turn. On subsequent turns, you can use your action to extend the duration of this effect on the frightened creature until the end of your next turn. This effect ends if the creature ends its turn out of line of sight or more than 60 feet away from you.

If the creature succeeds on its saving throw, you can’t use this feature on that creature again for 24 hours.`,
        },
        {
          key: 'retaliation',
          level: 14,
          name: 'Retaliation',
          desc:
            'Starting at 14th level, when you take damage from a creature that is within 5 feet of you, you can use your reaction to make a melee weapon attack against that creature.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'bard-lore',
    doc: {
      class: 'class-bard',
      name: 'College of Lore',
      desc: `Bards of the College of Lore know something about most things, collecting bits of knowledge from sources as diverse as scholarly tomes and peasant tales. Whether singing folk ballads in taverns or elaborate compositions in royal courts, these bards use their gifts to hold audiences spellbound. When the applause dies down, the audience members might find themselves questioning everything they held to be true, from their faith in the priesthood of the local temple to their loyalty to the king.

The loyalty of these bards lies in the pursuit of beauty and truth, not in fealty to a monarch or following the tenets of a deity. A noble who keeps such a bard as a herald or advisor knows that the bard would rather be honest than politic.

The college’s members gather in libraries and sometimes in actual colleges, complete with classrooms and dormitories, to share their lore with one another. They also meet at festivals or affairs of state, where they can expose corruption, unravel lies, and poke fun at self-important figures of authority.`,
      features: [
        {
          key: 'bonusProf',
          level: 3,
          name: 'Bonus Proficiencies',
          desc:
            'When you join the College of Lore at 3rd level, you gain proficiency with three skills of your choice.',
        },
        {
          key: 'cuttingWords',
          level: 3,
          name: 'Cutting Words',
          desc:
            'Also at 3rd level, you learn how to use your wit to distract, confuse, and otherwise sap the confidence and competence of others. When a creature that you can see within 60 feet of you makes an attack roll, an ability check, or a damage roll, you can use your reaction to expend one of your uses of Bardic Inspiration, rolling a Bardic Inspiration die and subtracting the number rolled from the creature’s roll. You can choose to use this feature after the creature makes its roll, but before the GM determines whether the attack roll or ability check succeeds or fails, or before the creature deals its damage. The creature is immune if it can’t hear you or if it’s immune to being charmed.',
        },
        {
          key: 'addMagicalSecrets',
          level: 6,
          name: 'Additional Magical Secrets',
          desc: `At 6th level, you learn two spells of your choice from any class. A spell you choose must be of a level you can cast, as shown on the Bard table, or a cantrip.

The chosen spells count as bard spells for you but don’t count against the number of bard spells you know.`,
        },
        {
          key: 'peerlessSkill',
          level: 14,
          name: 'Peerless Skill',
          desc:
            'Starting at 14th level, when you make an ability check, you can expend one use of Bardic Inspiration. Roll a Bardic Inspiration die and add the number rolled to your ability check. You can choose to do so after you roll the die for the ability check, but before the GM tells you whether you succeed or fail.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'cleric-life',
    doc: {
      class: 'class-cleric',
      name: 'Life',
      desc:
        'The Life Domain focuses on the vibrant positive energy—one of the fundamental forces of the universe—that sustains all life. The gods of life promote vitality and health through Healing the sick and wounded, caring for those in need, and driving away the forces of death and undeath. Almost any non-evil deity can claim influence over this domain, particularly agricultural deities, sun gods, gods of Healing or endurance, and gods of home and community.',
      features: [
        {
          key: 'bonusProficiency',
          level: 1,
          name: 'Bonus Proficiency',
          desc:
            'When you choose this domain at 1st level, you gain proficiency with Heavy Armor.',
        },
        {
          key: 'discipleOfLife',
          level: 1,
          name: 'Disciple of Life',
          desc:
            'Also starting at 1st level, your Healing Spells are more effective. Whenever you use a spell of 1st level or higher to restore Hit Points to a creature, the creature regains additional Hit Points equal to 2 + the spell’s level.',
        },
        {
          key: 'preserveLife',
          level: 2,
          name: 'Channel Divinity: Preserve Life',
          desc: `Starting at 2nd level, you can use your Channel Divinity to heal the badly injured.

As an action, you present your holy Symbol and evoke Healing energy that can restore a number of Hit Points equal to five times your Cleric level.

Choose any creatures within 30 feet of you, and divide those Hit Points among them. This feature can restore a creature to no more than half of its hit point maximum. You can’t use this feature on an Undead or a construct.`,
        },
        {
          key: 'blessedHealer',
          level: 6,
          name: 'Blessed Healer',
          desc:
            'Beginning at 6th level, the Healing Spells you cast on others heal you as well. When you Cast a Spell of 1st level or higher that restores Hit Points to a creature other than you, you regain Hit Points equal to 2 + the spell’s level.',
        },
        {
          key: 'divineStrike',
          level: 8,
          name: 'Divine Strike',
          desc: `At 8th level, you gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon Attack, you can cause the Attack to deal an extra 1d8 radiant damage to the target. When you reach 14th level, the extra damage increases to 2d8.`,
        },
        {
          key: 'supremHealing',
          level: 17,
          name: 'Supreme Healing',
          desc:
            'Starting at 17th level, when you would normally roll one or more dice to restore Hit Points with a spell, you instead use the highest number possible for each die. For example, instead of restoring 2d6 Hit Points to a creature, you restore 12.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'druid-land',
    doc: {
      class: 'class-druid',
      name: 'Circle of Land',
      desc:
        'The Circle of the Land is made up of mystics and sages who safeguard ancient knowledge and rites through a vast oral tradition. These druids meet within sacred circles of trees or standing stones to whisper primal secrets in Druidic. The circle’s wisest members preside as the chief priests of communities that hold to the Old Faith and serve as advisors to the rulers of those folk. As a member of this circle, your magic is influenced by the land where you were initiated into the circle’s mysterious rites.',
      features: [
        {
          key: 'bonusCantrip',
          level: 2,
          name: 'Bonus Cantrip',
          desc:
            'When you choose this circle at 2nd level, you learn one additional druid cantrip of your choice.',
          choices: {
            cantrip: {
              type: 'spell',
              level: 0,
              list: 'druid',
            },
          },
        },
        {
          key: 'naturalRecovery',
          level: 2,
          name: 'Natural Recovery',
          desc: `Starting at 2nd level, you can regain some of your magical energy by sitting in meditation and communing with nature. During a short rest, you choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your druid level (rounded up), and none of the slots can be 6th level or higher. You can’t use this feature again until you finish a long rest.

For example, when you are a 4th-level druid, you can recover up to two levels worth of spell slots. You can recover either a 2nd-level slot or two 1st-level slots.`,
        },
        {
          key: 'circleSpells',
          level: 3,
          name: 'Circle Spells',
          choices: {
            circle: {
              type: 'custom',
              options: `
| Land         | Effects                                                                                                                                                       |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Arctic       | **3rd**: hold person, spike growth, **5th**: sleet storm, slow, **7th**: freedom of movement, ice storm, **9th**: commune with nature, cone of cold           |
| Coast        | **3rd**: mirror image, misty step, **5th**: water breathing, water walk, **7th**: control water, freedom of movement, **9th**: conjure elemental, scrying     |
| Desert       | **3rd**: blur, silence, **5th**: create food and water, protection from energy, **7th**: blight, hallucinatory terrain, **9th**: insect plague, wall of stone |
| Forest       | **3rd**: barkskin, spider climb,**5th**: call lightning, plant growth,**7th**: divination, freedom of movement,**9th**: commune with nature, tree stride      | 
| Grassland    | **3rd**: invisibility, pass without trace,**5th**: daylight, haste,**7th**: divination, freedom of movement,**9th**: dream, insect plague                     |
| Mountain     | **3rd**: spider climb, spike growth,**5th**: lightning bolt, meld into stone,**7th**: stone shape, stoneskin,**9th**: passwall, wall of stone                 |
| Swamp        | **3rd**: acid arrow, darkness,**5th**: water walk, stinking cloud,**7th**: freedom of movement, locate creature,**9th**: insect plague, scrying               |`,
            },
          },
          desc: `Your mystical connection to the land infuses you with the ability to cast certain spells. At 3rd, 5th, 7th, and 9th level you gain access to circle spells connected to the land where you became a druid.

Choose that land—arctic, coast, desert, forest, grassland, mountain, or swamp—and consult the associated list of spells.

Once you gain access to a circle spell, you always have it prepared, and it doesn’t count against the number of spells you can prepare each day. If you gain access to a spell that doesn’t appear on the druid spell list, the spell is nonetheless a druid spell for you.`,
        },
        {
          key: 'landsStride',
          level: 6,
          name: 'Land’s Stride',
          desc: `Starting at 6th level, moving through nonmagical difficult terrain costs you no extra movement. You can also pass through nonmagical plants without being slowed by them and without taking damage from them if they have thorns, spines, or a similar hazard.

In addition, you have advantage on saving throws against plants that are magically created or manipulated to impede movement, such those created by the entangle spell.`,
        },
        {
          key: 'naturesWard',
          level: 10,
          name: 'Nature’s Ward',
          desc:
            'When you reach 10th level, you can’t be charmed or frightened by elementals or fey, and you are immune to poison and disease.',
        },
        {
          key: 'naturesSanctuary',
          level: 14,
          name: 'Nature’s Sanctuary',
          desc:
            'When you reach 14th level, creatures of the natural world sense your connection to nature and become hesitant to attack you. When a beast or plant creature attacks you, that creature must make a Wisdom saving throw against your druid spell save DC. On a failed save, the creature must choose a different target, or the attack automatically misses. On a successful save, the creature is immune to this effect for 24 hours.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'fighter-champion',
    doc: {
      class: 'class-fighter',
      name: 'Champion',
      desc:
        'The champion focuses on the development of raw physical power honed to deadly perfection. Those who model themselves on this archetype combine rigorous training with physical excellence to deal devastating blows.',
      features: [
        {
          key: 'improvedCrit',
          level: 3,
          name: 'Improved Critical',
          desc:
            'Beginning when you choose this archetype at 3rd level, your weapon attacks score a critical hit on a roll of 19 or 20.',
        },
        {
          key: 'remarkableAthlete',
          level: 7,
          name: 'Remarkable Athlete',
          desc: `Starting at 7th level, you can add half your proficiency bonus (round up) to any Strength, Dexterity, or Constitution check you make that doesn’t already use your proficiency bonus.

In addition, when you make a running long jump, the distance you can cover increases by a number of feet equal to your Strength modifier.`,
        },
        {
          key: 'addFightingStyle',
          level: 10,
          name: 'Additional Fighting Style',
          desc:
            'At 10th level, you can choose a second option from the Fighting Style class feature.',
          choices: {
            additionalFightingStyle: {
              type: 'custom',
              options: `
| Style                   | Effects                                                                                                                                                                                                                                                                                             |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Archery	                | You gain a +2 bonus to attack rolls you make with ranged weapons.                                                                                                                                                                                                                                   |
| Defense	                | While you are wearing armor you gain a +1 bonus to AC.                                                                                                                                                                                                                                              |
| Dueling	                | When you are wielding a melee weapon in one hand and no other weapons you gain a +2 bonus to damage rolls with that weapon.you gain a +2 bonus to damage rolls with that weapon.                                                                                                                    |
| Great Weapon Fighting	  | When you roll a 1 or 2 on a damage die for an attack you make with a melee weapon that you are wielding with two hands you can reroll the die and must use the new roll even if the new roll is a 1 or a 2. The weapon must have the two-handed or versatile property for you to gain this benefit. |
| Protection		          | When a creature you can see attacks a target other than you that is within 5 feet of you you can use your reaction to impose disadvantage on the attack roll. You must be wielding a shield.                                                                                                        |
| Two-Weapon Fighting	    | When you engage in two-weapon fighting you can add your ability modifier to the damage of the second attack.                                                                                                                                                                                        |`,
            },
          },
        },
        {
          key: 'superiorCritical',
          level: 15,
          name: 'Superior Critical',
          desc:
            'Starting at 15th level, your weapon attacks score a critical hit on a roll of 18–20.',
        },
        {
          key: 'survivor',
          level: 18,
          name: 'Survivor',
          desc: `At 18th level, you attain the pinnacle of resilience in battle. At the start of each of your turns, you regain hit points equal to 5 + your Constitution modifier if you have no more than half of your hit points left.

You don’t gain this benefit if you have 0 hit points.`,
        },
      ],
    } as ISubclass,
  },
  {
    id: 'monk-open-hand',
    doc: {
      class: 'class-monk',
      name: 'Way of the Open Hand',
      desc:
        'Monks of the Way of the Open Hand are the ultimate masters of martial arts combat, whether armed or unarmed. They learn techniques to push and trip their opponents, manipulate ki to heal damage to their bodies, and practice advanced meditation that can protect them from harm.',
      features: [
        {
          key: 'openHand',
          level: 3,
          name: 'Open Hand Technique',
          desc: `Starting when you choose this tradition at 3rd level, you can manipulate your enemy’s ki when you harness your own. Whenever you hit a creature with one of the attacks granted by your Flurry of Blows, you can impose one of the following effects on that target:

- It must succeed on a Dexterity saving throw or be knocked prone.
- It must make a Strength saving throw. If it fails, you can push it up to 15 feet away from you.
- It can’t take reactions until the end of your next turn.`,
        },
        {
          key: 'wholenessOfBody',
          level: 6,
          name: 'Wholeness of Body',
          desc:
            'At 6th level, you gain the ability to heal yourself. As an action, you can regain hit points equal to three times your monk level. You must finish a long rest before you can use this feature again.',
        },
        {
          key: 'tranquility',
          level: 11,
          name: 'Tranquility',
          desc:
            'Beginning at 11th level, you can enter a special meditation that surrounds you with an aura of peace. At the end of a long rest, you gain the effect of a sanctuary spell that lasts until the start of your next long rest (the spell can end early as normal). The saving throw DC for the spell equals 8 + your Wisdom modifier + your proficiency bonus.',
        },
        {
          key: 'quiveringPalm',
          level: 17,
          name: 'Quivering Palm',
          desc: `At 17th level, you gain the ability to set up lethal vibrations in someone’s body. When you hit a creature with an unarmed strike, you can spend 3 ki points to start these imperceptible vibrations, which last for a number of days equal to your monk level. The vibrations are harmless unless you use your action to end them. To do so, you and the target must be on the same plane of existence. When you use this action, the creature must make a Constitution saving throw. If it fails, it is reduced to 0 hit points. If it succeeds, it takes 10d10 necrotic damage.

You can have only one creature under the effect of this feature at a time. You can choose to end the vibrations harmlessly without using an action`,
        },
      ],
    } as ISubclass,
  },
  {
    id: 'paladin-devotion',
    doc: {
      class: 'class-paladin',
      name: 'Oath of Devotion',
      desc: `The Oath of Devotion binds a paladin to the loftiest ideals of justice, virtue, and order. Sometimes called cavaliers, white knights, or holy warriors, these paladins meet the ideal of the knight in shining armor, acting with honor in pursuit of justice and the greater good. They hold themselves to the highest standards of conduct, and some, for better or worse, hold the rest of the world to the same standards.

Many who swear this oath are devoted to gods of law and good and use their gods’ tenets as the measure of their devotion. They hold angels—the perfect servants of good—as their ideals, and incorporate images of angelic wings into their helmets or coats of arms.

## Tenets of Devotion
Though the exact words and strictures of the Oath of Devotion vary, paladins of this oath share these tenets.

- **Honesty**: Don’t lie or cheat. Let your word be your promise.
- **Courage**: Never fear to act, though caution is wise.
- **Compassion**: Aid others, protect the weak, and punish those who threaten them. Show mercy to your foes, but temper it with wisdom.
- **Honor**: Treat others with fairness, and let your honorable deeds be an example to them. Do as much good as possible while causing the least amount of harm.
- **Duty**: Be responsible for your actions and their consequences, protect those entrusted to your care, and obey those who have just authority over you`,
      features: [
        {
          key: 'oathSpells',
          level: 3,
          name: 'Oath Spells',
          desc: `You gain oath spells at the paladin levels listed
 
| Level | Spells                                   |
|-------|------------------------------------------|
| 3rd   | protection from evil and good, sanctuary |
| 5th   | lesser restoration, zone of truth        |
| 9th   | beacon of hope, dispel magic             |
| 13th  | freedom of movement, guardian of faith   |
| 17th  | commune, flame strike                    |`,
        },
        {
          key: 'channelDivinity',
          level: 3,
          name: 'Channel Divinity',
          desc: `When you take this oath at 3rd level, you gain the following two Channel Divinity options.

**Sacred Weapon**: As an action, you can imbue one weapon that you are holding with positive energy, using your Channel Divinity. For 1 minute, you add your Charisma modifier to attack rolls made with that weapon (with a minimum bonus of +1). The weapon also emits bright light in a 20-foot radius and dim light 20 feet beyond that. If the weapon is not already magical, it becomes magical for the duration.

You can end this effect on your turn as part of any other action. If you are no longer holding or carrying this weapon, or if you fall unconscious, this effect ends.

**Turn the Unholy**. As an action, you present your holy symbol and speak a prayer censuring fiends and undead, using your Channel Divinity. Each fiend or undead that can see or hear you within 30 feet of you must make a Wisdom saving throw. If the creature fails its saving throw, it is turned for 1 minute or until it takes damage.

A turned creature must spend its turns trying to move as far away from you as it can, and it can’t willingly move to a space within 30 feet of you. It also can’t take reactions. For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there’s nowhere to move, the creature can use the Dodge action`,
        },
        {
          key: 'auraOfDevotion',
          level: 7,
          name: 'Aura of Devotion',
          desc: `Starting at 7th level, you and friendly creatures within 10 feet of you can’t be charmed while you are conscious.

At 18th level, the range of this aura increases to 30 feet.`,
        },
        {
          key: 'purityOfSpirit',
          level: 15,
          name: 'Purity of Spirit',
          desc:
            'Beginning at 15th level, you are always under the effects of a protection from evil and good spell.',
        },
        {
          key: 'holyNimbus',
          level: 20,
          name: 'Holy Nimbus',
          desc: `At 20th level, as an action, you can emanate an aura of sunlight. For 1 minute, bright light shines from you in a 30-foot radius, and dim light shines 30 feet beyond that.

Whenever an enemy creature starts its turn in the bright light, the creature takes 10 radiant damage.

In addition, for the duration, you have advantage on saving throws against spells cast by fiends or undead.`,
        },
      ],
    } as ISubclass,
  },
  {
    id: 'ranger-hunter',
    doc: {
      class: 'class-ranger',
      name: 'Hunter',
      desc:
        'Emulating the Hunter archetype means accepting your place as a bulwark between civilization and the terrors of the wilderness. As you walk the Hunter’s path, you learn specialized techniques for fighting the threats you face, from rampaging ogres and hordes of orcs to towering giants and terrifying dragons.',
      features: [
        {
          key: 'huntersPrey',
          level: 3,
          name: 'Hunter’s Prey',
          desc: `At 3rd level, you gain one of the following features of your choice.

- **Colossus Slayer**: Your tenacity can wear down the most potent foes. When you hit a creature with a weapon attack, the creature takes an extra 1d8 damage if it’s below its hit point maximum. You can deal this extra damage only once per turn.
- **Giant Killer**: When a Large or larger creature within 5 feet of you hits or misses you with an attack, you can use your reaction to attack that creature immediately after its attack, provided that you can see the creature.
- **Horde Breaker**: Once on each of your turns when you make a weapon attack, you can make another attack with the same weapon against a different creature that is within 5 feet of the original target and within range of your weapon.`,
        },
        {
          key: 'defensiveTactics',
          level: 7,
          name: 'Defensive Tactics',
          desc: `At 7th level, you gain one of the following features of your choice.

- **Escape the Horde**: Opportunity attacks against you are made with disadvantage.
- **Multiattack Defense**: When a creature hits you with an attack, you gain a +4 bonus to AC against all subsequent attacks made by that creature for the rest of the turn.
- **Steel Will**: You have advantage on saving throws against being frightened.`,
        },
        {
          key: 'multiattack',
          level: 11,
          name: 'Multiattack',
          desc: `At 11th level, you gain one of the following features of your choice.

- **Volley**: You can use your action to make a ranged attack against any number of creatures within 10 feet of a point you can see within your weapon’s range. You must have ammunition for each target, as normal, and you make a separate attack roll for each target.
- **Whirlwind Attack**: You can use your action to make a melee attack against any number of creatures within 5 feet of you, with a separate attack roll for each target.`,
        },
        {
          key: 'superiorHuntersDefense',
          level: 15,
          name: 'Superior Hunter’s Defense',
          desc: `At 15th level, you gain one of the following features of your choice.

- **Evasion**: When you are subjected to an effect, such as a red dragon’s fiery breath or a lightning bolt spell, that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.
- **Stand Against the Tide**: When a hostile creature misses you with a melee attack, you can use your reaction to force that creature to repeat the same attack against another creature (other than itself) of your choice.
- **Uncanny Dodge**: When an attacker that you can see hits you with an attack, you can use your reaction to halve the attack’s damage against you`,
        },
      ],
    } as ISubclass,
  },
  {
    id: 'rogue-thief',
    doc: {
      class: 'class-rogue',
      name: 'Thief',
      desc:
        'You hone your skills in the larcenous arts. Burglars, bandits, cutpurses, and other criminals typically follow this archetype, but so do rogues who prefer to think of themselves as professional treasure seekers, explorers, delvers, and investigators. In addition to improving your agility and stealth, you learn skills useful for delving into ancient ruins, reading unfamiliar languages, and using magic items you normally couldn’t employ.',
      features: [
        {
          key: 'fastHands',
          level: 3,
          name: 'Fast Hands',
          desc:
            'Starting at 3rd level, you can use the bonus action granted by your Cunning Action to make a Dexterity (Sleight of Hand) check, use your thieves’ tools to disarm a trap or open a lock, or take the Use an Object action.',
        },
        {
          key: 'secondStoryWork',
          level: 3,
          name: 'Second-Story Work',
          desc: `When you choose this archetype at 3rd level, you gain the ability to climb faster than normal; climbing no longer costs you extra movement.

In addition, when you make a running jump, the distance you cover increases by a number of feet equal to your Dexterity modifier.`,
        },
        {
          key: 'supremeSneak',
          level: 9,
          name: 'Supreme Sneak',
          desc:
            'Starting at 9th level, you have advantage on a Dexterity (Stealth) check if you move no more than half your speed on the same turn.',
        },
        {
          key: 'useMagicDevice',
          level: 13,
          name: 'Use Magic Device',
          desc:
            'By 13th level, you have learned enough about the workings of magic that you can improvise the use of items even when they are not intended for you. You ignore all class, race, and level requirements on the use of magic items.',
        },
        {
          key: 'thiefsReflexes',
          level: 17,
          name: 'Thief’s Reflexes',
          desc:
            'When you reach 17th level, you have become adept at laying ambushes and quickly escaping danger. You can take two turns during the first round of any combat. You take your first turn at your normal initiative and your second turn at your initiative minus 10. You can’t use this feature when you are surprised.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'sorcerer-draconic',
    doc: {
      class: 'class-sorcerer',
      name: 'Draconic Bloodline',
      desc:
        'r innate magic comes from draconic magic that was mingled with your blood or that of your ancestors. Most often, sorcerers with this origin trace their descent back to a mighty sorcerer of ancient times who made a bargain with a dragon or who might even have claimed a dragon parent. Some of these bloodlines are well established in the world, but most are obscure. Any given sorcerer could be the first of a new bloodline, as a result of a pact or some other exceptional circumstance.',
      features: [
        {
          key: 'dragonAncestor',
          level: 1,
          name: 'Dragon Ancestor',
          desc: `At 1st level, you choose one type of dragon as your ancestor. The damage type associated with each dragon is used by features you gain later.

You can speak, read, and write Draconic. Additionally, whenever you make a Charisma check when interacting with dragons, your proficiency bonus is doubled if it applies to the check.`,
          choices: {
            ancestor: {
              type: 'custom',
              options: `
| Dragon | Damage Type |
|--------|-------------|
| Black  | Acid        |
| Blue   | Lightning   |
| Brass  | Fire        |
| Bronze | Lightning   |
| Copper | Acid        |
| Gold   | Fire        |
| Green  | Poison      |
| Red    | Fire        |
| Silver | Cold        |
| White  | Cold        |`,
            },
          },
        },
        {
          key: 'draconicResilience',
          level: 1,
          name: 'Draconic Resilience',
          desc: `As magic flows through your body, it causes physical traits of your dragon ancestors to emerge. At 1st level, your hit point maximum increases by 1 and increases by 1 again whenever you gain a level in this class.

Additionally, parts of your skin are covered by a thin sheen of dragon-like scales. When you aren’t wearing armor, your AC equals 13 + your Dexterity modifier.`,
          effects: [
            {
              key: 'dwarvenToughness',
              affects: ['healthMaximum'],
              formula: 'this.level',
              visibility: 'hidden',
            },
          ],
        },
        {
          key: 'elementalAffinity',
          level: 6,
          name: 'Elemental Affinity',
          desc:
            'Starting at 6th level, when you cast a spell that deals damage of the type associated with your draconic ancestry, you can add your Charisma modifier to one damage roll of that spell. At the same time, you can spend 1 sorcery point to gain resistance to that damage type for 1 hour.',
        },
        {
          key: 'dragonWings',
          level: 14,
          name: 'Dragon Wings',
          desc: `At 14th level, you gain the ability to sprout a pair of dragon wings from your back, gaining a flying speed equal to your current speed. You can create these wings as a bonus action on your turn. They last until you dismiss them as a bonus action on your turn.

You can’t manifest your wings while wearing armor unless the armor is made to accommodate them, and clothing not made to accommodate your wings might be destroyed when you manifest them.`,
        },
        {
          key: 'draconicPresence',
          level: 18,
          name: 'Draconic Presence',
          desc:
            'Beginning at 18th level, you can channel the dread presence of your dragon ancestor, causing those around you to become awestruck or frightened. As an action, you can spend 5 sorcery points to draw on this power and exude an aura of awe or fear (your choice) to a distance of 60 feet. For 1 minute or until you lose your concentration (as if you were casting a concentration spell), each hostile creature that starts its turn in this aura must succeed on a Wisdom saving throw or be charmed (if you chose awe) or frightened (if you chose fear) until the aura ends. A creature that succeeds on this saving throw is immune to your aura for 24 hours.',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'warlock-fiend',
    doc: {
      class: 'class-warlock',
      name: 'Fiend',
      desc:
        'You have made a pact with a fiend from the lower planes of existence, a being whose aims are evil, even if you strive against those aims. Such beings desire the corruption or destruction of all things, ultimately including you. Fiends powerful enough to forge a pact include demon lords such as Demogorgon, Orcus, Fraz’Urb-luu, and Baphomet; archdevils such as Asmodeus, Dispater, Mephistopheles, and Belial; pit fiends and balors that are especially mighty; and ultroloths and other lords of the yugoloths.',
      features: [
        {
          key: 'extendedSpellList',
          level: 1,
          name: '',
          desc: `The Fiend lets you choose from an expanded list of spells when you learn a warlock spell. The following spells are added to the warlock spell list for you.
          
| Spell Level	 | Spells                             |
|----------------|------------------------------------|
| 1st	         | burning hands, command             |
| 2nd	         | blindness/deafness, scorching ray  |
| 3rd	         | fireball, stinking cloud           |
| 4th	         | fire shield, wall of fire          |
| 5th	         | flame strike, hallow               |`,
        },
        {
          key: 'darkOnesBlessing',
          level: 1,
          name: 'Dark One’s Blessing',
          desc:
            'Starting at 1st level, when you reduce a hostile creature to 0 hit points, you gain temporary hit points equal to your Charisma modifier + your warlock level (minimum of 1).',
        },
        {
          key: 'darkOnesOwnLuck',
          level: 6,
          name: 'Dark One’s Own Luck',
          desc:
            'Starting at 6th level, you can call on your patron to alter fate in your favor. When you make an ability check or a saving throw, you can use this feature to add a d10 to your roll. You can do so after seeing the initial roll but before any of the roll’s effects occur. Once you use this feature, you can’t use it again until you finish a short or long rest.',
        },
        {
          key: 'fiendishResilience',
          level: 10,
          name: 'Fiendish Resilience',
          desc:
            'Starting at 10th level, you can choose one damage type when you finish a short or long rest. You gain resistance to that damage type until you choose a different one with this feature. Damage from magical weapons or silver weapons ignores this resistance.',
        },
        {
          key: 'hurlThroughHell',
          level: 14,
          name: 'Hurl Through Hell',
          desc:
            'Starting at 14th level, when you hit a creature with an attack, you can use this feature to instantly transport the target through the lower planes. The creature disappears and hurtles through a nightmare landscape. At the end of your next turn, the target returns to the space it previously occupied, or the nearest unoccupied space. If the target is not a fiend, it takes 10d10 psychic damage as it reels from its horrific experience. Once you use this feature, you can’t use it again until you finish a long rest',
        },
      ],
    } as ISubclass,
  },
  {
    id: 'wizard-evocation',
    doc: {
      class: 'class-wizard',
      name: 'School of Evocation',
      desc:
        'You focus your study on magic that creates powerful elemental effects such as bitter cold, searing flame, rolling thunder, crackling lightning, and burning acid. Some evokers find employment in military forces, serving as artillery to blast enemy armies from afar. Others use their spectacular power to protect the weak, while some seek their own gain as bandits, adventurers, or aspiring tyrants.',
      features: [
        {
          key: 'evocationSavant',
          level: 2,
          name: 'Evocation Savant',
          desc:
            'Beginning when you select this school at 2nd level, the gold and time you must spend to copy an evocation spell into your spellbook is halved.',
        },
        {
          key: 'sculptSpells',
          level: 2,
          name: 'Sculpt Spells',
          desc:
            'Beginning at 2nd level, you can create pockets of relative safety within the effects of your evocation spells. When you cast an evocation spell that affects other creatures that you can see, you can choose a number of them equal to 1 + the spell’s level. The chosen creatures automatically succeed on their saving throws against the spell, and they take no damage if they would normally take half damage on a successful save.',
        },
        {
          key: 'potentCantrip',
          level: 6,
          name: 'Potent Cantrip',
          desc:
            'Starting at 6th level, your damaging cantrips affect even creatures that avoid the brunt of the effect. When a creature succeeds on a saving throw against your cantrip, the creature takes half the cantrip’s damage (if any) but suffers no additional effect from the cantrip.',
        },
        {
          key: 'empoweredEvocation',
          level: 10,
          name: 'Empowered Evocation',
          desc:
            'Beginning at 10th level, you can add your Intelligence modifier to one damage roll of any wizard evocation spell you cast.',
        },
        {
          key: 'overchannel',
          level: 14,
          name: 'Overchannel',
          desc: `Starting at 14th level, you can increase the power of your simpler spells. When you cast a wizard spell of 1st through 5th level that deals damage, you can deal maximum damage with that spell.

The first time you do so, you suffer no adverse effect. If you use this feature again before you finish a long rest, you take 2d12 necrotic damage for each level of the spell, immediately after you cast it. Each time you use this feature again before finishing a long rest, the necrotic damage per spell level increases by 1d12. This damage ignores resistance and immunity.`,
        },
      ],
    } as ISubclass,
  },
];
