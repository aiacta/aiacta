import { ILanguage } from '../../models';

export default [
  {
    id: 'common',
    doc: {
      name: 'Common',
      script: 'Common',
    } as ILanguage,
  },
  {
    id: 'dwarvish',
    doc: {
      name: 'Dwarvish',
      script: 'Dwarvish',
    } as ILanguage,
  },
  {
    id: 'elvish',
    doc: {
      name: 'Elvish',
      script: 'Elvish',
    } as ILanguage,
  },
  {
    id: 'giant',
    doc: {
      name: 'Giant',
      script: 'Giant',
    } as ILanguage,
  },
  {
    id: 'gnomish',
    doc: {
      name: 'Gnomish',
      script: 'Gnomish',
    } as ILanguage,
  },
  {
    id: 'goblin',
    doc: {
      name: 'Goblin',
      script: 'Goblin',
    } as ILanguage,
  },
  {
    id: 'halfling',
    doc: {
      name: 'Halfling',
      script: 'Halfling',
    } as ILanguage,
  },
  {
    id: 'orc',
    doc: {
      name: 'Orc',
      script: 'Orc',
    } as ILanguage,
  },
  {
    id: 'abyssal',
    doc: {
      name: 'Abyssal',
      script: 'Abyssal',
    } as ILanguage,
  },
  {
    id: 'celestial',
    doc: {
      name: 'Celestial',
      script: 'Celestial',
    } as ILanguage,
  },
  {
    id: 'draconic',
    doc: {
      name: 'Draconic',
      script: 'Draconic',
    } as ILanguage,
  },
  {
    id: 'speech',
    doc: {
      name: 'Speech',
      script: 'Speech',
    } as ILanguage,
  },
  {
    id: 'infernal',
    doc: {
      name: 'Infernal',
      script: 'Infernal',
    } as ILanguage,
  },
  {
    id: 'primordial',
    doc: {
      name: 'Primordial',
      script: 'Primordial',
    } as ILanguage,
  },
  {
    id: 'sylvan',
    doc: {
      name: 'Sylvan',
      script: 'Sylvan',
    } as ILanguage,
  },
  {
    id: 'undercommon',
    doc: {
      name: 'Undercommon',
      script: 'Undercommon',
    } as ILanguage,
  },
];
