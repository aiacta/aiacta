import { ISubrace } from '../../models';

export default [
  {
    id: 'hillDwarf',
    doc: {
      race: 'race-dwarf',
      name: 'Hill Dwarf',
      desc:
        'As a hill dwarf, you have keen senses, deep intuition, and remarkable resilience.',
      attributeBonuses: {
        wisdom: 1,
      },
      sight: {},
      speed: {},
      saves: {},
      skills: {},
      languages: [],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Increase',
          desc: 'Your Wisdom score increases by 1.',
        },
        {
          key: 'dwarvenToughness',
          name: 'Dwarven Toughness',
          desc:
            'Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.',
          effects: [
            {
              key: 'dwarvenToughness',
              affects: ['healthMaximum'],
              formula: 'this.level',
              visibility: 'hidden',
            },
          ],
        },
      ],
    } as ISubrace,
  },
  {
    id: 'highElf',
    doc: {
      race: 'race-elf',
      name: 'High Elf',
      desc:
        'As a high elf, you have a keen mind and a mastery of at least the basics of magic. In many fantasy gaming worlds, there are two kinds of high elves. One type is haughty and reclusive, believing themselves to be superior to non-elves and even other elves. The other type is more common and more friendly, and often encountered among humans and other races.',
      attributeBonuses: {
        intelligence: 1,
      },
      sight: {},
      speed: {},
      saves: {},
      skills: {},
      languages: [],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: ['longsword', 'shortsword', 'shortbow', 'longbow'],
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Intelligence score increses by 1.',
        },
        {
          key: 'weaponTraining',
          name: 'Elf Weapon Training',
          desc:
            'You have proficiency with the longsword, shortsword, shortbow, and longbow.',
        },
        {
          key: 'cantrip',
          name: 'Cantrip',
          desc:
            'You know one cantrip of your choice from the wizard spell list. Intelligence is your spellcasting ability for it.',
          choices: {
            cantrip: {
              type: 'spell',
              level: 0,
              list: 'wizard',
              attribute: 'intelligence',
            },
          },
        },
        {
          key: 'Extra language',
          name: 'Extra Language',
          desc:
            'You can speak, read, and write one extra language of your choice.',
          choices: {
            language: {
              type: 'language',
            },
          },
        },
      ],
    } as ISubrace,
  },
  {
    id: 'rockGnome',
    doc: {
      race: 'race-gnome',
      name: 'Rock Gnome',
      desc: '',
      attributeBonuses: {
        constitution: 1,
      },
      sight: {},
      speed: {},
      saves: {},
      skills: {},
      languages: [],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: ["tinker's tools"],
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Constitution score increses by 1.',
        },
        {
          key: 'artificersLore',
          name: 'Artificer’s Lore',
          desc:
            'Whenever you make an Intelligence (History) check related to magic items, alchemical objects, or technological devices, you can add twice your proficiency bonus, instead of any proficiency bonus you normally apply.',
        },
        {
          key: 'tinker',
          name: 'Tinker',
          desc: `You have proficiency with artisan’s tools (tinker’s tools). Using those tools, you can spend 1 hour and 10 gp worth of materials to construct a Tiny clockwork device (AC 5, 1 hp). The device ceases to function after 24 hours (unless you spend 1 hour repairing it to keep the device functioning), or when you use your action to dismantle it; at that time, you can reclaim the materials used to create it. You can have up to three such devices active at a time. When you create a device, choose one of the following options:

 - **Clockwork Toy**: This toy is a clockwork animal, monster, or person, such as a frog, mouse, bird, dragon, or soldier. When placed on the ground, the toy moves 5 feet across the ground on each of your turns in a random direction. It makes noises as appropriate to the creature it represents.
 - **Fire Starter**: The device produces a miniature flame, which you can use to light a candle, torch, or campfire. Using the device requires your action.
 - **Music Box**: When opened, this music box plays a single song at a moderate volume. The box stops playing when it reaches the song’s end or when it is closed.`,
        },
      ],
    } as ISubrace,
  },
  {
    id: 'lightfoot',
    doc: {
      race: 'race-halfling',
      name: 'Lightfoot',
      desc: '',
      attributeBonuses: {
        charisma: 1,
      },
      sight: {},
      speed: {},
      saves: {},
      skills: {},
      languages: [],
      conditionImmunities: [],
      immunities: [],
      resistances: [],
      vulnerabilities: [],
      proficiencies: [],
      traits: [
        {
          key: 'asi',
          name: 'Ability Score Improvment',
          desc: 'Your Charisma score increses by 1.',
        },
        {
          key: 'stealthiness',
          name: 'Naturally Stealthy',
          desc:
            'You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.',
        },
      ],
    } as ISubrace,
  },
];
