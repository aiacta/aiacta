import { createMuiTheme } from '@material-ui/core/es';

export const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});

const markdownHeading = {
  fontSize: '1em',
  marginTop: 8,
  marginBottom: 4,
};
export const markdownTheme = createMuiTheme({
  typography: {
    h1: markdownHeading,
    h2: markdownHeading,
    h3: markdownHeading,
    h4: markdownHeading,
    h5: markdownHeading,
  },
});
