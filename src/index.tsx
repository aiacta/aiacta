import { Location } from 'history';
import * as React from 'react';
import { hydrate, render } from 'react-dom';
import ReactGA from 'react-ga';
import browserHistory from './browserHistory';
import registerServiceWorker from './registerServiceWorker';
import Routing from './Routing';

setup();
run();
registerServiceWorker();

if (process.env.NODE_ENV === 'development') {
  if ((module as any).hot) {
    (module as any).hot.accept('./Routing', () => {
      const NextRouting = require('./Routing').default;
      hydrate(<NextRouting />, document.getElementById('root'));
    });
  }
}

function setup() {
  // setup analytics
  ReactGA.initialize(process.env.REACT_APP_GA_TRACKING_ID || '', {
    debug: !process.env.REACT_APP_GA_TRACKING_ID,
  });
  // track pageviews
  let latestPathname = '';
  browserHistory.listen(trackPageview);
  trackPageview(browserHistory.location);
  function trackPageview(location: Location) {
    if (location.pathname !== latestPathname) {
      ReactGA.pageview((latestPathname = location.pathname));
    }
  }
  // report errors
  window.addEventListener('unhandledrejection', rejection => {
    ReactGA.exception({
      description: `Unhandled rejection: ${rejection.reason}`,
      fatal: true,
    });
  });
  window.addEventListener('error', err => {
    ReactGA.exception({
      description: `Unhandled error: ${err.message}${
        err.error ? ` ${err.error}` : ''
      }`,
      fatal: true,
    });
  });
}

function run() {
  const rootElement = document.getElementById('root')!;
  if (rootElement.hasChildNodes()) {
    hydrate(<Routing />, rootElement);
  } else {
    render(<Routing />, rootElement);
  }
}
