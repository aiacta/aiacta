import * as React from 'react';
import { Context as DropZoneProvider } from './components/DropZone';
import { KeyboardHost } from './components/hooks/useKeyCombos';
import useObservable from './components/hooks/useObservable';
import InGame from './components/InGame';
import InLobby from './components/InLobby';
import SignIn from './components/SignIn';
import { myself, game } from './stores/modules/gameData';

export interface AppProps {}

export default App;

function App() {
  const isLoggedIn = useObservable(() => myself()._id);
  const isInGame = useObservable(() => !!game());
  return (
    <DropZoneProvider>
      <KeyboardHost window={window}>
        {isLoggedIn ? isInGame ? <InGame /> : <InLobby /> : <SignIn />}
      </KeyboardHost>
    </DropZoneProvider>
  );
}
