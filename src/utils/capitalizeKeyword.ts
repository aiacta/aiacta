export default function capitalizeKeyword(name: string) {
  return (
    name[0].toUpperCase() + name.substr(1).replace(/[A-Z]/g, chr => ' ' + chr)
  );
}
