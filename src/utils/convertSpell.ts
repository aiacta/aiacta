import { ISpell } from '../stores/models';

export default function convertSpell(data: any): ISpell | null {
  try {
    const expected = [
      'name',
      'desc',
      'range',
      'components',
      'ritual',
      'duration',
      'concentration',
      'casting_time',
      'level',
      'school',
      'classes',
    ];
    if (!expected.every(key => key in data)) {
      throw new Error(
        `Missing ${expected
          .filter(key => !(key in data))
          .join(', ')} in data for ${data.name || JSON.stringify(data)}.`,
      );
    }

    const input: ISpell = {
      name: data.name,
      desc: data.desc.join('\n\n'),
      higherLevel: (data.higher_level || []).join('\n\n'),
      range: data.range,
      components: data.components.join(''),
      materials: data.material || '',
      ritual: data.ritual === 'yes',
      concentration: data.concentration === 'yes',
      duration: data.duration,
      castingTime: data.casting_time,
      level: +data.level,
      school: data.school.name,
      classes: data.classes.map((c: any) => `class-${c.name.toLowerCase()}`),
    };

    return input;
  } catch (err) {
    return null;
  }
}

if (process.env.NODE_ENV === 'development') {
  (window as any).__convertSpell = convertSpell;
}
