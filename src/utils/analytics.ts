export enum Category {
  Game = 'game',
}

export enum Action {
  Create = 'create',
  Join = 'join',
  Import = 'import',
  Roll = 'roll',
  Attack = 'attack',
  Damage = 'damage',
  Save = 'save',
}
