import { Scene } from 'babylonjs';

export default function pickGround(
  scene: Scene,
  pointerX = scene.pointerX,
  pointerY = scene.pointerY,
) {
  const hit = scene.pick(pointerX, pointerY, () => true);
  if (hit && hit.pickedPoint) {
    return hit.pickedPoint;
  }
  return null;
}
