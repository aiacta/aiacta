import uuid from 'uuid/v4';
import { IMonster } from '../stores/models';

export default function convertMonster(data: any): IMonster | null {
  try {
    const expected = [
      'name',
      'size',
      'type',
      'alignment',
      'armor_class',
      'hit_dice',
      'speed',
      'strength',
      'dexterity',
      'constitution',
      'intelligence',
      'wisdom',
      'charisma',
      'actions',
      'challenge_rating',
      'senses',
    ];
    if (!expected.every(key => key in data)) {
      throw new Error(
        `Missing ${expected
          .filter(key => !(key in data))
          .join(', ')} in data for ${data.name || JSON.stringify(data)}.`,
      );
    }

    const speed = (data.speed as string).split(',').reduce(
      (acc, d) => {
        const match = /([a-zA-Z]+)?\s*(\d+)\s*ft\./.exec(d);
        if (match) {
          const [, type = 'walk', value] = match;
          if (type in acc) {
            return { ...acc, [type]: +value };
          }
        }
        return acc;
      },
      {
        walk: 0,
        swim: 0,
        hover: 0,
        fly: 0,
        climb: 0,
      },
    );
    const sight = (data.senses as string).split(',').reduce(
      (acc, d) => {
        const match = /(\w+) (\d+) ft\./.exec(d);
        if (match) {
          const [, type, value] = match;
          if (type in acc) {
            return { ...acc, [type]: +value };
          }
        }
        return acc;
      },
      {
        // TODO parse normal sight?
        normal: true,
        darkvision: 0,
        blindsight: 0,
        truesight: 0,
      },
    );
    function extractProficiency(num: number, attr: string) {
      const pr = 2 + Math.max(0, Math.floor((+data.challenge_rating - 1) / 4));
      const mod = Math.floor((+data[attr] - 10) / 2);
      return Math.round((num - mod) / pr);
    }
    const input: IMonster = {
      name: data.name,
      size: data.size.toLowerCase(),
      mtype: data.type.toLowerCase(),
      subtype: data.subtype.toLowerCase(),
      alignment: data.alignment.toLowerCase(),
      armorClass: +data.armor_class,
      hitDice: data.hit_dice,
      speed,
      attributes: {
        strength: +data.strength,
        dexterity: +data.dexterity,
        constitution: +data.constitution,
        intelligence: +data.intelligence,
        wisdom: +data.wisdom,
        charisma: +data.charisma,
      },
      saves: {
        // TODO parse prof-bonus
        strength:
          'strength_save' in data
            ? extractProficiency(+data.strength_save, 'strength')
            : 0,
        dexterity:
          'dexterity_save' in data
            ? extractProficiency(+data.dexterity_save, 'dexterity')
            : 0,
        constitution:
          'constitution_save' in data
            ? extractProficiency(+data.constitution_save, 'constitution')
            : 0,
        intelligence:
          'intelligence_save' in data
            ? extractProficiency(+data.intelligence_save, 'intelligence')
            : 0,
        wisdom:
          'wisdom_save' in data
            ? extractProficiency(+data.wisdom_save, 'wisdom')
            : 0,
        charisma:
          'charisma_save' in data
            ? extractProficiency(+data.charisma_save, 'charisma')
            : 0,
      },
      skills: {
        // TODO parse prof-bonus
        athletics:
          'athletics' in data
            ? extractProficiency(+data.athletics, 'strength')
            : 0,
        acrobatics:
          'acrobatics' in data
            ? extractProficiency(+data.acrobatics, 'dexterity')
            : 0,
        sleightOfHand:
          'sleightOfHand' in data
            ? extractProficiency(+data.sleightOfHand, 'dexterity')
            : 0,
        stealth:
          'stealth' in data
            ? extractProficiency(+data.stealth, 'dexterity')
            : 0,
        arcana:
          'arcana' in data
            ? extractProficiency(+data.arcana, 'intelligence')
            : 0,
        history:
          'history' in data
            ? extractProficiency(+data.history, 'intelligence')
            : 0,
        investigation:
          'investigation' in data
            ? extractProficiency(+data.investigation, 'intelligence')
            : 0,
        nature:
          'nature' in data
            ? extractProficiency(+data.nature, 'intelligence')
            : 0,
        religion:
          'religion' in data
            ? extractProficiency(+data.religion, 'intelligence')
            : 0,
        animalHandling:
          'animalHandling' in data
            ? extractProficiency(+data.animalHandling, 'wisdom')
            : 0,
        insight:
          'insight' in data ? extractProficiency(+data.insight, 'wisdom') : 0,
        medicine:
          'medicine' in data ? extractProficiency(+data.medicine, 'wisdom') : 0,
        perception:
          'perception' in data
            ? extractProficiency(+data.perception, 'wisdom')
            : 0,
        survival:
          'survival' in data ? extractProficiency(+data.survival, 'wisdom') : 0,
        deception:
          'deception' in data
            ? extractProficiency(+data.deception, 'charisma')
            : 0,
        intimidation:
          'intimidation' in data
            ? extractProficiency(+data.intimidation, 'charisma')
            : 0,
        performance:
          'performance' in data
            ? extractProficiency(+data.performance, 'charisma')
            : 0,
        persuation:
          'persuation' in data
            ? extractProficiency(+data.persuation, 'charisma')
            : 0,
      },
      challengeRating: +data.challenge_rating,
      vulnerabilities: data.damage_vulnerabilities || '',
      resistances: data.damage_resistances || '',
      immunities: data.damage_immunities || '',
      conditionImmunities: data.condition_immunities || '',
      sight,
      languages: data.languages || '',
      specialAbilities:
        'special_abilities' in data
          ? (data.special_abilities as Array<{
              name: string;
              desc: string;
            }>).map(({ name, desc }) => ({
              key: uuid(),
              name,
              description: desc,
            }))
          : [],
      actions: (data.actions as Array<{ name: string; desc: string }>).map(
        ({ name, desc }) => ({
          key: uuid(),
          name,
          desc,
          type: 'action' as 'action',
        }),
      ),
      legendaryActions:
        'legendary_actions' in data
          ? (data.legendary_actions as Array<{
              name: string;
              desc: string;
            }>).map(({ name, desc }) => ({
              key: uuid(),
              name,
              desc,
              type: 'legendary' as 'legendary',
            }))
          : [],
      _attachments: {},
    };

    return input;
  } catch (err) {
    return null;
  }
}

if (process.env.NODE_ENV === 'development') {
  (window as any).__convertMonster = convertMonster;
}
