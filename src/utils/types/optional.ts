export type Optional<T, K extends keyof any> = T extends any
  ? { [P in keyof Pick<T, K>]?: T[P] } & Pick<T, Exclude<keyof T, K>>
  : never;
