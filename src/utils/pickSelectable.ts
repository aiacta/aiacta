import { Scene } from 'babylonjs';

export default function pickSelectable(
  scene: Scene,
  pointerX = scene.pointerX,
  pointerY = scene.pointerY,
) {
  const hit = scene.pick(pointerX, pointerY, mesh => !!mesh.selectable);
  if (hit && hit.pickedMesh) {
    return hit.pickedMesh.selectable!;
  }
  return null;
}
