import { Angle, DynamicTexture, Vector2 } from 'babylonjs';
import { TokenSight, WallModel } from '../stores/models';

interface Segment {
  start: Vector2;
  end: Vector2;
}

interface Point {
  position: Vector2;
  isStart: boolean;
  segment: Segment;
  angle: number;
}

export function clearLineOfSight(texture: DynamicTexture) {
  const ctx = texture.getContext();
  ctx.fillStyle = 'rgba(255,255,255,1)';
  ctx.fillRect(0, 0, texture.getSize().width, texture.getSize().height);
  texture.update(false);
}

export function pointIsVisible(
  textureOrData: DynamicTexture | ImageData,
  position: Vector2,
  worldSize: Vector2,
) {
  const { width, height } =
    textureOrData instanceof ImageData
      ? textureOrData
      : textureOrData.getSize();
  const textureDimensions = new Vector2(width, height);

  function worldToTexture(v: Vector2) {
    return v
      .add(worldSize.scale(0.5))
      .divideInPlace(worldSize)
      .multiplyInPlace(textureDimensions);
  }

  const { x, y } = worldToTexture(position);

  const [r, g, b] =
    textureOrData instanceof ImageData
      ? textureOrData.data.subarray(
          (x + y * width) * 4,
          (x + y * width) * 4 + 4,
        )
      : (() => textureOrData.getContext().getImageData(x, y, 1, 1).data)();

  return r > 0 && (g > 0 || b > 0);
}

export function somePointIsVisible(
  textureOrData: DynamicTexture | ImageData,
  topLeft: Vector2,
  bottomRight: Vector2,
  worldSize: Vector2,
) {
  const { width, height } =
    textureOrData instanceof ImageData
      ? textureOrData
      : textureOrData.getSize();
  const textureDimensions = new Vector2(width, height);

  function worldToTexture(v: Vector2) {
    return v
      .add(worldSize.scale(0.5))
      .divideInPlace(worldSize)
      .multiplyInPlace(textureDimensions);
  }

  const data =
    textureOrData instanceof ImageData
      ? textureOrData.data
      : (() =>
          textureOrData.getContext().getImageData(0, 0, width, height).data)();

  const { x: left0, y: top0 } = worldToTexture(topLeft);
  const { x: left1, y: top1 } = worldToTexture(bottomRight);

  const subdata = data.subarray(
    (left0 + top0 * width) * 4,
    (left1 + top1 * width) * 4,
  );

  for (let i = 0; i < subdata.length; i += 4) {
    const [r, g, b] = subdata.slice(i, i + 4);
    if (r > 0 && (g > 0 || b > 0)) {
      return true;
    }
  }
  return false;
}

function createHelperContext() {
  const helperCanvas = document.createElement('canvas');
  const helperContext = helperCanvas.getContext('2d')!;
  helperContext.imageSmoothingEnabled = false;
  return helperContext;
}

const contextLineOfSight = createHelperContext();
const contextLights = createHelperContext();
const contextLightsLineOfSight = createHelperContext();
const contextNewExploration = createHelperContext();

export default function lineOfSight(
  texture: DynamicTexture,
  exploredTexture: DynamicTexture,
  actors: Array<{
    position: Vector2;
    sight: TokenSight;
  }>,
  map: {
    lightSources: Array<{
      position: Vector2;
      lightSource: {
        dim: number;
        bright: number;
      };
    }>;
    walls: WallModel[];
    size: Vector2;
  },
) {
  // const { position, sight } = actors[0];
  const { lightSources, walls, size: worldSize } = map;
  const { width, height } = texture.getSize();
  const textureDimensions = new Vector2(width, height);

  contextLineOfSight.canvas.width = width;
  contextLineOfSight.canvas.height = height;
  contextLineOfSight.clearRect(0, 0, width, height);
  contextLights.canvas.width = width;
  contextLights.canvas.height = height;
  contextLights.clearRect(0, 0, width, height);
  contextLightsLineOfSight.canvas.width = width;
  contextLightsLineOfSight.canvas.height = height;
  contextLightsLineOfSight.clearRect(0, 0, width, height);
  contextNewExploration.canvas.width = width;
  contextNewExploration.canvas.height = height;
  contextNewExploration.clearRect(0, 0, width, height);

  const contextTexture = texture.getContext();
  contextTexture.clearRect(0, 0, width, height);
  contextTexture.globalCompositeOperation = 'source-over';
  contextTexture.imageSmoothingEnabled = false;

  const contextExploration = exploredTexture.getContext();
  contextExploration.globalCompositeOperation = 'source-over';
  contextExploration.imageSmoothingEnabled = false;

  /**
   * channel infos
   *    red = line-of-sight
   *  green = bright visibility (e.g. light, truesight)
   *   blue = dim visibility (e.g. darkvision)
   */

  enum Channel {
    LineOfSight = 'rgba(255,0,0,1)',
    BrightLight = 'rgba(0,255,0,1)',
    DimLight = 'rgba(0,100,0,1)',
    DarkVision = 'rgba(0,0,255,1)',
    Any = 'rgba(255,255,255,1)',
  }

  console.time('actors');
  contextTexture.globalCompositeOperation = 'source-over';
  actors.forEach(({ position, sight }) => {
    // draw a sight-range circle for this actor [blue] into main texture
    drawCircle(
      contextTexture,
      Channel.DarkVision,
      position,
      sight.darkvision / 5,
    );
    drawCircle(
      contextNewExploration,
      Channel.LineOfSight,
      position,
      sight.darkvision / 5,
    );
    // draw a truesight-range circle for this actor [green] into main texture
    // TODO figure this out
    // drawCircle(position, sight.truesight / 5, ctx, Channel.BrightLight);
    // calculate line of sight [red]
    drawLineOfSight(contextLineOfSight, Channel.LineOfSight, position);
  });
  // trim these circles by actual line of sight
  contextTexture.globalCompositeOperation = 'destination-in';
  contextTexture.drawImage(contextLineOfSight.canvas, 0, 0);
  // then draw line of sight in red channel
  contextTexture.globalCompositeOperation = 'screen';
  contextTexture.drawImage(contextLineOfSight.canvas, 0, 0);
  console.timeEnd('actors');

  console.time('lights');
  lightSources.forEach(source => {
    contextLights.globalCompositeOperation = 'source-over';
    contextLights.clearRect(0, 0, width, height);
    contextLightsLineOfSight.clearRect(0, 0, width, height);
    // draw dim light-range circle [green]
    drawCircle(
      contextLights,
      Channel.DimLight,
      source.position,
      source.lightSource.dim / 5,
    );
    // draw bright light-range circle [green]
    drawCircle(
      contextLights,
      Channel.BrightLight,
      source.position,
      source.lightSource.bright / 5,
    );
    drawCircle(
      contextNewExploration,
      Channel.LineOfSight,
      source.position,
      Math.max(source.lightSource.bright, source.lightSource.dim) / 5,
    );
    // calculate line of sight [green]
    drawLineOfSight(
      contextLightsLineOfSight,
      Channel.LineOfSight,
      source.position,
    );
    // trim these circles by actual line of sight
    contextLights.globalCompositeOperation = 'destination-in';
    contextLights.drawImage(contextLightsLineOfSight.canvas, 0, 0);
    // commit to main texture
    contextTexture.globalCompositeOperation = 'screen';
    contextTexture.drawImage(contextLights.canvas, 0, 0);
  });
  console.timeEnd('lights');

  // trim new exploration to line of sight
  contextNewExploration.globalCompositeOperation = 'destination-in';
  contextNewExploration.drawImage(contextLineOfSight.canvas, 0, 0);

  contextExploration.globalCompositeOperation = 'source-over';
  contextExploration.drawImage(contextNewExploration.canvas, 0, 0);

  console.time('update');
  texture.update(false);
  exploredTexture.update(false);
  console.timeEnd('update');

  function worldToTexture(v: Vector2) {
    return v
      .add(worldSize.scale(0.5))
      .divideInPlace(worldSize)
      .multiplyInPlace(textureDimensions);
  }

  // ctx.font = '22px monospace';
  // points
  //   // .filter((p, i, a) => a.findIndex(d => d.position === p.position) === i)
  //   .forEach(({ position, isStart, segment }, i, a) => {
  //     const { x, y } = worldToTexture(position);
  //     ctx.fillStyle = isStart ? 'red' : 'green';
  //     ctx.save();
  //     ctx.translate(x, y);
  //     if (a.findIndex(d => d.position === position) !== i) {
  //       ctx.translate(0, 20);
  //     }
  //     ctx.scale(1, -1);
  //     ctx.beginPath();
  //     ctx.fillText(`${i} [${segments.indexOf(segment)}]`, 0, 0);
  //     ctx.restore();
  //   });

  function drawCircle(
    context: CanvasRenderingContext2D,
    color: string,
    origin: Vector2,
    r: number,
  ) {
    const { x: originX, y: originY } = worldToTexture(origin);
    const radius = (r / worldSize.x) * textureDimensions.x;
    context.fillStyle = color;
    context.strokeStyle = color;
    context.beginPath();
    context.ellipse(originX, originY, radius, radius, 0, 0, Math.PI * 2);
    context.fill();
    context.stroke();
  }

  function drawLineOfSight(
    context: CanvasRenderingContext2D,
    color: string,
    origin: Vector2,
    context2?: CanvasRenderingContext2D,
    color2?: string,
  ) {
    const segments: Segment[] = [];
    const points: Point[] = [];

    addSegment(
      worldSize.multiply(new Vector2(-0.5, -0.5)),
      worldSize.multiply(new Vector2(0.5, -0.5)),
    );
    addSegment(
      worldSize.multiply(new Vector2(0.5, -0.5)),
      worldSize.multiply(new Vector2(0.5, 0.5)),
    );
    addSegment(
      worldSize.multiply(new Vector2(0.5, 0.5)),
      worldSize.multiply(new Vector2(-0.5, 0.5)),
    );
    addSegment(
      worldSize.multiply(new Vector2(-0.5, 0.5)),
      worldSize.multiply(new Vector2(-0.5, -0.5)),
    );
    walls.forEach(wall => {
      wall.vertices.reduce((start, end) => {
        addSegment(start, end);
        return end;
      });
      if (wall.closed) {
        addSegment(wall.vertices[wall.vertices.length - 1], wall.vertices[0]);
      }
    });

    points.sort((a, b) => {
      const d = a.angle - b.angle;
      if (Math.abs(d) < Number.EPSILON) {
        return a.isStart ? -1 : 1;
      }
      return d;
    });

    // add initial open segments (all that intersect 0 radian ray)
    const ray = new Vector2(worldSize.x, 0);
    const currentSegments = segments
      .filter(segment =>
        segmentsIntersect(origin, origin.add(ray), segment.start, segment.end),
      )
      .sort(
        (a, b) =>
          Vector2.DistanceOfPointFromSegment(origin, a.start, a.end) -
          Vector2.DistanceOfPointFromSegment(origin, b.start, b.end),
      );

    let angle = 0;
    const originOnTexture = worldToTexture(origin);
    const { x: originX, y: originY } = originOnTexture;
    points.forEach(point => {
      const nearestSegment = currentSegments[0];
      if (point.isStart) {
        const idx = currentSegments.findIndex(
          seg =>
            Vector2.DistanceOfPointFromSegment(origin, seg.start, seg.end) >
            Vector2.DistanceOfPointFromSegment(
              origin,
              point.segment.start,
              point.segment.end,
            ),
        );
        switch (idx) {
          // case 0: // there is no segment before, inset infront
          //   currentSegments.unshift(point.segment);
          //   break;
          case -1: // all segments are infront
            currentSegments.push(point.segment);
            break;
          default:
            currentSegments.splice(idx, 0, point.segment);
            break;
        }
      } else {
        const idx = currentSegments.indexOf(point.segment);
        if (idx >= 0) {
          currentSegments.splice(idx, 1);
        }
      }

      if (nearestSegment !== currentSegments[0]) {
        const { x: x1, y: y1 } = worldToTexture(
          lineIntersection(
            nearestSegment.start,
            nearestSegment.end,
            origin,
            origin.add(new Vector2(Math.cos(angle), Math.sin(angle))),
          ),
        );
        const { x: x2, y: y2 } = worldToTexture(
          lineIntersection(
            nearestSegment.start,
            nearestSegment.end,
            origin,
            origin.add(
              new Vector2(Math.cos(point.angle), Math.sin(point.angle)),
            ),
          ),
        );

        context.fillStyle = color;
        context.strokeStyle = color;
        drawTriangle(context, originX, originY, x1, y1, x2, y2);
        if (context2) {
          context2.fillStyle = color2 || color;
          context2.strokeStyle = color2 || color;
          drawTriangle(context2, originX, originY, x1, y1, x2, y2);
        }

        angle = point.angle;
      }
    });

    if (currentSegments.length > 0) {
      const { x: x1, y: y1 } = worldToTexture(
        lineIntersection(
          currentSegments[0].start,
          currentSegments[0].end,
          origin,
          origin.add(new Vector2(Math.cos(angle), Math.sin(angle))),
        ),
      );
      const { x: x2, y: y2 } = worldToTexture(
        lineIntersection(
          currentSegments[0].start,
          currentSegments[0].end,
          origin,
          origin.add(new Vector2(1, 0)),
        ),
      );

      context.fillStyle = color;
      context.strokeStyle = color;
      drawTriangle(context, originX, originY, x1, y1, x2, y2);
      if (context2) {
        context2.fillStyle = color2 || color;
        context2.strokeStyle = color2 || color;
        drawTriangle(context2, originX, originY, x1, y1, x2, y2);
      }
    }

    function drawTriangle(
      theContext: CanvasRenderingContext2D,
      ...ps: [number, number, number, number, number, number]
    ) {
      theContext.beginPath();
      theContext.moveTo(ps[0], ps[1]);
      theContext.lineTo(ps[2], ps[3]);
      theContext.lineTo(ps[4], ps[5]);
      theContext.closePath();
      theContext.fill();
      theContext.stroke();
    }

    function addSegment(start: Vector2, end: Vector2) {
      let startAngle = Angle.BetweenTwoPoints(origin, start).radians();
      let endAngle = Angle.BetweenTwoPoints(origin, end).radians();

      let dAngle = endAngle - startAngle;
      if (dAngle <= -Math.PI) {
        dAngle += 2 * Math.PI;
      }
      if (dAngle > Math.PI) {
        dAngle -= 2 * Math.PI;
      }

      if (dAngle <= 0) {
        [start, end] = [end, start];
        [startAngle, endAngle] = [endAngle, startAngle];
      }

      const segment = { start, end };
      segments.push(segment);
      points.push({
        position: start,
        isStart: true,
        angle: startAngle,
        segment,
      });
      points.push({
        position: end,
        isStart: false,
        angle: endAngle,
        segment,
      });
    }
  }
}

function orientation(a: Vector2, b: Vector2, c: Vector2) {
  const value = (b.y - a.y) * (c.x - b.x) - (b.x - a.x) * (c.y - b.y);
  if (Math.abs(value) < Number.EPSILON) {
    return 0;
  }
  return value > 0 ? -1 : +1;
}

function pointOnLine(a: Vector2, b: Vector2, c: Vector2) {
  return (
    orientation(a, b, c) === 0 &&
    Math.min(a.x, b.x) <= c.x &&
    c.x <= Math.max(a.x, b.x) &&
    Math.min(a.y, b.y) <= c.y &&
    c.y <= Math.max(a.y, b.y)
  );
}

function segmentsIntersect(p1: Vector2, p2: Vector2, p3: Vector2, p4: Vector2) {
  // Get the orientation of points p3 and p4 in relation
  // to the line segment (p1, p2)
  const o1 = orientation(p1, p2, p3);
  const o2 = orientation(p1, p2, p4);
  const o3 = orientation(p3, p4, p1);
  const o4 = orientation(p3, p4, p2);

  // If the points p1, p2 are on opposite sides of the infinite
  // line formed by (p3, p4) and conversly p3, p4 are on opposite
  // sides of the infinite line formed by (p1, p2) then there is
  // an intersection.
  if (o1 !== o2 && o3 !== o4) {
    return true;
  }

  // Collinear special cases
  if (o1 === 0 && pointOnLine(p1, p2, p3)) {
    return true;
  }
  if (o2 === 0 && pointOnLine(p1, p2, p4)) {
    return true;
  }
  if (o3 === 0 && pointOnLine(p3, p4, p1)) {
    return true;
  }
  if (o4 === 0 && pointOnLine(p3, p4, p2)) {
    return true;
  }

  return false;
}

function lineIntersection(p1: Vector2, p2: Vector2, p3: Vector2, p4: Vector2) {
  const s =
    ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) /
    ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
  return new Vector2(p1.x + s * (p2.x - p1.x), p1.y + s * (p2.y - p1.y));
}
