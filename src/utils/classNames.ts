export default function classNames(
  ...names: Array<string | boolean | null | undefined>
) {
  return names.filter(Boolean).join(' ');
}
