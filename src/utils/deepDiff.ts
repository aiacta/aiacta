export default function deepDiff<T extends { [key: string]: any }>(
  next: T,
  prev?: T | null,
): Partial<T> {
  if (!prev || !next) {
    return next;
  }
  return Object.keys(next).reduce((acc, key) => {
    switch (typeof next[key]) {
      case 'undefined':
      case 'number':
      case 'string':
      case 'boolean':
        if (next[key] !== prev[key]) {
          return { ...acc, [key]: next[key] };
        }
        return acc;
      default:
        if (Array.isArray(next[key])) {
          // TODO maybe implement better array-diffing
          if (!Array.isArray(prev[key])) {
            return { ...acc, [key]: next[key] };
          }
          if ((next[key] as any[]).some(d => typeof d !== 'object')) {
            if (
              (next[key] as any[]).some((d, i) => d !== prev[key][i]) ||
              next[key].length !== prev[key].length
            ) {
              return { ...acc, [key]: next[key] };
            }
          } else {
            const diffArray = (next[key] as Array<{ key: string }>)
              .map((val, idx) => {
                const prevVal = (prev[key] as Array<{ key: string }>).find(
                  d => d.key === val.key,
                );
                if (!prevVal) {
                  return { key: val.key, add: val };
                } else {
                  const prevIdx = (prev[key] as Array<{ key: string }>).indexOf(
                    prevVal,
                  );
                  if (idx === prevIdx) {
                    const d = deepDiff(val, prevVal);
                    if (Object.keys(d).length > 0) {
                      return { key: val.key, change: d };
                    }
                    return { key: val.key };
                  } else {
                    const d = deepDiff(val, prevVal);
                    if (Object.keys(d).length > 0) {
                      return { key: val.key, oldIndex: prevIdx, change: d };
                    }
                    return { key: val.key, oldIndex: prevIdx };
                  }
                }
              })
              .concat(
                (prev[key] as Array<{ key: string }>)
                  .filter(
                    (val, idx) =>
                      !(next[key] as Array<{ key: string }>).some(
                        d => d.key === val.key,
                      ),
                  )
                  .map(val => ({ key: val.key, delete: true })),
              );
            if (diffArray.some(d => Object.keys(d).length > 1)) {
              return { ...acc, [key]: diffArray };
            }
          }
        } else if (next[key] !== prev[key]) {
          // TODO dont rely on assumption that different blobs are always different size
          if (next[key] instanceof Blob && next[key].size !== prev[key].size) {
            return { ...acc, [key]: next[key] };
          }
          const diff = deepDiff(next[key], prev[key]);
          if (!diff || Object.keys(diff).length > 0) {
            return { ...acc, [key]: diff };
          }
        }
        return acc;
    }
  }, {});
}
