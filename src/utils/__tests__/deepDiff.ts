import deepDiff from '../deepDiff';

describe('deepDiff', () => {
  it('lists diffs between strings', () => {
    const diff = deepDiff({ a: 'new', b: 'same' }, { a: 'old', b: 'same' });
    expect(diff).toEqual({ a: 'new' });
  });

  it('lists diffs between numbers', () => {
    const diff = deepDiff({ a: 0, b: 42 }, { a: 1337, b: 42 });
    expect(diff).toEqual({ a: 0 });
  });

  it('lists diffs between booleans', () => {
    const diff = deepDiff({ a: false, b: true }, { a: true, b: true });
    expect(diff).toEqual({ a: false });
  });

  it('lists diffs between simple arrays', () => {
    const diff = deepDiff(
      { a: [0, 'a', true], b: [0, 'a', true] },
      { a: [0, 'a', false], b: [0, 'a', true] },
    );
    expect(diff).toEqual({ a: [0, 'a', true] });
  });

  it('lists diffs between simple arrays', () => {
    const diff = deepDiff(
      {
        a: [
          { key: '0', prop0: 0, prop1: 'test' },
          { key: '1', prop0: 0, prop1: 'test' },
          { key: '3', prop0: 0, prop1: 'test' },
          { key: '2', prop0: 0, prop1: 'new' },
        ],
        b: [
          { key: '0', prop0: 0, prop1: 'test' },
          { key: '1', prop0: 0, prop1: 'test' },
          { key: '2', prop0: 0, prop1: 'test' },
        ],
      },
      {
        a: [
          { key: '0', prop0: 0, prop1: 'old' },
          { key: '1', prop0: 0, prop1: 'test' },
          { key: '2', prop0: 0, prop1: 'test' },
        ],
        b: [
          { key: '0', prop0: 0, prop1: 'test' },
          { key: '1', prop0: 0, prop1: 'test' },
          { key: '2', prop0: 0, prop1: 'test' },
        ],
      },
    );
    expect(diff).toEqual({
      a: [
        { change: { prop1: 'test' }, key: '0' },
        { key: '1' },
        { add: { key: '3', prop0: 0, prop1: 'test' }, key: '3' },
        { change: { prop1: 'new' }, key: '2', oldIndex: 2 },
      ],
    });
  });
});
