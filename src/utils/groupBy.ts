export default function groupBy<
  T extends { [P in K]: string },
  K extends string & keyof T
>(arr: Array<{ doc?: T }>, field: K) {
  const groups: { [key: string]: any[] | undefined } = {};
  for (let { doc } of arr) {
    if (doc) {
      if (!groups[doc[field]]) {
        groups[doc[field]] = [doc];
      } else {
        groups[doc[field]]!.push(doc);
      }
    }
  }
  return groups;
}
