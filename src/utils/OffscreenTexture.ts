import { Texture, Scene, Nullable } from 'babylonjs';

export default class OffscreenTexture extends Texture {
  private _canvas = document.createElement('canvas');
  private _offscreen: any = null;

  constructor(
    name: string,
    options: { width: number; height: number },
    scene: Scene,
    url: Nullable<string> = null,
  ) {
    super(null, scene, true);

    this.name = name;

    this._texture = scene
      .getEngine()
      .createDynamicTexture(
        options.width,
        options.height,
        false,
        Texture.NEAREST_SAMPLINGMODE,
      );

    this._canvas.width = options.width;
    this._canvas.height = options.height;

    this._offscreen = (this._canvas as any).transferControlToOffscreen();

    if (url) {
      const img = document.createElement('img');
      img.onload = () => {
        this._offscreen.getContext('2d').drawImage(img, 0, 0);
      };
      img.src = url;
    }
  }

  public getOffscreenCanvas() {
    return this._offscreen;
  }

  public update(invertY = true) {
    this.getScene()!
      .getEngine()
      .updateDynamicTexture(this._texture, this._canvas, invertY);
  }
}
