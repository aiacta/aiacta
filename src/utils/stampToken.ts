import PlainRing from '../assets/plain_ring_1.png';

const FaceDetector = (window as any).FaceDetector;

export default async function stampToken(
  file: File,
  sw: number,
  sh: number,
): Promise<File> {
  const canvas = document.createElement('canvas');
  const img = await createImageBitmap(file);

  let sx = 0;
  let sy = 0;
  if (FaceDetector) {
    const detector = new FaceDetector();
    const [face] = await detector.detect(img);
    if (face) {
      const size = Math.max(face.boundingBox.width, face.boundingBox.height);
      const padding = size * 1.25;
      sx = face.boundingBox.left - padding;
      sy = face.boundingBox.top - padding;
      sw = sh = size + padding * 2;
      console.log(face);
    } else {
      console.warn('No face detected :(');
    }
  }

  const size = Math.min(sw, sh);
  canvas.width = size;
  canvas.height = size;
  const ctx = canvas.getContext('2d')!;
  const ring = await colorizedRing('#bc986f');
  const ringWidth = 16 * (size / ring.width);
  const radius = Math.min(sw, sh) / 2 - ringWidth - 2;
  ctx.drawImage(
    img,
    sx,
    sy,
    sw,
    sh,
    sw > sh ? (sh - sw) / 2 : 0,
    sw < sh ? (sw - sh) / 2 : 0,
    sw,
    sh,
  );
  ctx.globalCompositeOperation = 'destination-in';
  ctx.ellipse(size / 2, size / 2, radius, radius, 0, 0, Math.PI * 2);
  ctx.fillStyle = 'white';
  ctx.fill();
  ctx.globalCompositeOperation = 'destination-over';
  ctx.drawImage(ring, 2, 2, (radius + ringWidth) * 2, (radius + ringWidth) * 2);
  return new Promise<File>((resolve, reject) =>
    canvas.toBlob(blob => {
      if (blob) {
        resolve(new File([blob], 'token', { type: 'image/png' }));
      }
    }, 'image/png'),
  );
}

const plainRingImg = new Promise<HTMLImageElement>(resolve => {
  const img = new Image();
  img.src = PlainRing;
  img.onload = () => resolve(img);
});

async function colorizedRing(color: string) {
  const ring = await plainRingImg;
  const canvas = document.createElement('canvas');
  canvas.width = ring.width;
  canvas.height = ring.width;
  const ctx = canvas.getContext('2d')!;
  const radius = ring.width / 2;
  ctx.ellipse(radius, radius, radius, radius, 0, 0, Math.PI * 2);
  ctx.fillStyle = color;
  ctx.fill();
  ctx.globalCompositeOperation = 'multiply';
  ctx.drawImage(ring, 0, 0, ring.width, ring.width);
  return canvas;
}
