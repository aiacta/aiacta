export default function<T extends any, K extends keyof WindowEventMap>(
  target: T,
  evt: K,
  fn: (this: T, ev: WindowEventMap[K]) => any,
) {
  target.addEventListener(evt, fn);
  return () => {
    target.removeEventListener(evt, fn);
  };
}
