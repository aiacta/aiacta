export default {
  async roll(faces: number) {
    return 1 + Math.floor(Math.max(0, Math.random() * faces - 0.0001));
  },
};
