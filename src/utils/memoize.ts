import { useMemo } from 'react';

export default function memoize<TKey, TReturn>(
  factory: (key: TKey) => TReturn,
) {
  const memo = new Map<TKey, any>();
  return <R>(key: TKey) =>
    (memo.get(key) as R & TReturn) ||
    (memo.set(key, factory(key)).get(key) as R & TReturn);
}

export function memoizeMulti<TKey, TKeys extends any[], TReturn>(
  factory: (key: TKey, ...keys: TKeys) => TReturn,
) {
  // tslint:disable-next-line:ban-types
  const memo = new Map<any, Function>();
  const generator = (key: TKey, ...keys: TKeys) => {
    const fn = memo.get(key);

    const bound = factory.bind(undefined, key) as any;

    if (keys.length > 0) {
      return (fn || memo.set(key, memoizeMulti(bound)).get(key)!)(
        ...keys,
      ) as TReturn;
    }
    return (fn || memo.set(key, memoize(bound)).get(key)!)() as TReturn;
  };
  return generator;
}

export function useMemoize<TFactory extends (...args: any[]) => any>(
  factory: TFactory,
  deps: any[] = [],
): TFactory {
  return useMemo(
    () => memoizeMulti(factory) as TFactory,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    deps,
  );
}
