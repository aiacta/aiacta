import { ICalendar } from '../stores/data/Calendar';

const nums = { 1: 'st', 2: 'nd', 3: 'rd' };

export default function formatTime(
  calendar: ICalendar,
  timestamp: number,
  format: string,
) {
  const time = calendar.timestampToDate(timestamp);
  return format
    .replace('hh', String(time.hour).padStart(2, '0'))
    .replace('ii', String(time.minute).padStart(2, '0'))
    .replace('ss', String(time.second).padStart(2, '0'))
    .replace('dd', String(time.day).padStart(2, '0'))
    .replace('DD', `${time.day}${nums[time.day as keyof typeof nums] || 'th'}`)
    .replace('mm', String(time.month + 1).padStart(2, '0'))
    .replace('MM', calendar.months[time.month][0])
    .replace('yyyy', String(time.year));
}
