import { lineOfSight } from '@aiacta/visibility';
import { TokenSight } from '../stores/models';

// eslint-disable-next-line no-restricted-globals
const ctx: Worker = self as any;

let drawingContext: null | CanvasRenderingContext2D = null;
let helperContext: null | CanvasRenderingContext2D = null;

enum Channel {
  LineOfSight = 'rgba(255,0,0,1)',
  BrightLight = 'rgba(0,255,0,1)',
  DimLight = 'rgba(0,100,0,1)',
  DarkVision = 'rgba(0,0,255,1)',
  Any = 'rgba(255,255,255,1)',
}

ctx.addEventListener('message', ({ data }) => {
  switch (data.type) {
    case 'setup':
      drawingContext = data.canvas.getContext('2d')!;
      drawingContext!.imageSmoothingEnabled = false;
      helperContext = new OffscreenCanvas(
        data.canvas.width,
        data.canvas.height,
      ).getContext('2d');
      helperContext!.imageSmoothingEnabled = false;
      break;
    default: {
      const {
        actors,
        segments,
        lightSources,
        isDungeonMaster,
        globalIllumination,
      } = data as {
        actors: {
          position: { x: number; y: number };
          sight: TokenSight;
        }[];
        segments: {
          start: { x: number; y: number };
          end: { x: number; y: number };
        }[];
        lightSources: {
          position: { x: number; y: number };
          lightSource: {
            dim: number;
            bright: number;
          };
        }[];
        isDungeonMaster: boolean;
        globalIllumination: number;
      };
      if (drawingContext && helperContext) {
        const context = drawingContext;
        const contextLOS = helperContext;

        const { width, height } = context.canvas;

        context.clearRect(0, 0, width, height);

        context.globalCompositeOperation = 'screen';
        lightSources.forEach(({ position, lightSource }) => {
          const triangles = lineOfSight([position], segments);
          contextLOS.clearRect(0, 0, width, height);
          contextLOS.globalCompositeOperation = 'source-over';
          contextLOS.fillStyle = contextLOS.strokeStyle = Channel.LineOfSight;
          triangles.forEach(([x0, y0, x1, y1, x2, y2]) => {
            contextLOS.beginPath();
            contextLOS.moveTo(x0, y0);
            contextLOS.lineTo(x1, y1);
            contextLOS.lineTo(x2, y2);
            contextLOS.closePath();
            contextLOS.fill();
            contextLOS.stroke();
          });

          contextLOS.globalCompositeOperation = 'source-in';
          contextLOS.fillStyle = Channel.DimLight;
          contextLOS.beginPath();
          contextLOS.ellipse(
            position.x,
            position.y,
            lightSource.dim,
            lightSource.dim,
            0,
            0,
            Math.PI * 2,
          );
          contextLOS.fill();

          contextLOS.globalCompositeOperation = 'source-atop';
          contextLOS.fillStyle = Channel.BrightLight;
          contextLOS.beginPath();
          contextLOS.ellipse(
            position.x,
            position.y,
            lightSource.bright,
            lightSource.bright,
            0,
            0,
            Math.PI * 2,
          );
          contextLOS.fill();

          context.drawImage(contextLOS.canvas, 0, 0);
        });

        context.globalCompositeOperation = 'screen';
        context.fillStyle = context.strokeStyle = Channel.LineOfSight;
        actors.forEach(({ position, sight }) => {
          const triangles = lineOfSight([position], segments);
          triangles.forEach(([x0, y0, x1, y1, x2, y2]) => {
            context.beginPath();
            context.moveTo(x0, y0);
            context.lineTo(x1, y1);
            context.lineTo(x2, y2);
            context.closePath();
            context.fill();
            context.stroke();
          });

          contextLOS.clearRect(0, 0, width, height);
          contextLOS.globalCompositeOperation = 'source-over';
          contextLOS.fillStyle = contextLOS.strokeStyle = Channel.LineOfSight;
          triangles.forEach(([x0, y0, x1, y1, x2, y2]) => {
            contextLOS.beginPath();
            contextLOS.moveTo(x0, y0);
            contextLOS.lineTo(x1, y1);
            contextLOS.lineTo(x2, y2);
            contextLOS.closePath();
            contextLOS.fill();
            contextLOS.stroke();
          });

          contextLOS.globalCompositeOperation = 'source-in';
          contextLOS.fillStyle = Channel.DarkVision;
          contextLOS.beginPath();
          contextLOS.ellipse(
            position.x,
            position.y,
            sight.darkvision,
            sight.darkvision,
            0,
            0,
            Math.PI * 2,
          );
          contextLOS.fill();

          context.drawImage(contextLOS.canvas, 0, 0);
        });

        if (globalIllumination > 0) {
          context.globalCompositeOperation = 'screen';
          context.fillStyle = `rgba(0,${globalIllumination * 255},0,1)`;
          context.fillRect(0, 0, width, height);
        }

        if (isDungeonMaster && actors.length === 0) {
          context.globalCompositeOperation = 'source-over';
          context.fillStyle = `rgba(255,255,255,1)`;
          context.fillRect(0, 0, width, height);
        }

        ctx.postMessage('done');
      }
    }
  }
});

export default {} as typeof Worker & { new (): Worker };
