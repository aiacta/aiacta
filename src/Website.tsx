import {
  Button,
  createStyles,
  Link,
  MuiThemeProvider,
  Theme,
  Typography,
  WithStyles,
  withStyles,
} from '@material-ui/core/es';
import * as React from 'react';
import { Link as RouterLink, Route, Switch } from 'react-router-dom';
import FourOFour from './pages/404';
import About from './pages/About';
import Contact from './pages/Contact';
import FAQ from './pages/FAQ';
import Homepage from './pages/Homepage';
import { darkTheme } from './themes';
import classNames from './utils/classNames';

const styles = (theme: Theme) =>
  createStyles({
    container: {
      maxWidth: 1080,
      margin: '0 auto',
      boxShadow: '0 20px 48px rgba(22,30,42,0.16)',
      '& h1,h2': {
        ...theme.typography.h3,
      },
      '& h3,h4,h5': {
        ...theme.typography.h4,
      },
    },
    containerSm: {
      maxWidth: 830,
      margin: '0 auto',
      padding: '0 24px',
    },
    centered: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    main: {
      paddingTop: 48,
      '& section': {
        margin: '48px 24px',
        '@media(min-width: 830px)': {
          margin: 48,
        },
      },
    },
    feature: {
      display: 'grid',
      gridTemplateAreas: `
      'head'
      'pic'
      'text'`,
      '@media(min-width: 830px)': {
        gridTemplateAreas: `
        'pic head'
        'pic text'`,
        gridTemplateColumns: 'auto 1fr',
        gridTemplateRows: 'auto 1fr',
        '&:nth-child(even)': {
          gridTemplateAreas: `
          'head pic'
          'text pic'`,
          gridTemplateColumns: '1fr auto',
        },
      },
      '&:not(:last-child)': {
        marginBottom: 20,
      },
      '& h3': {
        gridArea: 'head',
      },
      '& p': {
        gridArea: 'text',
      },
      '& div': {
        gridArea: 'pic',
        paddingBottom: '56.25%',
        position: 'relative',
        marginBottom: 12,
        '@media(min-width: 830px)': {
          minWidth: 384,
          marginRight: 12,
        },
        '& img': {
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
          left: 0,
        },
      },
    },
    footer: {
      color: '#606483',
      backgroundColor: '#0b0d1a',
      padding: '88px 0',
    },
    topBorder: {
      paddingTop: 24,
      position: 'relative',
      '&:before': {
        content: '""',
        position: 'absolute',
        backgroundColor: '#606483',
        opacity: 0.5,
        width: '100%',
        height: 1,
        left: 0,
      },
    },
    linkList: {
      listStyle: 'none',
      display: 'flex',
      justifyContent: 'flex-end',
      '& li:not(:first-child)': {
        display: 'inline-flex',
        marginLeft: 16,
      },
    },
    socialList: {
      listStyle: 'none',
      display: 'flex',
      justifyContent: 'flex-end',
      '& li': {
        display: 'inline-flex',
      },
      '& a': {
        padding: 8,
      },
      '& span': {
        clip: 'rect(1px, 1px, 1px, 1px)',
        position: 'absolute',
        width: 1,
        height: 1,
        overflow: 'hidden',
      },
    },
  });

export interface WebsiteProps extends WithStyles<typeof styles> {}

export default withStyles(styles)(Website);

function Website({ classes }: WebsiteProps) {
  return (
    <div className={classes.container}>
      <main className={classes.main}>
        <Switch>
          <Route path="/contact" exact component={Contact} />
          <Route path="/about" exact component={About} />
          <Route path="/faq" exact component={FAQ} />
          <Route path="/" exact component={Homepage} />
          <Route exact component={FourOFour} />
        </Switch>
      </main>
      <MuiThemeProvider theme={darkTheme}>
        <footer className={classes.footer}>
          <section
            className={classNames(classes.containerSm, classes.centered)}
          >
            {/* <Typography variant="h2">Play now!</Typography> */}
            <Button
              color="primary"
              variant="contained"
              size="large"
              component={RoutedLink}
              href="/app"
            >
              Play now!
            </Button>
          </section>
          <div className={classNames(classes.containerSm, classes.topBorder)}>
            <ul className={classes.linkList}>
              <Typography component="li" color="inherit">
                <Link color="inherit" component={RoutedLink} href="/contact">
                  Contact
                </Link>
              </Typography>
              <Typography component="li" color="inherit">
                <Link color="inherit" component={RoutedLink} href="/about">
                  About
                </Link>
              </Typography>
              <Typography component="li" color="inherit">
                <Link color="inherit" component={RoutedLink} href="/faq">
                  FAQ's
                </Link>
              </Typography>
            </ul>
            <ul className={classes.socialList}>
              <li>
                <a href="https://www.facebook.com">
                  <span>Facebook</span>
                  <svg
                    width="16"
                    height="16"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M6.023 16L6 9H3V6h3V4c0-2.7 1.672-4 4.08-4 1.153 0 2.144.086 2.433.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536V6H13l-1 3H9.28v7H6.023z"
                      fill="#FFF"
                    />
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://www.twitter.com">
                  <span>Twitter</span>
                  <svg
                    width="16"
                    height="16"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M16 3c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1-1.7 0-3.2 1.5-3.2 3.3 0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4C.7 7.7 1.8 9 3.3 9.3c-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H0c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4C15 4.3 15.6 3.7 16 3z"
                      fill="#FFF"
                    />
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </footer>
      </MuiThemeProvider>
    </div>
  );
}

const RoutedLink = (props: any) => <RouterLink to={props.href} {...props} />;
