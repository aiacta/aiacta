const equipment = require('./5e-SRD-Equipment.json');

const mapped = equipment.map(eq => ({
  id: eq.name
    .replace(/[,():’]/g, '')
    .replace(/ ?\w/g, _ =>
      _[0] === ' ' ? _[1].toUpperCase() : _[0].toLowerCase(),
    ),
  doc: {
    name: eq.name,
    desc: (eq.desc || []).join('\n\n'),
    damage: eq.damage
      ? [
          {
            key: '0',
            formula: `${eq.damage.dice_count}d${eq.damage.dice_value}`,
            type: eq.damage.damage_type.name.toLowerCase(),
          },
        ]
      : undefined,
    range: eq.range
      ? eq.range.long === null
        ? eq.range.normal
        : [eq.range.normal, eq.range.long]
      : undefined,
    category: eq.equipment_category,
    subcategory: subcategory(eq),
    identified: true,
    properties: eq.properties
      ? eq.properties
          .map(p =>
            p.name.toLowerCase() === 'thrown'
              ? `thrown (${eq.throw_range.normal}/${eq.throw_range.long})`
              : p.name.toLowerCase() === 'versatile'
              ? `versatile (${eq['2h_damage'].dice_count}d${
                  eq['2h_damage'].dice_value
                })`
              : p.name.toLowerCase(),
          )
          .filter(n => n !== 'monk')
      : undefined,
    price:
      eq.cost.quantity *
      (eq.cost.unit === 'sp'
        ? 10
        : eq.cost.unit === 'gp'
        ? 100
        : eq.cost.unit === 'pp'
        ? 1000
        : eq.cost.unit === 'ep'
        ? 50
        : 1),
    weight: eq.weight,
    features: undefined,
    rarity: 'common',
    armorClass: eq.armor_class && eq.armor_class.base,
    requiresAttunement: false,
    maxDexterityBonus:
      eq.armor_class &&
      (eq.armor_class.dex_bonus ? eq.armor_class.max_bonus : 0),
    stealthDisadvantage: eq.stealth_disadvantage,
    minimumStrength: eq.str_minimum,
    use:
      eq.name === 'Potion of healing'
        ? {
            action: 'heal',
            formula: '2d4 + 2',
          }
        : undefined,
  },
}));

function subcategory(eq) {
  switch (eq.equipment_category) {
    case 'Weapon':
      return eq.weapon_category;
    case 'Armor':
      return eq.armor_category;
    case 'Adventuring Gear':
      return eq.gear_category;
    case 'Tools':
      return eq.tool_category;
    case 'Mounts and Vehicles':
      return eq.vehicle_category;
    default:
      eq.equipment_category; //?
      return 'Unknown';
  }
}

require('fs').writeFileSync(
  './5e-srd/transform.json',
  JSON.stringify(mapped, null, 2),
);
